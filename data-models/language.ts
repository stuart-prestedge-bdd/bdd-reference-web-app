/**
 * Class representing a language.
 */
export class Language {
    public icon: string;
    public name: string;
    public code: string;
}   // Language

