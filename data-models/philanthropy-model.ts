import { Text, Image } from '../generic/common-object-model';

/**
 * Class representing a cause.
 */
export class Cause {
    public id: string;
    public name: string;
    public icon: Image[];
    public videoURL: string;
    public description: Text;
}   // Cause

/**
 * Class representing a charity.
 */
export class Charity {
    public id: string;
    public name: string;
    public logo: Image[];
    public summary: Text;
    public description: Text;
    public videoURL: string;
    public causes: string[];
}   // Charity

/**
 * Class representing a ambassador.
 */
export class Ambassador {
    public id: string;
    public name: string;
    public avatar: Image[];
    public videoURL: string;
    public images: Image[];
    public description: Text;
    public charities: string[];
}   // Ambassador

