import { Debug } from '../helpers/debug';

/**
 * Gender enum.
 */
export enum Gender {
    unknown = 0,
    male = 1,
    female = 2,
    unspecified = 3
}   // Gender

/**
 * Class representing an address.
 */
export class Address {
    public Line1: string;
    public Line2?: string;
    public Line3?: string;
    public Line4?: string;
    public City?: string;
    public Region?: string;
    public Country: string;
    public PostalCode: string;
}   // Address

/**
 * Class representing a country.
 */
export class Country {
    public Name: string;
    public Code: string;
}   // Country

/**
 * Month enum.
 */
export enum Month {
    unknown = 0,
    jan = 1,
    feb = 2,
    mar = 3,
    apr = 4,
    may = 5,
    jun = 6,
    jul = 7,
    aug = 8,
    sep = 9,
    oct = 10,
    nov = 11,
    dec = 12
}   // Month

/**
 * Date only (no time component).
 */
export class DateOnly {
    public Year: number;
    public Month: Month;
    public Day: number; // 1-based
    constructor(year: number, month: Month, day: number) {
        Debug.Tested();
        this.Year = year;
        this.Month = month;
        this.Day = day;
    }
}   // DateOnly

/**
 * Class representing a user details.
 * This includes all information such as name, email, address, phone, dob, gender.
 * See: https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/44826670/5.2.+Data+Structure
 */
export class UserDetails {
    public ID: string;
    public EmailAddress: string;
    public EmailAddressVerified: boolean;
    // public PasswordHash: string;
    public GivenName: string;
    public FamilyName: string;
    public PreferredName?: string;
    public FullName?: string;
    // public Blocked?: boolean;
    public DateOfBirth: DateOnly;
    public Gender?: Gender;
    public Address1: string;
    public Address2?: string;
    public Address3?: string;
    public Address4?: string;
    public City?: string;
    public Region?: string;
    public Country: string;
    public PostalCode: string;
    public PhoneNumber?: string;
    // public PhoneNumberVerified?: boolean;
    // public LastLoggedIn?: Date;
    // public LastLoggedOut?: Date;
    // public NewEmailAddress?: string;
    // public Closed?: Date;
    // public IsAnonymized?: boolean;
    // public Deleted?: Date;
    public AllowNonEssentialEmails?: boolean;
    // public ANEEOnTimestamp?: Date;
    // public ANEEOffTimestamp?: Date;
    // public TotalTicketsPurchased?: number;
    // public TicketsPurchasedInCurrentGame?: number;
    public PreferredLanguage?: string;
    // public FailedLoginAttempts?: number;
    // public KYCStatus?: string;
    // public KCYTimestamp?: Date;
    public MaxDailySpendingAmount?: number;
    public NewMaxDailySpendingAmount?: number;
    public NewMaxDailySpendingAmountTime?: Date;
    public MaxTimeLoggedIn?: number;
    public NewMaxTimeLoggedIn?: number;
    public NewMaxTimeLoggedInTime?: Date;
    public ExcludeUntil?: Date;
    public NewExcludeUntil?: Date;
    public NewExcludeUntilTime?: Date;
}   // UserDetails

