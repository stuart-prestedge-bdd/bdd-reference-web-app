import { Target } from '../generic/common-object-model';

/**
 * Class representing a link.
 */
export class Link {
    public id: string;
    public name: string;
    public link: Target;
}   // Link

