/**
 * Class representing a game.
 */
export class Game {
    public idHash: string;
    public name: string;
    public openDate: string;
    public closeDate: string;
    public ticketPrice: number;
    public frozen: boolean;
    public ticketServiceURL: string;
    public draws: Draw[];
    public prizes: Prize[];
}   // Game

/**
 * Class representing a draw.
 */
export class Draw {
    public idHash: string;
    public gameIDHash: string;
    public drawDate: string;
    public drawnDate: string;
    public amount: number;
    public prizeIDHash: string;
    public winningNumber: number;
}   // Draw

/**
 * Class representing a prize.
 */
export class Prize {
    public idHash: string;
    public gameIDHash: string;
    public drawIDHash: string;
    public name: string;
    public shortDescription: string;
    public smallImageURL: string;
    public largeImageURL: string;
}   // Prize

