import { Debug } from '../helpers/debug';
import { WebApp } from '../components/web-app';
import { ContentService } from './content-service';
import { Link } from '../data-models/links-model';
import { CallbackWaiter } from '../helpers/callback-waiter';
import { Text } from '../generic/common-object-model';
import { ErrorHelper } from '../helpers/error-helper';
import { TextContentService } from './text-content-service';

/**
 * Class providing access to causes content service.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/20644019/6.3.+Links+Data+Files
 */
export class LinksContentService extends ContentService {

    /**
     * The cached causes.
     */
    private static Links: Link[];

    /**
     * The callback waiter.
     */
    private static CallbackWaiter: CallbackWaiter;

    /**
     * Clear the cache.
     */
    public static clearCache() {
        Debug.Tested();

        this.Links = null;
    }

    /**
     * Get the links available to show.
     */
    public static getLinks(callback: (links: Link[]) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        if (LinksContentService.Links == null) {
            if (LinksContentService.CallbackWaiter == null) {
                LinksContentService.CallbackWaiter = new CallbackWaiter(callback);
                WebApp.getInstance().getLanguageCode((languageCode) => {
                    Debug.AssertString(languageCode);
                    ContentService.getContent(`/links/root-${languageCode}.json`, (error, content) => {
                        Debug.AssertErrorOrObject(error, content);
                        ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_LINKS);
                        LinksContentService.Links = (content != null) ? content.links : [];
                        LinksContentService.CallbackWaiter.CallCallbacksNoError(LinksContentService.Links);
                        LinksContentService.CallbackWaiter = null;
                    });
                });
            } else {
                LinksContentService.CallbackWaiter.AddCallback(callback);
            }
        } else {
            callback(LinksContentService.Links);
        }
    }

    /**
     * Get a target link data.
     * It is up to the caller which language this is in which will be encoded in the link URL.
     */
    public static getLink(linkUrl: string, callback: (link: { title: string, text: Text }) => void) {
        Debug.Tested();
        Debug.AssertString(linkUrl);
        Debug.AssertValid(callback);

        ContentService.getContent(`/links/${linkUrl}`, (error, content) => {
            Debug.AssertErrorOrObject(error, content);
            ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_LINK);
            callback(content);
        });
    }

}   // LinksContentService

