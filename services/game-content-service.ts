import { Debug } from '../helpers/debug';
import { ContentService } from './content-service';
import { Game } from '../data-models/game-model';
import { CallbackWaiter } from '../helpers/callback-waiter';
import { ErrorHelper } from '../helpers/error-helper';
import { DateHelper } from '../helpers/date-helper';
import { TextContentService } from './text-content-service';

/**
 * Class providing access to games content service.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/59703342/6.5.+Game+Information
 */
export class GameContentService extends ContentService {

    /**
     * The cached games.
     */
    private static Games: Game[];

    /**
     * The callback waiter.
     */
    private static CallbackWaiter: CallbackWaiter;

    /**
     * Get the games available to show.
     */
    public static getGames(callback: (games: Game[]) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValidOrNull(GameContentService.Games);
        Debug.AssertValidOrNull(GameContentService.CallbackWaiter);

        if (GameContentService.Games == null) {
            if (GameContentService.CallbackWaiter == null) {
                GameContentService.CallbackWaiter = new CallbackWaiter(callback);
                ContentService.getContent('/games/game-info.json', (error, content) => {
                    Debug.AssertErrorOrObject(error, content);
                    ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_GAMES);
                    GameContentService.Games = (content != null) ? content.games : [];
                    GameContentService.CallbackWaiter.CallCallbacksNoError(GameContentService.Games);
                    GameContentService.CallbackWaiter = null;
                });
            } else {
                GameContentService.CallbackWaiter.AddCallback(callback);
            }
        } else {
            callback(GameContentService.Games);
        }
    }

    /**
     * Get the current game (or null if there is no current game).
     */
    public static getCurrentGame(callback: (currentGame: Game) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        GameContentService.getGames((games) => {
            Debug.AssertValidOrNull(games);
            if (games != null) {
                let currentGame = null;
                let now = new Date();
                for (let game_ of games) {
                    Debug.AssertValid(game_);
                    let openDate = DateHelper.timestampTextToDate(game_.openDate);
                    let closeDate = DateHelper.timestampTextToDate(game_.closeDate);
                    if ((openDate <= now) && (closeDate > now)) {
                        currentGame = game_;
                        break;
                    }
                }
                callback(currentGame);
            } else {
                callback(null);
            }
        });
    }

    /**
     * Get a game by its ID hash.
     */
    public static getGameByIDHash(idHash: string, callback: (error: Error, game: Game) => void) {
        Debug.Tested();
        Debug.AssertString(idHash);
        Debug.AssertValid(callback);

        GameContentService.getGames((games) => {
            Debug.AssertValidOrNull(games);
            if (games != null) {
                let game = games.find(g => (g.idHash === idHash));
                Debug.AssertValidOrNull(game);
                if (game != null) {
                    callback(null, game);
                } else {
                    callback(new Error(), null);
                }
            } else {
                Debug.Untested();
                callback(new Error(), null);
            }
        });
    }
    
}   // GameContentService

