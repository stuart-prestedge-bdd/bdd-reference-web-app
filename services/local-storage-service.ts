import { Debug } from '../helpers/debug';
import { ContentService } from './content-service';
import { Language } from '../data-models/language';
import { CallbackWaiter } from '../helpers/callback-waiter';
import { ObjectMap } from '../helpers/object-map';

/**
 * Class providing access to local content storage service.
 */
export class LocalStorageService extends ContentService {

    /**
     * The cached values.
     */
    private static Cache = new ObjectMap<any>();

    /**
     * The callbacks.
     */
    private static ReadCallback: (key: string, callback: (error: Error, value: any) => void) => void;
    private static WriteCallback: (key: string, value: any, callback: (error: Error) => void) => void;

    /**
     * Set the callbacks.
     */
    public static setCallbacks(readCallback: (key: string, callback: (error: Error, value: any) => void) => void,
                               writeCallback: (key: string, value: any, callback: (error: Error) => void) => void) {
        Debug.Tested();
        Debug.AssertValid(readCallback);
        Debug.AssertValid(writeCallback);

        LocalStorageService.ReadCallback = readCallback;
        LocalStorageService.WriteCallback = writeCallback;
    }

    /**
     * Read a value from local storage.
     */
    public static readValue(key: string, callback: (error: Error, value: any) => void) {
        Debug.Tested();
        Debug.AssertString(key);
        Debug.AssertValid(callback);
        Debug.AssertValid(LocalStorageService.Cache);
        Debug.AssertValid(LocalStorageService.ReadCallback);

        if (LocalStorageService.Cache.has(key)) {
            let value = LocalStorageService.Cache.get(key);
            Debug.AssertValidOrNull(value);
            callback(null, value);
        } else if (LocalStorageService.ReadCallback != null) {
            LocalStorageService.ReadCallback(key, (error, value) => {
                Debug.AssertValidOrNull(error);
                Debug.AssertValidOrNull(value);
                LocalStorageService.Cache.set(key, value);
                callback(error, value);
            });
        } else {
            Debug.Untested();
            LocalStorageService.Cache.set(key, null);
            callback(null, null);
        }
    }

    /**
     * Write a value to local storage.
     */
    public static writeValue(key: string, value: any, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertString(key);
        Debug.AssertValidOrNull(value);
        Debug.AssertValid(callback);
        Debug.AssertValid(LocalStorageService.Cache);
        Debug.AssertValid(LocalStorageService.WriteCallback);

        LocalStorageService.Cache.set(key, value);        
        if (LocalStorageService.WriteCallback != null) {
            LocalStorageService.WriteCallback(key, value, (error) => {
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(null);
        }
    }

}   // LocalStorageService
