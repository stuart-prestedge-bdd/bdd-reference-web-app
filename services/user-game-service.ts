import { Debug } from '../helpers/debug';
import { ObjectMap } from '../helpers/object-map';
import { DateHelper } from '../helpers/date-helper';
import { DateOnly } from '../data-models/identity-model';
import { RandomHelper } from '../helpers/random-helper';
import { APIHelper } from '../helpers/api-helper';

/**
 * Data structures.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/24543848/5.2.+Data+Structure
 */

/**
 * Class representing a game (from user's perspective).
 */
export class UserGame {
    public idHash: string;
    public name: string;
    public openDate: DateOnly;
    public closeDate: DateOnly;
    public ticketPrice: number;
    public frozen: boolean;
    public ticketServiceURL: string;
    public donatedToCharity: number;
}   // UserGame

/**
 * Class representing a draw (from user's perspective).
 */
export class UserDraw {
    public idHash: string;
    public gameIDHash: string;
    public drawDate: Date;
    public drawnDate: Date;
    public amount: number;
    public amountIsUpTo: boolean;
    public prizeIDHash: string;
    public winningNumber: number;//??++CHECK THIS IS OK
}   // UserDraw

/**
 * Class providing access to the admin game service.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/24576107/5.1.+Requirements
 */
export class UserGameService {

    /**
     * Possible errors.
     */
    public static readonly ERROR_INVALID_ARGS = 'INVALID_ARGS';
    public static readonly ERROR_UNAUTHORIZED = 'UNAUTHORIZED';
    public static readonly ERROR_NOT_FOUND = 'NOT_FOUND';

    /**
     * Get an error from an API result.
     */
    private static errorFromResult(result: number): Error {
        Debug.Untested();

        let error = null;
        switch (result) {
            case APIHelper.RESULT_400_BAD_REQUEST: {
                error = new Error(UserGameService.ERROR_INVALID_ARGS);
                break;
            }
            case APIHelper.RESULT_401_UNAUTHORIZED: {
                error = new Error(UserGameService.ERROR_UNAUTHORIZED);
                break;
            }
            case APIHelper.RESULT_404_NOT_FOUND: {
                error = new Error(UserGameService.ERROR_NOT_FOUND);
                break;
            }
            case APIHelper.RESULT_200_OK:
            case APIHelper.RESULT_201_CREATED:
            case APIHelper.RESULT_204_NO_CONTENT:
                break;
            default:
                Debug.Unreachable();
                break;
        }
        return error;
    }

    /**
     * Games
     */

    /**
     * Get games.
     * GET /api/games
     */
    public static GetGames(callback: (error: Error, games: UserGame[]) => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        UserGameServiceAPI.GetGames((result, responseBody) => {
            Debug.AssertValid(responseBody);
            let error = UserGameService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            let games: UserGame[] = null;
            if ((error == null) && (responseBody != null)) {
                games = [];
                responseBody.forEach(element => {
                    games.push(UserGameServiceAPI.APIGameToGame(element));
                });
            }
            callback(error, games);
        });
    }

    /**
     * Get game.
     * GET /api/games/<GAME_ID_HASH>
     */
    public static GetGame(gameIdHash: string, callback: (error: Error, game: UserGame) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameIdHash);
        Debug.AssertValid(callback);

        if (!!gameIdHash) {
            UserGameServiceAPI.GetGame(gameIdHash, (result, responseBody) => {
                Debug.AssertValid(responseBody);
                let error = UserGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error, UserGameServiceAPI.APIGameToGame(responseBody));
            });
        } else {
            callback(new Error(UserGameService.ERROR_INVALID_ARGS), null);
        }
    }

    /**
     * Draws
     */

    /**
     * Get draws.
     * GET /api/draws/<GAME_ID_HASH>
     */
    public static GetDraws(gameIdHash: string, callback: (error: Error, draws: UserDraw[]) => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        if (!!gameIdHash) {
            UserGameServiceAPI.GetDraws(gameIdHash, (result, responseBody) => {
                Debug.AssertValid(responseBody);
                let error = UserGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                let draws: UserDraw[] = null;
                if ((error == null) && (responseBody != null)) {
                    draws = [];
                    responseBody.forEach(element => {
                        draws.push(UserGameServiceAPI.APIDrawToDraw(element));
                    });
                }
                callback(error, draws);
            });
        } else {
            callback(new Error(UserGameService.ERROR_INVALID_ARGS), null);
        }
    }

    /**
     * Get draws.
     * GET /api/draws-won/<GAME_ID_HASH>/<TICKET_ID>
     */
    /*???public static GetDrawsWon(gameIdHash: string, ticketID, callback: (error: Error, draws: UserDraw[]) => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        if (!!gameIdHash) {
            UserGameServiceAPI.GetDrawsWon(gameIdHash, (result, responseBody) => {
                Debug.AssertValid(responseBody);
                let error = UserGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                let draws: UserDraw[] = null;
                if ((error == null) && (responseBody != null)) {
                    draws = [];
                    responseBody.forEach(element => {
                        draws.push(UserGameServiceAPI.APIDrawToDraw(element));
                    });
                }
                callback(error, draws);
            });
        } else {
            callback(new Error(UserGameService.ERROR_INVALID_ARGS), null);
        }
    }*/

    /**
     * Get draw.
     * GET /api/draws/<GAME_ID_HASH>/<DRAW_ID_HASH>
     */
    public static GetDraw(gameIdHash: string, drawIdHash: string, callback: (error: Error, draw: UserDraw) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameIdHash);
        Debug.AssertValidOrNull(drawIdHash);
        Debug.AssertValid(callback);

        if (!!gameIdHash && !!drawIdHash) {
            UserGameServiceAPI.GetDraw(gameIdHash, drawIdHash, (result, responseBody) => {
                Debug.AssertValid(responseBody);
                let error = UserGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error, UserGameServiceAPI.APIDrawToDraw(responseBody));
            });
        } else {
            callback(new Error(UserGameService.ERROR_INVALID_ARGS), null);
        }
    }

}   // UserGameService

/**
 * Class providing access to the admin game service.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/45056041/5.1.+Requirements
 */
export class UserGameServiceAPI {

    /**
     * The game data.
     */
    private static Games: ObjectMap<UserGame> = new ObjectMap<UserGame>();
    private static Draws: ObjectMap<UserDraw> = new ObjectMap<UserDraw>();
    //??++private static Prizes: ObjectMap<UserPrize> = new ObjectMap<UserPrize>();

    /**
     * Is the system locked.
     */
    private static IsSystemLocked(): boolean {
        Debug.UntestedMock();
        Debug.Uncoded();
        return false;
    }

    /**
     * Games
     */

    /**
     * Convert an API game into an UserGame.
     */
    public static APIGameToGame(game: any): UserGame {
        Debug.UntestedMock();
        let retVal: UserGame = null;
        if (game != null) {
            retVal = <UserGame>{
                idHash: game.idHash,
                name: game.name,
                openDate: DateHelper.APIDateToDateOnly(game.openDate),
                closeDate: DateHelper.APIDateToDateOnly(game.closeDate),
                //??--ticketCount: game.ticketCount,
                ticketPrice: game.ticketPrice,
                //??--locked: game.locked,
                //??--published: game.published,
                frozen: game.frozen,
                ticketServiceURL: game.ticketServiceURL,
                donatedToCharity: game.donatedToCharity
            };
        }
        return retVal;
    }

    /**
     * Convert an UserGame into an API game.
     */
    private static GameToAPIGame(game: UserGame): any {
        Debug.UntestedMock();
        let retVal = null;
        if (game != null) {
            retVal = {
                idHash: game.idHash,
                name: game.name,
                openDate: DateHelper.DateOnlyToAPIDate(game.openDate),
                closeDate: DateHelper.DateOnlyToAPIDate(game.closeDate),
                //??--ticketCount: game.ticketCount,
                ticketPrice: game.ticketPrice,
                //??--locked: game.locked,
                //??--published: game.published,
                frozen: game.frozen,
                ticketServiceURL: game.ticketServiceURL,
                donatedToCharity: game.donatedToCharity
            };
        }
        return retVal;
    }

    /**
     * Get all games.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432577/5.4.+User+Game+Service#id-5.4.UserGameService-GetGames
     */
    public static GetGames(callback: (result: number, body: any) => void) {
        Debug.UntestedMock();
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!UserGameServiceAPI.IsSystemLocked()) {
                let games = [];
                UserGameServiceAPI.Games.forEach(game => {
                    Debug.AssertValid(game);
                    games.push(UserGameServiceAPI.GameToAPIGame(game));
                });
                callback(APIHelper.RESULT_200_OK, games);
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
            }
        });
    }

    /**
     * Get a game.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432577/5.4.+User+Game+Service#id-5.4.UserGameService-GetGame
     */
    public static GetGame(gameIdHash: string, callback: (result: number, body: any) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(gameIdHash);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!UserGameServiceAPI.IsSystemLocked()) {
                if (!!gameIdHash) {
                    let game = UserGameServiceAPI.Games.get(gameIdHash);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        callback(APIHelper.RESULT_200_OK, UserGameServiceAPI.GameToAPIGame(game));
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND, {});
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST, {});
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
            }
        });
    }

    /**
     * Draws
     */

    /**
     * Convert an API draw into an UserDraw.
     */
    public static APIDrawToDraw(draw: any): UserDraw {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(draw);
        let retVal: UserDraw = null;
        if (draw != null) {
            retVal = <UserDraw>{
                idHash: draw.idHash,
                gameIDHash: draw.gameIDHash,
                drawDate: DateHelper.APITimestampToDate(draw.drawDate),
                drawnDate: DateHelper.APITimestampToDate(draw.drawnDate),
                amount: draw.amount,
                amountIsUpTo: draw.amountIsUpTo,
                prizeIDHash: draw.prizeIDHash,
                winningNumber: draw.winningNumber
            };
        }
        return retVal;
    }

    /**
     * Convert an UserDraw into an API draw.
     */
    private static DrawToAPIDraw(draw: UserDraw): any {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(draw);
        let retVal = null;
        if (draw != null) {
            retVal = {
                idHash: draw.idHash,
                gameIDHash: draw.gameIDHash,
                drawDate: DateHelper.DateToAPITimestamp(draw.drawDate),
                drawnDate: DateHelper.DateToAPITimestamp(draw.drawnDate),
                amount: draw.amount,
                amountIsUpTo: draw.amountIsUpTo,
                prizeIDHash: draw.prizeIDHash,
                winningNumber: draw.winningNumber
            };
        }
        return retVal;
    }

    /**
     * Get draws in a game.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432577/5.4.+User+Game+Service#id-5.4.UserGameService-GetDraws
     */
    public static GetDraws(gameIdHash: string,
                           callback: (result: number, body: any) => void) {
        Debug.UntestedMock();
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!UserGameServiceAPI.IsSystemLocked()) {
                if (!!gameIdHash) {
                    let game = UserGameServiceAPI.Games.get(gameIdHash);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        Debug.AssertString(game.idHash);
                        let draws = UserGameServiceAPI.GetGameDraws(game.idHash);
                        Debug.AssertValid(draws);
                        callback(APIHelper.RESULT_200_OK, draws);
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND, {});
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST, {});
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
            }
        });
    }

    //???https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432577/5.4.+User+Game+Service#id-5.4.UserGameService-GetDrawsWon

    /**
     * Get a draw.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432577/5.4.+User+Game+Service#id-5.4.UserGameService-GetDraw
     */
    public static GetDraw(gameIdHash: string, drawIdHash: string, callback: (result: number, body: any) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(gameIdHash);
        Debug.AssertValidOrNull(drawIdHash);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!UserGameServiceAPI.IsSystemLocked()) {
                if (!!gameIdHash && !!drawIdHash) {
                    let game = UserGameServiceAPI.Games.get(gameIdHash);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        let draw = UserGameServiceAPI.Draws.get(drawIdHash);
                        Debug.AssertValidOrNull(draw);
                        if (draw != null) {
                            callback(APIHelper.RESULT_200_OK, draw);
                        } else {
                            callback(APIHelper.RESULT_404_NOT_FOUND, {});
                        }
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND, {});
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST, {});
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
            }
        });
    }

    /**
     * Internal helper methods.
     */

    /**
     * Get a game's draws.
     */
    private static GetGameDraws(gameIdHash: string): UserDraw[] {
        Debug.UntestedMock();
        Debug.AssertString(gameIdHash);

        let draws: UserDraw[] = [];
        UserGameServiceAPI.Draws.forEach(draw => {
            Debug.AssertValid(draw);
            if (draw.gameIDHash === gameIdHash) {
                draws.push(draw);
            }
        });
        return draws;
    }

    //??++ADD PRIZES

}   // UserGameServiceAPI
