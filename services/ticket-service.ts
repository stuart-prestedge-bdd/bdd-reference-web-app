import { Debug } from '../helpers/debug';
import { ticket_data } from '../dummy-data/game-service-data/ticket-data';
import { RandomHelper } from '../helpers/random-helper';
import { IdentityServiceAPI } from './identity-service';
import { TextContentService } from './text-content-service';
import { APIHelper } from '../helpers/api-helper';

/**
 * Class representing a ticket.
 */
export class Ticket {
    public ID: string;
    public GameID: string;
    public Number: number;
    public ReserverOrOwnerID: string;
    public ReservedDate: Date;
    public PurchaseDate: Date;
    public OfferedToID: string;
    public OfferedDate: Date;
}   // Ticket

/**
 * Class representing a ticket information.
 */
export class TicketInfo {
    public ID: string;
    public OwnerID: string;
    public GameID: string;
    public Number: number;
    public PurchaseDate: Date;
    public OfferedToEmail: string;
    public OfferedDate: Date;
}   // TicketInfo

/**
 * Class providing access to the ticket service.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/11730949/4.5.+Ticketing+Payment+Service#id-4.5.Ticketing/PaymentService-GetPaymentDetails
 */
export class TicketService {

    /**
     * Get an error from an API result.
     */
    private static errorFromResult(result: number): Error {
        Debug.Untested();

        let error = null;
        switch (result) {
            case APIHelper.RESULT_400_BAD_REQUEST:
            case APIHelper.RESULT_401_UNAUTHORIZED:
            case APIHelper.RESULT_404_NOT_FOUND: {
                error = new Error();
                break;
            }
            case APIHelper.RESULT_200_OK:
            case APIHelper.RESULT_201_CREATED:
            case APIHelper.RESULT_204_NO_CONTENT:
                break;
            default:
                Debug.Unreachable();
                break;
        }
        return error;
    }

    /**
     * Get the details of a ticket with a given ID.
     * The ticket must be owned by the specified user or offered to the specified user.
     * If the ticket is only reserved by the user then this function fails.
     * The specified user must match the JWT auth token.
     * 02/05/18 SMP Consistent with specification document.
     * GET /api/tickets/self/<TICKET_ID>
     */
    public static getTicket(userID: string, ticketID: string, accessToken: string, callback: (error: Error, ticketInfo: TicketInfo) => void) {
        Debug.TestedMock();
        Debug.AssertString(userID);
        Debug.AssertString(ticketID);
        Debug.AssertString(accessToken);
        Debug.AssertValid(callback);

        TicketServiceAPI.getTicket(ticketID, accessToken, (result, body) => {
            let error = TicketService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            //???convert body to ticket?
            let ticket = body;
            callback(error, ticket);
        });
        //??-- Debug.scheduleCallback(() => {
        //     let jwtUserID = accessToken;//??++JWT
        //     if (userID === jwtUserID) {
        //         let ticket = ticket_data.find(_ticket => (_ticket.ID === ticketID));
        //         Debug.AssertValidOrNull(ticket);
        //         if (ticket != null) {
        //             if (((ticket.ReserverOrOwnerID === userID) && (ticket.PurchaseDate != null)) ||
        //                 (ticket.OfferedToID === userID)) {
        //                 let ticketInfo = TicketService.ticketInfoFromTicket(ticket);
        //                 Debug.AssertValid(ticketInfo);
        //                 callback(null, ticketInfo);
        //             }
        //         } else {
        //             callback(new Error(), null);
        //         }
        //     } else {
        //         callback(new Error(), null);
        //     }
        // });
    }

    /**
     * Get the tickets owned by the specified user.
     * The specified user must match the JWT auth token.
     * 02/05/18 SMP Consistent with specification document.
     */
    public static getUserTickets(userID: string, accessToken: string, callback: (error: Error, ticketInfos: TicketInfo[]) => void) {
        Debug.TestedMock();
        Debug.AssertString(userID);
        Debug.AssertString(accessToken);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            let jwtUserID = accessToken;//??++JWT
            if (userID === jwtUserID) {
                let tickets = ticket_data.filter(_ticket => ((_ticket.ReserverOrOwnerID === userID) && (_ticket.PurchaseDate != null)));
                Debug.AssertValid(tickets);
                let ticketInfos = [];
                tickets.forEach(ticket => {
                    Debug.AssertValid(ticket);
                    let ticketInfo = TicketService.ticketInfoFromTicket(ticket);
                    Debug.AssertValid(ticketInfo);
                    ticketInfos.push(ticketInfo);
                });
                callback(null, ticketInfos);
            } else {
                callback(new Error(), null);
            }
        });
    }
    
    /**
     * Reserve a ticket selected by the user.
     */
    public static reserveTicket(userID: string, number: number, callback: (error: Error, ticket: Ticket) => void) {
        Debug.UntestedMock();
        Debug.AssertValid(number);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            let ticket = ticket_data.find(_ticket => (_ticket.Number === number));//??++WRITE REAL SDK
            Debug.AssertValidOrNull(ticket);
            if (ticket == null) {
                ticket = new Ticket();
                ticket.ID = `ticket_${RandomHelper.getRandomNumber()}`;
                ticket.Number = number;
                ticket_data.push(ticket);
            }
            let error: Error = null;
            if ((ticket.ReservedDate == null) && (ticket.PurchaseDate == null)) {
                ticket.ReserverOrOwnerID = userID;
                ticket.ReservedDate = new Date();
            } else {
                error = new Error(TextContentService.TEXT_NAME_ERROR_FAILED_TO_RESERVE_TICKET);
            }
            callback(error, ticket);
        });
    }
    
    /**
     * Reserve a ticket selected by the system.
     */
    public static chooseTicket(userID: string, callback: (error: Error, ticket: Ticket) => void) {
        Debug.TestedMock();
        Debug.AssertString(userID);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            let ticket = new Ticket();
            ticket.ID = `ticket_${RandomHelper.getRandomNumber()}`;
            ticket.Number = RandomHelper.getRandomNumber();
            ticket.ReservedDate = new Date();
            ticket.ReserverOrOwnerID = userID;
            ticket_data.push(ticket);
            callback(null, ticket);
        });
    }
    
    /**
     * Buy one or more tickets.
     */
    public static buyTickets(userID: string,
                             numbers: number[],
                             giftEmailAddress: string,
                             giftMessage: string,
                             callback: (errors: Error[], tickets: Ticket[]) => void) {
        Debug.TestedMock();
        Debug.AssertString(userID);
        Debug.AssertValid(numbers);
        Debug.AssertStringOrNull(giftEmailAddress);
        Debug.AssertValidOrNull(giftMessage);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            TicketService.doBuyTickets(userID, numbers, (errors, tickets) => {
                Debug.AssertValid(errors);
                Debug.AssertValid(tickets);
                if (giftEmailAddress != null) {
                    TicketService.offerTickets(userID, errors, tickets, giftEmailAddress, giftMessage, callback);
                } else {
                    callback(errors, tickets);
                }
            })
        });
    }

    /**
     * Offer a ticket.
     */
    public static offerTicket(userID: string, ticketID: string, emailAddress: string, message: string, callback: (error: Error) => void) {
        Debug.TestedMock();
        Debug.AssertString(userID);
        Debug.AssertString(ticketID);
        Debug.AssertString(emailAddress);
        Debug.AssertStringOrNull(message);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            TicketService.doOfferTicket(userID, ticketID, emailAddress, message, callback);
        });
    }
    
    /**
     * Revoke a ticket.
     */
    public static revokeTicket(id: string, xxx: any, callback: (error: Error) => void) {
        Debug.UntestedMock();
        Debug.AssertString(id);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            callback(null);//??++        
        });
    }
    
    /**
     * Redeem a ticket.
     * The ticket specified by ID is redeemed by the user specified.
     * The ticket must be offered to the user or an error will occur.
     */
    public static redeemTicket(ticketID: string, userID: string, callback: (error: Error) => void) {
        Debug.TestedMock();
        Debug.AssertString(ticketID);
        Debug.AssertString(userID);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            let ticket = ticket_data.find(_ticket => (_ticket.ID === ticketID));//??++WRITE REAL SDK
            Debug.AssertValidOrNull(ticket);
            if (ticket != null) {
                if ((ticket.PurchaseDate != null) && (ticket.ReserverOrOwnerID !== userID) && (ticket.OfferedToID === userID)) {
                    ticket.ReserverOrOwnerID = userID;
                    ticket.OfferedToID = null;
                    ticket.OfferedDate = null;
                    callback(null);
                } else {
                    callback(new Error(TextContentService.TEXT_NAME_ERROR_TICKET_FAILED_TO_REDEEM));
                }
            } else {
                callback(new Error(TextContentService.TEXT_NAME_ERROR_TICKET_FAILED_TO_REDEEM));
            }
        });
    }    

    /**
     * Helper methods.
     */

    /**
     * Buy one or more tickets.
     */
    private static doBuyTickets(userID: string,
                                numbers: number[],
                                callback: (errors: Error[], tickets: Ticket[]) => void) {
        Debug.TestedMock();//??++WRITE REAL SDK
        Debug.AssertString(userID);
        Debug.AssertValid(numbers);
        Debug.AssertValid(callback);

        let errors = [];
        let tickets = [];
        numbers.forEach(number => {
            let ticket = ticket_data.find(_ticket => (_ticket.Number === number));
            Debug.AssertValidOrNull(ticket);
            if (ticket != null) {
                if ((ticket.PurchaseDate == null) && ((ticket.ReservedDate == null) || (ticket.ReserverOrOwnerID === userID))) {
                    ticket.PurchaseDate = new Date();
                    ticket.ReservedDate = null;
                    ticket.ReserverOrOwnerID = userID;
                    tickets.push(ticket);
                } else {
                    Debug.UntestedMock();
                    errors.push(new Error(TextContentService.TEXT_NAME_ERROR_FAILED_TO_BUY_TICKETS));
                }
            } else {
                Debug.UntestedMock();
                ticket = new Ticket();
                ticket.ID = `ticket_${RandomHelper.getRandomNumber()}`;
                ticket.Number = number;
                ticket.PurchaseDate = new Date();
                ticket.ReservedDate = null;
                ticket.ReserverOrOwnerID = userID;
                ticket_data.push(ticket);
                tickets.push(ticket);
            }
        });
        callback(errors, tickets);
    }
    
    /**
     * Offer the tickets to the specified email.
     */
    private static offerTickets(userID: string,
                                errors: Error[],
                                tickets: Ticket[],
                                giftEmailAddress: string,
                                giftMessage: string,
                                callback: (errors: Error[], tickets: Ticket[]) => void) {
        Debug.Tested();
        Debug.AssertString(userID);
        Debug.AssertValid(errors);
        Debug.AssertValid(tickets);
        Debug.AssertString(giftEmailAddress);
        Debug.AssertValidOrNull(giftMessage);
        Debug.AssertValid(callback);

        TicketService.doOfferTickets(userID, errors, tickets, 0, giftEmailAddress, giftMessage, callback);
    }
    
    /**
     * Offer the tickets to the specified email.
     */
    private static doOfferTickets(userID: string,
                                  errors: Error[],
                                  tickets: Ticket[],
                                  index: number,
                                  giftEmailAddress: string,
                                  giftMessage: string,
                                  callback: (errors: Error[], tickets: Ticket[]) => void) {
        Debug.Tested();
        Debug.AssertValid(errors);
        Debug.AssertValid(tickets);
        Debug.AssertString(giftEmailAddress);
        Debug.AssertValidOrNull(giftMessage);
        Debug.AssertValid(callback);
        
        if (index < tickets.length) {
            let ticket = tickets[index];
            Debug.AssertValid(ticket);
            TicketService.doOfferTicket(userID, ticket.ID, giftEmailAddress, giftMessage, (error) => {
                Debug.AssertValidOrNull(error);
                if (error != null) {
                    errors.push(new Error(TextContentService.TEXT_NAME_ERROR_FAILED_TO_OFFER_TICKET));
                }
                TicketService.doOfferTickets(userID, errors, tickets, (index + 1), giftEmailAddress, giftMessage, callback);
            });
        } else {
            callback(errors, tickets);
        }
    }
    
    /**
     * Offer a ticket.
     */
    private static doOfferTicket(userID: string, ticketID: string, emailAddress: string, message: string, callback: (error: Error) => void) {
        Debug.TestedMock();
        Debug.AssertString(userID);
        Debug.AssertString(ticketID);
        Debug.AssertString(emailAddress);
        Debug.AssertStringOrNull(message);
        Debug.AssertValid(callback);

        let ticket = ticket_data.find(_ticket => (_ticket.ID === ticketID));//??++WRITE REAL SDK
        Debug.AssertValidOrNull(ticket);
        if (ticket != null) {
            if ((ticket.PurchaseDate != null) && (ticket.ReserverOrOwnerID === userID) && (ticket.OfferedToID == null)) {
                let user = IdentityServiceAPI.getUserByEmailInternal(emailAddress);
                Debug.AssertValidOrNull(user);
                if (user == null) {
                    user = IdentityServiceAPI.createUserInternal(emailAddress);
                    Debug.AssertValid(user);
                }
                Debug.AssertString(user.ID);
                ticket.OfferedToID = user.ID;
                ticket.OfferedDate = new Date();
                callback(null);
            } else {
                Debug.UntestedMock();
                callback(new Error(TextContentService.TEXT_NAME_ERROR_FAILED_TO_OFFER_TICKET));
            }
        } else {
            Debug.UntestedMock();
            callback(new Error(TextContentService.TEXT_NAME_ERROR_FAILED_TO_OFFER_TICKET));
        }
    }
    
}   // TicketService

/**
 * Class providing access to the ticket service.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/11730949/4.5.+Ticketing+Payment+Service#id-4.5.Ticketing/PaymentService-GetPaymentDetails
 */
class TicketServiceAPI {

    /**
     * Is the system locked.
     */
    private static IsSystemLocked(): boolean {
        Debug.UntestedMock();
        Debug.Uncoded();
        return false;
    }

    /**
     * Get the details of a ticket with a given ticket ID.
     * The ticket must be owned by the specified user or offered to the specified user.
     * If the ticket is only reserved by the user then this function fails.
     * The specified user must match the JWT auth token.
     * 02/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/11730949/5.5.+Ticketing+Payment+Service#id-5.5.Ticketing/PaymentService-GetTicket
     */
    public static getTicket(ticketID: string, accessToken: string, callback: (result: number, body: any) => void) {
        Debug.TestedMock();
        Debug.AssertString(ticketID);
        Debug.AssertString(accessToken);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!TicketServiceAPI.IsSystemLocked()) {
                if (!!ticketID) {
                    let user = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
                    Debug.AssertValidOrNull(user);
                    if ((user != null) && (user.user != null)) {
                        Debug.AssertString(user.user.ID);
                        let ticket = ticket_data.find(_ticket => (_ticket.ID === ticketID));
                        Debug.AssertValidOrNull(ticket);
                        if (ticket != null) {
                            if (((ticket.ReserverOrOwnerID === user.user.ID) && (ticket.PurchaseDate != null)) ||
                                (ticket.OfferedToID === user.user.ID)) {
                                let ticketInfo = TicketServiceAPI.ticketInfoFromTicket(ticket);
                                Debug.AssertValid(ticketInfo);
                                callback(APIHelper.RESULT_200_OK, ticketInfo);
                            } else {
                                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
                            }
                        } else {
                            callback(APIHelper.RESULT_404_NOT_FOUND, {});
                        }
                    } else {
                        callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST, {});
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
            }
        });
    }

    /**
     * Helper methods.
     */

    /**
     * Convert a ticket into a ticket info.
     */
    private static ticketInfoFromTicket(ticket: Ticket): TicketInfo {
        Debug.Tested();
        Debug.AssertValid(ticket);

        let offeredToEmail = null;
        if (ticket.OfferedToID != null) {
            let user = IdentityServiceAPI.getUserByIDInternal(ticket.OfferedToID);
            Debug.AssertValid(user);
            Debug.AssertString(user.EmailAddress);
            offeredToEmail = user.EmailAddress;
        }
        let ticketInfo = <TicketInfo>{
            ID: ticket.ID,
            OwnerID: ticket.ReserverOrOwnerID,
            GameID: ticket.GameID,
            Number: ticket.Number,
            PurchaseDate: ticket.PurchaseDate,
            OfferedToEmail: offeredToEmail,
            OfferedDate: ticket.OfferedDate
        }
        return ticketInfo;
    }

}   // TicketServiceAPI
