import { UserDetails, Gender, Address, DateOnly, Month } from '../data-models/identity-model';
import { Debug } from '../helpers/debug';
import { RandomHelper } from '../helpers/random-helper';
import { AddressHelper } from '../helpers/address-helper';
import { PasswordHelper } from '../helpers/password-helper';
import { ObjectMap } from '../helpers/object-map';
import { EmailHelper } from '../helpers/email-helper';
import { DateHelper } from '../helpers/date-helper';
import { CountryHelper } from '../helpers/country-helper';
import { GenderHelper } from '../helpers/gender-helper';
import { APIHelper } from '../helpers/api-helper';

/**
 * Class providing access to the identity service.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/45056041/5.1.+Requirements
 */
export class IdentityService {

    /**
     * Possible errors.
     */
    public static readonly ERROR_INVALID_ARGS = 'INVALID_ARGS';
    public static readonly ERROR_INVALID_ACCESS_TOKEN = 'INVALID_ACCESS_TOKEN';
    //???public static readonly ERROR_INVALID_VERIFICATION_TOKEN = 'INVALID_VERIFICATION_TOKEN';
    public static readonly ERROR_USER_ALREADY_EXISTS = 'USER_ALREADY_EXISTS';
    //public static readonly ERROR_ACCOUNT_ALREADY_CLOSED = 'ACCOUNT_ALREADY_CLOSED';
    public static readonly ERROR_ACCOUNT_CLOSED = 'ACCOUNT_CLOSED';
    public static readonly ERROR_USER_NOT_FOUND = 'USER_NOT_FOUND';
    //public static readonly ERROR_INVALID_EMAIL_ADDRESS = 'INVALID_EMAIL_ADDRESS';
    //public static readonly ERROR_INVALID_PASSWORD = 'INVALID_PASSWORD';
    //public static readonly ERROR_USER_EMAIL_NOT_VERIFIED = 'USER_EMAIL_NOT_VERIFIED';
    //public static readonly ERROR_USER_EMAIL_VERIFIED = 'USER_EMAIL_VERIFIED';
    public static readonly ERROR_USER_BLOCKED = 'USER_BLOCKED';
    public static readonly ERROR_INVALID_LINK = 'INVALID_LINK';

    /**
     * Get an error from an API result.
     */
    private static errorFromResult(result: number): Error {
        Debug.Tested();

        let error = null;
        switch (result) {
            case APIHelper.RESULT_400_BAD_REQUEST: {
                error = new Error(IdentityService.ERROR_INVALID_ARGS);
                break;
            }
            case APIHelper.RESULT_401_UNAUTHORIZED: {
                error = new Error(IdentityService.ERROR_INVALID_ACCESS_TOKEN);
                break;
            }
            case APIHelper.RESULT_200_OK:
            case APIHelper.RESULT_201_CREATED:
            case APIHelper.RESULT_204_NO_CONTENT:
                break;
            default:
                Debug.Unreachable();
                break;
        }
        return error;
    }

    /**
     * Create account.
     * Caller must validate arguments.
     * 28/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-CreateAccount
     */
    public static createAccount(givenName: string,
                                familyName: string,
                                emailAddress: string,
                                password: string,
                                dateOfBirth: DateOnly,
                                address1: string,
                                address2: string,
                                address3: string,
                                address4: string,
                                city: string,
                                region: string,
                                country: string,
                                postalCode: string,
                                allowNonEssentialEmails: boolean,
                                callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(givenName);
        Debug.AssertValidOrNull(familyName);
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValidOrNull(password);
        Debug.AssertValidOrNull(dateOfBirth);
        Debug.AssertValidOrNull(address1);
        Debug.AssertValidOrNull(address2);
        Debug.AssertValidOrNull(address3);
        Debug.AssertValidOrNull(address4);
        Debug.AssertValidOrNull(city);
        Debug.AssertValidOrNull(region);
        Debug.AssertValidOrNull(country);
        Debug.AssertValidOrNull(postalCode);
        Debug.AssertValidOrNull(allowNonEssentialEmails);
        Debug.AssertValid(callback);

        let apiDateOfBirth = DateHelper.DateOnlyToAPIDate(dateOfBirth);
        Debug.AssertString(apiDateOfBirth);
        IdentityServiceAPI.createAccount(givenName, familyName, emailAddress, password, apiDateOfBirth,
                                         address1, address2, address3, address4, city, region, country, postalCode,
                                         allowNonEssentialEmails,
                                         (result) => {
            let error = null;
            switch (result) {
                case APIHelper.RESULT_403_FORBIDDEN:
                    error = new Error(IdentityService.ERROR_USER_ALREADY_EXISTS);
                    break;
                default:
                    error = IdentityService.errorFromResult(result);
                    Debug.AssertValidOrNull(error);
                    break;
            }
            callback(error);
        });
    }

    /**
     * Login.
     * Uses the identity service to check that the specified email address and password match a user.
     * Caller must validate arguments.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-Login
     */
    public static login(emailAddress: string, password: string, callback: (error: Error, accessToken: string) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValidOrNull(password);
        Debug.AssertValid(callback);

        IdentityServiceAPI.login(emailAddress, password, (result, body) => {
            Debug.AssertValid(body);
            let error = null;
            let accessToken = null;
            switch (result) {
                case APIHelper.RESULT_400_BAD_REQUEST: {
                    error = new Error(IdentityService.ERROR_INVALID_ARGS);
                    break;
                }
                case APIHelper.RESULT_401_UNAUTHORIZED: {
                    Debug.Untested();
                    switch (body.Error) {
                        case IdentityServiceAPI.SERVER_ERROR_BAD_CREDENTIALS: {
                            error = new Error(IdentityService.ERROR_USER_NOT_FOUND);
                            break;
                        }
                        case IdentityServiceAPI.SERVER_ERROR_USER_BLOCKED: {
                            error = new Error(IdentityService.ERROR_USER_BLOCKED);
                            break;
                        }
                        case IdentityServiceAPI.SERVER_ERROR_ACCOUNT_CLOSED: {
                            error = new Error(IdentityService.ERROR_ACCOUNT_CLOSED);
                            break;
                        }
                    }
                    break;
                }
                case APIHelper.RESULT_200_OK:
                    accessToken = body.AccessToken;
                    break;
                default:
                    Debug.Unreachable();
                    break;
            }
            callback(error, accessToken);
        });
    }

    /**
     * Logout.
     * Uses the identity service to invalidate the authentication token for the specified user.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-Logout
     */
    public static logout(accessToken: string, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertString(accessToken);
        Debug.AssertValid(callback);

        IdentityServiceAPI.logout(accessToken, (result) => {
            let error = IdentityService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            callback(error);
        });
    }

    /**
     * Extend the access token's expiry time.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-Extendaccesstoken
     */
    public static extendAccessToken(accessToken: string, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertString(accessToken);
        Debug.AssertValid(callback);

        IdentityServiceAPI.extendAccessToken(accessToken, (result) => {
            let error = null;
            switch (result) {
                case APIHelper.RESULT_403_FORBIDDEN:
                    break;
                default:
                    error = IdentityService.errorFromResult(result);
                    Debug.AssertValidOrNull(error);
                    break;
            }
            callback(error);
        });
    }

    /**
     * Get the user details for an authenticated user.
     * May cache this value for efficiency.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-GetUserDetails
     */
    public static getUserDetails(accessToken: string, callback: (error: Error, userDetails: UserDetails) => void) {
        Debug.Tested();
        Debug.AssertString(accessToken);
        Debug.AssertValid(callback);

        IdentityServiceAPI.getUserDetails(accessToken, (result, body) => {
            let error = null;
            let userDetails: UserDetails = null;
            switch (result) {
                case APIHelper.RESULT_401_UNAUTHORIZED: {
                    error = new Error(IdentityService.ERROR_INVALID_ACCESS_TOKEN);
                    break;
                }
                case APIHelper.RESULT_200_OK: {
                    userDetails = <UserDetails>{
                        ID: null,
                        PasswordHash: null,
                        EmailAddress: body.emailAddress,
                        EmailAddressVerified: true,
                        GivenName: body.givenName,
                        FamilyName: body.familyName,
                        PreferredName: body.preferredName,
                        FullName: body.fullName,
                        DateOfBirth: DateHelper.APIDateToDateOnly(body.DateOfBirth),
                        Gender: body.gender,
                        Address1: body.address1,
                        Address2: body.address2,
                        Address3: body.address3,
                        Address4: body.address4,
                        City: body.city,
                        Region: body.region,
                        Country: body.country,
                        PostalCode: body.postalCode,
                        PhoneNumber: body.phoneNumber,
                        PhoneNumberVerified: body.phoneNumberVerified,
                        NewEmailAddress: body.newEmailAddress,
                        AllowNonEssentialEmails: body.allowNonEssentialEmails,
                        TotalTicketsPurchased: body.totalTicketsPurchased,
                        TicketsPurchasedInCurrentGame: body.ticketsPurchasedInCurrentGame,
                        PreferredLanguage: body.preferredLanguage,
                        MaxDailySpendingAmount: body.maxDailySpendingAmount,
                        NewMaxDailySpendingAmount: body.newMaxDailySpendingAmount,
                        NewMaxDailySpendingAmountTime: body.newMaxDailySpendingAmountTime,
                        MaxTimeLoggedIn: body.maxTimeLoggedIn,
                        NewMaxTimeLoggedIn: body.newMaxTimeLoggedIn,
                        NewMaxTimeLoggedInTime: body.newMaxTimeLoggedInTime,
                        ExcludeUntil: body.excludeUntil,
                        NewExcludeUntil: body.newExcludeUntil,
                        NewExcludeUntilTime: body.newExcludeUntilTime
                        };
                    break;
                }
                default:
                    Debug.Unreachable();
                    break;
            }
            callback(error, userDetails);
        });
    }

    /**
     * Get the user permissions for an authenticated user.
     * May cache these for efficiency.
     * 16/07/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-GetUserPermissions
     */
    public static getUserPermissions(accessToken: string, callback: (error: Error, permissions: string[]) => void) {
        Debug.Tested();
        Debug.AssertString(accessToken);
        Debug.AssertValid(callback);

        IdentityServiceAPI.getUserPermissions(accessToken, (result, body) => {
            let error = null;
            let permissions: string[] = null;
            switch (result) {
                case APIHelper.RESULT_401_UNAUTHORIZED: {
                    error = new Error(IdentityService.ERROR_INVALID_ACCESS_TOKEN);
                    break;
                }
                case APIHelper.RESULT_200_OK: {
                    permissions = body.permissions;
                    break;
                }
                default:
                    Debug.Unreachable();
                    break;
            }
            callback(error, permissions);
        });
    }

    /**
     * Close account.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-CloseAccount
     */
    public static closeAccount(accessToken: string, password: string, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(password);
        Debug.AssertValid(callback);

        IdentityServiceAPI.closeAccount(accessToken, password, (result) => {
            let error = IdentityService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            callback(error);
        });
    }

    /**
     * Set the user's name'.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserName
     */
    public static setUserName(accessToken: string, givenName: string, familyName: string, fullName: string, preferredName: string, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(givenName);
        Debug.AssertValidOrNull(familyName);
        Debug.AssertValidOrNull(fullName);
        Debug.AssertValidOrNull(preferredName);
        Debug.AssertValid(callback);

        IdentityServiceAPI.setUserName(accessToken, givenName, familyName, fullName, preferredName, (result) => {
            let error = IdentityService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            callback(error);
        });
    }

    /**
     * Set the user's email'.
     * Initiates the set email flow.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserEmail
     */
    public static setUserEmail(accessToken: string, emailAddress: string, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValid(callback);

        IdentityServiceAPI.setUserEmail(accessToken, emailAddress, (result) => {
            let error = IdentityService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            callback(error);
        });
    }

    /**
     * Sets the user's password.
     * User must be logged in. Access token and old password are required.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserPassword
     */
    public static setUserPassword(accessToken: string, oldPassword: string, newPassword: string, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(oldPassword);
        Debug.AssertValidOrNull(newPassword);
        Debug.AssertValid(callback);

        IdentityServiceAPI.setUserPassword(accessToken, oldPassword, newPassword, (result) => {
            let error = IdentityService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            callback(error);
        });
    }

    /**
     * Set the user's gender.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserGender
     */
    public static setUserGender(accessToken: string, gender: Gender, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValid(callback);

        IdentityServiceAPI.setUserGender(accessToken, gender, (result) => {
            let error = IdentityService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            callback(error);
        });
    }

    /**
     * Set the user's address.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserAddress
     */
    public static setUserAddress(accessToken: string, address: Address, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValid(address);
        Debug.AssertValidOrNull(address.Line1);
        Debug.AssertValidOrNull(address.Line2);
        Debug.AssertValidOrNull(address.Line3);
        Debug.AssertValidOrNull(address.Line4);
        Debug.AssertValidOrNull(address.City);
        Debug.AssertValidOrNull(address.Region);
        Debug.AssertValidOrNull(address.Country);
        Debug.AssertValidOrNull(address.PostalCode);
        Debug.AssertValid(callback);

        IdentityServiceAPI.setUserAddress(accessToken, address.Line1, address.Line2, address.Line3, address.Line4, address.City, address.Region, address.Country, address.PostalCode, (result) => {
            let error = IdentityService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            callback(error);
        });
    }

    /**
     * Set the user's phone number.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserPhoneNumber
     */
    public static setUserPhoneNumber(accessToken: string, phoneNumber: string, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(phoneNumber);
        Debug.AssertValid(callback);

        IdentityServiceAPI.setUserPhoneNumber(accessToken, phoneNumber, (result) => {
            let error = IdentityService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            callback(error);
        });
    }

    /**
     * Set the user's 'allow non-essential emails' flag.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserAllowNon-EssentialEmails
     */
    public static setUserAllowNonEssentialEmails(accessToken: string, allowNonEssentialEmails: boolean, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(allowNonEssentialEmails);
        Debug.AssertValid(callback);

        IdentityServiceAPI.setUserAllowNonEssentialEmails(accessToken, allowNonEssentialEmails, (result) => {
            let error = IdentityService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            callback(error);
        });
    }

    /**
     * Set the user's 'allow non-essential emails' flag.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserLimits
     */
    public static setUserLimits(accessToken: string,
                                maxDailySpendingAmount: number,
                                maxTimeLoggedIn: number,
                                excludeUntil: Date,
                                callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(maxDailySpendingAmount);
        Debug.AssertValidOrNull(maxTimeLoggedIn);
        Debug.AssertValidOrNull(excludeUntil);
        Debug.AssertValid(callback);

        IdentityServiceAPI.setUserLimits(accessToken, maxDailySpendingAmount, maxTimeLoggedIn, excludeUntil, (result) => {
            let error = IdentityService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            callback(error);
        });
    }

    /**
     * Initiates the reset (forgot) password flow.
     * Email a link to the user for them to reset their password.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-ResetUserPassword
     */
    public static resetUserPassword(emailAddress: string, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValid(callback);

        IdentityServiceAPI.resetUserPassword(emailAddress, (result) => {
            let error = null;
            switch (result) {
                case APIHelper.RESULT_403_FORBIDDEN:
                    error = new Error(IdentityService.ERROR_USER_NOT_FOUND);
                    break;
                default:
                    error = IdentityService.errorFromResult(result);
                    Debug.AssertValidOrNull(error);
                    break;
            }
            callback(error);
        });
    }

    /**
     * Sets the user's password.
     * User should not be logged in (i.e. forgot password flow) and the reset password link ID and email address are required.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserPasswordafterReset
     */
    public static setUserPasswordAfterReset(resetPasswordLinkID: string,
                                            emailAddress: string,
                                            newPassword: string,
                                            callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(resetPasswordLinkID);
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValidOrNull(newPassword);
        Debug.AssertValid(callback);

        IdentityServiceAPI.setUserPasswordAfterReset(resetPasswordLinkID, emailAddress, newPassword, (result) => {
            let error = null;
            switch (result) {
                case APIHelper.RESULT_401_UNAUTHORIZED:
                    error = new Error(IdentityService.ERROR_INVALID_LINK);
                    break;
                default:
                    error = IdentityService.errorFromResult(result);
                    Debug.AssertValidOrNull(error);
                    break;
            }
            callback(error);
        });
    }

    /**
     * Resend the 'verify an email address' email.
     * Caller must validate arguments.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-ResendEmailVerification
     */
    public static resendEmailVerification(emailAddress: string, callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValid(callback);

        IdentityServiceAPI.resendEmailVerification(emailAddress, (result) => {
            let error = null;
            switch (result) {
                case APIHelper.RESULT_401_UNAUTHORIZED:
                    error = new Error(IdentityService.ERROR_USER_NOT_FOUND);
                    break;
                default:
                    error = IdentityService.errorFromResult(result);
                    Debug.AssertValidOrNull(error);
                    break;
            }
            callback(error);
        });
    }

    /**
     * Verify an email address.
     * Caller must validate arguments.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-VerifyEmail(newa/c,changeemail)
     */
    public static verifyEmail(emailAddress: string, verifyEmailLinkID: string, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValidOrNull(verifyEmailLinkID);
        Debug.AssertValid(callback);

        IdentityServiceAPI.verifyEmail(emailAddress, verifyEmailLinkID, (result) => {
            let error = null;
            switch (result) {
                case APIHelper.RESULT_401_UNAUTHORIZED:
                    error = new Error(IdentityService.ERROR_INVALID_LINK);
                    break;
                default:
                    error = IdentityService.errorFromResult(result);
                    Debug.AssertValidOrNull(error);
                    break;
            }
            callback(error);
        });
    }

    /**
     * Confirm a new user's details.
     * Caller must validate arguments.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-VerifyEmailw/name/pwd
     */
    public static verifyEmailWithDetails(emailAddress: string,
                                         verifyEmailLinkID: string,
                                         givenName: string,
                                         familyName: string,
                                         password: string,
                                         dateOfBirth: DateOnly,
                                         address1: string,
                                         address2: string,
                                         address3: string,
                                         address4: string,
                                         city: string,
                                         region: string,
                                         country: string,
                                         postalCode: string,
                                         allowNonEssentialEmails: boolean,
                                         callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValidOrNull(verifyEmailLinkID);
        Debug.AssertValidOrNull(givenName);
        Debug.AssertValidOrNull(familyName);
        Debug.AssertValidOrNull(password);
        Debug.AssertValidOrNull(dateOfBirth);
        Debug.AssertValidOrNull(address1);
        Debug.AssertValidOrNull(address2);
        Debug.AssertValidOrNull(address3);
        Debug.AssertValidOrNull(address4);
        Debug.AssertValidOrNull(city);
        Debug.AssertValidOrNull(region);
        Debug.AssertValidOrNull(country);
        Debug.AssertValidOrNull(postalCode);
        Debug.AssertValidOrNull(allowNonEssentialEmails);
        Debug.AssertValid(callback);

        let apiDateOfBirth = DateHelper.DateOnlyToAPIDate(dateOfBirth);
        Debug.AssertString(apiDateOfBirth);
        IdentityServiceAPI.verifyEmailWithDetails(emailAddress, verifyEmailLinkID, givenName, familyName, password,
                                                  apiDateOfBirth, address1, address2, address3, address4, city, region, country, postalCode, allowNonEssentialEmails,
                                                  (result) => {
            let error = null;
            switch (result) {
                case APIHelper.RESULT_401_UNAUTHORIZED:
                    error = new Error(IdentityService.ERROR_INVALID_LINK);
                    break;
                default:
                    error = IdentityService.errorFromResult(result);
                    Debug.AssertValidOrNull(error);
                    break;
            }
            callback(error);
        });
    }

}   // IdentityService

/**
 * Class representing a user record. Used for the mock APIs only.
 * This includes all information such as name, email, address, phone, dob, gender.
 * See: https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/44826670/5.2.+Data+Structure
 */
export class UserRecord {
    public ID: string;
    public EmailAddress: string;
    public EmailAddressVerified: boolean;
    public PasswordHash: string;
    public GivenName: string;
    public FamilyName: string;
    public PreferredName?: string;
    public FullName?: string;
    public Blocked?: boolean;
    public DateOfBirth: DateOnly;
    public Gender?: Gender;
    public Address1: string;
    public Address2?: string;
    public Address3?: string;
    public Address4?: string;
    public City?: string;
    public Region?: string;
    public Country: string;
    public PostalCode: string;
    public PhoneNumber?: string;
    public PhoneNumberVerified?: boolean;
    public LastLoggedIn?: Date;
    public LastLoggedOut?: Date;
    public NewEmailAddress?: string;
    public Closed?: Date;
    // public IsAnonymized?: boolean;
    // public Deleted?: Date;
    public AllowNonEssentialEmails?: boolean;
    public ANEEOnTimestamp?: Date;
    public ANEEOffTimestamp?: Date;
    public TotalTicketsPurchased?: number;
    public TicketsPurchasedInCurrentGame?: number;
    public PreferredLanguage?: string;
    public FailedLoginAttempts?: number;
    // public KYCStatus?: string;
    // public KCYTimestamp?: Date;
    public MaxDailySpendingAmount?: number;
    public NewMaxDailySpendingAmount?: number;
    public NewMaxDailySpendingAmountTime?: Date;
    public MaxTimeLoggedIn?: number;
    public NewMaxTimeLoggedIn?: number;
    public NewMaxTimeLoggedInTime?: Date;
    public ExcludeUntil?: Date;
    public NewExcludeUntil?: Date;
    public NewExcludeUntilTime?: Date;
    public Permissions: string[];
}   // UserRecord

/**
 * Dummy (test) users to test identity service.
 */
var DummyUsers: UserRecord[] = [
    {
        ID: 'user_1',
        GivenName: 'Test',
        FamilyName: 'User 1',
        EmailAddress: 'test1@test.com',
        EmailAddressVerified: true,
        PasswordHash: PasswordHelper.getPasswordHash('Passw0rd'),
        DateOfBirth: new DateOnly(1970, Month.jan, 2),
        Address1: 'Address 1',
        Country: 'gb',
        PostalCode: 'postcode',
        Permissions: []
    },
    {
        ID: 'user_2',
        GivenName: 'Test',
        FamilyName: 'User 2',
        EmailAddress: 'test2@test.com',
        EmailAddressVerified: true,
        PasswordHash: PasswordHelper.getPasswordHash('Passw0rd'),
        DateOfBirth: new DateOnly(1970, Month.jan, 2),
        Address1: 'Address 1',
        Country: 'us',
        PostalCode: 'zip code',
        Permissions: []
    }
];

/**
 * An access token.
 */
class AccessToken {
    ID: string;
    UserID: string;
    Expires: Date;
    MaxExpiry: Date;

    /**
     * Constructor.
     */
    constructor(user: UserRecord) {
        Debug.Tested();
        Debug.AssertValid(user);
        Debug.AssertString(user.ID);
        Debug.AssertValidOrNull(user.MaxTimeLoggedIn);
        Debug.AssertValidOrNull(IdentityServiceAPI.MaxUserLoginTime);

        this.ID = `ACCESS_TOKEN_${RandomHelper.getRandomNumber()}`;
        this.UserID = user.ID;
        this.setupExpiry();
        let maxTimeLoggedIn = null;
        if ((user.MaxTimeLoggedIn != null) && (user.MaxTimeLoggedIn > 0)) {
            maxTimeLoggedIn = user.MaxTimeLoggedIn;
        }
        if ((IdentityServiceAPI.MaxUserLoginTime != null) && (IdentityServiceAPI.MaxUserLoginTime > 0)) {
            Debug.Untested();
            if (maxTimeLoggedIn == null) {
                maxTimeLoggedIn = IdentityServiceAPI.MaxUserLoginTime;
            } else if (IdentityServiceAPI.MaxUserLoginTime < maxTimeLoggedIn) {
                maxTimeLoggedIn = IdentityServiceAPI.MaxUserLoginTime;
            }
        }
        if (maxTimeLoggedIn != null) {
            Debug.Untested();
            this.MaxExpiry = new Date();
            this.MaxExpiry.setTime(this.MaxExpiry.getTime() + (maxTimeLoggedIn * 60 * 1000));
        }
    }

    /**
     * Set the expiry time.
     * Returns true if it was extended and false if not.
     */
    public setupExpiry(): boolean {
        Debug.Tested();

        let retVal = false;
        if ((this.Expires == null) || (this.Expires !== this.MaxExpiry)) {
            this.Expires = new Date();
            this.Expires.setTime(this.Expires.getTime() + (IdentityServiceAPI.AccessTokenLifetime * 1000));
            if ((this.MaxExpiry != null) && (this.Expires > this.MaxExpiry)) {
                Debug.Untested();
                this.Expires = this.MaxExpiry;
            }
            retVal = true;
        }
        return retVal;
    }

}   // AccessToken

/**
 * A link.
 */
class Link {
    ID: string;
    Type: string;
    Expires: Date;
    Used: boolean;
    Info: any;

    /**
     * Constructor.
     */
    constructor(type: string, info: any) {
        Debug.Tested();

        this.ID = `LINK_${RandomHelper.getRandomNumber()}`;
        this.Type = type;
        this.Expires = new Date();
        this.Expires.setTime(this.Expires.getTime() + (IdentityServiceAPI.ResetPasswordLinkLifetime * 60 * 1000));
        this.Used = false;
        this.Info = info;
    }

}   // Link

/**
 * Class providing access to the identity service.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/45056041/5.1.+Requirements
 */
export class IdentityServiceAPI {

    /**
     * Server errors.
     */
    public static readonly SERVER_ERROR_BAD_CREDENTIALS = 'bad-credentials';
    public static readonly SERVER_ERROR_USER_BLOCKED = 'user-blocked';
    public static readonly SERVER_ERROR_ACCOUNT_CLOSED = 'account-closed';

    /**
     * Global settings.
     */
    public static BlockOnFailedLoginAttempts: number = 5;
    public static AccessTokenLifetime = 60;
    public static ResetPasswordLinkLifetime = 60;
    public static EmailVerificationLifetime = 60;
    public static FailForgotPasswordIfEmailNotVerified = true;
    public static MaxUserLoginTime: number = null;

    /**
     * Create account.
     * Caller must validate arguments.
     * 28/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-CreateAccount
     */
    public static createAccount(givenName: string,
                                familyName: string,
                                emailAddress: string,
                                password: string,
                                dateOfBirth: string,
                                address1: string,
                                address2: string,
                                address3: string,
                                address4: string,
                                city: string,
                                region: string,
                                country: string,
                                postalCode: string,
                                allowNonEssentialEmails: boolean,
                                callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(givenName);
        Debug.AssertValidOrNull(familyName);
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValidOrNull(password);
        Debug.AssertValidOrNull(address1);
        Debug.AssertValidOrNull(address2);
        Debug.AssertValidOrNull(address3);
        Debug.AssertValidOrNull(address4);
        Debug.AssertValidOrNull(city);
        Debug.AssertValidOrNull(region);
        Debug.AssertValidOrNull(country);
        Debug.AssertValidOrNull(postalCode);
        Debug.AssertValidOrNull(allowNonEssentialEmails);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!!givenName &&
                !!familyName &&
                EmailHelper.isValidEmailAddress(emailAddress) &&
                PasswordHelper.isValidPassword(password) &&
                DateHelper.isValidAPIDate(dateOfBirth) &&
                !!address1 &&
                CountryHelper.isValidCountryCode(country) &&
                !!postalCode) {
                let user = IdentityServiceAPI.getUserByEmailInternal(emailAddress);
                Debug.AssertValidOrNull(user);
                if (user == null) {
                    //??++CHECK USER WITH NewEmailAddress
                    let fullName = givenName + ' ' + familyName;
                    let user: UserRecord = <UserRecord>{
                        ID: 'account_' + RandomHelper.getRandomNumber(),
                        GivenName: givenName,
                        FamilyName: familyName,
                        FullName: fullName,
                        EmailAddress: emailAddress,
                        EmailAddressVerified: false,
                        PasswordHash: PasswordHelper.getPasswordHash(password),
                        DateOfBirth: DateHelper.APIDateToDateOnly(dateOfBirth),
                        Address1: address1,
                        Address2: address2,
                        Address3: address3,
                        Address4: address4,
                        City: city,
                        Region: region,
                        Country: country,
                        PostalCode: postalCode,
                        AllowNonEssentialEmails: !!allowNonEssentialEmails,
                        ANEEOnTimestamp: allowNonEssentialEmails ? new Date() : null,
                        ANEEOffTimestamp: allowNonEssentialEmails ? null : new Date()
                    };
                    DummyUsers.push(user);
                    let linkID = IdentityServiceAPI.CreateVerifyEmailLink(user.ID, emailAddress, false);
                    console.log(`URL: /verify-email?linkId=${linkID}`);
                    // Real service would send email with URL containing the link ID.
                    callback(APIHelper.RESULT_201_CREATED);
                } else {
                    //??++IF EXISTING USER NOT VERIFIED - UPDATE DETAILS AND RESEND VERIFICATION EMAIL AND SUCCEED
                    //??++REOPEN HERE. CHECK SECURITY OF DOING THIS
                    callback(APIHelper.RESULT_403_FORBIDDEN);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Login.
     * Uses the identity service to check that the specified email address and password match a user.
     * Caller must validate arguments.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-Login
     */
    public static login(emailAddress: string, password: string, callback: (result: number, body: { Error?: string, AccessToken?: string }) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValidOrNull(password);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!!emailAddress && !!password) {
                let user = IdentityServiceAPI.getUserByEmailInternal(emailAddress);
                Debug.AssertValidOrNull(user);
                if (user != null) {
                    if (!user.Blocked) {
                        if (user.PasswordHash === PasswordHelper.getPasswordHash(password)) {
                            if (user.Closed == null) {
                                user.LastLoggedIn = new Date();
                                let accessToken = IdentityServiceAPI.CreateAccessToken(user);
                                Debug.AssertString(accessToken);
                                callback(APIHelper.RESULT_200_OK, { AccessToken: accessToken });
                            } else {
                                callback(APIHelper.RESULT_401_UNAUTHORIZED, { Error: IdentityServiceAPI.SERVER_ERROR_ACCOUNT_CLOSED });
                            }
                        } else {
                            Debug.UntestedMock();
                            if (++user.FailedLoginAttempts >= IdentityServiceAPI.BlockOnFailedLoginAttempts) {
                                user.Blocked = true;
                                callback(APIHelper.RESULT_401_UNAUTHORIZED, { Error: IdentityServiceAPI.SERVER_ERROR_USER_BLOCKED });
                            } else {
                                callback(APIHelper.RESULT_401_UNAUTHORIZED, { Error: IdentityServiceAPI.SERVER_ERROR_BAD_CREDENTIALS });
                            }
                        }
                    } else {
                        callback(APIHelper.RESULT_401_UNAUTHORIZED, { Error: IdentityServiceAPI.SERVER_ERROR_USER_BLOCKED });
                    }
                } else {
                    callback(APIHelper.RESULT_401_UNAUTHORIZED, { Error: IdentityServiceAPI.SERVER_ERROR_BAD_CREDENTIALS });
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST, { });
            }
        });
    }

    /**
     * Logout.
     * Uses the identity service to invalidate the authentication token for the specified user.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-Logout
     */
    public static logout(accessToken: string, callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
            Debug.AssertValid(accessTokenDetails);
            Debug.AssertValidOrNull(accessTokenDetails.user);
            if (accessTokenDetails.user != null) {
                accessTokenDetails.user.LastLoggedOut = new Date();
                IdentityServiceAPI.RevokeAccessToken(accessToken);
                callback(APIHelper.RESULT_204_NO_CONTENT);
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Extend the access token's expiry time.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-Extendaccesstoken
     */
    public static extendAccessToken(accessToken: string, callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
            Debug.AssertValid(accessTokenDetails);
            Debug.AssertValidOrNull(accessTokenDetails.user);
            if (accessTokenDetails.user != null) {
                Debug.AssertNull(accessTokenDetails.user.Closed);
                if (accessTokenDetails.extended) {
                    callback(APIHelper.RESULT_204_NO_CONTENT);
                } else {
                    callback(APIHelper.RESULT_403_FORBIDDEN);
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Get the user details for an authenticated user.
     * May cache this value for efficiency.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-GetUserDetails
     */
    public static getUserDetails(accessToken: string, callback: (result: number, body?: any) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
            Debug.AssertValid(accessTokenDetails);
            Debug.AssertValidOrNull(accessTokenDetails.user);
            if (accessTokenDetails.user != null) {
                let body = {
                    emailAddress: accessTokenDetails.user.EmailAddress,
                    givenName: accessTokenDetails.user.GivenName,
                    familyName: accessTokenDetails.user.FamilyName,
                    preferredName: accessTokenDetails.user.PreferredName,
                    fullName: accessTokenDetails.user.FullName,
                    dateOfBirth: DateHelper.DateOnlyToAPIDate(accessTokenDetails.user.DateOfBirth),
                    gender: accessTokenDetails.user.Gender,
                    address1: accessTokenDetails.user.Address1,
                    address2: accessTokenDetails.user.Address2,
                    address3: accessTokenDetails.user.Address3,
                    address4: accessTokenDetails.user.Address4,
                    city: accessTokenDetails.user.City,
                    region: accessTokenDetails.user.Region,
                    country: accessTokenDetails.user.Country,
                    postalCode: accessTokenDetails.user.PostalCode,
                    phoneNumber: accessTokenDetails.user.PhoneNumber,
                    phoneNumberVerified: accessTokenDetails.user.PhoneNumberVerified,
                    newEmailAddress: accessTokenDetails.user.NewEmailAddress,
                    allowNonEssentialEmails: accessTokenDetails.user.AllowNonEssentialEmails,
                    totalTicketsPurchased: accessTokenDetails.user.TotalTicketsPurchased,
                    ticketsPurchasedInCurrentGame: accessTokenDetails.user.TicketsPurchasedInCurrentGame,
                    preferredLanguage: accessTokenDetails.user.PreferredLanguage,
                    maxDailySpendingAmount: accessTokenDetails.user.MaxDailySpendingAmount,
                    newMaxDailySpendingAmount: accessTokenDetails.user.NewMaxDailySpendingAmount,
                    newMaxDailySpendingAmountTime: accessTokenDetails.user.NewMaxDailySpendingAmountTime,
                    maxTimeLoggedIn: accessTokenDetails.user.MaxTimeLoggedIn,
                    newMaxTimeLoggedIn: accessTokenDetails.user.NewMaxTimeLoggedIn,
                    newMaxTimeLoggedInTime: accessTokenDetails.user.NewMaxTimeLoggedInTime,
                    excludeUntil: accessTokenDetails.user.ExcludeUntil,
                    newExcludeUntil: accessTokenDetails.user.NewExcludeUntil,
                    newExcludeUntilTime: accessTokenDetails.user.NewExcludeUntilTime
                };
                callback(APIHelper.RESULT_200_OK, body);
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Get the user permissions for an authenticated user.
     * May cache these for efficiency.
     * 16/07/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-GetUserPermissions
     */
    public static getUserPermissions(accessToken: string, callback: (result: number, body?: any) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            IdentityServiceAPI.doGetUserPermissions(accessToken, callback);
        });
    }

    /**
     * Get the user permissions for an authenticated user.
     * May cache these for efficiency.
     * 16/07/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-GetUserPermissions
     */
    public static doGetUserPermissions(accessToken: string, callback: (result: number, body?: any) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValid(callback);

        let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
        Debug.AssertValid(accessTokenDetails);
        Debug.AssertValidOrNull(accessTokenDetails.user);
        if (accessTokenDetails.user != null) {
            let body = {
                permissions: accessTokenDetails.user.Permissions
            };
            callback(APIHelper.RESULT_200_OK, body);
        } else {
            callback(APIHelper.RESULT_401_UNAUTHORIZED);
        }
    }

    /**
     * Close account.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-CloseAccount
     */
    public static closeAccount(accessToken: string, password: string, callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(password);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!!password) {
                let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
                Debug.AssertValid(accessTokenDetails);
                Debug.AssertValidOrNull(accessTokenDetails.user);
                if (accessTokenDetails.user != null) {
                    Debug.AssertNull(accessTokenDetails.user.Closed);
                    if (accessTokenDetails.user.PasswordHash === PasswordHelper.getPasswordHash(password)) {
                        accessTokenDetails.user.Closed = new Date();
                        IdentityServiceAPI.RevokeAccessToken(accessToken);
                        callback(APIHelper.RESULT_204_NO_CONTENT);
                    } else {
                        callback(APIHelper.RESULT_401_UNAUTHORIZED);
                    }
                } else {
                    callback(APIHelper.RESULT_401_UNAUTHORIZED);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Set the user's name'.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserName
     */
    public static setUserName(accessToken: string, givenName: string, familyName: string, fullName: string, preferredName: string, callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(givenName);
        Debug.AssertValidOrNull(familyName);
        Debug.AssertValidOrNull(fullName);
        Debug.AssertValidOrNull(preferredName);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!!givenName && !!familyName) {
                let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
                Debug.AssertValid(accessTokenDetails);
                Debug.AssertValidOrNull(accessTokenDetails.user);
                if (accessTokenDetails.user != null) {
                    accessTokenDetails.user.GivenName = givenName;
                    accessTokenDetails.user.FamilyName = familyName;
                    accessTokenDetails.user.FullName = fullName;
                    accessTokenDetails.user.PreferredName = preferredName;
                    callback(APIHelper.RESULT_204_NO_CONTENT);
                } else {
                    callback(APIHelper.RESULT_401_UNAUTHORIZED);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Set the user's email'.
     * Initiates the set email flow.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserEmail
     */
    public static setUserEmail(accessToken: string, emailAddress: string, callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (EmailHelper.isValidEmailAddress(emailAddress)) {
                let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
                Debug.AssertValid(accessTokenDetails);
                Debug.AssertValidOrNull(accessTokenDetails.user);
                if (accessTokenDetails.user != null) {
                    Debug.AssertString(accessTokenDetails.user.EmailAddress);
                    Debug.AssertStringOrNull(accessTokenDetails.user.NewEmailAddress);
                    if ((emailAddress !== accessTokenDetails.user.EmailAddress) && (emailAddress !== accessTokenDetails.user.NewEmailAddress)) {
                        accessTokenDetails.user.NewEmailAddress = emailAddress;
                        IdentityServiceAPI.CreateVerifyEmailLink(accessTokenDetails.user.ID, emailAddress);
                        // Real service would send email with URL containing the link ID.
                        console.log(`Verification email sent to ${emailAddress} (after account creation)`);
                    }
                    callback(APIHelper.RESULT_204_NO_CONTENT);
                } else {
                    callback(APIHelper.RESULT_401_UNAUTHORIZED);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Sets the user's password.
     * User must be logged in. Access token and old password are required.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserPassword
     */
    public static setUserPassword(accessToken: string, oldPassword: string, newPassword: string, callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(oldPassword);
        Debug.AssertValidOrNull(newPassword);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!!oldPassword && PasswordHelper.isValidPassword(newPassword)) {
                let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
                Debug.AssertValid(accessTokenDetails);
                Debug.AssertValidOrNull(accessTokenDetails.user);
                if (accessTokenDetails.user != null) {
                    if (accessTokenDetails.user.PasswordHash === PasswordHelper.getPasswordHash(oldPassword)) {
                        accessTokenDetails.user.PasswordHash = PasswordHelper.getPasswordHash(newPassword);
                        callback(APIHelper.RESULT_204_NO_CONTENT);
                    } else {
                        callback(APIHelper.RESULT_401_UNAUTHORIZED);
                    }
                } else {
                    callback(APIHelper.RESULT_401_UNAUTHORIZED);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Set the user's gender.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserGender
     */
    public static setUserGender(accessToken: string, gender: Gender, callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (GenderHelper.isValidGender(gender)) {
                let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
                Debug.AssertValid(accessTokenDetails);
                Debug.AssertValidOrNull(accessTokenDetails.user);
                if (accessTokenDetails.user != null) {
                    accessTokenDetails.user.Gender = gender;
                    callback(APIHelper.RESULT_204_NO_CONTENT);
                } else {
                    callback(APIHelper.RESULT_401_UNAUTHORIZED);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Set the user's address.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserAddress
     */
    public static setUserAddress(accessToken: string,
                                 address1: string,
                                 address2: string,
                                 address3: string,
                                 address4: string,
                                 city: string,
                                 region: string,
                                 country: string,
                                 postalCode: string,
                                 callback: (result: number) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(address1);
        Debug.AssertValidOrNull(address2);
        Debug.AssertValidOrNull(address3);
        Debug.AssertValidOrNull(address4);
        Debug.AssertValidOrNull(city);
        Debug.AssertValidOrNull(region);
        Debug.AssertValidOrNull(country);
        Debug.AssertValidOrNull(postalCode);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!!address1 && !!country && !!postalCode) {
                let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
                Debug.AssertValid(accessTokenDetails);
                Debug.AssertValidOrNull(accessTokenDetails.user);
                if (accessTokenDetails.user != null) {
                    accessTokenDetails.user.Address1 = address1;
                    accessTokenDetails.user.Address2 = address2;
                    accessTokenDetails.user.Address3 = address3;
                    accessTokenDetails.user.Address4 = address4;
                    accessTokenDetails.user.City = city;
                    accessTokenDetails.user.Region = region;
                    accessTokenDetails.user.Country = country;
                    accessTokenDetails.user.PostalCode = postalCode;
                    callback(APIHelper.RESULT_204_NO_CONTENT);
                } else {
                    callback(APIHelper.RESULT_401_UNAUTHORIZED);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Set the user's phone number.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserPhoneNumber
     */
    public static setUserPhoneNumber(accessToken: string, phoneNumber: string, callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(phoneNumber);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!!phoneNumber) {
                let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
                Debug.AssertValid(accessTokenDetails);
                Debug.AssertValidOrNull(accessTokenDetails.user);
                if (accessTokenDetails.user != null) {
                    if (phoneNumber !== accessTokenDetails.user.PhoneNumber) {
                        accessTokenDetails.user.PhoneNumber = phoneNumber;
                        accessTokenDetails.user.PhoneNumberVerified = false;
                    }
                    callback(APIHelper.RESULT_204_NO_CONTENT);
                } else {
                    callback(APIHelper.RESULT_401_UNAUTHORIZED);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Set the user's 'allow non-essential emails' flag.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserAllowNon-EssentialEmails
     */
    public static setUserAllowNonEssentialEmails(accessToken: string, allowNonEssentialEmails: boolean, callback: (result: number) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(allowNonEssentialEmails);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (allowNonEssentialEmails != null) {
                let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
                Debug.AssertValid(accessTokenDetails);
                Debug.AssertValidOrNull(accessTokenDetails.user);
                if (accessTokenDetails.user != null) {
                    accessTokenDetails.user.AllowNonEssentialEmails = allowNonEssentialEmails;
                    if (allowNonEssentialEmails) {
                        accessTokenDetails.user.ANEEOnTimestamp = new Date();
                    } else {
                        accessTokenDetails.user.ANEEOffTimestamp = new Date();
                    }
                    callback(APIHelper.RESULT_204_NO_CONTENT);
                } else {
                    callback(APIHelper.RESULT_401_UNAUTHORIZED);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Set the user's 'allow non-essential emails' flag.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserLimits
     */
    public static setUserLimits(accessToken: string,
                                maxDailySpendingAmount: number,
                                maxTimeLoggedIn: number,
                                excludeUntil: Date,
                                callback: (result: number) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessToken);
        Debug.AssertValidOrNull(maxDailySpendingAmount);
        Debug.AssertValidOrNull(maxTimeLoggedIn);
        Debug.AssertValidOrNull(excludeUntil);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            let accessTokenDetails = IdentityServiceAPI.GetUserFromAccessToken_(accessToken);
            Debug.AssertValid(accessTokenDetails);
            Debug.AssertValidOrNull(accessTokenDetails.user);
            if (accessTokenDetails.user != null) {
                let oneWeekHence = new Date();
                oneWeekHence.setTime(oneWeekHence.getTime() + (7 * 24 * 60 * 60 * 1000));
                if (maxDailySpendingAmount !== accessTokenDetails.user.MaxDailySpendingAmount) {
                    if ((accessTokenDetails.user.MaxDailySpendingAmount == null) || (maxDailySpendingAmount < accessTokenDetails.user.MaxDailySpendingAmount)) {
                        accessTokenDetails.user.MaxDailySpendingAmount = maxDailySpendingAmount;
                    } else {
                        accessTokenDetails.user.NewMaxDailySpendingAmount = maxDailySpendingAmount;
                        accessTokenDetails.user.NewMaxDailySpendingAmountTime = oneWeekHence;
                    }
                }
                if (maxTimeLoggedIn !== accessTokenDetails.user.MaxTimeLoggedIn) {
                    if ((accessTokenDetails.user.MaxTimeLoggedIn == null) || (maxTimeLoggedIn < accessTokenDetails.user.MaxTimeLoggedIn)) {
                        accessTokenDetails.user.MaxTimeLoggedIn = maxTimeLoggedIn;
                    } else {
                        accessTokenDetails.user.NewMaxTimeLoggedIn = maxTimeLoggedIn;
                        accessTokenDetails.user.NewMaxTimeLoggedInTime = oneWeekHence;
                    }
                }
                if (excludeUntil !== accessTokenDetails.user.ExcludeUntil) {
                    if ((accessTokenDetails.user.ExcludeUntil == null) || (excludeUntil > accessTokenDetails.user.ExcludeUntil)) {
                        accessTokenDetails.user.ExcludeUntil = excludeUntil;
                    } else {
                        accessTokenDetails.user.NewExcludeUntil = excludeUntil;
                        accessTokenDetails.user.NewExcludeUntilTime = oneWeekHence;
                    }
                }
                callback(APIHelper.RESULT_204_NO_CONTENT);
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Initiates the reset (forgot) password flow.
     * Email a link to the user for them to reset their password.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-ResetUserPassword
     */
    public static resetUserPassword(emailAddress: string, callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!!emailAddress) {
                let user = IdentityServiceAPI.getUserByEmailInternal(emailAddress);
                Debug.AssertValidOrNull(user);
                if (user != null) {
                    if (user.EmailAddressVerified || !IdentityServiceAPI.FailForgotPasswordIfEmailNotVerified) {
                        IdentityServiceAPI.CreateResetPasswordLink(user.ID, emailAddress);
                        // Real service would send email with URL containing the link ID.
                        callback(APIHelper.RESULT_204_NO_CONTENT);
                    } else {
                        callback(APIHelper.RESULT_403_FORBIDDEN);
                    }
                } else {
                    callback(APIHelper.RESULT_403_FORBIDDEN);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Sets the user's password.
     * User should not be logged in (i.e. forgot password flow) and the reset password link ID and email address are required.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-SetUserPasswordafterReset
     */
    public static setUserPasswordAfterReset(resetPasswordLinkID: string,
                                            emailAddress: string,
                                            newPassword: string,
                                            callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(resetPasswordLinkID);//??++CHECK THIS - CAN APP EVER PASS IN NULL/EMPTY?
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValidOrNull(newPassword);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!!resetPasswordLinkID && !!emailAddress && PasswordHelper.isValidPassword(newPassword)) {
                if (IdentityServiceAPI.UseLink(resetPasswordLinkID, IdentityServiceAPI.LINK_TYPE_RESET_PASSWORD, linkInfo => (linkInfo.emailAddress === emailAddress))) {
                    let user = IdentityServiceAPI.getUserByEmailInternal(emailAddress);
                    Debug.AssertValidOrNull(user);
                    if (user != null) {
                        user.PasswordHash = PasswordHelper.getPasswordHash(newPassword);
                        IdentityServiceAPI.RevokeUserAccessTokens(user.ID);
                        callback(APIHelper.RESULT_204_NO_CONTENT);
                    } else {
                        callback(APIHelper.RESULT_401_UNAUTHORIZED);
                    }
                } else {
                    callback(APIHelper.RESULT_401_UNAUTHORIZED);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Resend the 'verify an email address' email.
     * Caller must validate arguments.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-ResendEmailVerification
     */
    public static resendEmailVerification(emailAddress: string, callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!!emailAddress) {
                let user = IdentityServiceAPI.getUserByEmailInternal(emailAddress);
                Debug.AssertValidOrNull(user);
                if (user != null) {
                    if (user.EmailAddressVerified) {
                        callback(APIHelper.RESULT_401_UNAUTHORIZED);
                    } else {
                        IdentityServiceAPI.CreateVerifyEmailLink(user.ID, emailAddress);
                        // Real service would send email with URL containing the link ID.
                        console.log(`Verification email sent to ${emailAddress} (after account creation)`);
                        callback(APIHelper.RESULT_204_NO_CONTENT);
                    }
                } else {
                    user = DummyUsers.find(u => (u.NewEmailAddress === emailAddress));
                    Debug.AssertValidOrNull(user);
                    if (user != null) {
                        IdentityServiceAPI.CreateVerifyEmailLink(user.ID, emailAddress);
                        // Real service would send email with URL containing the link ID.
                        console.log(`Verification email sent to ${emailAddress} (after email changed)`);
                        callback(APIHelper.RESULT_204_NO_CONTENT);
                    } else {
                        callback(APIHelper.RESULT_401_UNAUTHORIZED);
                    }
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Verify an email address.
     * Caller must validate arguments.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-VerifyEmail(newa/c,changeemail)
     */
    public static verifyEmail(emailAddress: string, verifyEmailLinkID: string, callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValidOrNull(verifyEmailLinkID);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!!emailAddress && !!verifyEmailLinkID) {
                if (IdentityServiceAPI.UseLink(verifyEmailLinkID, IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress))) {
                    let user = IdentityServiceAPI.getUserByEmailInternal(emailAddress);
                    Debug.AssertValidOrNull(user);
                    if (user != null) {
                        if (!user.EmailAddressVerified) {
                            user.EmailAddressVerified = true;
                            callback(APIHelper.RESULT_204_NO_CONTENT);
                        } else {
                            callback(APIHelper.RESULT_401_UNAUTHORIZED);
                        }
                    } else {
                        user = DummyUsers.find(u => (u.NewEmailAddress === emailAddress));
                        Debug.AssertValidOrNull(user);
                        if (user != null) {
                            user.EmailAddress = user.NewEmailAddress;
                            user.NewEmailAddress = null;
                            user.EmailAddressVerified = true;
                            callback(APIHelper.RESULT_204_NO_CONTENT);
                        } else {
                            callback(APIHelper.RESULT_401_UNAUTHORIZED);
                        }
                    }
                } else {
                    callback(APIHelper.RESULT_401_UNAUTHORIZED);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Confirm a new user's details.
     * Caller must validate arguments.
     * 24/05/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/145784833/6.3.+User+API+Specification#id-6.3.UserAPISpecification-VerifyEmailw/name/pwd
     */
    public static verifyEmailWithDetails(emailAddress: string,
                                         verifyEmailLinkID: string,
                                         givenName: string,
                                         familyName: string,
                                         password: string,
                                         dateOfBirth: string,
                                         address1: string,
                                         address2: string,
                                         address3: string,
                                         address4: string,
                                         city: string,
                                         region: string,
                                         country: string,
                                         postalCode: string,
                                         allowNonEssentialEmails: boolean,
                                         callback: (result: number) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(emailAddress);
        Debug.AssertValidOrNull(verifyEmailLinkID);
        Debug.AssertValidOrNull(givenName);
        Debug.AssertValidOrNull(familyName);
        Debug.AssertValidOrNull(password);
        Debug.AssertValidOrNull(dateOfBirth);
        Debug.AssertValidOrNull(address1);
        Debug.AssertValidOrNull(address2);
        Debug.AssertValidOrNull(address3);
        Debug.AssertValidOrNull(address4);
        Debug.AssertValidOrNull(city);
        Debug.AssertValidOrNull(region);
        Debug.AssertValidOrNull(country);
        Debug.AssertValidOrNull(postalCode);
        Debug.AssertValidOrNull(allowNonEssentialEmails);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!!emailAddress &&
                !!verifyEmailLinkID &&
                !!givenName &&
                !!familyName &&
                PasswordHelper.isValidPassword(password) &&
                DateHelper.isValidAPIDate(dateOfBirth) &&
                !!address1 &&
                CountryHelper.isValidCountryCode(country) &&
                !!postalCode) {
                if (IdentityServiceAPI.UseLink(verifyEmailLinkID, IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress))) {
                    let user = IdentityServiceAPI.getUserByEmailInternal(emailAddress);
                    Debug.AssertValidOrNull(user);
                    if (user != null) {
                        let fullName = givenName + ' ' + familyName;
                        user.GivenName = givenName;
                        user.FamilyName = familyName;
                        user.FullName = fullName;
                        user.PasswordHash = PasswordHelper.getPasswordHash(password);
                        user.DateOfBirth = DateHelper.APIDateToDateOnly(dateOfBirth);
                        user.Address1 = address1;
                        user.Address2 = address2;
                        user.Address3 = address3;
                        user.Address4 = address4;
                        user.City = city;
                        user.Region = region;
                        user.Country = country;
                        user.PostalCode = postalCode;
                        user.EmailAddressVerified = true;
                        user.AllowNonEssentialEmails = !!allowNonEssentialEmails;
                        user.ANEEOnTimestamp = allowNonEssentialEmails ? new Date() : null;
                        user.ANEEOffTimestamp = allowNonEssentialEmails ? null : new Date();
                        callback(APIHelper.RESULT_204_NO_CONTENT);
                    } else {
                        callback(APIHelper.RESULT_401_UNAUTHORIZED);
                    }
                } else {
                    callback(APIHelper.RESULT_401_UNAUTHORIZED);
                }
            } else {
                callback(APIHelper.RESULT_400_BAD_REQUEST);
            }
        });
    }

    /**
     * Access tokens.
     */

    /**
     * The access tokens.
     */
    private static AccessTokens: ObjectMap<AccessToken> = new ObjectMap<AccessToken>();

    /**
     * Create an access token from a user.
     */
    private static CreateAccessToken(user: UserRecord): string {
        Debug.Tested();
        Debug.AssertValid(user);

        let accessToken = new AccessToken(user);
        Debug.AssertString(accessToken.ID);
        IdentityServiceAPI.AccessTokens.set(accessToken.ID, accessToken);
        return accessToken.ID;
    }

    /**
     * Get the user details from an access token.
     * Either returns a valid user details object or throws an error.
     */
    public static GetUserFromAccessToken_(accessTokenID: string): { user: UserRecord, extended: boolean } {
        Debug.Tested();
        Debug.AssertValidOrNull(accessTokenID);

        let retVal: { user: UserRecord, extended: boolean } = { user: null, extended: false };
        if (!!accessTokenID) {
            let accessToken = IdentityServiceAPI.AccessTokens.get(accessTokenID);
            Debug.AssertValidOrNull(accessToken);
            if (accessToken != null) {
                Debug.AssertString(accessToken.UserID);
                Debug.AssertValid(accessToken.Expires);
                if (accessToken.Expires > new Date()) {
                    retVal.user = DummyUsers.find(u => (u.ID === accessToken.UserID));
                    Debug.AssertValidOrNull(retVal.user);
                    if (retVal.user != null) {
                        retVal.extended = accessToken.setupExpiry();
                    }
                }
            }
        }
        return retVal;
    }

    /**
     * Revoke the specified access token.
     */
    private static RevokeAccessToken(accessTokenID: string) {
        Debug.Tested();
        Debug.AssertValidOrNull(accessTokenID);

        if (!!accessTokenID) {
            IdentityServiceAPI.AccessTokens.set(accessTokenID, null);
        }
    }

    /**
     * Revoke any access tokens for the specified user.
     */
    private static RevokeUserAccessTokens(userID: string) {
        Debug.Tested();
        Debug.AssertString(userID);

        IdentityServiceAPI.AccessTokens.forEach(accessToken => {
            Debug.AssertValidOrNull(accessToken);
            if (accessToken != null) {
                Debug.AssertString(accessToken.ID);
                if (accessToken.UserID === userID) {
                    IdentityServiceAPI.RevokeAccessToken(accessToken.ID);
                }
            }
        });
    }

    /**
     * Links.
     */
    public static readonly LINK_TYPE_RESET_PASSWORD = 'reset-password';
    public static readonly LINK_TYPE_VERIFY_EMAIL = 'verify-email';

    /**
     * The links.
     */
    private static Links: ObjectMap<Link> = new ObjectMap<Link>();

    /**
     * Create an access token from a user.
     */
    private static CreateLink(type: string, info: any): string {
        Debug.Tested();
        Debug.AssertString(type);
        Debug.AssertValidOrNull(info);

        let link = new Link(type, info);
        Debug.AssertString(link.ID);
        IdentityServiceAPI.Links.set(link.ID, link);
        return link.ID;
    }

    /**
     * Revoke all links of the specified type that pass the supplied filter.
     */
    private static RevokeLinks(type: string, filter: (linkInfo: any) => boolean) {
        Debug.Tested();
        Debug.AssertString(type);
        Debug.AssertValid(filter);

        IdentityServiceAPI.Links.forEach(link => {
            Debug.AssertValidOrNull(link);
            if (link != null) {
                Debug.AssertValidOrNull(link.Info);
                if (link.Type === type) {
                    if (filter(link.Info)) {
                        IdentityServiceAPI.Links.set(link.ID, null);
                    }
                }
            }
        });
    }
    
    /**
     * Use a link.
     * Returns true if it is found, not already used and not expired.
     */
    private static UseLink(linkID: string, type: string, filter: (linkInfo: any) => boolean): boolean {
        Debug.Tested();
        Debug.AssertValidOrNull(linkID);
        Debug.AssertString(type);
        Debug.AssertValid(filter);

        let retVal = false;
        if (!!linkID) {
            let link = IdentityServiceAPI.Links.get(linkID);
            Debug.AssertValidOrNull(link);
            if (link != null) {
                if (link.Type === type) {
                    if (!link.Used) {
                        if (link.Expires > new Date()) {
                            if (filter(link.Info)) {
                                link.Used = true;
                                retVal = true;
                            }
                        }
                    }
                }
            }
        }
        return retVal;
    }

    /**
     * Internal hel method to get a link ID.
     */
    public static GetLinkIDInternal(type: string, filter: (linkInfo: any) => boolean): string {
        Debug.Tested();
        Debug.AssertString(type);
        Debug.AssertValid(filter);

        let retVal = null;
        IdentityServiceAPI.Links.forEach(link => {
            Debug.AssertValidOrNull(link);
            if ((retVal == null) && (link != null)) {
                Debug.AssertValidOrNull(link.Info);
                if (!link.Used && (link.Type === type) && (link.Expires > new Date())) {
                    if (filter(link.Info)) {
                        retVal = link.ID;
                    }
                }
            }
        });
        return retVal;
    }

    /**
     * Create a verify email link. 
     */
    private static CreateVerifyEmailLink(userID: string, emailAddress: string, revokeExisting: boolean = true) {
        Debug.Tested();
        Debug.AssertString(userID);
        Debug.AssertString(emailAddress);

        if (revokeExisting) {
            IdentityServiceAPI.RevokeLinks(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.userID === userID));
        }
        return IdentityServiceAPI.CreateLink(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, { userID, emailAddress });
    }
    
    /**
     * Create a reset password link. 
     */
    private static CreateResetPasswordLink(userID: string, emailAddress: string, revokeExisting: boolean = true) {
        Debug.Tested();
        Debug.AssertString(userID);
        Debug.AssertString(emailAddress);

        if (revokeExisting) {
            IdentityServiceAPI.RevokeLinks(IdentityServiceAPI.LINK_TYPE_RESET_PASSWORD, linkInfo => (linkInfo.userID === userID));
        }
        return IdentityServiceAPI.CreateLink(IdentityServiceAPI.LINK_TYPE_RESET_PASSWORD, { userID, emailAddress });
    }
    
    /**
     * Internal test methods.
     */

    /**
     * Internal method to find user from an ID.
     * There is no endpoint for this.
     * This is used to test the mock identity/ticket service.
     */
    public static getUserByIDInternal(userID: string): UserRecord {
        Debug.UntestedMock();
        Debug.AssertString(userID);

        let user = DummyUsers.find(u => (u.ID === userID));
        Debug.AssertValidOrNull(user);
        return user;
    }

    /**
     * Internal method to find user from an email.
     * There is no endpoint for this.
     * This is used to test the mock identity/ticket service.
     */
    public static getUserByEmailInternal(emailAddress: string): UserRecord {
        Debug.TestedMock();
        Debug.AssertString(emailAddress);

        let user = DummyUsers.find(u => (u.EmailAddress === emailAddress));
        Debug.AssertValidOrNull(user);
        return user;
    }

    /**
     * Internal method to create a user.
     * There is no endpoint for this.
     * This is used to test the mock identity/ticket service.
     */
    public static createUserInternal(emailAddress: string): UserRecord {
        Debug.TestedMock();
        Debug.AssertString(emailAddress);

        let user: UserRecord = <UserRecord>{
            ID: 'account_' + RandomHelper.getRandomNumber(),
            EmailAddress: emailAddress,
            EmailAddressVerified: false
        };
        DummyUsers.push(user);
        IdentityServiceAPI.CreateVerifyEmailLink(user.ID, emailAddress, false);
        // Real service would send email with URL containing the link ID.
        return user;
    }

}   // IdentityServiceAPI
