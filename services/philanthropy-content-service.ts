import { Debug } from '../helpers/debug';
import { WebApp } from '../components/web-app';
import { ContentService } from './content-service';
import { Cause, Charity, Ambassador } from '../data-models/philanthropy-model';
import { CallbackWaiter } from '../helpers/callback-waiter';
import { ObjectMap } from '../helpers/object-map';
import { ErrorHelper } from '../helpers/error-helper';
import { TextContentService } from './text-content-service';

/**
 * Class providing access to causes content service.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432608/6.4.+Philanthropy+CMS+Data+Format
 */
export class PhilanthropyContentService extends ContentService {

    /**
     * The cached causes video URL.
     */
    private static CausesVideoURL: string;

    /**
     * The cached causes.
     */
    private static Causes: ObjectMap<Cause>;  // <causes> in http://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47284333/2.2.3.+Global+Variables

    /**
     * The callback waiter.
     */
    private static CausesCallbackWaiter: CallbackWaiter;

    /**
     * The cached charities.
     */
    private static Charities: ObjectMap<Charity>;

    /**
     * The callback waiter.
     */
    private static CharitiesCallbackWaiter: CallbackWaiter;

    /**
     * The cached ambassadors.
     */
    private static Ambassadors: ObjectMap<Ambassador>;

    /**
     * The callback waiter.
     */
    private static AmbassadorsCallbackWaiter: CallbackWaiter;

    /**
     * Clear the cache.
     */
    public static clearCache() {
        Debug.Tested();

        PhilanthropyContentService.CausesVideoURL = null;
        PhilanthropyContentService.Causes = null;
        PhilanthropyContentService.Charities = null;
        PhilanthropyContentService.Ambassadors = null;
    }

    /**
     * Get the causes available to show.
     */
    public static getCausesInfo(callback: (causesInfo: { causesVideoURL: string, causes: ObjectMap<Cause> }) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        if (PhilanthropyContentService.Causes == null) {
            if (PhilanthropyContentService.CausesCallbackWaiter == null) {
                PhilanthropyContentService.CausesCallbackWaiter = new CallbackWaiter(callback);
                WebApp.getInstance().getLanguageCode((languageCode) => {
                    Debug.AssertString(languageCode);
                    ContentService.getContent(`/philanthropy/causes-${languageCode}.json`, (error, content) => {
                        Debug.AssertErrorOrObject(error, content);
                        ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_CAUSES);
                        PhilanthropyContentService.CausesVideoURL = (content != null) ? content.videoUrl : null;
                        PhilanthropyContentService.Causes = new ObjectMap((content != null) ? content.causes.map(cause => [cause.id, cause]) : null);
                        PhilanthropyContentService.CausesCallbackWaiter.CallCallbacksNoError({ causesVideoURL: PhilanthropyContentService.CausesVideoURL, causes: PhilanthropyContentService.Causes });
                        PhilanthropyContentService.CausesCallbackWaiter = null;
                    });
                });
            } else {
                PhilanthropyContentService.CausesCallbackWaiter.AddCallback(callback);
            }
        } else {
            Debug.AssertString(PhilanthropyContentService.CausesVideoURL);
            callback({ causesVideoURL: PhilanthropyContentService.CausesVideoURL, causes: PhilanthropyContentService.Causes });
        }
    }

    /**
     * Get the charities available to show.
     */
    public static getCharities(callback: (charities: ObjectMap<Charity>) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        if (PhilanthropyContentService.Charities == null) {
            if (PhilanthropyContentService.CharitiesCallbackWaiter == null) {
                PhilanthropyContentService.CharitiesCallbackWaiter = new CallbackWaiter(callback);
                WebApp.getInstance().getLanguageCode((languageCode) => {
                    Debug.AssertString(languageCode);
                    ContentService.getContent(`/philanthropy/charities-${languageCode}.json`, (error, content) => {
                        Debug.AssertErrorOrObject(error, content);
                        ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_CHARITIES);
                        PhilanthropyContentService.Charities = new ObjectMap((content != null) ? content.charities.map(charity => [charity.id, charity]) : null);
                        PhilanthropyContentService.CharitiesCallbackWaiter.CallCallbacksNoError(PhilanthropyContentService.Charities);
                        PhilanthropyContentService.CharitiesCallbackWaiter = null;
                    });
                });
            } else {
                PhilanthropyContentService.CharitiesCallbackWaiter.AddCallback(callback);
            }
        } else {
            callback(PhilanthropyContentService.Charities);
        }
    }

    /**
     * Get the ambassadors available to show.
     */
    public static getAmbassadors(callback: (ambassadors: ObjectMap<Ambassador>) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        if (PhilanthropyContentService.Ambassadors == null) {
            if (PhilanthropyContentService.AmbassadorsCallbackWaiter == null) {
                PhilanthropyContentService.AmbassadorsCallbackWaiter = new CallbackWaiter(callback);
                WebApp.getInstance().getLanguageCode((languageCode) => {
                    Debug.AssertString(languageCode);
                    ContentService.getContent(`/philanthropy/ambassadors-${languageCode}.json`, (error, content) => {
                        Debug.AssertErrorOrObject(error, content);
                        ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_AMBASSADORS);
                        PhilanthropyContentService.Ambassadors = new ObjectMap((content != null) ? content.ambassadors.map(ambassador => [ambassador.id, ambassador]) : null);
                        PhilanthropyContentService.AmbassadorsCallbackWaiter.CallCallbacksNoError(PhilanthropyContentService.Ambassadors);
                        PhilanthropyContentService.AmbassadorsCallbackWaiter = null;
                    });
                });
            } else {
                PhilanthropyContentService.AmbassadorsCallbackWaiter.AddCallback(callback);
            }
        } else {
            callback(PhilanthropyContentService.Ambassadors);
        }
    }

}   // PhilanthropyContentService

