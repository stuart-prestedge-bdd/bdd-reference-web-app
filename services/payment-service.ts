import { Debug } from '../helpers/debug';
import { ObjectMap } from '../helpers/object-map';

/**
 * Dummy user payment details.
 */
var DummyUserPaymentDetails = new ObjectMap<any>([ [ 'user_1', { setup: true, requireConfirmation: false } ] ]);

/**
 * Class providing access to the payment service.
 */
export class PaymentService {

    /**
     * Are payment details setup for a user?
     */
    public static paymentDetailsSetup(userID: string, callback: (error: Error, setup: boolean) => void) {
        Debug.UntestedMock();//??++TO WRITE
        Debug.AssertString(userID);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            let userPaymentDetails = DummyUserPaymentDetails.get(userID);
            Debug.AssertValidOrNull(userPaymentDetails);
            let setup = false;
            if (userPaymentDetails != null) {
                setup = userPaymentDetails.setup;
            }
            callback(null, userPaymentDetails);
        });
    }

    /**
     * Get payment details for a user.
     */
    public static getUserPaymentDetails(userID: string, callback: (error: Error, details: any) => void) {
        Debug.TestedMock();//??++TO WRITE
        Debug.AssertString(userID);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            let userPaymentDetails = DummyUserPaymentDetails.get(userID);
            Debug.AssertValidOrNull(userPaymentDetails);
            callback(null, (userPaymentDetails || { setup: false }));
        });
    }

    /**
     * Set payment details for a user.
     */
    public static setupUserPaymentDetails(userID: string, redirectURL: string, callback: (error: Error, htmlContent: string) => void) {
        Debug.UntestedMock();//??++TO WRITE
        Debug.AssertString(userID);
        Debug.AssertString(redirectURL);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            //??++HOW DO WE SIMULATE THIS TO SETUP PAYMENT DETAILS?
            callback(null, redirectURL);
        });
    }

}   // PaymentService
