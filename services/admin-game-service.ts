import { Debug } from '../helpers/debug';
import { ObjectMap } from '../helpers/object-map';
import { DateHelper } from '../helpers/date-helper';
import { DateOnly } from '../data-models/identity-model';
import { RandomHelper } from '../helpers/random-helper';
import { APIHelper } from '../helpers/api-helper';

/**
 * Data structures.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/24543848/5.2.+Data+Structure
 */

/**
 * Class representing a game (from administrator's perspective).
 */
export class AdminGame {
    public id: string;
    public name: string;
    public openDate: DateOnly;
    public closeDate: DateOnly;
    public ticketCount: number;
    public ticketPrice: number;
    public locked: boolean;
    public published: boolean;
    public frozen: boolean;
    public ticketServiceURL: string;
    public donatedToCharity: number;
}   // AdminGame

/**
 * Class representing a draw (from administrator's perspective).
 */
export class AdminDraw {
    public id: string;
    public gameID: string;
    public drawDate: Date;
    public drawnDate: Date;
    public amount: number;
    public amountIsUpTo: boolean;
    public prizeID: string;
    public winningNumber: number;
    public winningUserID: string;
    public winningTicketID: string;
    public autoDraw: boolean;
    public autoDrawn: boolean;
}   // AdminDraw

/**
 * The change type.
 */
export enum AdminGameAuditChangeType {
    create = 1,
    update = 2,
    delete = 3,
}   // AdminGameAuditChangeType

/**
 * The change type.
 */
export enum AdminGameAuditDataType {
    game = 1,
    draw = 2,
    prize = 3,
}   // AdminGameAuditDataType

/**
 * Class representing a game service change audit.
 */
export class AdminGameAudit {
    public id: string;
    public timestamp: Date;
    public changeType: AdminGameAuditChangeType;
    public dataType: AdminGameAuditDataType;
    public targetID: string;
    public userID: string;
    public targetCopy: any;

}   // AdminGameAudit

/**
 * Permissions
 */
const IS_ADMIN = "IS_ADMIN";
const CAN_CREATE_GAME = "create-game";
const CAN_UPDATE_GAME = "update-game";
const CAN_DELETE_GAME = "delete-game";
const CAN_LOCK_GAME = "lock-game";
const CAN_UNLOCK_GAME = "unlock-game";
const CAN_PUBLISH_GAME = "publish-game";
const CAN_FREEZE_GAME = "freeze-game";
const CAN_SET_GAME_CLOSE_DATE = "set-game-close-date";
const CAN_SET_GAME_DONATED_TO_CHARITY = "set-game-donated-to-charity";
const CAN_CREATE_DRAW = "create-draw";
const CAN_UPDATE_DRAW = "update-draw";
const CAN_DELETE_DRAW = "delete-draw";
const CAN_SET_DRAW_DATE = "set-draw-date";
const CAN_SET_DRAW_AMOUNT = "set-draw-amount";
const CAN_SET_DRAW_AUTO_DRAW = "set-draw-auto-draw";
const CAN_SET_DRAW_WINNING_NUMBER = "set-draw-winning-number";
const CAN_CLEAR_DRAW_WINNING_NUMBER = "clear-draw-winning-number";

/**
 * Class providing access to the admin game service.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/24576107/5.1.+Requirements
 */
export class AdminGameService {

    /**
     * Possible errors.
     */
    public static readonly ERROR_INVALID_ARGS = 'INVALID_ARGS';
    public static readonly ERROR_UNAUTHORIZED = 'UNAUTHORIZED';
    public static readonly ERROR_NOT_FOUND = 'NOT_FOUND';

    /**
     * Get an error from an API result.
     */
    private static errorFromResult(result: number): Error {
        Debug.Untested();

        let error = null;
        switch (result) {
            case APIHelper.RESULT_400_BAD_REQUEST: {
                error = new Error(AdminGameService.ERROR_INVALID_ARGS);
                break;
            }
            case APIHelper.RESULT_401_UNAUTHORIZED: {
                error = new Error(AdminGameService.ERROR_UNAUTHORIZED);
                break;
            }
            case APIHelper.RESULT_404_NOT_FOUND: {
                error = new Error(AdminGameService.ERROR_NOT_FOUND);
                break;
            }
            case APIHelper.RESULT_200_OK:
            case APIHelper.RESULT_201_CREATED:
            case APIHelper.RESULT_204_NO_CONTENT:
                break;
            default:
                Debug.Unreachable();
                break;
        }
        return error;
    }

    /**
     * Games
     */

    /**
     * Get games.
     * GET /api/games
     */
    public static GetGames(accessToken, callback: (error: Error, games: AdminGame[]) => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        AdminGameServiceAPI.GetGames(accessToken, (result, responseBody) => {
            Debug.AssertValid(responseBody);
            let error = AdminGameService.errorFromResult(result);
            Debug.AssertValidOrNull(error);
            let games: AdminGame[] = null;
            if ((error == null) && (responseBody != null)) {
                games = [];
                responseBody.forEach(element => {
                    games.push(AdminGameServiceAPI.APIGameToGame(element));
                });
            }
            callback(error, games);
        });
    }

    /**
     * Get game.
     * GET /api/games/<GAME_ID>
     */
    public static GetGame(gameId: string, callback: (error: Error, game: AdminGame) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        if (!!gameId) {
            AdminGameServiceAPI.GetGame(gameId, (result, responseBody) => {
                Debug.AssertValid(responseBody);
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error, AdminGameServiceAPI.APIGameToGame(responseBody));
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS), null);
        }
    }

    /**
     * Create game.
     * POST /api/games
     */
    public static CreateGame(name: string,
                             openDate: DateOnly,
                             closeDate: DateOnly,
                             ticketPrice: number,
                             callback: (error: Error, game: AdminGame) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(name);
        Debug.AssertValidOrNull(openDate);
        Debug.AssertValidOrNull(closeDate);
        Debug.AssertValidOrNull(ticketPrice);
        Debug.AssertValid(callback);

        if (!!name &&
            !!openDate &&
            !!ticketPrice) {
            let requestBody = {
                name,
                openDate: DateHelper.DateOnlyToAPIDate(openDate),
                closeDate: DateHelper.DateOnlyToAPIDate(closeDate),
                ticketCount: 1000000000,
                ticketPrice,
                ticketServiceURL: null
            };
            AdminGameServiceAPI.CreateGame(requestBody, (result, responseBody) => {
                Debug.AssertValid(responseBody);
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error, AdminGameServiceAPI.APIGameToGame(responseBody));
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS), null);
        }
    }

    /**
     * Update game.
     * PUT /api/games/<GAME_ID>
     */
    public static UpdateGame(game: AdminGame,
                             callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(game);
        Debug.AssertString(game.id);
        Debug.AssertValid(callback);

        AdminGameService.UpdateGameEx(game.id, game.name, game.openDate, game.closeDate, game.ticketPrice, game.donatedToCharity, callback);
    }

    /**
     * Update game.
     * PUT /api/games/<GAME_ID>
     */
    public static UpdateGameEx(gameId: string,
                               name: string,
                               openDate: DateOnly,
                               closeDate: DateOnly,
                               ticketPrice: number,
                               donatedToCharity: number,
                               callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValidOrNull(name);
        Debug.AssertValidOrNull(openDate);
        Debug.AssertValidOrNull(closeDate);
        Debug.AssertValidOrNull(ticketPrice);
        Debug.AssertValid(callback);

        if (!!gameId &&
            !!name &&
            !!openDate &&
            !!ticketPrice) {
            let requestBody = {
                name,
                openDate: DateHelper.DateOnlyToAPIDate(openDate),
                closeDate: DateHelper.DateOnlyToAPIDate(closeDate),
                ticketCount: 1000000000,
                ticketPrice,
                ticketServiceURL: null,
                donatedToCharity
            };
            AdminGameServiceAPI.UpdateGame(gameId, requestBody, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Delete game.
     * DELETE /api/games/<GAME_ID>
     */
    public static DeleteGame(game: AdminGame,
                             callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(game);
        Debug.AssertString(game.id);
        Debug.AssertValid(callback);

        AdminGameService.DeleteGameEx(game.id, callback);
    }

    /**
     * Delete game.
     * DELETE /api/games/<GAME_ID>
     */
    public static DeleteGameEx(gameId: string,
                               callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        if (!!gameId) {
            AdminGameServiceAPI.DeleteGame(gameId, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Lock game.
     * PUT /api/games/lock/<GAME_ID>
     */
    public static LockGame(game: AdminGame,
                             callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(game);
        Debug.AssertString(game.id);
        Debug.AssertValid(callback);

        AdminGameService.LockGameEx(game.id, callback);
    }

    /**
     * Lock game.
     * PUT /api/games/lock/<GAME_ID>
     */
    public static LockGameEx(gameId: string,
                             callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        if (!!gameId) {
            AdminGameServiceAPI.LockGame(gameId, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Unlock game.
     * PUT /api/games/unlock/<GAME_ID>
     */
    public static UnlockGame(game: AdminGame,
                             callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(game);
        Debug.AssertString(game.id);
        Debug.AssertValid(callback);

        AdminGameService.UnlockGameEx(game.id, callback);
    }

    /**
     * Unlock game.
     * PUT /api/games/unlock/<GAME_ID>
     */
    public static UnlockGameEx(gameId: string,
                               callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        if (!!gameId) {
            AdminGameServiceAPI.UnlockGame(gameId, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Publish game.
     * PUT /api/games/publish/<GAME_ID>
     */
    public static PublishGame(game: AdminGame,
                              callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(game);
        Debug.AssertString(game.id);
        Debug.AssertValid(callback);

        AdminGameService.PublishGameEx(game.id, callback);
    }

    /**
     * Publish game.
     * PUT /api/games/publish/<GAME_ID>
     */
    public static PublishGameEx(gameId: string,
                                callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        if (!!gameId) {
            AdminGameServiceAPI.PublishGame(gameId, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Set game frozen.
     * PUT /api/games/frozen/<GAME_ID>
     */
    public static SetGameFrozen(game: AdminGame,
                                callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(game);
        Debug.AssertString(game.id);
        Debug.AssertValid(callback);

        AdminGameService.SetGameFrozenEx(game.id, game.frozen, callback);
    }

    /**
     * Set game frozen.
     * PUT /api/games/frozen/<GAME_ID>
     */
    public static SetGameFrozenEx(gameId: string,
                                  frozen: boolean,
                                  callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        if (!!gameId) {
            let requestBody = {
                frozen
            };
            AdminGameServiceAPI.SetGameFrozen(gameId, requestBody, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Set game frozen.
     * PUT /api/games/frozen/<GAME_ID>
     */
    public static SetGameCloseDate(game: AdminGame,
                                   callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(game);
        Debug.AssertString(game.id);
        Debug.AssertValid(callback);

        AdminGameService.SetGameCloseDateEx(game.id, game.closeDate, callback);
    }

    /**
     * Set game frozen.
     * PUT /api/games/frozen/<GAME_ID>
     */
    public static SetGameCloseDateEx(gameId: string,
                                     closeDate: DateOnly,
                                     callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValidOrNull(closeDate);
        Debug.AssertValid(callback);

        if (!!gameId) {
            let requestBody = {
                closeDate: DateHelper.DateOnlyToAPIDate(closeDate)
            };
            AdminGameServiceAPI.SetGameCloseDate(gameId, requestBody, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Set game donated to charity.
     * PUT /api/games/donated-to-charity/<GAME_ID>
     */
    public static SetGameDonatedToCharity(game: AdminGame,
                                          callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(game);
        Debug.AssertString(game.id);
        Debug.AssertValid(callback);

        AdminGameService.SetGameDonatedToCharityEx(game.id, game.donatedToCharity, callback);
    }

    /**
     * Set game donated to charity.
     * PUT /api/games/donated-to-charity/<GAME_ID>
     */
    public static SetGameDonatedToCharityEx(gameId: string,
                                            donatedToCharity: number,
                                            callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        if (!!gameId) {
            let requestBody = {
                donatedToCharity
            };
            AdminGameServiceAPI.SetGameDonatedToCharity(gameId, requestBody, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Draws
     */

    /**
     * Get draws.
     * GET /api/draws
     */
    public static GetDraws(gameId: string, callback: (error: Error, draws: AdminDraw[]) => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        if (!!gameId) {
            AdminGameServiceAPI.GetDraws(gameId, (result, responseBody) => {
                Debug.AssertValid(responseBody);
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                let draws: AdminDraw[] = null;
                if ((error == null) && (responseBody != null)) {
                    draws = [];
                    responseBody.forEach(element => {
                        draws.push(AdminGameServiceAPI.APIDrawToDraw(element));
                    });
                }
                callback(error, draws);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS), null);
        }
    }

    /**
     * Get draw.
     * GET /api/draws/<GAME_ID>
     */
    public static GetDraw(gameId: string, drawId: string, callback: (error: Error, draw: AdminDraw) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValid(callback);

        if (!!gameId && !!drawId) {
            AdminGameServiceAPI.GetDraw(gameId, drawId, (result, responseBody) => {
                Debug.AssertValid(responseBody);
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error, AdminGameServiceAPI.APIDrawToDraw(responseBody));
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS), null);
        }
    }

    /**
     * Create draw.
     * POST /api/draws
     */
    public static CreateDraw(gameId: string,
                             drawDate: Date,
                             amount: number,
                             amountIsUpTo: boolean,
                             prizeID: string,
                             autoDraw: boolean,
                             callback: (error: Error, draw: AdminDraw) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValidOrNull(drawDate);
        Debug.AssertValidOrNull(amount);
        Debug.AssertValidOrNull(amountIsUpTo);
        Debug.AssertValidOrNull(prizeID);
        Debug.AssertValidOrNull(autoDraw);
        Debug.AssertValid(callback);

        if (!!gameId &&
            !!amount) {
            let requestBody = {
                gameId,
                drawDate: DateHelper.DateToAPITimestamp(drawDate),
                amount,
                amountIsUpTo: !!amountIsUpTo,
                prizeID,
                autoDraw: !!autoDraw
            };
            AdminGameServiceAPI.CreateDraw(requestBody, (result, responseBody) => {
                Debug.AssertValid(responseBody);
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error, AdminGameServiceAPI.APIDrawToDraw(responseBody));
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS), null);
        }
    }

    /**
     * Update draw.
     * PUT /api/draws/<GAME_ID>
     */
    public static UpdateDraw(draw: AdminDraw,
                             callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(draw);
        Debug.AssertString(draw.id);
        Debug.AssertValid(callback);

        AdminGameService.UpdateDrawEx(draw.id, draw.drawDate, draw.amount, draw.amountIsUpTo, draw.prizeID, draw.autoDraw, callback);
    }

    /**
     * Update draw.
     * PUT /api/draws/<GAME_ID>
     */
    public static UpdateDrawEx(drawId: string,
                               drawDate: Date,
                               amount: number,
                               amountIsUpTo: boolean,
                               prizeID: string,
                               autoDraw: boolean,
                               callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValidOrNull(drawDate);
        Debug.AssertValidOrNull(amount);
        Debug.AssertValidOrNull(amountIsUpTo);
        Debug.AssertValidOrNull(prizeID);
        Debug.AssertValidOrNull(autoDraw);
        Debug.AssertValid(callback);

        if (!!amount) {
            let requestBody = {
                drawDate: DateHelper.DateToAPITimestamp(drawDate),
                amount,
                amountIsUpTo: !!amountIsUpTo,
                prizeID,
                autoDraw: !!autoDraw
            };
            AdminGameServiceAPI.UpdateDraw(drawId, requestBody, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Delete draw.
     * DELETE /api/draws/<GAME_ID>
     */
    public static DeleteDraw(draw: AdminDraw,
                             callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(draw);
        Debug.AssertString(draw.id);
        Debug.AssertString(draw.gameID);
        Debug.AssertValid(callback);

        AdminGameService.DeleteDrawEx(draw.gameID, draw.id, callback);
    }

    /**
     * Delete draw.
     * DELETE /api/draws/<GAME_ID>
     */
    public static DeleteDrawEx(gameId: string,
                               drawId: string,
                               callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValid(callback);

        if (!!drawId) {
            AdminGameServiceAPI.DeleteDraw(gameId, drawId, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Set draw date.
     * PUT /api/draws/draw-date/<DRAW_ID>
     */
    public static SetDrawDate(draw: AdminDraw,
                              callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(draw);
        Debug.AssertString(draw.id);
        Debug.AssertValid(callback);

        AdminGameService.SetDrawDateEx(draw.id, draw.gameID, draw.drawDate, callback);
    }

    /**
     * Set draw date.
     * PUT /api/draws/draw-date/<DRAW_ID>
     */
    public static SetDrawDateEx(drawId: string,
                                gameID: string,
                                drawDate: Date,
                                callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValidOrNull(gameID);
        Debug.AssertValidOrNull(drawDate);
        Debug.AssertValid(callback);

        if (!!drawId && !!gameID && !!drawDate) {
            let requestBody = {
                gameID,
                drawDate
            };
            AdminGameServiceAPI.SetDrawDate(drawId, requestBody, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Set draw amount.
     * PUT /api/draws/amount/<DRAW_ID>
     */
    public static SetDrawAmount(draw: AdminDraw,
                                callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(draw);
        Debug.AssertString(draw.id);
        Debug.AssertValid(callback);

        AdminGameService.SetDrawAmountEx(draw.id, draw.gameID, draw.amount, draw.amountIsUpTo, callback);
    }

    /**
     * Set draw amount.
     * PUT /api/draws/amount/<DRAW_ID>
     */
    public static SetDrawAmountEx(drawId: string,
                                  gameID: string,
                                  amount: number,
                                  amountIsUpTo: boolean,
                                  callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValidOrNull(gameID);
        Debug.AssertValidOrNull(amount);
        Debug.AssertValidOrNull(amountIsUpTo);
        Debug.AssertValid(callback);

        if (!!drawId && !!gameID && !!amount) {
            let requestBody = {
                gameID,
                amount,
                amountIsUpTo: !!amountIsUpTo
            };
            AdminGameServiceAPI.SetDrawAmount(drawId, requestBody, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Set draw auto draw.
     * PUT /api/draws/auto-draw/<DRAW_ID>
     */
    public static SetDrawAutoDraw(draw: AdminDraw,
                                  callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(draw);
        Debug.AssertString(draw.id);
        Debug.AssertValid(callback);

        AdminGameService.SetDrawAutoDrawEx(draw.id, draw.gameID, draw.autoDraw, callback);
    }

    /**
     * Set draw auto draw.
     * PUT /api/draws/auto-draw/<DRAW_ID>
     */
    public static SetDrawAutoDrawEx(drawId: string,
                                    gameID: string,
                                    autoDraw: boolean,
                                    callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValidOrNull(gameID);
        Debug.AssertValidOrNull(autoDraw);
        Debug.AssertValid(callback);

        if (!!drawId && !!gameID) {
            let requestBody = {
                gameID,
                autoDraw: !!autoDraw
            };
            AdminGameServiceAPI.SetDrawAutoDraw(drawId, requestBody, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Set draw winning number.
     * PUT /api/draws/set-winning-number/<DRAW_ID>
     */
    public static SetDrawWinningNumber(draw: AdminDraw,
                                       callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(draw);
        Debug.AssertString(draw.id);
        Debug.AssertValid(callback);

        AdminGameService.SetDrawWinningNumberEx(draw.id, draw.gameID, draw.winningNumber, draw.amount, callback);
    }

    /**
     * Set draw winning number.
     * PUT /api/draws/set-winning-number/<DRAW_ID>
     */
    public static SetDrawWinningNumberEx(drawId: string,
                                         gameID: string,
                                         winningNumber: number,
                                         amount: number,
                                         callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValidOrNull(gameID);
        Debug.AssertValidOrNull(winningNumber);
        Debug.AssertValidOrNull(amount);
        Debug.AssertValid(callback);

        if (!!drawId && !!gameID && !!winningNumber) {
            let requestBody = {
                gameID,
                winningNumber,
                amount
            };
            AdminGameServiceAPI.SetDrawWinningNumber(drawId, requestBody, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

    /**
     * Clear draw winning number.
     * PUT /api/draws/clear-winning-number/<DRAW_ID>
     */
    public static ClearDrawWinningNumber(draw: AdminDraw,
                                         callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValid(draw);
        Debug.AssertString(draw.id);
        Debug.AssertValid(callback);

        AdminGameService.ClearDrawWinningNumberEx(draw.id, draw.gameID, draw.autoDraw, callback);
    }

    /**
     * Clear draw winning number.
     * PUT /api/draws/clear-winning-number/<DRAW_ID>
     */
    public static ClearDrawWinningNumberEx(drawId: string,
                                           gameID: string,
                                           autoDraw: boolean,
                                           callback: (error: Error) => void) {
        Debug.Untested();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValidOrNull(gameID);
        Debug.AssertValidOrNull(autoDraw);
        Debug.AssertValid(callback);

        if (!!drawId && !!gameID) {
            let requestBody = {
                gameID,
                autoDraw: !!autoDraw
            };
            AdminGameServiceAPI.ClearDrawWinningNumber(drawId, requestBody, (result) => {
                let error = AdminGameService.errorFromResult(result);
                Debug.AssertValidOrNull(error);
                callback(error);
            });
        } else {
            callback(new Error(AdminGameService.ERROR_INVALID_ARGS));
        }
    }

}   // AdminGameService

/**
 * Class providing access to the admin game service.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/45056041/5.1.+Requirements
 */
export class AdminGameServiceAPI {

    /**
     * The game data.
     */
    private static Games: ObjectMap<AdminGame> = new ObjectMap<AdminGame>();
    private static Draws: ObjectMap<AdminDraw> = new ObjectMap<AdminDraw>();
    //??++private static Prizes: ObjectMap<AdminPrize> = new ObjectMap<AdminPrize>();
    private static Audits: AdminGameAudit[] = [];

    /**
     * Is the system locked?
     */
    private static IsSystemLocked(): boolean {
        Debug.UntestedMock();
        Debug.Uncoded();
        return false;
    }

    /**
     * Is an administrator logged in?
     */
    private static IsAdminLoggedIn(): boolean {
        Debug.UntestedMock();

        return AdminGameServiceAPI.UserHasPermission(IS_ADMIN);
    }

    /**
     * Games
     */

    /**
     * Convert an API game into an AdminGame.
     */
    public static APIGameToGame(game: any): AdminGame {
        Debug.UntestedMock();
        let retVal: AdminGame = null;
        if (game != null) {
            retVal = <AdminGame>{
                id: game.id,
                name: game.name,
                openDate: DateHelper.APIDateToDateOnly(game.openDate),
                closeDate: DateHelper.APIDateToDateOnly(game.closeDate),
                ticketCount: game.ticketCount,
                ticketPrice: game.ticketPrice,
                locked: game.locked,
                published: game.published,
                frozen: game.frozen,
                ticketServiceURL: game.ticketServiceURL,
                donatedToCharity: game.donatedToCharity
            };
        }
        return retVal;
    }

    /**
     * Convert an AdminGame into an API game.
     */
    private static GameToAPIGame(game: AdminGame): any {
        Debug.UntestedMock();
        let retVal = null;
        if (game != null) {
            retVal = {
                id: game.id,
                name: game.name,
                openDate: DateHelper.DateOnlyToAPIDate(game.openDate),
                closeDate: DateHelper.DateOnlyToAPIDate(game.closeDate),
                ticketCount: game.ticketCount,
                ticketPrice: game.ticketPrice,
                locked: game.locked,
                published: game.published,
                frozen: game.frozen,
                ticketServiceURL: game.ticketServiceURL,
                donatedToCharity: game.donatedToCharity
            };
        }
        return retVal;
    }

    /**
     * Get all games.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-GetGames
     */
    public static GetGames(accessToken, callback: (result: number, body: any) => void) {
        Debug.UntestedMock();
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            //??++CHECK PERMISSION
            if (!AdminGameServiceAPI.IsSystemLocked() && AdminGameServiceAPI.IsAdminLoggedIn()) {
                let games = [];
                AdminGameServiceAPI.Games.forEach(game => {
                    Debug.AssertValid(game);
                    games.push(AdminGameServiceAPI.GameToAPIGame(game));
                });
                callback(APIHelper.RESULT_200_OK, games);
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
            }
        });
    }

    /**
     * Get a game.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-GetGame
     */
    public static GetGame(gameId: string, callback: (result: number, body: any) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() && AdminGameServiceAPI.IsAdminLoggedIn()) {
                if (!!gameId) {
                    let game = AdminGameServiceAPI.Games.get(gameId);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        callback(APIHelper.RESULT_200_OK, AdminGameServiceAPI.GameToAPIGame(game));
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND, {});
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST, {});
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
            }
        });
    }

    /**
     * Create game.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-CreateGame
     */
    public static CreateGame(body: any,
                             callback: (result: number, body: any) => void) {
        Debug.UntestedMock();
        Debug.AssertValid(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_CREATE_GAME)) {
                if (!!body &&
                    !!body.name &&
                    !!body.openDate &&
                    (body.ticketCount > 0) &&
                    (body.ticketPrice > 0) &&
                    !!body.ticketServiceURL) {
                    //??++CHECK HASH OF ID IS UNIQUE.
                    let game = <AdminGame>{
                        id: `GAME_${RandomHelper.getRandomNumber()}`,
                        name: body.name,
                        openDate: DateHelper.APIDateToDateOnly(body.openDate),
                        closeDate: DateHelper.APIDateToDateOnly(body.closeDate),
                        ticketCount: body.ticketCount,
                        ticketPrice: body.ticketPrice,
                        locked: false,
                        published: false,
                        frozen: false,
                        ticketServiceURL: body.ticketServiceURL,
                        donatedToCharity: body.donatedToCharity
                    };
                    AdminGameServiceAPI.Games.set(game.id, game);
                    AdminGameServiceAPI.AddCreateGameAudit(game);
                    callback(APIHelper.RESULT_201_CREATED, AdminGameServiceAPI.GameToAPIGame(game));
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST, {});
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
            }
        });
    }

    /**
     * Update game.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-UpdateGame
     */
    public static UpdateGame(gameId: string,
                             body: any,
                             callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_UPDATE_GAME)) {
                if (!!gameId) {
                    let game = AdminGameServiceAPI.Games.get(gameId);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        if (!game.locked) {
                            if (!!body &&
                                !!body.name &&
                                !!body.openDate &&
                                (body.ticketCount > 0) &&
                                (body.ticketPrice > 0) &&
                                !!body.ticketServiceURL) {
                                game.name = body.name;
                                game.openDate = body.openDate;
                                game.closeDate = body.closeDate;
                                game.ticketCount = body.ticketCount;
                                game.ticketPrice = body.ticketPrice;
                                game.ticketServiceURL = body.ticketServiceURL;
                                game.donatedToCharity = body.donatedToCharity;
                                AdminGameServiceAPI.AddUpdateGameAudit(game);
                                callback(APIHelper.RESULT_200_OK);
                            } else {
                                callback(APIHelper.RESULT_400_BAD_REQUEST);
                            }
                        } else {
                            callback(APIHelper.RESULT_401_UNAUTHORIZED);
                        }
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND);
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Delete game.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-DeleteGame
     */
    public static DeleteGame(gameId: string,
                             callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_DELETE_GAME)) {
                if (!!gameId) {
                    let game = AdminGameServiceAPI.Games.get(gameId);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        Debug.AssertString(game.id);
                        if (!game.locked) {
                            AdminGameServiceAPI.Games.set(game.id, null);
                            AdminGameServiceAPI.AddDeleteGameAudit(game);
                            callback(APIHelper.RESULT_204_NO_CONTENT);
                        } else {
                            callback(APIHelper.RESULT_401_UNAUTHORIZED);
                        }
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND);
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Lock game.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-LockGame
     */
    public static LockGame(gameId: string,
                           callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_LOCK_GAME)) {
                if (!!gameId) {
                    let game = AdminGameServiceAPI.Games.get(gameId);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        Debug.AssertString(game.id);
                        if (!game.locked) {
                            let draws = AdminGameServiceAPI.GetGameDraws(game.id);
                            Debug.AssertValid(draws);
                            if (draws.length > 0) {
                                game.locked = true;
                                AdminGameServiceAPI.AddUpdateGameAudit(game);
                                callback(APIHelper.RESULT_200_OK);
                            } else {
                                callback(APIHelper.RESULT_400_BAD_REQUEST);
                            }
                        } else {
                            callback(APIHelper.RESULT_401_UNAUTHORIZED);
                        }
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND);
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Unlock game.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-UnlockGame
     */
    public static UnlockGame(gameId: string,
                             callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_UNLOCK_GAME)) {
                if (!!gameId) {
                    let game = AdminGameServiceAPI.Games.get(gameId);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        if (game.locked && !game.published) {
                            game.locked = false;
                            AdminGameServiceAPI.AddUpdateGameAudit(game);
                            callback(APIHelper.RESULT_200_OK);
                        } else {
                            callback(APIHelper.RESULT_401_UNAUTHORIZED);
                        }
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND);
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Publish game.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-PublishGame
     */
    public static PublishGame(gameId: string,
                              callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_PUBLISH_GAME)) {
                if (!!gameId) {
                    let game = AdminGameServiceAPI.Games.get(gameId);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        if (game.locked && !game.published) {
                            game.published = true;
                            AdminGameServiceAPI.AddUpdateGameAudit(game);
                            callback(APIHelper.RESULT_200_OK);
                        } else {
                            callback(APIHelper.RESULT_401_UNAUTHORIZED);
                        }
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND);
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Set game frozen (or unfrozen).
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-SetGameFrozen
     */
    public static SetGameFrozen(gameId: string,
                                body: any,
                                callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_FREEZE_GAME)) {
                if (!!gameId) {
                    let game = AdminGameServiceAPI.Games.get(gameId);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        if (game.locked) {
                            if (!!body) {
                                game.frozen = !!body.frozen;
                                AdminGameServiceAPI.AddUpdateGameAudit(game);
                                callback(APIHelper.RESULT_200_OK);
                            } else {
                                callback(APIHelper.RESULT_400_BAD_REQUEST);
                            }
                        } else {
                            callback(APIHelper.RESULT_401_UNAUTHORIZED);
                        }
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND);
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Set game close date.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.1.3.+Admin+Game+Service#id-5.1.3.AdminGameService-SetGameCloseDate
     */
    public static SetGameCloseDate(gameId: string,
                                   body: any,
                                   callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_SET_GAME_CLOSE_DATE)) {
                if (!!gameId) {
                    let game = AdminGameServiceAPI.Games.get(gameId);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        if (game.locked && (!game.published || !game.frozen)) {
                            if (!!body && !!body.closeDate) {
                                game.closeDate = DateHelper.APIDateToDateOnly(body.closeDate);
                                AdminGameServiceAPI.AddUpdateGameAudit(game);
                                callback(APIHelper.RESULT_200_OK);
                            } else {
                                callback(APIHelper.RESULT_400_BAD_REQUEST);
                            }
                        } else {
                            callback(APIHelper.RESULT_401_UNAUTHORIZED);
                        }
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND);
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Set game donated to charity.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-SetGameDonatedToCharity
     */
    public static SetGameDonatedToCharity(gameId: string,
                                          body: any,
                                          callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_SET_GAME_DONATED_TO_CHARITY)) {
                if (!!gameId) {
                    let game = AdminGameServiceAPI.Games.get(gameId);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        if (game.locked && (!game.published || !game.frozen)) {
                            if (!!body) {
                                game.donatedToCharity = body.donatedToCharity;
                                AdminGameServiceAPI.AddUpdateGameAudit(game);
                                callback(APIHelper.RESULT_200_OK);
                            } else {
                                callback(APIHelper.RESULT_400_BAD_REQUEST);
                            }
                        } else {
                            callback(APIHelper.RESULT_401_UNAUTHORIZED);
                        }
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND);
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Draws
     */

    /**
     * Convert an API draw into an AdminDraw.
     */
    public static APIDrawToDraw(draw: any): AdminDraw {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(draw);
        let retVal: AdminDraw = null;
        if (draw != null) {
            retVal = <AdminDraw>{
                id: draw.id,
                gameID: draw.gameID,
                drawDate: DateHelper.APITimestampToDate(draw.drawDate),
                drawnDate: DateHelper.APITimestampToDate(draw.drawnDate),
                amount: draw.amount,
                amountIsUpTo: draw.amountIsUpTo,
                prizeID: draw.prizeID,
                winningNumber: draw.winningNumber,
                winningUserID: draw.winningUserID,
                winningTicketID: draw.winningTicketID,
                autoDraw: draw.autoDraw,
                autoDrawn: draw.autoDrawn
            };
        }
        return retVal;
    }

    /**
     * Convert an AdminDraw into an API draw.
     */
    private static DrawToAPIDraw(draw: AdminDraw): any {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(draw);
        let retVal = null;
        if (draw != null) {
            retVal = {
                id: draw.id,
                gameID: draw.gameID,
                drawDate: DateHelper.DateToAPITimestamp(draw.drawDate),
                drawnDate: DateHelper.DateToAPITimestamp(draw.drawnDate),
                amount: draw.amount,
                amountIsUpTo: draw.amountIsUpTo,
                prizeID: draw.prizeID,
                winningNumber: draw.winningNumber,
                winningUserID: draw.winningUserID,
                winningTicketID: draw.winningTicketID,
                autoDraw: draw.autoDraw,
                autoDrawn: draw.autoDrawn
            };
        }
        return retVal;
    }

    /**
     * Get draws in a game.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-GetDraws
     */
    public static GetDraws(gameId: string,
                           callback: (result: number, body: any) => void) {
        Debug.UntestedMock();
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() && AdminGameServiceAPI.IsAdminLoggedIn()) {
                if (!!gameId) {
                    let game = AdminGameServiceAPI.Games.get(gameId);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        Debug.AssertString(game.id);
                        let draws = AdminGameServiceAPI.GetGameDraws(game.id);
                        Debug.AssertValid(draws);
                        callback(APIHelper.RESULT_200_OK, draws);
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND, {});
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST, {});
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
            }
        });
    }

    /**
     * Get a draw.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-GetDraw
     */
    public static GetDraw(gameId: string, drawId: string, callback: (result: number, body: any) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(gameId);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() && AdminGameServiceAPI.IsAdminLoggedIn()) {
                if (!!gameId && !!drawId) {
                    let game = AdminGameServiceAPI.Games.get(gameId);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        let draw = AdminGameServiceAPI.Draws.get(drawId);
                        Debug.AssertValidOrNull(draw);
                        if (draw != null) {
                            callback(APIHelper.RESULT_200_OK, draw);
                        } else {
                            callback(APIHelper.RESULT_404_NOT_FOUND, {});
                        }
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND, {});
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST, {});
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
            }
        });
    }

    /**
     * Create draw.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-CreateDraw
     */
    public static CreateDraw(body: any,
                             callback: (result: number, body: any) => void) {
        Debug.UntestedMock();
        Debug.AssertValid(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_CREATE_DRAW)) {
                if (!!body &&
                    !!body.gameID &&
                    !!body.amount) {
                    //??++CHECK HASH OF ID IS UNIQUE.
                    let game = AdminGameServiceAPI.Games.get(body.gameID);
                    Debug.AssertValidOrNull(game);
                    if (game != null) {
                        let draw = <AdminDraw>{
                            id: `DRAW_${RandomHelper.getRandomNumber()}`,
                            gameID: body.gameID,
                            drawDate: body.drawDate,
                            drawnDate: null,
                            amount: body.amount,
                            amountIsUpTo: !!body.amountIsUpTo,
                            prizeID: body.prizeID,
                            winningNumber: null,
                            winningTicketID: null,
                            winningUserID: null,
                            autoDraw: !!body.autoDraw,
                            autoDrawn: false
                        };
                        AdminGameServiceAPI.Draws.set(game.id, draw);
                        AdminGameServiceAPI.AddCreateDrawAudit(draw);
                        callback(APIHelper.RESULT_201_CREATED, game);
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND, {});
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST, {});
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
            }
        });
    }

    /**
     * Update draw.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-UpdateDraw
     */
    public static UpdateDraw(drawId: string,
                             body: any,
                             callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValidOrNull(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_UPDATE_DRAW)) {
                if (!!drawId) {
                    if (!!body &&
                        !!body.amount) {
                        let draw = AdminGameServiceAPI.Draws.get(drawId);
                        Debug.AssertValidOrNull(draw);
                        if (draw != null) {
                            Debug.AssertString(draw.gameID);
                            let game = AdminGameServiceAPI.Games.get(draw.gameID);
                            Debug.AssertValidOrNull(game);
                            if (game != null) {
                                if (!game.locked) {
                                    draw.drawDate = body.drawDate;
                                    draw.amount = body.amount;
                                    draw.amountIsUpTo = !!body.amountIsUpTo;
                                    draw.prizeID = body.prizeID;
                                    draw.autoDraw = !!body.autoDraw;
                                    AdminGameServiceAPI.AddUpdateDrawAudit(draw);
                                    callback(APIHelper.RESULT_200_OK);
                                } else {
                                    callback(APIHelper.RESULT_401_UNAUTHORIZED);
                                }
                            } else {
                                callback(APIHelper.RESULT_404_NOT_FOUND);
                            }
                        } else {
                            callback(APIHelper.RESULT_404_NOT_FOUND);
                        }
                    } else {
                        callback(APIHelper.RESULT_400_BAD_REQUEST);
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Delete draw.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-DeleteDraw
     */
    public static DeleteDraw(gameId: string,
                             drawId: string,
                             callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_DELETE_DRAW)) {
                if (!!drawId) {
                    let draw = AdminGameServiceAPI.Draws.get(drawId);
                    Debug.AssertValidOrNull(draw);
                    if (draw != null) {
                        Debug.AssertString(draw.id);
                        Debug.AssertString(draw.gameID);
                        if (draw.gameID === gameId) {
                            if (draw.drawnDate == null) {
                                let game = AdminGameServiceAPI.Games.get(draw.gameID);
                                Debug.AssertValidOrNull(game);
                                if (game != null) {
                                    if (!game.locked) {
                                        AdminGameServiceAPI.Draws.set(draw.id, null);
                                        AdminGameServiceAPI.AddDeleteDrawAudit(draw);
                                        callback(APIHelper.RESULT_204_NO_CONTENT);
                                    } else {
                                        callback(APIHelper.RESULT_401_UNAUTHORIZED);
                                    }
                                } else {
                                    callback(APIHelper.RESULT_404_NOT_FOUND);
                                }
                            } else {
                                callback(APIHelper.RESULT_401_UNAUTHORIZED);
                            }
                        } else {
                            callback(APIHelper.RESULT_401_UNAUTHORIZED);
                        }
                    } else {
                        callback(APIHelper.RESULT_404_NOT_FOUND);
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);
            }
        });
    }

    /**
     * Set draw date.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.1.3.+Admin+Game+Service#id-5.1.3.AdminGameService-SetDrawDate
     */
    public static SetDrawDate(drawId: string,
                              body: any,
                              callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValidOrNull(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_SET_DRAW_DATE)) {
                if (!!drawId) {
                    if (!!body &&
                        !!body.gameID &&
                        !!body.drawDate) {
                        let draw = AdminGameServiceAPI.Draws.get(drawId);
                        Debug.AssertValidOrNull(draw);
                        if (draw != null) {
                            Debug.AssertString(draw.id);
                            Debug.AssertString(draw.gameID);
                            if (draw.gameID === body.gameId) {
                                if (draw.drawDate == null) {
                                    if (draw.drawnDate == null) {
                                        let game = AdminGameServiceAPI.Games.get(draw.gameID);
                                        Debug.AssertValidOrNull(game);
                                        if (game != null) {
                                            if (game.locked && game.published && !game.frozen) {
                                                draw.drawDate = body.drawDate;
                                                AdminGameServiceAPI.AddUpdateDrawAudit(draw);
                                                callback(APIHelper.RESULT_200_OK);
                                            } else {
                                                callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Game not published (or it is frozen)
                                            }
                                        } else {
                                            callback(APIHelper.RESULT_404_NOT_FOUND); // Game not found
                                        }
                                    } else {
                                        callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Already drawn
                                    }
                                } else {
                                    callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Draw date already set.
                                }
                            } else {
                                callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Specified game ID does not match draw's game ID
                            }
                        } else {
                            callback(APIHelper.RESULT_404_NOT_FOUND); // Draw not found
                        }
                    } else {
                        callback(APIHelper.RESULT_400_BAD_REQUEST);   // Invalid body (or game ID or winning number)
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);   // Invalid draw ID
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);  // System locked or not logged in as an administrator
            }
        });
    }

    /**
     * Set draw amount.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.1.3.+Admin+Game+Service#id-5.1.3.AdminGameService-SetDrawAmount
     */
    public static SetDrawAmount(drawId: string,
                                body: any,
                                callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValidOrNull(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_SET_DRAW_AMOUNT)) {
                if (!!drawId) {
                    if (!!body &&
                        !!body.gameID &&
                        !!body.amount) {
                        let draw = AdminGameServiceAPI.Draws.get(drawId);
                        Debug.AssertValidOrNull(draw);
                        if (draw != null) {
                            Debug.AssertString(draw.id);
                            Debug.AssertString(draw.gameID);
                            if (draw.gameID === body.gameId) {
                                let game = AdminGameServiceAPI.Games.get(draw.gameID);
                                Debug.AssertValidOrNull(game);
                                if (game != null) {
                                    if (game.locked && game.published && !game.frozen) {
                                        draw.amount = body.amount;
                                        draw.amountIsUpTo = !!body.amountIsUpTo;
                                        AdminGameServiceAPI.AddUpdateDrawAudit(draw);
                                        callback(APIHelper.RESULT_200_OK);
                                    } else {
                                        callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Game not published (or it is frozen)
                                    }
                                } else {
                                    callback(APIHelper.RESULT_404_NOT_FOUND); // Game not found
                                }
                            } else {
                                callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Specified game ID does not match draw's game ID
                            }
                        } else {
                            callback(APIHelper.RESULT_404_NOT_FOUND); // Draw not found
                        }
                    } else {
                        callback(APIHelper.RESULT_400_BAD_REQUEST);   // Invalid body (or game ID or winning number)
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);   // Invalid draw ID
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);  // System locked or not logged in as an administrator
            }
        });
    }

    /**
     * Set draw auto draw.
     * 03/07/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.1.3.+Admin+Game+Service#id-5.1.3.AdminGameService-SetDrawAutoDraw
     */
    public static SetDrawAutoDraw(drawId: string,
                                  body: any,
                                  callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValidOrNull(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_SET_DRAW_AUTO_DRAW)) {
                if (!!drawId) {
                    if (!!body &&
                        !!body.gameID) {
                        let draw = AdminGameServiceAPI.Draws.get(drawId);
                        Debug.AssertValidOrNull(draw);
                        if (draw != null) {
                            Debug.AssertString(draw.id);
                            Debug.AssertString(draw.gameID);
                            if (draw.gameID === body.gameId) {
                                if (draw.drawnDate == null) {
                                    let game = AdminGameServiceAPI.Games.get(draw.gameID);
                                    Debug.AssertValidOrNull(game);
                                    if (game != null) {
                                        if (game.locked && game.published && !game.frozen) {
                                            draw.autoDraw = !!body.autoDraw;
                                            AdminGameServiceAPI.AddUpdateDrawAudit(draw);
                                            callback(APIHelper.RESULT_200_OK);
                                        } else {
                                            callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Game not published (or it is frozen)
                                        }
                                    } else {
                                        callback(APIHelper.RESULT_404_NOT_FOUND); // Game not found
                                    }
                                } else {
                                    callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Specified draw is already drawn
                                }
                            } else {
                                callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Specified game ID does not match draw's game ID
                            }
                        } else {
                            callback(APIHelper.RESULT_404_NOT_FOUND); // Draw not found
                        }
                    } else {
                        callback(APIHelper.RESULT_400_BAD_REQUEST);   // Invalid body (or game ID or winning number)
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);   // Invalid draw ID
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);  // System locked or not logged in as an administrator
            }
        });
    }

    /**
     * Set draw winning number.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-SetDrawWinningNumber
     */
    public static SetDrawWinningNumber(drawId: string,
                                       body: any,
                                       callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValidOrNull(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_SET_DRAW_WINNING_NUMBER)) {
                if (!!drawId) {
                    if (!!body &&
                        !!body.gameID &&
                        !!body.winningNumber) {
                        let draw = AdminGameServiceAPI.Draws.get(drawId);
                        Debug.AssertValidOrNull(draw);
                        if (draw != null) {
                            Debug.AssertString(draw.id);
                            Debug.AssertString(draw.gameID);
                            if (draw.gameID === body.gameId) {
                                if ((draw.drawDate == null) || (draw.drawDate <= new Date())) {
                                    if (draw.drawnDate == null) {
                                        let game = AdminGameServiceAPI.Games.get(draw.gameID);
                                        Debug.AssertValidOrNull(game);
                                        if (game != null) {
                                            if (game.locked && game.published && !game.frozen) {
                                                draw.winningNumber = body.winningNumber;
                                                draw.winningTicketID = "winning_ticket_id";
                                                draw.winningUserID = "winning_user_id";
                                                draw.drawnDate = new Date();
                                                if (body.amount != null) {
                                                    draw.amount = body.amount;
                                                    draw.amountIsUpTo = false;
                                                }
                                                AdminGameServiceAPI.AddUpdateDrawAudit(draw);
                                                callback(APIHelper.RESULT_200_OK);
                                            } else {
                                                callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Game not published (or it is frozen)
                                            }
                                        } else {
                                            callback(APIHelper.RESULT_404_NOT_FOUND); // Game not found
                                        }
                                    } else {
                                        callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Already drawn
                                    }
                                } else {
                                    callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Not time to perform the draw
                                }
                            } else {
                                callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Specified game ID does not match draw's game ID
                            }
                        } else {
                            callback(APIHelper.RESULT_404_NOT_FOUND); // Draw not found
                        }
                    } else {
                        callback(APIHelper.RESULT_400_BAD_REQUEST);   // Invalid body (or game ID or winning number)
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);   // Invalid draw ID
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);  // System locked or not logged in as an administrator
            }
        });
    }

    /**
     * Clear draw winning number.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-ClearDrawWinningNumber
     */
    public static ClearDrawWinningNumber(drawId: string,
                                         body: any,
                                         callback: (result: number) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(drawId);
        Debug.AssertValidOrNull(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() &&
                AdminGameServiceAPI.IsAdminLoggedIn() &&
                AdminGameServiceAPI.UserHasPermission(CAN_CLEAR_DRAW_WINNING_NUMBER)) {
                if (!!drawId) {
                    if (!!body &&
                        !!body.gameID) {
                        let draw = AdminGameServiceAPI.Draws.get(drawId);
                        Debug.AssertValidOrNull(draw);
                        if (draw != null) {
                            Debug.AssertString(draw.id);
                            Debug.AssertString(draw.gameID);
                            if (draw.gameID === body.gameId) {
                                if (draw.drawnDate != null) {
                                    let game = AdminGameServiceAPI.Games.get(draw.gameID);
                                    Debug.AssertValidOrNull(game);
                                    if (game != null) {
                                        if (game.locked && game.published && !game.frozen) {
                                            draw.winningNumber = null;
                                            draw.winningTicketID = null;
                                            draw.winningUserID = null;
                                            if (body.autoDraw != null) {
                                                draw.autoDraw = body.autoDraw;
                                            }
                                            AdminGameServiceAPI.AddUpdateDrawAudit(draw);
                                            callback(APIHelper.RESULT_200_OK);
                                        } else {
                                            callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Game not published (or it is frozen)
                                        }
                                    } else {
                                        callback(APIHelper.RESULT_404_NOT_FOUND); // Game not found
                                    }
                                } else {
                                    callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Not time to perform the draw
                                }
                            } else {
                                callback(APIHelper.RESULT_401_UNAUTHORIZED);  // Specified game ID does not match draw's game ID
                            }
                        } else {
                            callback(APIHelper.RESULT_404_NOT_FOUND); // Draw not found
                        }
                    } else {
                        callback(APIHelper.RESULT_400_BAD_REQUEST);   // Invalid body (or game ID or winning number)
                    }
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST);   // Invalid draw ID
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED);  // System locked or not logged in as an administrator
            }
        });
    }

    /**
     * Audits.
     */

    /**
     * Convert an API audit into an AdminGameAudit.
     */
    private static APIAuditToAudit(audit: any): AdminGameAudit {
        Debug.UntestedMock();
        let retVal: AdminGameAudit = <AdminGameAudit>{
            id: audit.id,
            timestamp: DateHelper.APITimestampToDate(audit.timestamp),
            changeType: audit.changeType,
            dataType: audit.dataType,
            targetID: audit.targetID,
            userID: audit.userID,
        };
        if (audit.dataType === AdminGameAuditDataType.game) {
            retVal.targetCopy = AdminGameServiceAPI.APIGameToGame(audit.targetCopy);
        } else if (audit.dataType === AdminGameAuditDataType.draw) {
            retVal.targetCopy = AdminGameServiceAPI.APIDrawToDraw(audit.targetCopy);
        } else {
            Debug.Uncoded();
        }
        return retVal;
    }

    /**
     * Convert an AdminGameAudit into an API audit.
     */
    private static AuditToAPIAudit(audit: AdminGameAudit): any {
        Debug.UntestedMock();
        let retVal = {
            id: audit.id,
            timestamp: DateHelper.DateToAPITimestamp(audit.timestamp),
            changeType: audit.changeType,
            dataType: audit.dataType,
            targetID: audit.targetID,
            userID: audit.userID,
            targetCopy: null
        };
        if (audit.dataType === AdminGameAuditDataType.game) {
            retVal.targetCopy = AdminGameServiceAPI.GameToAPIGame(audit.targetCopy);
        } else if (audit.dataType === AdminGameAuditDataType.draw) {
            retVal.targetCopy = AdminGameServiceAPI.DrawToAPIDraw(audit.targetCopy);
        } else {
            Debug.Uncoded();
        }
        return retVal;
    }

    /**
     * Create game audit.
     */
    private static AddCreateGameAudit(game: AdminGame) {
        Debug.UntestedMock();
        Debug.AssertValid(game);
        AdminGameServiceAPI.AddGameAudit(game, AdminGameAuditChangeType.create);
    }

    /**
     * Update game audit.
     */
    private static AddUpdateGameAudit(game: AdminGame) {
        Debug.UntestedMock();
        Debug.AssertValid(game);
        AdminGameServiceAPI.AddGameAudit(game, AdminGameAuditChangeType.update);
    }

    /**
     * Game audit.
     */
    private static AddGameAudit(game: AdminGame, changeType: AdminGameAuditChangeType) {
        Debug.UntestedMock();
        Debug.AssertValid(game);
        let targetCopy = <AdminGame>{
            id: game.id,
            name: game.name,
            openDate: game.openDate,
            closeDate: game.closeDate,
            ticketCount: game.ticketCount,
            ticketPrice: game.ticketPrice,
            locked: game.locked,
            published: game.published,
            frozen: game.frozen,
            ticketServiceURL: game.ticketServiceURL,
            donatedToCharity: game.donatedToCharity
        };
        AdminGameServiceAPI.AddAudit(game.id, changeType, AdminGameAuditDataType.game, targetCopy);
    }

    /**
     * Delete game audit.
     */
    private static AddDeleteGameAudit(game: AdminGame) {
        Debug.UntestedMock();
        Debug.AssertValid(game);
        AdminGameServiceAPI.AddAudit(game.id, AdminGameAuditChangeType.delete, AdminGameAuditDataType.game, null);
    }

    /**
     * Create draw audit.
     */
    private static AddCreateDrawAudit(draw: AdminDraw) {
        Debug.UntestedMock();
        Debug.AssertValid(draw);
        AdminGameServiceAPI.AddDrawAudit(draw, AdminGameAuditChangeType.create);
    }

    /**
     * Update draw audit.
     */
    private static AddUpdateDrawAudit(draw: AdminDraw) {
        Debug.UntestedMock();
        Debug.AssertValid(draw);
        AdminGameServiceAPI.AddDrawAudit(draw, AdminGameAuditChangeType.update);
    }

    /**
     * Draw audit.
     */
    private static AddDrawAudit(draw: AdminDraw, changeType: AdminGameAuditChangeType) {
        Debug.UntestedMock();
        Debug.AssertValid(draw);
        let targetCopy = <AdminDraw>{
            id: draw.id,
            gameID: draw.gameID,
            drawDate: draw.drawDate,
            drawnDate: draw.drawnDate,
            amount: draw.amount,
            amountIsUpTo: draw.amountIsUpTo,
            prizeID: draw.prizeID,
            winningNumber: draw.winningNumber,
            winningUserID: draw.winningUserID,
            winningTicketID: draw.winningTicketID,
            autoDraw: draw.autoDraw,
            autoDrawn: draw.autoDrawn
    };
        AdminGameServiceAPI.AddAudit(draw.id, changeType, AdminGameAuditDataType.draw, targetCopy);
    }

    /**
     * Draw audit.
     */
    private static AddDeleteDrawAudit(draw: AdminDraw) {
        Debug.UntestedMock();
        Debug.AssertValid(draw);
        AdminGameServiceAPI.AddAudit(draw.id, AdminGameAuditChangeType.delete, AdminGameAuditDataType.draw, null);
    }

    /**
     * Add an audit.
     */
    private static AddAudit(targetID: string, changeType: AdminGameAuditChangeType, dataType: AdminGameAuditDataType, targetCopy: any) {
        Debug.UntestedMock();
        Debug.AssertString(targetID);
        Debug.AssertValidOrNull(targetCopy);
        let audit: AdminGameAudit = <AdminGameAudit>{
            id: `AUDIT_${RandomHelper.getRandomNumber()}`,
            timestamp: new Date(),
            changeType: changeType,
            dataType: dataType,
            targetID: targetID,
            userID: "userID",
            targetCopy: targetCopy
        };
        AdminGameServiceAPI.Audits.push(audit);
    }

    /**
     * Get audit records in the specified date range.
     * 21/06/18 SMP Consistent with specification document.
     * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/17432581/5.3.+Admin+Game+Service#id-5.3.AdminGameService-GetAuditRecords
     */
    public static GetAuditRecords(body: any, callback: (result: number, body: any) => void) {
        Debug.UntestedMock();
        Debug.AssertValidOrNull(body);
        Debug.AssertValid(callback);

        Debug.scheduleCallback(() => {
            if (!AdminGameServiceAPI.IsSystemLocked() && AdminGameServiceAPI.IsAdminLoggedIn()) {
                if (!!body && !!body.from && !!body.to) {
                    let from = DateHelper.APITimestampToDate(body.from);
                    let to = DateHelper.APITimestampToDate(body.to);
                    let audits = [];
                    AdminGameServiceAPI.Audits.forEach(audit => {
                        Debug.AssertValid(audit);
                        Debug.AssertValid(audit.timestamp);
                        if ((audit.timestamp >= from) && (audit.timestamp < to)) {
                            if ((body.dateType == null) || (audit.dataType === body.dateType)) {
                                audits.push(AdminGameServiceAPI.AuditToAPIAudit(audit));
                            }
                        }
                    });
                    callback(APIHelper.RESULT_200_OK, audits);
                } else {
                    callback(APIHelper.RESULT_400_BAD_REQUEST, {});
                }
            } else {
                callback(APIHelper.RESULT_401_UNAUTHORIZED, {});
            }
        });
    }

    /**
     * Internal helper methods.
     */

    /**
     * Get a game's draws.
     */
    private static GetGameDraws(gameID: string): AdminDraw[] {
        Debug.UntestedMock();
        Debug.AssertString(gameID);

        let draws: AdminDraw[] = [];
        AdminGameServiceAPI.Draws.forEach(draw => {
            Debug.AssertValid(draw);
            if (draw.gameID === gameID) {
                draws.push(draw);
            }
        });
        return draws;
    }

    /**
     * Has the user got the specified permission?
     */
    private static UserHasPermission(permission: string): boolean {
        Debug.UntestedMock();
        Debug.AssertString(permission);

        return true;
    }

    /**
     * Has the user got all the specified permission?
     */
    private static UserHasAllPermissions(permissions: string[]): boolean {
        Debug.UntestedMock();
        Debug.AssertValid(permissions);

        let retVal = true;
        permissions.forEach(permission => {
            Debug.AssertString(permission);
            if (!AdminGameServiceAPI.UserHasPermission(permission)) {
                retVal = false;
            }
        });
        return retVal;
    }

    //??++ADD PRIZES

}   // AdminGameServiceAPI
