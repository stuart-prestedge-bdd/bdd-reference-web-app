import { Debug } from '../helpers/debug';
import { CallbackWaiter } from '../helpers/callback-waiter';
import { WebApp } from '../components/web-app';
import { ContentService } from './content-service';
import { ObjectMap } from '../helpers/object-map';
import { ErrorHelper } from '../helpers/error-helper';

/**
 * Class providing access to languages content service.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47153234/6.2.+Languages
 */
export class TextContentService extends ContentService {

    /**
     * The text names.
     */
    static readonly TEXT_NAME_BDD_TITLE = 'bdd_title';
    static readonly TEXT_NAME_UNKNOWN = 'text_unknown';
    static readonly TEXT_NAME_TITLE_ACCOUNT_CREATED = 'title_account_created';
    static readonly TEXT_NAME_MESSAGE_ACCOUNT_CREATED = 'message_account_created';
    static readonly TEXT_NAME_TITLE_LOGIN = 'title_login';
    static readonly TEXT_NAME_BUTTON_LOGIN = 'button_login';
    static readonly TEXT_NAME_TITLE_CAUSES = 'title_causes';
    static readonly TEXT_NAME_DESCRIPTION_CAUSES = 'description_causes';
    static readonly TEXT_NAME_TITLE_AMBASSADORS = 'title_ambassadors';
    static readonly TEXT_NAME_DESCRIPTION_AMBASSADORS = 'description_ambassadors';
    static readonly TEXT_NAME_TITLE_CHARITIES = 'title_charities';
    static readonly TEXT_NAME_DESCRIPTION_CHARITIES = 'description_charities';
    static readonly TEXT_NAME_TITLE_EMAIL_VERIFIED = 'title_email_verified';
    static readonly TEXT_NAME_MESSAGE_EMAIL_VERIFIED = 'message_email_verified';
    static readonly TEXT_NAME_TITLE_LOGGED_OUT = 'title_logged_out';
    static readonly TEXT_NAME_MESSAGE_LOGGED_OUT = 'message_logged_out';
    static readonly TEXT_NAME_TITLE_TICKET = 'title_ticket';
    static readonly TEXT_NAME_LABEL_PURCHASED = 'label_purchased';
    static readonly TEXT_NAME_LABEL_WON = 'label_won';
    static readonly TEXT_NAME_LABEL_NEXT_DRAW = 'label_next_draw';
    static readonly TEXT_NAME_LABEL_GIFTED_FROM = 'label_gifted_from';
    static readonly TEXT_NAME_LABEL_GIFTED_DATE = 'label_gifted_date';
    static readonly TEXT_NAME_LABEL_REMAINING_DRAWS = 'label_remaining_draws';
    static readonly TEXT_NAME_LABEL_REDEEMED_DATE = 'label_redeemed_date';
    static readonly TEXT_NAME_BUTTON_GIFT = 'button_gift';
    static readonly TEXT_NAME_BUTTON_OFFER_GIFT = 'button_offer_gift';
    static readonly TEXT_NAME_BUTTON_REDEEM = 'button_redeem';
    static readonly TEXT_NAME_TITLE_GIFT_TICKET = 'title_gift_ticket';
    static readonly TEXT_NAME_GIFT_EMAIL_HINT = 'gift_email_hint';
    static readonly TEXT_NAME_GIFT_MESSAGE_HINT = 'gift_message_hint';
    static readonly TEXT_NAME_TITLE_BUY_TICKET = 'title_buy_ticket';
    static readonly TEXT_NAME_BUTTON_BUY = 'button_buy';
    static readonly TEXT_NAME_BUTTON_ADD = 'button_add';
    static readonly TEXT_NAME_BUTTON_REMOVE = 'button_remove';
    static readonly TEXT_NAME_DESCRIPTION_BUY_TICKETS = 'description_buy_tickets';
    static readonly TEXT_NAME_LINK_CHOOSE_MY_OWN = 'link_choose_my_own';
    static readonly TEXT_NAME_LINK_CHOOSE_FOR_ME = 'link_choose_for_me';
    static readonly TEXT_NAME_TITLE_PASSWORD_RESET_SENT = 'title_password_reset_sent';
    static readonly TEXT_NAME_MESSAGE_PASSWORD_RESET_SENT = 'message_password_reset_sent';
    static readonly TEXT_NAME_TITLE_USER_PROFILE = 'title_user_profile';
    static readonly TEXT_NAME_SUBTITLE_PERSONAL = 'subtitle_personal';
    static readonly TEXT_NAME_YOUR_NAME_LABEL = 'your_name_label';
    static readonly TEXT_NAME_GENDER_MALE = 'gender_male';
    static readonly TEXT_NAME_GENDER_FEMALE = 'gender_female';
    static readonly TEXT_NAME_GENDER_UNSPECIFIED = 'gender_unspecified';
    static readonly TEXT_NAME_TITLE_ACCOUNT_CLOSED = 'title_account_closed';
    static readonly TEXT_NAME_MESSAGE_ACCOUNT_CLOSED = 'message_page_account_closed';
    static readonly TEXT_NAME_EMAIL_HINT = 'email_hint';
    static readonly TEXT_NAME_PASSWORD_HINT = 'password_hint';
    static readonly TEXT_NAME_REMEMBER_ME = 'remember_me';
    static readonly TEXT_NAME_NO_ACCOUNT_CREATE_ACCOUNT = 'no_account_create_account';
    static readonly TEXT_NAME_TITLE_RESET_PASSWORD = 'title_reset_password';
    static readonly TEXT_NAME_BUTTON_RESET_PASSWORD = 'button_reset_password';
    static readonly TEXT_NAME_GIVEN_NAME_HINT = 'given_name_hint';
    static readonly TEXT_NAME_FAMILY_NAME_HINT = 'family_name_hint';
    static readonly TEXT_NAME_TITLE_CREATE_ACCOUNT = 'title_create_account';
    static readonly TEXT_NAME_BUTTON_CREATE_ACCOUNT = 'button_create_account';
    static readonly TEXT_NAME_TITLE_CONFIRM_DETAILS = 'title_confirm_details';
    static readonly TEXT_NAME_BUTTON_CONFIRM_DETAILS = 'button_confirm_details';
    static readonly TEXT_NAME_REFER_FRIEND_SUBJECT = 'refer_friend_subject';
    static readonly TEXT_NAME_REFER_FRIEND_BODY_HTML = 'refer_friend_body_html';
    static readonly TEXT_NAME_CHANGE_PASSWORD_BUTTON = 'change_password_button';
    static readonly TEXT_NAME_WHERE_YOUR_MONEY_GOES = 'where_your_money_goes';

    /**
     * The menu text names.
     */
    static readonly TEXT_NAME_MENU_BUY_TICKETS = 'menu_buy_tickets';
    static readonly TEXT_NAME_MENU_MY_TICKETS = 'menu_my_tickets';
    static readonly TEXT_NAME_MENU_CURRENT_GAME = 'menu_current_game';
    static readonly TEXT_NAME_MENU_PRIZES = 'menu_prizes';
    static readonly TEXT_NAME_MENU_CAUSES = 'menu_causes';
    static readonly TEXT_NAME_MENU_CHARITIES = 'menu_charities';
    static readonly TEXT_NAME_MENU_AMBASSADORS = 'menu_ambassadors';
    static readonly TEXT_NAME_MENU_REFER_FRIEND = 'menu_refer_friend';

    /**
     * The error text names.
     */
    static readonly TEXT_NAME_ERROR_NOT_LOGGED_IN = 'error_not_logged_in';
    static readonly TEXT_NAME_ERROR_FAILED_TO_GET_USER_DETAILS = 'error_failed_to_get_user_details';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOAD_SETTINGS = 'error_failed_to_load_settings';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOAD_TEXT = 'error_failed_to_load_text';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOAD_CAUSES = 'error_failed_to_load_causes';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOAD_CHARITIES = 'error_failed_to_load_charities';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOAD_AMBASSADORS = 'error_failed_to_load_ambassadors';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOAD_TICKET = 'error_failed_to_load_ticket';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOAD_TICKETS = 'error_failed_to_load_tickets';
    static readonly TEXT_NAME_ERROR_FAILED_TO_READ_TEXT = 'error_failed_to_read_text';
    static readonly TEXT_NAME_ERROR_INVALID_GIVEN_NAME = 'error_invalid_given_name';
    static readonly TEXT_NAME_ERROR_INVALID_FAMILY_NAME = 'error_invalid_family_name';
    static readonly TEXT_NAME_ERROR_INVALID_EMAIL = 'error_invalid_email';
    static readonly TEXT_NAME_ERROR_INVALID_PASSWORD = 'error_invalid_password';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOGIN = 'error_failed_to_login';
    static readonly TEXT_NAME_ERROR_FAILED_TO_CREATE_ACCOUNT = 'error_failed_to_create_account';
    static readonly TEXT_NAME_ERROR_FAILED_TO_CLOSE_ACCOUNT = 'error_failed_to_close_account';
    static readonly TEXT_NAME_ERROR_TICKET_FAILED_TO_REDEEM = 'error_ticket_failed_to_redeem';
    static readonly TEXT_NAME_ERROR_FAILED_TO_SET_USER_NAME = 'error_failed_to_set_user_name';
    static readonly TEXT_NAME_ERROR_FAILED_TO_SET_GENDER = 'error_failed_to_set_gender';
    static readonly TEXT_NAME_ERROR_FAILED_TO_SET_DOB = 'error_failed_to_set_date_of_birth';
    static readonly TEXT_NAME_ERROR_FAILED_TO_SET_ADDRESS = 'error_failed_to_set_address';
    static readonly TEXT_NAME_ERROR_FAILED_TO_CHOOSE_TICKET = 'error_failed_to_choose_ticket';
    static readonly TEXT_NAME_ERROR_FAILED_TO_BUY_TICKETS = 'error_failed_to_buy_tickets';
    static readonly TEXT_NAME_ERROR_FAILED_TO_RESET_PASSWORD = 'error_failed_to_reset_password';
    static readonly TEXT_NAME_ERROR_FAILED_TO_RESERVE_TICKET = 'error_failed_to_reserve_ticket';
    static readonly TEXT_NAME_ERROR_FAILED_TO_CHECK_PAYMENT_DETAILS = 'error_failed_to_check_payment_details';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOAD_NUMBERS = 'error_failed_to_load_numbers';
    static readonly TEXT_NAME_ERROR_FAILED_TO_SAVE_NUMBERS = 'error_failed_to_save_numbers';
    static readonly TEXT_NAME_ERROR_FAILED_TO_OFFER_TICKET = 'error_failed_to_offer_ticket';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOAD_GAMES = 'error_failed_to_load_games';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOAD_GAME = 'error_failed_to_load_game';
    static readonly TEXT_NAME_ERROR_FAILED_TO_VERIFY_EMAIL_ADDRESS = 'error_failed_to_verify_email_address';
    static readonly TEXT_NAME_ERROR_INVALID_VERIFICATION_DETAILS = 'error_invalid_verification_details';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOAD_LINKS = 'error_failed_to_load_links';
    static readonly TEXT_NAME_ERROR_FAILED_TO_LOAD_LINK = 'error_failed_to_load_link';

    /**
     * The info text names.
     */
    static readonly TEXT_NAME_INFO_LIMIT_OF_LINES_REACHED = 'info_limit_of_lines_reached';
    static readonly TEXT_NAME_INFO_TICKET_REDEEMED = 'info_ticket_redeemed';
    static readonly TEXT_NAME_INFO_TICKET_PURCHASED = 'info_tickets_purchased';
    static readonly TEXT_NAME_INFO_MUST_SETUP_PAYMENT_DETAILS = 'info_must_setup_payment_details';
    static readonly TEXT_NAME_INFO_EMAIL_ADDRESS_VERIFIED = 'info_email_address_verified';

    /**
     * The cached languages.
     */
    private static CachedTexts: object;

    /**
     * The callback waiter.
     */
    private static CallbackWaiter: CallbackWaiter;

    /**
     * Clear the cache.
     */
    public static clearCache() {
        Debug.Tested();

        this.CachedTexts = null;
    }

    /**
     * Get the games available to show.
     */
    private static getTexts(callback: (texts: object) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValidOrNull(TextContentService.CachedTexts);

        if (TextContentService.CachedTexts == null) {
            if (TextContentService.CallbackWaiter == null) {
                TextContentService.CallbackWaiter = new CallbackWaiter(callback);
                WebApp.getInstance().getLanguageCode((languageCode) => {
                    Debug.AssertString(languageCode);
                    ContentService.getContent(`/text/text-${languageCode}.json`, (error, content) => {
                        Debug.AssertErrorOrObject(error, content);
                        TextContentService.CachedTexts = (content != null) ? content.texts : null;
                        TextContentService.CallbackWaiter.CallCallbacksNoError(TextContentService.CachedTexts);
                        TextContentService.CallbackWaiter = null;
                        ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_TEXT);
                    });
                });
            } else {
                TextContentService.CallbackWaiter.AddCallback(callback);
            }
        } else {
            callback(TextContentService.CachedTexts);
        }
    }

    /**
     * Asynchronously get a text value by name.
     * The text will be set to the name if texts fail to load or the value is not found.
     * This is not ideal but allows for troubleshooting and stops completely blank content.
     */
    public static getTextValue(name: string, callback: (value: string) => void) {
        Debug.Tested();
        Debug.AssertString(name);
        Debug.AssertValid(callback);

        TextContentService.getTexts((texts) => {
            Debug.AssertValidOrNull(texts);
            let value: string;
            if (texts != null) {
                // In the case where the text value is not found, use the name of the value.
                value = texts[name] || name;
            } else {
                // In the case where texts cannot be loaded, use the name of the value.
                value = name;
            }
            Debug.AssertString(value);
            callback(value);
        });
    }

    /**
     * Asynchronously get text values by name.
     * Always returns an array equal to the size of names.
     * If texts fail to load completely or if any text values are not found, the appropriate name is used.
     */
    public static getTextValues(names: string[], callback: (values_: ObjectMap<string>) => void) {
        Debug.Tested();
        Debug.AssertValid(names);
        Debug.AssertValid(callback);
        
        TextContentService.getTexts((texts) => {
            Debug.AssertValidOrNull(texts);
            let values = new ObjectMap<string>();
            names.forEach(name => {
                Debug.AssertString(name);
                let value: string;
                if (texts != null) {
                    // In the case where the text value is not found, use the name of the value.
                    value = texts[name] || name;
                } else {
                    // In the case where texts cannot be loaded, use the name of the value.
                    value = name;
                }
                Debug.AssertString(value);
                values.set(name, value);
            });
            callback(values);
        });
    }

}   // TextContentService

