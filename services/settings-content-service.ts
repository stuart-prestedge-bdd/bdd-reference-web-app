import { Debug } from '../helpers/debug';
import { ContentService } from './content-service';
import { Language } from '../data-models/language';
import { CallbackWaiter } from '../helpers/callback-waiter';
import { ErrorHelper } from '../helpers/error-helper';
import { TextContentService } from './text-content-service';

/**
 * Class providing access to settings data.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/???
 */
export class SettingsContentService extends ContentService {

    /**
     * The cached settings.
     */
    private static Settings: any;

    /**
     * The callback waiter.
     */
    private static CallbackWaiter: CallbackWaiter;

    /**
     * The cached languages.
     */
    private static Languages_: Language[];

    /**
     * Get the settings.
     * These are loaded once then cached.
     */
    private static getSettings(callback: (settings: any) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        if (SettingsContentService.Settings == null) {
            if (SettingsContentService.CallbackWaiter == null) {
                SettingsContentService.CallbackWaiter = new CallbackWaiter(callback);
                ContentService.getContent('/settings.json', (error, content) => {
                    Debug.AssertErrorOrObject(error, content);
                    SettingsContentService.Settings = content || {};
                    SettingsContentService.CallbackWaiter.CallCallbacksNoError(SettingsContentService.Settings);
                    SettingsContentService.CallbackWaiter = null;
                    ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_SETTINGS);
                });
            } else {
                SettingsContentService.CallbackWaiter.AddCallback(callback);
            }
        } else {
            callback(SettingsContentService.Settings);
        }
    }

    /**
     * Get the languages.
     * These are loaded once then cached.
     */
    public static getLanguages(callback: (languages: Language[]) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        SettingsContentService.getSettings((settings) => {
            Debug.AssertValid(settings);
            Debug.AssertValid(settings.languages);
            Debug.Assert(settings.languages.length > 0);
            callback(settings.languages);
        });
    }

    /**
     * Get the default language code.
     */
    public static getDefaultLanguageCode(callback: (defaultLanguageCode: string) => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        SettingsContentService.getSettings((settings) => {
            Debug.AssertValid(settings);
            Debug.AssertValid(settings.languages);
            Debug.Assert(settings.languages.length > 0);
            Debug.AssertStringOrNull(settings.defaultLanguage);
            let defaultLanguageCode = settings.defaultLanguage;
            if (!!defaultLanguageCode) {
                let defaultLanguage: Language = settings.languages[0];
                Debug.AssertValid(defaultLanguage);
                Debug.AssertString(defaultLanguage.code);
                defaultLanguageCode = defaultLanguage.code;
            }
            callback(defaultLanguageCode);
        });
    }

    /**
     * Get the languages.
     * These are loaded once then cached.
     */
    public static getPaymentProviders(callback: (paymentProviders: string[]) => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        SettingsContentService.getSettings((settings) => {
            Debug.AssertValid(settings);
            callback(settings.paymentProviders || []);
        });
    }

    /**
     * Get the fixed country.
     */
    public static getFixedCountry(callback: (country: string) => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        SettingsContentService.getSettings((settings) => {
            Debug.AssertValid(settings);
            callback(settings.fixedCountry);
        });
    }

    /**
     * Get the default country.
     */
    public static getDefaultCountry(callback: (country: string) => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        SettingsContentService.getSettings((settings) => {
            Debug.AssertValid(settings);
            callback(settings.defaultCountry || 'gb');
        });
    }

    /**
     * Get the region.
     */
    public static getRegion(callback: (region: string) => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        SettingsContentService.getSettings((settings) => {
            Debug.AssertValid(settings);
            callback(settings.region || 'global');
        });
    }

    /**
     * Get the fixed flavour.
     */
    public static getFixedFlavor(callback: (flavor: string) => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        SettingsContentService.getSettings((settings) => {
            Debug.AssertValid(settings);
            callback(settings.fixedFlavor);
        });
    }

    /**
     * Get the default flavour.
     */
    public static getDefaultFlavor(callback: (flavor: string) => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        SettingsContentService.getSettings((settings) => {
            Debug.AssertValid(settings);
            callback(settings.defaultFlavor || 'standard');
        });
    }

}   // SettingsContentService

