import { Debug } from '../helpers/debug';
import { Constants } from '../constants';
import { ErrorHelper } from '../helpers/error-helper';
import { TextContentService } from './text-content-service';

/**
 * HTTP object.
 */
declare function require(name:string);
var http = require('http');

/**
 * Class providing access to a content service.
 */
export class ContentService {

    /**
     * Asynchronously gets content from the specified file in the content store.
     * It is up to the caller to cache this.
     */
    protected static getContent(contentFile: string, callback: (error: Error, data: any) => void) {
        Debug.Tested();
        Debug.AssertString(contentFile);
        Debug.Assert(contentFile[0] === '/');
        Debug.AssertValid(callback);

        try {
            let cmsRoot = Constants.DATA_ROOT;//??++CALCULATE THIS FROM APP ROOT FOR WHITE LABEL CASE
            let req = http.get(`${cmsRoot}${contentFile}`,
                function(response) {
                    var body = '';
                    response.on('error', function() {
                        Debug.Untested();
                        console.log('error');
                        callback(new Error(), null);
                    });
                    response.on('data', function(d) {
                        body += d;
                    });
                    response.on('end', function() {
                        if (response.statusCode === 200) {
                            let data = JSON.parse(body);
                            callback(null, data);
                        } else {
                            console.log(`Status code: ${response.statusCode}`);
                            callback(new Error(), null);
                        }
                    });
                }
            );
            req.on('error', function(e) {
                Debug.Tested();
                console.log(e);
                callback(new Error(), null);
            });
        } catch (e) {
            Debug.Untested();
            console.log(e);
            callback(new Error(), null);
        }
    }
        
}   // ContentService

