import { Debug } from './debug';

/**
 * Class containing password helper methods.
 */
export class PasswordHelper {

    /**
     * Is the passed in password valid?
     */
    public static isValidPassword(password: string): boolean {
        Debug.Tested();
        Debug.AssertValidOrNull(password);

        let retVal = false;
        if (password != null) {
            let len = password.length;
            if (len >= 6 && len <= 100) {
                let hasUpper = false;
                let hasLower = false;
                let hasDigit = false;
                for (let i = 0; i < len; i++) {
                    let c = password[i];
                    if ((c >= 'A') && (c <= 'Z')) {
                        hasUpper = true;
                    } else if ((c >= 'a') && (c <= 'z')) {
                        hasLower = true;
                    } else if ((c >= '0') && (c <= '9')) {
                        hasDigit = true;
                    }
                }
                retVal = (hasUpper && hasLower && hasDigit);
            }
        }
        return retVal;
    }

    /**
     * Get the password hash.
     * This function is only used for the mocks. The client will not hash the password.
     */
    public static getPasswordHash(password: string): string {
        Debug.TestedMock();
        Debug.AssertValidOrNull(password);

        let retVal = null;
        if (password != null) {
            retVal = `HASH(${password})`;
        }
        return retVal;
    }

}   // PasswordHelper
