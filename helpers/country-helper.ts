import { Debug } from './debug';

/**
 * Class containing country helper methods.
 */
export class CountryHelper {

    /**
     * Is the specified country code valid?
     */
    public static isValidCountryCode(countryCode: string): boolean {
        Debug.Tested();
        Debug.AssertValidOrNull(countryCode);

        let retVal = false;
        if (!!countryCode) {
            if (countryCode === 'gb') {
                retVal = true;
            } else if (countryCode === 'us') {
                retVal = true;
            }
        }
        return retVal;
    }

}   // CountryHelper
