import { Debug } from './debug';
import { Image } from '../generic/common-object-model';
import { Constants } from '../constants';

/**
 * Asset helper class.
 */
export class AssetHelper {

    /**
     * The <ASSET_ROOT> replacement.
     */
    private static readonly ASSET_ROOT = '<ASSET_ROOT>';

    /**
     * Asset URLs.
     */
    public static readonly ASSET_MENU_ICON = `${AssetHelper.ASSET_ROOT}/menu-icon.png`;
    public static readonly ASSET_MENU_BUY_TICKETS_ICON = `${AssetHelper.ASSET_ROOT}/menu-buy-tickets-icon.png`;
    public static readonly ASSET_MENU_MY_TICKETS = `${AssetHelper.ASSET_ROOT}/menu-my-tickets-icon.png`;
    public static readonly ASSET_MENU_CURRENT_GAME = `${AssetHelper.ASSET_ROOT}/menu-current-game-icon.png`;
    public static readonly ASSET_MENU_PRIZES = `${AssetHelper.ASSET_ROOT}/menu-prizes-icon.png`;
    public static readonly ASSET_MENU_CAUSES = `${AssetHelper.ASSET_ROOT}/menu-causes-icon.png`;
    public static readonly ASSET_MENU_CHARITIES = `${AssetHelper.ASSET_ROOT}/menu-charities-icon.png`;
    public static readonly ASSET_MENU_AMBASSADORS = `${AssetHelper.ASSET_ROOT}/menu-ambassadors-icon.png`;
    public static readonly ASSET_MENU_REFER_FRIEND = `${AssetHelper.ASSET_ROOT}/menu-refer-friend-icon.png`;
    public static readonly ASSET_LANGUAGE_MENU_ICON = `${AssetHelper.ASSET_ROOT}/language-menu-icon.png`;
    
    /**
     * Convert an asset URL into an absolute URL.
     */
    public static assetURLToAbsoluteURL(assetURL: string): string {
        Debug.Untested();
        Debug.AssertString(assetURL);

        return assetURL.replace(AssetHelper.ASSET_ROOT, Constants.ASSET_ROOT);
    }

}   // AssetHelper
