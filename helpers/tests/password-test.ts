var valid_passwords = [
    'Passw0rd'
];

var invalid_passwords = [
    null,
    '',
    'a',
    'aA0',
    'aaaaaa',
    'AAAAAA',
    '000000'
];

