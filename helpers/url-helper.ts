import { Debug } from './debug';
import { Image } from '../generic/common-object-model';
import { Constants } from '../constants';

/**
 * Random number helper class.
 * The numbers are NOT random yet - this is just a mock for now.
 */
export class URLHelper {

    /**
     * Get a random number.
     */
    public static replaceURLParameter(url: string, paramterName: string, argument: string): string {
        Debug.Tested();

        return url.replace(`{${paramterName}}`, argument);
    }

}   // URLHelper
