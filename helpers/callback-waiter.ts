import { Debug } from './debug';

export class CallbackWaiter {
    
    /**
     * Callback functions.
     */
    private Callbacks: Function[] = [];

    /**
     * Constructor.
     */
    constructor(callback: Function) {
        Debug.Tested();
        Debug.AssertValid(callback);

        this.AddCallback(callback);
    }

    /**
     * Add a callback.
     */
    public AddCallback(callback: Function) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Callbacks);

        this.Callbacks.push(callback);
    }

    /**
     * Call the callbacks.
     */
    public CallCallbacks(error: Error, value: any) {
        Debug.Tested();
        Debug.AssertErrorOrObject(error, value);
        Debug.AssertValid(this.Callbacks);

        this.Callbacks.forEach(callback => {
            callback(error, value);
        });
    }

    /**
     * Call the callbacks (without an error which is already handled by the caller).
     */
    public CallCallbacksNoError(value: any) {
        Debug.Tested();
        Debug.AssertValidOrNull(value);
        Debug.AssertValid(this.Callbacks);

        this.Callbacks.forEach(callback => {
            callback(value);
        });
    }

}   // CallbackWaiter
