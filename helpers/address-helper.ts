import { Debug } from './debug';
import { Address, UserDetails } from '../data-models/identity-model';

/**
 * Class containing address helper methods.
 */
export class AddressHelper {

    /**
     * Get a user's address as a string.
     */
    public static getUserAddressText(user: UserDetails, callback: (addressText: string) => void) {
        Debug.Tested();
        Debug.AssertValid(user);
        Debug.AssertValid(callback);

        AddressHelper.addressFieldsToText(user.Address1, user.Address2, user.Address3, user.Address4, user.City, user.Region, user.PostalCode, callback);
    }

    /**
     * Convert an address into a multi-line string (possibly asynchronously).
     */
    public static addressToText(address: Address, callback: (addressText: string) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(address);

        if (address != null) {
            AddressHelper.addressFieldsToText(address.Line1, address.Line2, address.Line3, address.Line4, address.City, address.Region, address.PostalCode, callback);
        } else {
            callback('');
        }
    }

    /**
     * Convert an address into a multi-line string (possibly asynchronously).
     */
    public static addressFieldsToText(line1: string,
                                      line2: string,
                                      line3: string,
                                      line4: string,
                                      city: string,
                                      region: string,
                                      postalCode: string,
                                      callback: (addressText: string) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(line1);
        Debug.AssertValidOrNull(line2);
        Debug.AssertValidOrNull(line3);
        Debug.AssertValidOrNull(line4);
        Debug.AssertValidOrNull(city);
        Debug.AssertValidOrNull(region);
        Debug.AssertValidOrNull(postalCode);

        let addressText = '';
        if (!!line1) {
            Debug.Untested();
            addressText += line1;
        }
        if (!!line2) {
            Debug.Untested();
            addressText += line2;
        }
        if (!!line3) {
            Debug.Untested();
            addressText += line3;
        }
        if (!!line4) {
            Debug.Untested();
            addressText += line4;
        }
        if (!!city) {
            Debug.Untested();
            addressText += city;
        }
        if (!!region) {
            Debug.Untested();
            addressText += region;
        }
        if (!!postalCode) {
            Debug.Untested();
            addressText += postalCode;
        }
        callback(addressText);
    }

    /**
     * Create an address object and set it up with the user details' address fields.
     */
    public static addressFromUser(user: UserDetails): Address {
        Debug.Tested();
        Debug.AssertValid(user);

        let address = new Address();
        address.Line1 = user.Address1;
        address.Line2 = user.Address2;
        address.Line3 = user.Address3;
        address.Line4 = user.Address4;
        address.City = user.City;
        address.Region = user.Region;
        address.PostalCode = user.PostalCode;
        return address;
    }

    /**
     * Create an address object and set it up with the user details' address fields.
     */
    public static setUserAddress(user: UserDetails, address: Address) {
        Debug.Tested();
        Debug.AssertValid(user);
        Debug.AssertValid(address);

        user.Address1 = address.Line1;
        user.Address2 = address.Line2;
        user.Address3 = address.Line3;
        user.Address4 = address.Line4;
        user.City = address.City;
        user.Region = address.Region;
        user.Country = address.Country;
        user.PostalCode = address.PostalCode;
    }

}   // AddressHelper
