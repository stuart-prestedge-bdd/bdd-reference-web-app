import { Debug } from './debug';
import { Gender } from '../data-models/identity-model';
import { TextContentService } from '../services/text-content-service';
import { ErrorHelper } from './error-helper';

/**
 * Class containing gender helper methods.
 */
export class GenderHelper {

    /**
     * Is the passed in gender valid?
     */
    public static isValidGender(gender: Gender): boolean {
        Debug.Tested();
        Debug.AssertValidOrNull(gender);

        let retVal = false;
        if (gender != null) {
            if ((gender === Gender.male) || (gender === Gender.female) || (gender === Gender.unspecified)) {
                retVal = true;
            }
        }
        return retVal;
    }

    /**
     * Convert the gender enum to a text value (asynchronously).
     */
    public static genderToText(gender: Gender, callback: (genderText: string) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(gender);

        let textName;
        switch (gender) {
            default:
            case Gender.unknown: {
                textName = TextContentService.TEXT_NAME_UNKNOWN;
                break;
            }
            case Gender.male: {
                textName = TextContentService.TEXT_NAME_GENDER_MALE;
                break;
            }
            case Gender.female: {
                textName = TextContentService.TEXT_NAME_GENDER_FEMALE;
                break;
            }
            case Gender.unspecified: {
                textName = TextContentService.TEXT_NAME_GENDER_UNSPECIFIED;
                break;
            }
        }
        if (textName != null) {
            TextContentService.getTextValue(textName, (value) => {
                Debug.AssertString(value);
                callback(value);
            });
        }
    }

}   // GenderHelper
