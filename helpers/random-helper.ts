import { Debug } from './debug';
import { Image } from '../generic/common-object-model';
import { Constants } from '../constants';

/**
 * Random number helper class.
 * The numbers are NOT random yet - this is just a mock for now.
 */
export class RandomHelper {

    /**
     * The next 'random' number.
     */
    private static NextNumber = 1000;

    /**
     * Get a random number.
     */
    public static getRandomNumber(): number {
        Debug.Tested();

        return RandomHelper.NextNumber++;
    }

}   // RandomHelper
