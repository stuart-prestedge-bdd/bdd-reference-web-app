import { Debug } from './debug';

/**
 * Class containing API helper methods.
 */
export class APIHelper {

    /**
     * API result codes.
     */
    public static readonly RESULT_400_BAD_REQUEST = 400;
    public static readonly RESULT_401_UNAUTHORIZED = 401;
    public static readonly RESULT_403_FORBIDDEN = 403;
    public static readonly RESULT_404_NOT_FOUND = 404;
    public static readonly RESULT_200_OK = 200;
    public static readonly RESULT_201_CREATED = 201;
    public static readonly RESULT_204_NO_CONTENT = 204;

}   // APIHelper
