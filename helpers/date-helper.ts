import { Debug } from './debug';
import { DateOnly, Month } from '../data-models/identity-model';

/**
 * Class containing date helper methods.
 */
export class DateHelper {

    /**
     * Convert a timestamp (i.e. date/time) to a date.
     */
    public static timestampToDate(timestamp: Date): DateOnly {
        Debug.Untested();
        Debug.AssertValidOrNull(timestamp);

        let retVal: DateOnly = null;
        if (timestamp != null) {
            retVal = new DateOnly(timestamp.getUTCFullYear(), DateHelper.monthNumberToMonth(timestamp.getUTCMonth()), timestamp.getUTCDate());
        }
        return retVal;
    }

    /**
     * Convert a timestamp (i.e. date/time) to a date.
     */
    public static dateToTimestamp(date: DateOnly): Date {
        Debug.Tested();
        Debug.AssertValidOrNull(date);

        let retVal: Date = null;
        if (date != null) {
            let month = DateHelper.monthToMonthNumber(date.Month);
            retVal = new Date(Date.UTC(date.Year, month, date.Day));
        }
        return retVal;
    }

    /**
     * Convert a month number (returned from a Date) to a Month enum.
     */
    public static monthNumberToMonth(month: number): Month {
        Debug.Tested();

        switch (month) {
            case 1:
                return Month.jan;
            case 2:
                return Month.feb;
            case 3:
                return Month.mar;
            case 4:
                return Month.apr;
            case 5:
                return Month.may;
            case 6:
                return Month.jun;
            case 7:
                return Month.jul;
            case 8:
                return Month.aug;
            case 9:
                return Month.sep;
            case 10:
                return Month.oct;
            case 11:
                return Month.nov;
            case 12:
                return Month.dec;
        }
    }

    /**
     * Convert a month enum to a month number.
     */
    public static monthToMonthNumber(month: Month): number {
        Debug.Tested();

        switch (month) {
            case Month.jan:
                return 1;
            case Month.feb:
                return 2;
            case Month.mar:
                return 3;
            case Month.apr:
                return 4;
            case Month.may:
                return 5;
            case Month.jun:
                return 6;
            case Month.jul:
                return 7;
            case Month.aug:
                return 8;
            case Month.sep:
                return 9;
            case Month.oct:
                return 10;
            case Month.nov:
                return 11;
            case Month.dec:
                return 12;
        }
    }

    /**
     * Convert a date (no time) into a string (possibly asynchronously).
     */
    public static dateToText(date: DateOnly, callback: (dateText: string) => void) {
        Debug.TestedMock();
        Debug.AssertValidOrNull(date);

        let dateText = '';
        if (date != null) {
            dateText = `${date.Day}/${date.Month}/${date.Year}`;//??++USE LOCALE!
        }
        callback(dateText);
    }

    /**
     * Convert a timestamp into a string (possibly asynchronously).
     */
    public static timestampToText(timestamp: Date, callback: (timestampText: string) => void) {
        Debug.TestedMock();
        Debug.AssertValid(timestamp);
        Debug.AssertValid(callback);

        let timestampText = '';
        if (timestamp != null) {
            timestampText = `${timestamp.getDate()}/${timestamp.getMonth()}/${timestamp.getFullYear()} ${timestamp.getHours()}:${timestamp.getMinutes()}`;//??++USE LOCALE!
        }
        callback(timestampText);
    }

    /**
     * Convert a timestamp text string in the format YYYY-MM-DDTHH:MM:SS.mmmZ into a string.
     * Returns null if the conversion fails.
     */
    public static timestampTextToDate(timestamp: string): Date {
        Debug.Tested();
        Debug.AssertStringOrNull(timestamp);

        let retVal: Date = null;
        if (timestamp != null) {
            if (timestamp.length === 24) {
                if ((timestamp.substr(4, 1) === '-') &&
                    (timestamp.substr(7, 1) === '-') &&
                    (timestamp.substr(10, 1) === 'T') &&
                    (timestamp.substr(13, 1) === ':') &&
                    (timestamp.substr(16, 1) === ':') &&
                    (timestamp.substr(19, 1) === '.') &&
                    (timestamp.substr(23, 1) === 'Z')) {
                    let year = Number.parseInt(timestamp.substr(0, 4));
                    let month = Number.parseInt(timestamp.substr(5, 2));
                    let day = Number.parseInt(timestamp.substr(8, 2));
                    let hours = Number.parseInt(timestamp.substr(11, 2));
                    let minutes = Number.parseInt(timestamp.substr(14, 2));
                    let seconds = Number.parseInt(timestamp.substr(17, 2));
                    retVal = new Date(Date.UTC(year, month, day, hours, minutes, seconds));
                }
            }
        }
        return retVal;
    }

    /**
     * API dates
     */

    /**
     * Is the passed in API date string valid?
     * If must be a string in the format YYYY-MM-DD.
     */
    public static isValidAPIDate(date: string): boolean {
         Debug.Tested();
         Debug.AssertValidOrNull(date);

         return (DateHelper.APIDateToDateOnly(date) != null);
    }

    /**
     * Convert an API date string into a date only object.
     * If must be a string in the format YYYY-MM-DD.
     */
    public static APIDateToDateOnly(date: string): DateOnly {
        Debug.Tested();
        Debug.AssertValidOrNull(date);

        let retVal: DateOnly = null;
        if (date != null) {
            if (date.length === 10) {
                if ((date[4] === '-') && (date[7] === '-')) {
                    let year = Number.parseInt(date.slice(0, 4));
                    let month = Number.parseInt(date.slice(5, 7));
                    let day = Number.parseInt(date.slice(8, 10));
                    if ((year >= 1900) && (year < 2100)) {
                        if ((month >= 1) && (month <= 12)) {
                            if ((day >= 1) && (day <= 31)) {
                                retVal = new DateOnly(year, DateHelper.monthNumberToMonth(month), day);
                            }
                        }
                    }
                }
            }
        }
        return retVal;
   }

     /**
      * Convert a date only object to an API date string.
      * If will be a string in the format YYYY-MM-DD.
      * If null is passed in then null is returned.
      */
     public static DateOnlyToAPIDate(date: DateOnly): string {
        Debug.Tested();
        Debug.AssertValidOrNull(date);

        let retVal: string = null;
        if (date != null) {
            let year = ('000' + date.Year).slice(-4);
            let month = ('0' + DateHelper.monthToMonthNumber(date.Month)).slice(-2);
            let day = ('0' + date.Day).slice(-2);
            retVal = `${year}-${month}-${day}`
        }
        return retVal;
    }

    /**
     * Convert an API timestamp string (in UTC) to a date/time in the current locale.
     */
    public static APITimestampToDate(timestamp: string): Date {
        Debug.Uncoded();
        return null;
    }

    /**
     * Convert a date/time in the current locale to an API timestamp string (in UTC).
     */
    public static DateToAPITimestamp(date: Date): string {
        Debug.Uncoded();
        return null;
    }

}   // DateHelper
