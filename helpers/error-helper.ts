import { Debug } from './debug';
import { ToastHelper } from './toast-helper';

export class ErrorHelper {

    /**
     * Check the error passed in.
     * If it is null, return true.
     * If there is an error (i.e. it is not null) then show an error message and return false.
     */
    public static checkError(error: Error, errorTextName?: string): boolean {
        Debug.Tested();
        Debug.AssertValidOrNull(error);
        Debug.AssertStringOrNull(errorTextName);
        
        let retVal = true;
        if (error != null) {
            Debug.AssertValidOrNull(error.message);
            if ((errorTextName != null) || (error.message != null)) {
                ToastHelper.showError(errorTextName || error.message);
            }
            retVal = false;
        }
        return retVal;
    }

}   // ErrorHelper
