import { Debug } from "./debug";

/**
 * Our own implementation of map.
 * Avoids reliance on ES6.
 */
export class ObjectMap<T> {

    /**
     * Constructor.
     * Optionally takes an array of entries (each with a string and value).
     */
    constructor(entries?: (string | any)[][]) {
        Debug.Tested();
        Debug.AssertValidOrNull(entries);

        if (entries != null) {
            for (let entry of entries) {
                Debug.AssertValid(entry);
                Debug.Assert(entry.length === 2);
                this.set(entry[0], entry[1]);
            }
        }
    }

    /**
     * Does the map have the entry specified?
     */
    public has(name: string): boolean {
        Debug.Tested();
        Debug.AssertString(name);

        return (Object.keys(this).indexOf(name) !== -1);
    }

    /**
     * Get a map value. 
     * If the entry does not exist, undefined is returned.
     */
    public get(name: string): T {
        Debug.Tested();
        Debug.AssertString(name);

        return this[name];
    }

    /**
     * Set a map value. 
     * Replaces any existing value.
     */
    public set(name: string, value: T) {
        Debug.Tested();
        Debug.AssertString(name);
        Debug.AssertValidOrNull(value);

        this[name] = value;
    }

    /**
     * Iterate all the values.
     */
    public forEach(callback: (value: T) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        for (let key of Object.keys(this)) {
            callback(this[key]);
        }
    }
    
    /**
     * Iterate all the values.
     */
    public forEachKey(callback: (key: string) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        for (let key of Object.keys(this)) {
            callback(key);
        }
    }
    
}   // ObjectMap
