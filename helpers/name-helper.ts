import { Debug } from './debug';
import { UserDetails } from '../data-models/identity-model';

/**
 * Class containing address helper methods.
 */
export class NameHelper {

    /**
     * Get the informal display name of a user.
     */
    public static getUserInformalDisplayName(user: UserDetails): string {
        Debug.Untested();
        Debug.AssertValid(user);

        return NameHelper.getInformalDisplayName(user.GivenName, user.FamilyName, user.PreferredLanguage, user.FullName);
    }

    /**
     * Get the informal display name.
     */
    public static getInformalDisplayName(givenName: string, familyName: string, preferredName: string, fullName: string): string {
        Debug.Untested();
        Debug.AssertValidOrNull(givenName);
        Debug.AssertValidOrNull(familyName);
        Debug.AssertValidOrNull(preferredName);
        Debug.AssertValidOrNull(fullName);

        let retVal: string;
        if (!!preferredName) {
            retVal = preferredName;
        } else if (!!givenName) {
            retVal = givenName;
        } else if (!!fullName) {
            retVal = fullName;
        } else {
            retVal = (familyName || '');
        }
        return retVal;
    }

    /**
     * Get the formal display name of a user.
     */
    public static getUserFormalDisplayName(user: UserDetails): string {
        Debug.Tested();
        Debug.AssertValid(user);

        return NameHelper.getFormalDisplayName(user.GivenName, user.FamilyName, user.PreferredLanguage, user.FullName);
    }

    /**
     * Get the formal display name.
     */
    public static getFormalDisplayName(givenName: string, familyName: string, preferredName: string, fullName: string): string {
        Debug.Tested();
        Debug.AssertValidOrNull(givenName);
        Debug.AssertValidOrNull(familyName);
        Debug.AssertValidOrNull(preferredName);
        Debug.AssertValidOrNull(fullName);

        let retVal: string;
        if (!!fullName) {
            retVal = fullName;
        } else if (!!preferredName) {
            retVal = preferredName;
        } else {
            retVal = (givenName || '') + ' ' + (familyName || '');
        }
        return retVal;
    }

}   // NameHelper
