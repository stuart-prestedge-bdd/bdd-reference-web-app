import { Debug } from './debug';

/**
 * Class containing email helper methods.
 */
export class EmailHelper {

    /**
     * Is the passed in email address valid?
     */
    public static isValidEmailAddress(emailAddress: string): boolean {
        Debug.TestedMock();
        Debug.AssertValidOrNull(emailAddress);

        let retVal = false;
        if (emailAddress != null) {
            if (emailAddress.length <= 254) {   // See https://www.rfc-editor.org/errata_search.php?eid=1690
                retVal = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(emailAddress);
            }
        }
        return retVal;
    }

}   // EmailHelper
