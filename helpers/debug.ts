/**
 * Class containing debug methods.
 */
export class Debug {

    /**
     * Quick mode.
     * Does not properly simulate asynchronous callback - used for logical testing.
     */
    private static QuickMode = true;

    public static Unreachable() {
        console.log('Unreachable');
    }
    public static Uncoded() {
        console.log('Uncoded');
    }
    public static Untested() {
        console.log('Untested');
    }
    public static UntestedMock() {
        console.log('UntestedMock');
    }
    public static Tested() {}
    public static TestedMock() {}

    /**
     * Assertion methods.
     */
    public static Assert(b: boolean, info?: string)
    {
        if (!b) {
            console.log('Assertion: ' + (info || ''));
        }
    }
    public static AssertNull(o: any, info?: string) { Debug.Assert(o == null, info); }
    public static AssertValid(o: any, info?: string) { Debug.Assert(o != null, info); }
    public static AssertValidOrNull(o: any) { }
    public static AssertString(s: string, info?: string) { Debug.AssertValid(s, info); Debug.Assert(!!s, info); }
    public static AssertStringOrNull(s: string, info?: string) { if (s != null) { Debug.AssertString(s, info); } }
    public static AssertErrorOrObject(error: Error, obj: any, info?: string) { Debug.AssertValidOrNull(error); Debug.AssertValidOrNull(obj); Debug.Assert(((error == null) && (obj != null)) || ((error != null) && (obj == null)), info); }

    /**
     * Scheduling callbacks.
     */
    public static scheduleCallback(callback: () => void) {
        if (Debug.QuickMode) {
            callback();
        } else {
            setTimeout(callback, 1);
        }
    }

}   // Debug
