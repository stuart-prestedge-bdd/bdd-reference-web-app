import { Debug } from './debug';
import { TextContentService } from '../services/text-content-service';

/**
 * Class containing helper methods to show toast popups.
 */
export class ToastHelper {

    /**
     * The callback to the application to actually show the toast.
     */
    private static ShowToastCallback: (info: boolean, text: string) => void;

    /**
     * Show an info toast.
     */
    public static setShowToastCallback(showToastCallback: (info: boolean, text: string) => void) {
        Debug.Tested();
        Debug.AssertValid(showToastCallback);

        this.ShowToastCallback = showToastCallback;
    }

    /**
     * Show an info toast.
     */
    public static showInfo(infoTextName: string) {
        Debug.Tested();
        Debug.AssertString(infoTextName);
        Debug.AssertValid(this.ShowToastCallback);

        TextContentService.getTextValue(infoTextName, (value) => {
            Debug.AssertString(value);
            this.ShowToastCallback(true, value);
        });
    }

    /**
     * Show an error toast.
     */
    public static showError(errorTextName: string) {
        Debug.Tested();
        Debug.AssertString(errorTextName);
        Debug.AssertValid(this.ShowToastCallback);

        TextContentService.getTextValue(errorTextName, (value) => {
            Debug.AssertString(value);
            this.ShowToastCallback(false, value);
        });
    }

}   // ToastHelper
