import { Debug } from './debug';
import { Image } from '../generic/common-object-model';

export class ImageHelper {

    /**
     * Select an appropriate image from the images passed in.
     */
    public static selectImage(images: Image[]): Image {
        Debug.TestedMock();
        Debug.AssertValid(images);

        let image = null;
        if (images.length > 0) {
            image = images[0];
        }
        return image;
    }

}   // ImageHelper
