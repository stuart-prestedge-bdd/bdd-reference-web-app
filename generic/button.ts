import { Component } from './component';
import { Debug } from '../helpers/debug';
import { TextField } from './text-field';
import { Action } from './action';
import { TextContentService } from '../services/text-content-service';
import { ErrorHelper } from '../helpers/error-helper';

/**
 * Class representing a button.
 */
export class Button extends Component {

    /**
     * The text value name of the label to show.
     * Loads the text value from the text service initially and on language change.
     */
    private LabelName: string;

    /**
     * The button's label.
     */
    public readonly Label = new TextField();

    /**
     * Constructor.
     * May or may not be given a label at this time.
     */
    constructor(labelName?: string) {
        super();
        Debug.Tested();
        Debug.AssertStringOrNull(labelName);

        this.LabelName = labelName;
    }

    /**
     * Load the static text.
     * If there is a label name for this button then load the label text value.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertStringOrNull(this.LabelName);

        let this_ = this;
        super.doLoadStaticText(() => {
            if (this_.LabelName != null) {
                TextContentService.getTextValue(this_.LabelName, (value) => {
                    Debug.AssertString(value);
                    this_.setLabel(value);
                    callback();
                });
            } else {
                callback();
            }
        });
    }

    /**
     * Set the label.
     */
    public setLabel(label: string) {
        Debug.Tested();
        Debug.AssertString(label);

        this.Label.setValue(label);
    }

}   // Button
