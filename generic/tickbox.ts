import { Component } from './component';
import { Debug } from '../helpers/debug';
import { TextField } from './text-field';
import { Action } from './action';
import { TextContentService } from '../services/text-content-service';

/**
 * Class representing a tickbox.
 */
export class Tickbox extends Component {

    /**
     * The text value name of the label to show.
     * Loads the text value from the text service initially and on language change.
     */
    private LabelName: string;

    /**
     * The tick box's label.
     */
    public readonly Label = new TextField();

    /**
     * Is the tickbox ticked?
     */
    private IsTicked: boolean = false;

    /**
     * The optional inner action to perform when clicked / enter pressed.
     */
    protected InnerExecuteAction: Action;

    /**
     * Constructor.
     * May or may not be given a label at this time.
     */
    constructor(labelName?: string) {
        super();
        Debug.Tested();
        Debug.AssertStringOrNull(labelName);

        this.LabelName = labelName;
        this.setExecuteAction(null);
    }

    /**
     * Load the static text.
     * If there is a label name for this tickbox then load the label text value.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertStringOrNull(this.LabelName);

        let this_ = this;
        super.doLoadStaticText(() => {
            if (this_.LabelName != null) {
                TextContentService.getTextValue(this_.LabelName, (value) => {
                    Debug.AssertString(value);
                    this_.setLabel(value);
                    callback();
                });
            } else {
                callback();
            }
        });
    }

    /**
     * Set the label.
     */
    public setLabel(label: string) {
        Debug.Tested();
        Debug.AssertString(label);

        this.Label.setValue(label);
    }

    /**
     * Set the action.
     */
    public setExecuteAction(executeAction: Action) {
        Debug.Tested();
        Debug.AssertValidOrNull(executeAction);

        let this_ = this;     
        this_.InnerExecuteAction = executeAction;   
        super.setExecuteAction(new Action(null, (context, callback) => {
            Debug.Untested();
            Debug.AssertValid(callback);
            this_.IsTicked = !this_.IsTicked;
            this_.executeInner(callback);
        }));
    }

    /**
     * Called when the tickbox is executed.
     */
    private executeInner(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValidOrNull(this.InnerExecuteAction);
        Debug.Assert(!this.getHidden());

        if (this.InnerExecuteAction != null) {
            this.InnerExecuteAction.perform(callback);
        } else {
            callback();
        }
    }

    /**
     * Is the tickbox ticked?
     */
    public getIsTicked(): boolean {
        Debug.Tested();

        return this.IsTicked;
    }

    /**
     * Is the tickbox ticked?
     */
    public setIsTicked(isTicked: boolean, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValidOrNull(this.ExecuteAction);

        if (this.IsTicked !== isTicked) {
            this.IsTicked = isTicked;
            this.executeInner(callback);
        } else {
            callback();
        }
    }

}   // Tickbox
