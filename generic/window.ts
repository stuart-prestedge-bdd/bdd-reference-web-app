import { Debug } from '../helpers/debug';
import { Canvas } from './canvas';

/**
 * Class representing the browser window.
 * There is only one instance of this class and it contains a single instance of the canvas.
 */
class WindowX {
    private Canvas: Canvas;
    constructor() {
        this.Canvas = new Canvas();
    }
}   // WindowX

