import { Debug } from '../helpers/debug';
import { Component } from './component';
import { Action } from './action';
import { ImageComponent } from './image-component';

/**
 * Class representing a menu item.
 */
export class MenuItem {

    /**
     * The icon to show.
     */
    public Icon: string;

    /**
     * The label. Language dependant.
     */
    public Label: string;

    /**
     * The action to perform when the menu item is executed.
     */
    public Action: Action;

    /**
     * Constructor.
     */
    constructor(icon: string, action: Action) {
        Debug.Tested();
        Debug.AssertString(icon);
        Debug.AssertValid(action);

        this.Icon = icon;
        this.Action = action;
    }
    
}   // MenuItem

/**
 * Class representing a menu.
 */
export abstract class MenuBase extends Component {

    /**
     * The icon to show for the closed menu.
     */
    public readonly Icon = new ImageComponent();

    /**
     * The menu items.
     */
    public readonly MenuItems: MenuItem[] = [];

    /**
     * Is the actual menu shown?
     */
    private IsMenuShown: boolean = false;

    /**
     * Constructor.
     */
    constructor() {
        super();
        Debug.Tested();

        let this_ = this;        
        this_.setExecuteAction(new Action(null, (context, callback) => {
            Debug.Untested();
            this_.IsMenuShown = !this_.IsMenuShown;
            callback();
        }));
    }

    /**
     * Get the IsMenuShown flag.
     */
    public getIsMenuShown(): boolean {
        Debug.Tested();

        return this.IsMenuShown;
    }

    /**
     * Set the IsMenuShown flag.
     */
    public setIsMenuShown(isMenuShown: boolean) {
        Debug.Tested();

        this.IsMenuShown = isMenuShown;
    }

    /**
     * Setup the menu.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }

    /**
     * Execute a menu item.
     */
    public executeItem(menuItem: MenuItem, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(menuItem);
        Debug.AssertValid(menuItem.Action);
        Debug.AssertValid(callback);

        menuItem.Action.perform(callback);
    }

}   // MenuBase
