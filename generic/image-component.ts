import { Debug } from '../helpers/debug';
import { Component } from './component';
import { Image } from './common-object-model';

/**
 * Class representing an image.
 */
export class ImageComponent extends Component {

    /**
     * The actual image information.
     */
    private Image: Image;

    /**
     * Constructor.
     */
    constructor(image?: Image) {
        Debug.Tested();

        super();
        this.Image = image;
    }

    /**
     * Get the image.
     */
    public getImage(): Image {
        Debug.Tested();
        Debug.AssertValidOrNull(this.Image);

        return this.Image;
    }

    /**
     * Set the image.
     * This can be done any time after the component is created.
     */
    public setImage(image: Image, callback: () => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(image);
        Debug.AssertValid(callback);

        this.Image = image;
        this.setup(callback, true);
    }

    /**
     * Setup the component.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }

    /**
     * Reset (clear) the component.
     */
    public reset(callback: () => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        this.setImage(null, callback);
    }

}   // ImageComponent
