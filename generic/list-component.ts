import { Debug } from '../helpers/debug';
import { Component } from './component';
import { PageBase } from './page-base';

/**
 * Common list actions.
 */
export const LIST_ACTION_OPEN = 'open';

/**
 * Class representing the video tile.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47022614/2.2.4.13.+Video+Tile
 */
export class ListComponent extends Component {

    /**
     * The objects.
     */
    private Objects: any[];

    /**
     * The 'perform action' callback.
     */
    private PerformActionCallback: (context: object, action: string, obj: any, callback: () => void) => void;
    private PerformActionContext: object;

    /**
     * Constructor.
     */
    constructor() {
        Debug.Tested();

        super();
    }

    /**
     * Set the URL.
     * This can be done any time after the list component is created.
     */
    public setObjects(objects: any[], callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(objects);
        Debug.AssertValid(callback);

        this.Objects = objects;
        this.setup(callback, true);
    }

    /**
     * Set the perform action callback.
     */
    public setPerformActionCallback(performActionCallback: (context: any, action: string, obj: any, callback: () => void) => void, performActionContext: any) {
        Debug.Tested();
        Debug.AssertValid(performActionCallback);
        Debug.AssertValid(performActionContext);

        this.PerformActionCallback = performActionCallback;
        this.PerformActionContext = performActionContext;
    }

    /**
     * Setup the component.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }

    /**
     * Reset (clear) the component.
     */
    public reset(callback: () => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        this.setObjects(null, callback);
    }

    /**
     * Perform an action on an object.
     */
    public performAction(action: string, index: number, callback: () => void) {
        Debug.Tested();
        Debug.AssertString(action);
        Debug.AssertValid(callback);
        Debug.AssertValid(this.PerformActionCallback);
        Debug.AssertValid(this.PerformActionContext);

        if (this.Objects != null) {
            if ((index >= 0) && (index < this.Objects.length)) {
                let obj = this.Objects[index];
                Debug.AssertValid(obj);
                this.PerformActionCallback(this.PerformActionContext, action, obj, callback);
            }
        } else {
            callback();
        }
    }

}   // ListComponent
