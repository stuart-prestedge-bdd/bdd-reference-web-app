import { Debug } from '../helpers/debug';
import { Component } from './component';
import { IApplication } from '../interfaces/application';
import { Constants } from '../constants';
import { ObjectMap } from '../helpers/object-map';

/**
 * Abstract class representing base class for all pages.
 */
export class PageBase extends Component {

    /**
     * The application.
     */
    protected Application: IApplication;

    /**
     * The page's unique URL.
     */
    private URL: string;

    /**
     * Does this page require authentication.
     */
    private RequiresUser: boolean;

    /**
     * The possible parameters for the page.
     */
    private Parameters: string[] = [];

    /**
     * The current page arguments.
     */
    private CurrentArguments: ObjectMap<string>;

    /**
     * Constructor.
     */
    constructor(application: IApplication,
                url: string,
                requiresUser: boolean = true) {
        super();
        Debug.Tested();
        Debug.AssertString(url);

        this.Application = application;
        this.URL = url;
        this.RequiresUser = requiresUser;
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }
    
    /**
     * Get the page URL.
     */
    public getURL(): string {
        Debug.Tested();
        Debug.AssertString(this.URL);

        return this.URL;
    }

    /**
     * Add a parameter.
     */
    public addParameter(parameter: string) {
        Debug.Tested();
        Debug.AssertString(parameter);
        Debug.AssertValid(this.Parameters);

        this.Parameters.push(parameter);
    }

    /**
     * Get an argument.
     */
    public getArgument(argument: string): string {
        Debug.Tested();
        Debug.AssertValid(this.CurrentArguments);

        return this.CurrentArguments.get(argument);
    }

    /**
     * Called when the page is opened.
     * Will be passed arguments from the URL.
     * If a user is required to be logged in and no user is logged in then the home page is navigated to.
     */
    public open(args: ObjectMap<string>, callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(args);
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);

        let this_ = this;        
        this_.CurrentArguments = args;
        this_.setup(() => {
            this_.Application.getCurrentUser((currentUser, accessToken) => {
                Debug.AssertValidOrNull(currentUser);
                if (!this_.RequiresUser || !!currentUser) {
                    this_.doOpen(callback);
                } else {
                    this_.Application.navigateTo(Constants.PAGE_URL_HOME, callback);
                }
            });
        });
    }

    /**
     * Called when the page is opened.
     * Override to perform specific actions.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        callback(this);
    }

    /**
     * Called when the page is closed.
     */
    public close() {
        Debug.Tested();
    }

}   // PageBase
