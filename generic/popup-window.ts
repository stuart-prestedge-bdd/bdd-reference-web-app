import { Debug } from "../helpers/debug";
import { ObjectMap } from "../helpers/object-map";

export abstract class PopupWindow {

    /**
     * The name of the button input parameter.
     */
    static readonly ButtonInputName = '_button';
    static readonly ButtonInputValueOK = 'OK';
    static readonly ButtonInputValueCancel = 'Cancel';

    /**
     * Unique identifier of this popup window.
     */
    private ID: string;

    /**
     * Constructor.
     */
    constructor(id: string) {
        Debug.Tested();
        Debug.AssertString(id);

        this.ID = id;
    }

    /**
     * Get the ID of this popup window.
     */
    public getID(): string {
        Debug.Tested();
        Debug.AssertString(this.ID);

        return this.ID;
    }

    /**
     * Called when the popup is closed.
     */
    public close(inputs: ObjectMap<any>) {
        Debug.Tested();
        Debug.AssertValid(inputs);

        this.doClose(inputs);
    }
    
    /**
     * Override to perform operations on close.
     */
    protected doClose(inputs: ObjectMap<any>) {
        Debug.Tested();
        Debug.AssertValid(inputs);
    }
    
}   // PopupWindow
