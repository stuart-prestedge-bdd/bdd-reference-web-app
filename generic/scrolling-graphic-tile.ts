import { Debug } from '../helpers/debug';
import { Component } from '../generic/component';
import { TextField } from './text-field';
import { Image } from './common-object-model';

/**
 * Class representing the scrolling graphic tile.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47219056/2.2.4.8.+Scrolling+Graphic+Tile
 */
export class ScrollingGraphicTile extends Component {

    /**
     * The title.
     */
    public readonly Title = new TextField();

    /**
     * The images.
     */
    public readonly Images: Image[] = [];

    /**
     * Constructor.
     */
    constructor(titleValueName: string) {
        super();
        Debug.Tested();
        Debug.AssertString(titleValueName);

        this.Title.setValueName(titleValueName);
    }

    /**
     * Setup the component.
     */
    protected doSetup(callback: () => void) {
        Debug.Untested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);

        let this_ = this;
        super.doSetup(() => {
            this_.Title.setup(callback);
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Untested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.Title.languageChanged(callback);
        });
    }

    /**
     * Add an image.
     * This can be done any time after the component is created.
     */
    public addImage(image: Image) {
        Debug.Untested();
        Debug.AssertValid(image);
        Debug.AssertValid(this.Images);

        this.Images.push(image);
    }

    /**
     * Reset (clear) the component.
     */
    public reset() {
        Debug.Untested();
        Debug.AssertValid(this.Images);

        this.Images.splice(0);
    }

}   // ScrollingGraphicTile
