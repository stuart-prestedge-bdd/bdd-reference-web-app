import { Debug } from '../helpers/debug';
import { Action } from './action';
import { Component } from './component';
import { TextContentService } from '../services/text-content-service';
import { ErrorHelper } from '../helpers/error-helper';

/**
 * Class representing a text field.
 */
export class TextField extends Component {

    /**
     * The text value name of the text to show.
     * Loads the text value from the text service initially and on language change.
     */
    private ValueName: string;

    /**
     * The text value to show.
     */
    private Value: string;

    /**
     * Constructor.
     */
    constructor(valueName?: string) {
        Debug.Tested();
        Debug.AssertStringOrNull(valueName);

        super();
        this.ValueName = valueName;
    }

    /**
     * Load the static text.
     * If there is a value name for this text field then load the text value.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertStringOrNull(this.ValueName);

        let this_ = this;
        super.doLoadStaticText(() => {
            if (this_.ValueName != null) {
                TextContentService.getTextValue(this_.ValueName, (value) => {
                    Debug.AssertString(value);
                    this_.setValue(value);
                    callback();
                });
            } else {
                callback();
            }
        });
    }

    /**
     * Set the input field value name.
     * Must be called before this component is setup for the first time.
     */
    public setValueName(valueName: string) {
        Debug.Tested();
        Debug.AssertString(valueName);

        this.ValueName = valueName;
    }

    /**
     * Get the input field value.
     */
    public getValue(): string {
        Debug.Tested();
        Debug.AssertValidOrNull(this.Value);

        return this.Value;
    }

    /**
     * Set the input field value.
     * Allows null and empty. It is up to the page using this field to validate the value.
     */
    public setValue(value: string) {
        Debug.Tested();
        Debug.AssertValidOrNull(value);

        this.Value = value;
    }

}   // TextField
