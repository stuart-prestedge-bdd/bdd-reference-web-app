import { Debug } from '../helpers/debug';
import { Action } from './action';

/**
 * Abstract class representing a component.
 */
export class Component {

    /**
     * Setup yet?
     */
    private IsSetup = false;

    /**
     * Is this component hidden?
     */
    protected Hidden = false;

    /**
     * The optional action to perform when clicked / enter pressed.
     */
    protected ExecuteAction: Action;

    /**
     * Constructor.
     */
    constructor() {
        Debug.Tested();
    }

    /**
     * Setup the component.
     */
    public setup(callback: () => void, force: boolean = false) {
        Debug.Tested();
        Debug.AssertValid(callback);

        if (!this.IsSetup || force) {
            this.doSetup(callback);
            this.IsSetup = true;
        } else {
            callback();
        }
    }

    /**
     * Abstract method to do the setup.
     * Override to setup.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        this.doLoadStaticText(callback);
    }

    /**
     * Abstract method to load the static text.
     * Override to load static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        callback();
    }

    /**
     * Called when the language changes.
     */
    public languageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        this.doLanguageChanged(callback);
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        this.doLoadStaticText(callback);
    }

    /**
     * Set the hidden flag.
     */
    public setHidden(hidden: boolean) {
        Debug.Tested();

        this.Hidden = hidden;
    }

    /**
     * Get the hidden flag.
     */
    public getHidden(): boolean {
        Debug.Tested();

        return this.Hidden;
    }

    /**
     * Set the action.
     */
    public setExecuteAction(executeAction: Action) {
        Debug.Tested();
        Debug.AssertValid(executeAction);

        this.ExecuteAction = executeAction;
    }

    /**
     * Called when the button is executed.
     */
    public execute(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValidOrNull(this.ExecuteAction);
        Debug.Assert(!this.getHidden());

        if (this.ExecuteAction != null) {
            this.ExecuteAction.perform(callback);
        } else {
            callback();
        }
    }

}   // Component

