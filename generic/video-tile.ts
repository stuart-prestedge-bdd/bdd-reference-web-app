import { Debug } from '../helpers/debug';
import { Component } from './component';

/**
 * Class representing the video tile.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47022614/2.2.4.13.+Video+Tile
 */
export class VideoTile extends Component {

    /**
     * The URL of the video.
     */
    private URL: string;

    /**
     * Constructor.
     */
    constructor(url?: string) {
        Debug.Tested();
        Debug.AssertStringOrNull(url);

        super();
        this.URL = url;
    }

    /**
     * Get the URL.
     */
    public getURL(): string {
        Debug.Tested();

        return this.URL;
    }

    /**
     * Set the URL.
     * This can be done any time after the video tile component is created.
     */
    public setURL(url: string, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        this.URL = url;
        this.setup(callback, true);
    }

    /**
     * Setup the component.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }

    /**
     * Reset (clear) the component.
     */
    public reset(callback: () => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        this.setURL(null, callback);
    }

}   // VideoTile
