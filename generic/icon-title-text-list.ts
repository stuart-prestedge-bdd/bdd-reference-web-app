import { Debug } from '../helpers/debug';
import { Component } from './component';
import { Action } from './action';
import { Text, Image } from './common-object-model';

/**
 * Class representing an icon/title/text item.
 */
export class IconTitleTextItem {
    public Icon: Image;
    public Title: string;
    public Description: Text;
    public ClickAction: Action;
}   // IconTitleTextItem

/**
 * Class representing the icon/title/description list.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/48595080/2.2.4.16.+Icon+Title+Text+List
 */
export class IconTitleTextList extends Component {

    /**
     * The items to show.
     */
    public readonly Items: IconTitleTextItem[] = [];

    /**
     * Constructor.
     */
    constructor() {
        super();
        Debug.Tested();
    }

    /**
     * Setup the component.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }

    /**
     * Reset (clear) the component.
     */
    public reset(callback: () => void) {
        Debug.Untested();

        this.Items.splice(0);
        this.setup(callback, true);
    }

    /**
     * Called when an item is clicked.
     */
    public addItem(icon: Image, title: string, description: Text, clickAction: Action) {
        Debug.Tested();
        Debug.AssertValid(icon);
        Debug.AssertString(title);
        Debug.AssertValid(description);
        Debug.AssertValid(clickAction);

        this.Items.push(<IconTitleTextItem>{
            Icon: icon,
            Title: title,
            Description: description,
            ClickAction: clickAction
        });
    }

    /**
     * Called when an item is clicked.
     */
    public itemClicked(item: IconTitleTextItem, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(item);
        Debug.AssertValid(item.ClickAction);
        Debug.AssertValid(callback);

        item.ClickAction.perform(callback);
    }

}   // IconTitleTextList
