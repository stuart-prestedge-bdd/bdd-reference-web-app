import { Debug } from '../helpers/debug';
import { WebApp } from '../components/web-app';
import { PopupWindow } from './popup-window';

/**
 * Class representing an action.
 * This can be to open a page, open an external page or show a popup or call a function.
 */
export class Action {

    /**
     * The page URL to navigate to when the button is clicked.
     * Takes precedence over action callback and popup to show.
     */
    private PageURL: string;

    /**
     * The action callback and context to call when the button is clicked.
     * Takes precedence over the popup to show.
     */
    private ActionCallback: (context: object, callback: () => void) => void;
    private ActionContext: object;

    /**
     * The popup window.
     * Lowest precedence.
     */
    private PopupWindow: PopupWindow;

    /**
     * Constructor.
     */
    constructor(pageUrl: string, actionCallback?: (context: object, callback: () => void) => void, actionContext?: object) {
        Debug.Tested();
        Debug.AssertStringOrNull(pageUrl);
        Debug.AssertValidOrNull(actionCallback);
        Debug.AssertValidOrNull(actionContext);
        Debug.Assert((pageUrl != null) || (actionCallback != null));

        this.PageURL = pageUrl;
        this.ActionCallback = actionCallback;
        this.ActionContext = actionContext;
    }

    /**
     * Perform the action.
     */
    public perform(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertStringOrNull(this.PageURL);
        Debug.AssertValidOrNull(this.ActionCallback);
        Debug.Assert((this.PageURL != null) || (this.ActionCallback != null));

        let webApp = WebApp.getInstance();
        Debug.AssertValid(webApp);
        if (this.PageURL != null) {
            webApp.navigateTo(this.PageURL, (currentPage) => {
                Debug.AssertValid(currentPage);
                callback();
            });
        } else if (this.ActionCallback != null) {
            this.ActionCallback(this.ActionContext, callback);
        } else if (this.PopupWindow != null) {
            webApp.showPopup(this.PopupWindow, (inputs) => {
                Debug.AssertValid(inputs);
                callback();
            })
        } else {
            callback();
        }
    }

}   // Action
