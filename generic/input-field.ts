import { Debug } from '../helpers/debug';
import { Component } from '../generic/component';
import { TextContentService } from '../services/text-content-service';
import { ErrorHelper } from '../helpers/error-helper';

/**
 * Class representing an input field.
 */
export class InputField extends Component {

    private PasswordField = false;
    private ErrorState = false;
    private Value: string;
    private HintName: string;
    private Hint: string;

    /**
     * Constructor.
     */
    constructor(hintName?: string) {
        super();
        Debug.Tested();
        Debug.AssertStringOrNull(hintName);

        this.HintName = hintName;
    }

    /**
     * Load the static text.
     * If there is a value name for this input field's hint then load the text value.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertStringOrNull(this.HintName);

        let this_ = this;
        super.doLoadStaticText(() => {
            if (this_.HintName != null) {
                TextContentService.getTextValue(this_.HintName, (value) => {
                    Debug.AssertString(value);
                    this_.setHint(value);
                    callback();
                });
            } else {
                callback();
            }
        });
    }

    /**
     * Get the input field value.
     */
    public getValue(): string {
        Debug.Tested();
        Debug.AssertValidOrNull(this.Value);

        return this.Value;
    }

    /**
     * Set the input field value.
     * Allows null and empty. It is up to the page using this field to validate the value.
     */
    public setValue(value: string) {
        Debug.Tested();
        Debug.AssertValidOrNull(value);

        this.Value = value;
    }

    /**
     * Get the input field hint.
     */
    public getHint(): string {
        Debug.Tested();
        Debug.AssertValidOrNull(this.Hint);

        return this.Hint;
    }

    /**
     * Set the input field hint.
     */
    public setHint(hint: string) {
        Debug.Tested();
        Debug.AssertValidOrNull(hint);

        this.Hint = hint;
    }

    /**
     * Get the error state of the input field.
     */
    public getErrorState(): boolean {
        Debug.Tested();

        return this.ErrorState;
    }
    
    /**
     * Set the error state of the input field.
     */
    public setErrorState() {
        Debug.Tested();

        this.ErrorState = true;
    }
    
    /**
     * Reset the error state of the input field.
     */
    public resetState() {
        Debug.Tested();

        this.ErrorState = false;
    }
    
}   // InputField
