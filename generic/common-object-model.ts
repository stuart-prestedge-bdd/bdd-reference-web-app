/**
 * Common object model.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/20611255/6.1.+Common+Objects
 */

/**
 * The text types.
 */
export const TEXT_TYPE_TEXT = 'text';
export const TEXT_TYPE_SVG = 'svg';
export const TEXT_TYPE_HTML = 'html';
 
/**
 * The target source types.
 */
export const TARGET_SRC_TYPE_URL = 'url';
export const TARGET_SRC_TYPE_JSON = 'json';

/**
 * Class representing a cause.
 */
export class Image {
    public width?: number;
    public height?: number;
    public srcType: string;
    public url?: string;
    public svg?: string;
}   // Image

/**
 * Class representing text.
 */
export class Text {
    public type: string;
    public text: string;
    public html: string;
}   // Text

/**
 * Class representing a target.
 */
export class Target {
    public srcType: string;
    public json?: string;
    public url?: string;
}   // Target

