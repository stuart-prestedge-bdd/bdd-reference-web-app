import { Debug } from '../helpers/debug';
import { Component } from './component';
import { Image } from './common-object-model';
import { ImageComponent } from './image-component';
import { Action } from './action';

/**
 * Class representing an image bar.
 */
export class IconBar extends Component {

    /**
     * The image component.
     */
    public readonly Images: ImageComponent[] = [];

    /**
     * Constructor.
     */
    constructor() {
        super();
        Debug.Tested();
    }

    /**
     * Get the image components.
     */
    public getImages(): ImageComponent[] {
        Debug.Tested();
        Debug.AssertValidOrNull(this.Images);

        return this.Images;
    }

    /**
     * Add an image.
     * The caller must setup this component after all images have been added.
     */
    public addImage(image: Image, executeAction: Action) {
        Debug.Tested();
        Debug.AssertValid(image);
        Debug.AssertValid(executeAction);

        let imageComponent = new ImageComponent(image);
        imageComponent.setExecuteAction(executeAction);
        this.Images.push(imageComponent);
    }

    /**
     * Setup the component.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }

    /**
     * Reset (clear) the component.
     */
    public reset(callback: () => void) {
        Debug.Untested();
        Debug.AssertValid(callback);

        this.Images.splice(0);
        this.setup(callback, true);
    }

}   // IconBar
