import { PopupWindow } from '../generic/popup-window';

/**
 * Class representing the choose number popup window.
 */
export class ChooseNumberPopupWindow extends PopupWindow {

    /**
     * The name of the number input parameter.
     */
    static readonly NumberInputName = 'number';

}   // ChooseNumberPopupWindow
