import { Debug } from '../helpers/debug';
import { TextField } from '../generic/text-field';
import { Button } from '../generic/button';
import { Page } from './page';
import { Action } from '../generic/action';

/**
 * Class representing a form page.
 * This is a base class used by a number of concrete classes to simply a form.
 * ???
 */
export abstract class FormPage extends Page {

    /**
     * The page title.
     */
    public readonly Title = new TextField();

    /**
     * The main form button.
     */
    public readonly Button = new Button();
    
    /**
     * Constructor.
     */
    constructor(url: string,
                requiresUser: boolean = true) {
        super(url, requiresUser);
        Debug.Tested();
        Debug.AssertString(url);
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;        
        super.doSetup(() => {
            this_.Title.setup(() => {
                this_.Button.setExecuteAction(new Action(null, FormPage.buttonClicked, this_));
                this_.Button.setup(() => {
                    callback();
                });
            });
        });
    }
    
    /**
     * Reset the input fields so they are no longer in error state.
     */
    private static buttonClicked(context: object, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(context);

        let formPage = <FormPage>context;
        formPage.resetInputFieldStates();
        if (formPage.validateInputs()) {
            formPage.doButtonClicked(callback);
        } else {
            callback();
        }
    }

    /**
     * Called when the button is clicked.
     * Override this method.
     */
    protected doButtonClicked(callback: () => void) {
        Debug.Unreachable();

        throw new Error();
    }

    /**
     * Reset the input fields so they are no longer in error state.
     */
    protected resetInputFieldStates() {
        Debug.Tested();

        this.doResetInputFieldStates();
    }

    /**
     * Reset the input fields so they are no longer in error state.
     * Override this method.
     */
    protected doResetInputFieldStates() {
        Debug.Unreachable();

        throw new Error();
    }

    /**
     * Validate the inputs.
     */
    protected validateInputs(): boolean {
        Debug.Tested();

        return this.doValidateInputs();
    }

    /**
     * Validate the inputs.
     * Override this method.
     */
    protected doValidateInputs(): boolean {
        Debug.Unreachable();

        throw new Error();
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.Button);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.Title.languageChanged(() => {
                this_.Button.languageChanged(() => {
                    callback();
                });
            });
        });
    }

}   // FormPage

