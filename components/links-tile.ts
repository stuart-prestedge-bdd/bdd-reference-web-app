import { Debug } from '../helpers/debug';
import { Component } from '../generic/component';
import { LinksContentService } from '../services/links-content-service';
import { TextField } from '../generic/text-field';
import { ErrorHelper } from '../helpers/error-helper';
import { TextContentService } from '../services/text-content-service';
import { Link } from '../data-models/links-model';
import { Action } from '../generic/action';

/**
 * Class representing the links tile.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47219072/2.2.4.11.+Links+Tile
 */
export class LinksTile extends Component {

    /**
     * The list of text fields. One for each link.
     */
    public readonly LinkFields: TextField[] = [];

    /**
     * Constructor.
     */
    constructor() {
        super();
        Debug.Tested();
    }

    /**
     * Setup the component.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doSetup(() => {
            LinksContentService.getLinks((links) => {
                Debug.AssertValidOrNull(links);
                if (links != null) {
                    let tmpLinks = [ ...links ];
                    this_.setupLinks(tmpLinks, callback);
                } else {
                    callback();
                }
            });
        });
    }

    /**
     * Setup the links
     */
    private setupLinks(links: Link[], callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(links);
        Debug.AssertValid(callback);

        let this_ = this;
        if (links.length > 0) {
            let link = links.splice(0, 1)[0];
            Debug.AssertValid(link);
            Debug.AssertString(link.id);
            Debug.AssertString(link.name);
            let linkField = new TextField();
            linkField.setValue(link.name);
            linkField.setExecuteAction(new Action(`/links/${link.id}`));
            this_.LinkFields.push(linkField);
            linkField.setup(() => {
                this_.setupLinks(links, callback);
            });
        } else {
            callback();
        }
    }
    
}   // LinksTile
