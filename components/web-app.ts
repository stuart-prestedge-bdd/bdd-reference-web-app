import { IApplication } from '../interfaces/application';
import { Debug } from '../helpers/debug';

/**
 * The WebApp application object.
 */
export class WebApp {

    /**
     * Singleton instance.
     */
    private static _instance: IApplication;

    /**
     * Set the application instance.
     */
    public static setInstance(value: IApplication) {
        Debug.Tested();
        Debug.AssertValid(value);
        Debug.AssertNull(WebApp._instance);

        WebApp._instance = value;
    }

    /**
     * Get the application instance.
     */
    public static getInstance(): IApplication {
        Debug.Tested();
        Debug.AssertValid(WebApp._instance);

        return WebApp._instance;
    }

}   // WebApp
