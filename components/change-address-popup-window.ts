import { PopupWindow } from '../generic/popup-window';
import { Address } from '../data-models/identity-model';
import { Debug } from '../helpers/debug';
import { ObjectMap } from '../helpers/object-map';

/**
 * Class representing the change address popup window.
 */
export class ChangeAddressPopupWindow extends PopupWindow {

    /**
     * The name of the address input parameter.
     */
    static readonly AddressInputName = 'address';

    /**
     * The address.
     */
    public Address: Address;

    /**
     * Constructor.
     */
    constructor(id: string, address: Address) {
        Debug.Tested();
        Debug.AssertString(id);

        super(id);
        this.Address = address;
    }

    /**
     * Perform operations on close.
     */
    protected doClose(inputs: ObjectMap<any>) {
        Debug.Tested();
        Debug.AssertValid(inputs);

        super.doClose(inputs);
        inputs.set(ChangeAddressPopupWindow.AddressInputName, this.Address);
    }
    
}   // ChangeAddressPopupWindow
