import { Debug } from '../helpers/debug';
import { Component } from '../generic/component';

/**
 * Class representing a ticket number component.
 */
export class TicketNumberComponent extends Component {

    /**
     * The ticket number value.
     */
    private Value: number;

    /**
     * Is the number fixed?.
     */
    private Fixed: boolean;

    /**
     * Constructor.
     */
    constructor(value?: number, fixed: boolean = true) {
        Debug.Tested();

        super();
        this.Value = value;
        this.Fixed = fixed;
    }

    /**
     * Get the number value.
     */
    public getValue(): number {
        Debug.Tested();
        Debug.AssertValidOrNull(this.Value);

        return this.Value;
    }

    /**
     * Set the input field value.
     * Allows null and empty. It is up to the page using this field to validate the value.
     */
    public setValue(value: number) {
        Debug.Tested();
        Debug.AssertValidOrNull(value);

        this.Value = value;
    }

}   // TicketNumberComponent
