import { PopupWindow } from '../generic/popup-window';
import { Debug } from '../helpers/debug';
import { InputField } from '../generic/input-field';
import { ObjectMap } from '../helpers/object-map';

/**
 * Class representing the change name popup window.
 */
export class ChangeNamePopupWindow extends PopupWindow {

    /**
     * The name of the input parameters.
     */ 
    static readonly GivenNameInputName = 'given-name';
    static readonly FamilyNameInputName = 'family-name';
    static readonly FullNameInputName = 'full-name';
    static readonly PreferredNameInputName = 'preferred-name';

    /**
     * The input fields.
     */
    public readonly GivenNameField = new InputField();
    public readonly FamilyNameField = new InputField();
    public readonly FullNameField = new InputField();
    public readonly PreferredNameField = new InputField();

    /**
     * Constructor.
     */
    constructor(id: string) {
        super(id);
        Debug.Tested();
    }

    /**
     * Perform operations on close.
     */
    protected doClose(inputs: ObjectMap<any>) {
        Debug.Tested();
        Debug.AssertValid(inputs);

        super.doClose(inputs);
        inputs.set(ChangeNamePopupWindow.GivenNameInputName, this.GivenNameField.getValue());
        inputs.set(ChangeNamePopupWindow.FamilyNameInputName, this.FamilyNameField.getValue());
        inputs.set(ChangeNamePopupWindow.FullNameInputName, this.FullNameField.getValue());
        inputs.set(ChangeNamePopupWindow.PreferredNameInputName, this.PreferredNameField.getValue());
    }
    
}   // ChangeNamePopupWindow
