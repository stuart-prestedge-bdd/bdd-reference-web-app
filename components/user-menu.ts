import { Debug } from '../helpers/debug';
import { MenuBase } from '../generic/menu-base';
import { LanguageMenu } from './language-menu';
import { SettingsContentService } from '../services/settings-content-service';

/**
 * Class representing the user menu.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47219050/2.2.4.4.+User+Menu
 */
export class UserMenu extends MenuBase {

    public readonly LanguageMenu = new LanguageMenu();

    /**
     * Setup the user menu.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.LanguageMenu);

        super.doSetup(() => {
            SettingsContentService.getLanguages((languages) => {
                Debug.AssertValid(languages);
                Debug.Assert(languages.length > 0);
                this.LanguageMenu.setHidden(languages.length <= 1);
                this.LanguageMenu.setup(() => {
                    callback();
                });
            });
        });
    }

}   // UserMenu

