import { Debug } from '../helpers/debug';
import { WebApp } from './web-app';
import { UserDetails, DateOnly } from '../data-models/identity-model';
import { Language } from '../data-models/language';
import { Page } from './page';
import { HomePage } from '../pages/home-page';
import { CausesPage } from '../pages/causes-page';
import { SettingsContentService } from '../services/settings-content-service';
import { PhilanthropyContentService } from '../services/philanthropy-content-service';
import { LinksContentService } from '../services/links-content-service';
import { TextContentService } from '../services/text-content-service';
import { CausePage } from '../pages/cause-page';
import { CharitiesPage } from '../pages/charities-page';
import { CharityPage } from '../pages/charity-page';
import { AmbassadorsPage } from '../pages/ambassadors-page';
import { LoggedOutPage } from '../pages/logged-out-page';
import { PasswordResetSentPage } from '../pages/password-reset-sent-page';
import { AccountCreatedPage } from '../pages/account-created-page';
import { AccountClosedPage } from '../pages/account-closed-page';
import { EmailVerifiedPage } from '../pages/email-verified-page';
import { PaymentDetailsPage } from '../pages/payment-details-page';
import { AmbassadorPage } from '../pages/ambassador-page';
import { BuyTicketsPage } from '../pages/buy-tickets-page';
import { MyTicketsPage } from '../pages/my-tickets-page';
import { TicketPage } from '../pages/ticket-page';
import { GamePage } from '../pages/game-page';
import { LoginPage } from '../pages/login-page';
import { ForgotPasswordPage } from '../pages/forgot-password-page';
import { CreateAccountPage } from '../pages/create-account-page';
import { ConfirmDetailsPage } from '../pages/confirm-details-page';
import { GiftTicketPage } from '../pages/gift-ticket-page';
import { UserProfilePage } from '../pages/user-profile-page';
import { PopupWindow } from '../generic/popup-window';
import { IdentityService } from '../services/identity-service';
import { IApplication } from '../interfaces/application';
import { DynamicLinkPage } from '../pages/dynamic-link-page';
import { ErrorHelper } from '../helpers/error-helper';
import { TARGET_SRC_TYPE_JSON } from '../generic/common-object-model';
import { Constants } from '../constants';
import { LocalStorageService } from '../services/local-storage-service';
import { ObjectMap } from '../helpers/object-map';
import { ChangeDateOfBirthPopupWindow } from './change-dob-popup-window';
import { DateHelper } from '../helpers/date-helper';
import { ToastHelper } from '../helpers/toast-helper';
import { PageBase } from '../generic/page-base';

/**
 * Class representing the application.
 * See https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47219235/2.9.10.+Operations
 */
export class Application implements IApplication {

    /**
     * The OAuth access token.
     * <authentication-token> in https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47284333/2.2.3.+Global+Variables
     */
    private AccessToken: string;

    /**
     * The currently logged in user's details.
     * <current-user> in https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47284333/2.2.3.+Global+Variables
     */
    private CurrentUser: UserDetails;

    /**
     * The currently used language.
     * <lang> in https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47284333/2.2.3.+Global+Variables
     */
    private CurrentLanguage: Language;

    /**
     * The pages.
     */
    private Pages: Page[] = [];

    /**
     * The currently displayed page.
     */
    private CurrentPage: Page;

    /**
     * The callback to show a popup.
     * The callback must itself call the specified callback when complete.
     */
    private PopupCallback: (popupWindow: PopupWindow, closedCallback: (closeAction: string) => void) => void;

    /**
     * The callback used when a page is navigated to automatically (e.g. on a timer).
     */
    private AutoNavigateCallback: (currentPage: Page) => void;

    /**
     * The callback the app uses to compose an email.
     */
    private ComposeEmail: (subject: string, body: string, bodyIsHTML: boolean, callback: () => void) => void;

    /**
     * The ID of the auto timeout.  
     */
    private AutoNavigateTimeoutID: number;

    /**
     * Setup the application and set as the singleton instance.
     */
    public static setup(callback: (application: IApplication) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let application = new Application();
        WebApp.setInstance(application);
        application.setupPages((currentPage) => {
            Debug.AssertValid(currentPage);
            callback(application);
        });
    }

    /**
     * Get the language to use.
     * Uses the current user, the local storage and the browser settings.
     * The fallback language code is the default language code in the settings.
     */
    public getLanguageCode(callback: (languageCode: string) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        this.setupCurrentLanguage((language) => {
            Debug.AssertValidOrNull(language);
            if (language != null) {
                callback(language.code);
             } else {
                SettingsContentService.getDefaultLanguageCode((defaultLanguageCode) => {
                    Debug.AssertString(defaultLanguageCode);
                    callback(defaultLanguageCode);
                })
             }
        });
    }

    /**
     * Get the language to use.
     * Uses the current user, the local storage and the browser settings.
     */
    public setCurrentLanguage(language: Language, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(language);
        Debug.AssertValid(callback);

        if (this.CurrentLanguage != language) {
            this.CurrentLanguage = language;
            LocalStorageService.writeValue('language', this.CurrentLanguage, () => 0);
            PhilanthropyContentService.clearCache();
            LinksContentService.clearCache();
            TextContentService.clearCache();
            this.languageChanged(callback);
        } else {
            callback();
        }
    }

    /**
     * Setup the current language.
     */
    private setupCurrentLanguage(callback: (language: Language) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValidOrNull(this.CurrentLanguage);
        //??++Handle different ways of setting language. User setting, browser, stored local setting etc.

        let this_ = this;
        if (this_.CurrentLanguage == null) {
            LocalStorageService.readValue('language', (error, language) => {
                Debug.AssertValidOrNull(error);
                Debug.AssertValidOrNull(language);
                if (language != null) {
                    this_.CurrentLanguage = language;
                    callback(this_.CurrentLanguage);
                } else {
                    SettingsContentService.getLanguages((languages) => {
                        Debug.AssertValid(languages);
                        Debug.Assert(languages.length > 0);
                        this_.CurrentLanguage = languages[0];
                        Debug.AssertValid(this_.CurrentLanguage);
                        LocalStorageService.writeValue('language', this_.CurrentLanguage, () => 0);
                        callback(this_.CurrentLanguage);
                    });
                }
            });
        } else {
            callback(this_.CurrentLanguage);
        }
    }
    
    /**
     * Called when the language changes.
     */
    private languageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Pages);

        let pages = [ ...this.Pages ];
        this.pageLanguageChanged(pages, callback);
    }
    
    /**
     * Changes the languages of the pages.
     */
    private pageLanguageChanged(pages: Page[], callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(pages);
        Debug.AssertValid(callback);

        if (pages.length > 0) {
            let page = pages.splice(0, 1)[0];
            Debug.AssertValid(page);
            page.languageChanged(() => {
                this.pageLanguageChanged(pages, callback);
            })
        } else {
            callback();
        }
    }
    
    /**
     * Setup the pages.
     * Set the current page to the home page.
     */
    private setupPages(callback: (currentPage: Page) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        this_.setupStaticPages();
        this_.setupDynamicPages(() => {
            callback(this_.CurrentPage);
        });
    }

    /**
     * Setup the static pages.
     */
    protected setupStaticPages() {
        Debug.Tested();

        this.CurrentPage = new HomePage();
        this.addPage(this.CurrentPage);
        this.addPage(new CausesPage());
        this.addPage(new CausePage());
        this.addPage(new CharitiesPage());
        this.addPage(new CharityPage());
        this.addPage(new AmbassadorsPage());
        this.addPage(new AmbassadorPage());
        this.addPage(new BuyTicketsPage());
        this.addPage(new MyTicketsPage());
        this.addPage(new TicketPage());
        this.addPage(new LoginPage());
        this.addPage(new ForgotPasswordPage());
        this.addPage(new CreateAccountPage());
        this.addPage(new PasswordResetSentPage());
        //???this.addPage(new ResetPasswordPage());
        this.addPage(new AccountCreatedPage());
        this.addPage(new EmailVerifiedPage());
        this.addPage(new LoggedOutPage());
        this.addPage(new UserProfilePage());
        this.addPage(new PaymentDetailsPage());
        this.addPage(new GamePage());
        this.addPage(new GiftTicketPage());
        this.addPage(new ConfirmDetailsPage());
        //???this.addPage(new PrizesPage());
        //???this.addPage(new PrizePage());
        //???this.addPage(new UserLevelPage());
        this.addPage(new AccountClosedPage());
    }

    /**
     * Setup the dynamic pages.
     */
    protected setupDynamicPages(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        LinksContentService.getLinks((links) => {
            Debug.AssertValidOrNull(links);
            if (links != null) {
                links.forEach(link => {
                    Debug.AssertValid(link);
                    Debug.AssertValid(link.link);
                    if (link.link.srcType === TARGET_SRC_TYPE_JSON) {
                        let dynamicLinkPage = new DynamicLinkPage(link);
                        this_.addPage(dynamicLinkPage);
                    }
                });
            }
            callback();
        });
    }

    /**
     * Add a page to the route map.
     */
    private addPage(page: Page) {
        Debug.Tested();
        Debug.AssertValid(page);
        Debug.AssertValid(this.Pages);

        this.Pages.push(page);
    }

    /**
     * Lookup a page by URL.
     */
    private lookupPage(url: string, args: ObjectMap<string>): Page {
        Debug.Tested();
        Debug.AssertString(url);
        Debug.AssertValid(args);
        Debug.AssertValid(this.Pages);

        let retVal: Page = null;
        for (let i = 0; i < this.Pages.length; i++) {
            let page = this.Pages[i];
            Debug.AssertValid(page);
            let pageUrl = page.getURL();
            Debug.AssertString(pageUrl);
            if (this.matchURL(pageUrl, url, args)) {
                retVal = page;
                break;
            }
        }
        return retVal;
    }

    /**
     * Determines if the page URL, which may contain parameters, matches the URL.
     * If so, returns true and puts the arguments into the passed in args (if not there already).
     */
    private matchURL(pageUrl: string, url: string, args: ObjectMap<string>): boolean {
        Debug.Tested();
        Debug.AssertString(pageUrl);
        Debug.AssertString(url);
        Debug.AssertValid(args);

        let retVal = false;
        let pageNodes = pageUrl.split('/');
        Debug.AssertValid(pageNodes);
        let nodes = url.split('/');
        Debug.AssertValid(nodes);
        if (pageNodes.length === nodes.length) {
            retVal = true;
            let urlArgs = new ObjectMap<string>();
            for (let i = 0; i < nodes.length; i++) {
                let pageNode = pageNodes[i];
                let node = nodes[i];
                if (pageNode[0] === '{') {
                    let pageNodeLen = pageNode.length;
                    Debug.Assert(pageNodeLen > 2);
                    Debug.Assert(pageNode[pageNodeLen - 1] === '}');
                    let parameter = pageNode.substr(1, (pageNodeLen - 2));
                    urlArgs.set(parameter, node);
                } else if (pageNode !== node) {
                    retVal = false;
                    break;
                }
            }
            if (retVal) {
                urlArgs.forEachKey(key => {
                    Debug.AssertString(key);
                    if (!args.has(key)) {
                        args.set(key, urlArgs.get(key));
                    }
                });
            }
        }
        return retVal;
    }

    /**
     * Navigate to a page by URL.
     */
    public navigateTo(url: string, callback: (currentPage: Page) => void) {
        Debug.Tested();
        Debug.AssertString(url);
        Debug.AssertValidOrNull(callback);
        Debug.AssertValidOrNull(this.CurrentPage);

        let this_ = this;
        if (this_.CurrentPage != null) {
            this_.CurrentPage.close();
        }
        let args = new ObjectMap<string>();
        let rootURL = this_.splitURL(url, args);
        Debug.AssertString(rootURL);
        let page = this_.lookupPage(rootURL, args);
        Debug.AssertValidOrNull(page);
        if (page == null) {
            Debug.Assert(rootURL !== Constants.PAGE_URL_HOME);
            page = this_.lookupPage(Constants.PAGE_URL_HOME, args);
            Debug.AssertValid(page);
        }
        this_.CurrentPage = page;
        this_.CurrentPage.open(args, () => {
            if (callback != null) {
                callback(this_.CurrentPage);
            }
        });
    }

    /**
     * Navigate to a page by URL after the specified number of seconds.
     * This method is used when the app logic itself causes an auto navigation without user input.
     */
    public autoNavigateTo(url: string, seconds: number) {
        Debug.Tested();
        Debug.AssertString(url);
        Debug.AssertValid(this.AutoNavigateCallback);

        let this_ = this;
        if (this_.AutoNavigateTimeoutID != null) {
            clearTimeout(this_.AutoNavigateTimeoutID);
        }
        this_.AutoNavigateTimeoutID = setTimeout(() => {
            this_.AutoNavigateTimeoutID = null;
            this_.navigateTo(url, this_.AutoNavigateCallback);
        }, (seconds * 1000));
    }

    /**
     * Split the URL passed in (which may have arguments) into root URL and arguments.
     */
    private splitURL(url: string, args: ObjectMap<string>): string {
        Debug.Tested();
        Debug.AssertString(url);
        Debug.AssertValid(args);

        let rootURL: string;
        let parts = url.split('?');
        Debug.AssertValid(parts);
        Debug.Assert(parts.length > 0);
        Debug.Assert(parts.length <= 2);
        rootURL = parts[0];
        if (parts.length > 1) {
            let argNVPairs = parts[1].split('&');
            Debug.AssertValid(argNVPairs);
            Debug.Assert(argNVPairs.length > 0);
            argNVPairs.forEach(argNVPair => {
                Debug.AssertString(argNVPair);
                let nv = argNVPair.split('=');
                Debug.AssertValid(nv);
                Debug.Assert(nv.length === 2);
                Debug.AssertString(nv[0]);
                Debug.AssertValid(nv[1]);
                args.set(nv[0], nv[1]);
            });
        }
        return rootURL;
    }
    
    /**
     * Get the current page.
     */
    public getCurrentPage(): Page {
        Debug.Tested();
        Debug.AssertValid(this.CurrentPage);

        return this.CurrentPage;
    }

    /**
     * Create a new account.
     */
    public createAccount(givenName: string,
                         familyName: string,
                         emailAddress: string,
                         password: string,
                         dateOfBirth: DateOnly,
                         address1: string,
                         address2: string,
                         address3: string,
                         address4: string,
                         city: string,
                         region: string,
                         country: string,
                         postalCode: string,
                  callback: () => void) {
        Debug.Tested();
        Debug.AssertString(givenName);
        Debug.AssertString(familyName);
        Debug.AssertString(emailAddress);
        Debug.AssertString(password);
        Debug.AssertValid(callback);

        let this_ = this;        
        IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, address1, address2, address3, address4, city, region, country, postalCode, true, (error) => {
            Debug.AssertValidOrNull(error);
            if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_CREATE_ACCOUNT)) {
                this_.navigateTo(Constants.PAGE_URL_ACCOUNT_CREATED, () => {
                    callback();
                });
            } else {
                callback();
            }
        });
    }

    /**
     * Verify an email address.
     * Does NOT set the current user - the user needs to login still.
     */
    public verifyEmail(emailAddress: string, verificationID: string, callback: () => void) {
        Debug.Tested();
        Debug.AssertString(emailAddress);
        Debug.AssertString(verificationID);
        Debug.AssertValid(callback);
        
        let this_ = this;
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertValidOrNull(error);
            if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_VERIFY_EMAIL_ADDRESS)) {
                ToastHelper.showInfo(TextContentService.TEXT_NAME_INFO_EMAIL_ADDRESS_VERIFIED);
            }
            callback();
        });
    }
    
    /**
     * Close the current account.
     * The user must be logged in.
     * Logs the current user out.
     */
    public closeAccount(password: string, callback: () => void) {
        Debug.Tested();
        Debug.AssertString(password);
        Debug.AssertValid(callback);
        Debug.AssertValidOrNull(this.AccessToken);
        Debug.AssertValidOrNull(this.CurrentUser);

        let this_ = this;        
        if (this_.AccessToken != null) {
            IdentityService.closeAccount(this_.AccessToken, password, (error) => {
                Debug.AssertValidOrNull(error);
                if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_CLOSE_ACCOUNT)) {
                    this_.AccessToken = null;
                    this_.CurrentUser = null;
                    this_.navigateTo(Constants.PAGE_URL_ACCOUNT_CLOSED, () => {
                        callback();
                    });
                } else {
                    callback();
                }
            });
        } else {
            Debug.AssertNull(this.CurrentUser);
            ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_FAILED_TO_CLOSE_ACCOUNT);
            callback();
        }
    }

    /**
     * Login.
     * Sets the current user to null on successful login so it is reloaded.
     */
    public login(emailAddress: string, password: string, callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertString(emailAddress);
        Debug.AssertString(password);
        Debug.AssertValid(callback);

        let this_ = this;
        IdentityService.login(emailAddress, password, (error, accessToken) => {
            Debug.AssertErrorOrObject(error, accessToken);
            this_.AccessToken = accessToken;
            this_.CurrentUser = null;
            callback(error);
        });
    }

    /**
     * Logout.
     */
    public logout(callback: (error: Error) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValidOrNull(this.AccessToken);
        Debug.AssertValidOrNull(this.CurrentUser);

        let this_ = this;
        this_.CurrentUser = null;
        if (this_.AccessToken != null) {
            IdentityService.logout(this_.AccessToken, (error) => {
                this_.AccessToken = null;
                this_.navigateTo(Constants.PAGE_URL_LOGGED_OUT, (currentPage) => {
                    callback(null);
                });
            });
        } else {
            this_.navigateTo(Constants.PAGE_URL_LOGGED_OUT, (currentPage) => {
                callback(null);
            });
        }
    }

    /**
     * Get the current user.
     * 'GetCurrentUser' in https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47219235/2.9.10.+Operations
     */
    public getCurrentUser(callback: (user: UserDetails, accessToken: string) => void) {
        Debug.Tested();
        Debug.AssertValidOrNull(this.AccessToken);
        Debug.AssertValidOrNull(this.CurrentUser);

        if (this.CurrentUser != null) {
            Debug.Untested();
            Debug.AssertString(this.AccessToken);
            callback(this.CurrentUser, this.AccessToken);
        } else if (this.AccessToken != null) {
            IdentityService.getUserDetails(this.AccessToken, (error, userDetails) => {
                Debug.AssertErrorOrObject(error, userDetails);
                ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_GET_USER_DETAILS);
                callback(userDetails, this.AccessToken);
            });
        } else {
            callback(null, null);
        }
    }

    /**
     * Set the actual callbacks.
     * These are the popup callback and the auto navigation callback
     */
    public setCallbacks(popupCallback: (popupWindow: PopupWindow, closedCallback: (closeAction: string) => void) => void,
                        autoNavigateCallback: (currentPage: Page) => void,
                        composeEmail: (subject: string, body: string, bodyIsHTML: boolean, callback: () => void) => void) {
        Debug.Tested();
        Debug.AssertValid(popupCallback);
        Debug.AssertValid(autoNavigateCallback);
        Debug.AssertValid(composeEmail);
        Debug.AssertNull(this.PopupCallback);
        Debug.AssertNull(this.AutoNavigateCallback);
        Debug.AssertNull(this.ComposeEmail);

        this.PopupCallback = popupCallback;
        this.AutoNavigateCallback = autoNavigateCallback;
        this.ComposeEmail = composeEmail;
    }
    
    /**
     * Show a popup window.
     * When closed any inputs entered into the popup are passed to the callback, including the name of the button pressed.
     */
    public showPopup(popupWindow: PopupWindow, callback: (inputs: ObjectMap<any>) => void) {
        Debug.Tested();
        Debug.AssertValid(popupWindow);
        Debug.AssertString(popupWindow.getID());
        Debug.AssertValid(callback);
        Debug.AssertValid(this.PopupCallback);

        this.PopupCallback(popupWindow, (closeAction) => {
            let inputs = new ObjectMap<any>();
            popupWindow.close(inputs);
            inputs.set(PopupWindow.ButtonInputName, closeAction);
            callback(inputs);
        });
    }

    /**
     * Change the current user's date of birth.
     */
    //??-- public changeUserDateOfBirth(userDetails: UserDetails, callback: (success: boolean) => void) {
    //     Debug.Tested();
    //     Debug.AssertValid(userDetails);
    //     Debug.AssertValid(callback);
    //     Debug.AssertValidOrNull(this.AccessToken);

    //     let this_ = this;
    //     if (this_.AccessToken != null) {
    //         this_.showPopup(new ChangeDateOfBirthPopupWindow('change-dob', userDetails.DateOfBirth), (inputs) => {
    //             Debug.AssertValid(inputs);
    //             if (inputs[PopupWindow.ButtonInputName] === PopupWindow.ButtonInputValueOK) {
    //                 let newDateOfBirth = inputs[ChangeDateOfBirthPopupWindow.DateOfBirthInputName];
    //                 //??++CHECK DOB (valid date, >=18?) HERE OR IN POPUP?
    //                 IdentityService.setUserDateOfBirth(this_.AccessToken, newDateOfBirth, (error) => {
    //                     if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_SET_DOB)) {
    //                         userDetails.DateOfBirth = newDateOfBirth;
    //                         callback(true);
    //                     } else {
    //                         callback(false);
    //                     }
    //                 });
    //             } else {
    //                 callback(false);
    //             }
    //         });
    //     } else {
    //         ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_NOT_LOGGED_IN);
    //     }
    // }

    /**
     * Compose an email with given subject and body.
     */
    public composeEmail(subject: string, body: string, bodyIsHTML, callback: () => void) {
        Debug.Tested();
        Debug.AssertString(subject);
        Debug.AssertStringOrNull(body);
        Debug.AssertValid(callback);
        Debug.AssertValid(this.ComposeEmail);

        this.ComposeEmail(subject, body, bodyIsHTML, callback);
    }

}   // Application
