import { Debug } from '../helpers/debug';
import { TopBanner } from './top-banner';
import { LinksTile } from './links-tile';
import { BottomBar } from './bottom-bar';
import { WebApp } from './web-app';
import { PageBase } from '../generic/page-base';

/**
 * Abstract class representing base class for all pages.
 */
export abstract class Page extends PageBase {

    /**
     * The top banner.
     * Most, but not all, pages have this.
     */
    public readonly TopBanner = new TopBanner();

    /**
     * The links tile.
     */
    public readonly LinksTile = new LinksTile();

    /**
     * The bottom bar.
     */
    public readonly BottomBar = new BottomBar();
    
    /**
     * Constructor.
     */
    constructor(url: string,
                requiresUser: boolean = true,
                hasTopBanner: boolean = true) {
        super(WebApp.getInstance(), url, requiresUser);
        Debug.Tested();

        this.TopBanner.setHidden(!hasTopBanner);
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.TopBanner);
        Debug.AssertValid(this.LinksTile);
        Debug.AssertValid(this.BottomBar);

        let this_ = this;
        super.doSetup(() => {
            this_.TopBanner.setup(() => {
                this_.LinksTile.setup(() => {
                    this_.BottomBar.setup(() => {
                        callback();
                    });
                });
            });
        });
    }
    
    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.TopBanner);
        Debug.AssertValid(this.LinksTile);
        Debug.AssertValid(this.BottomBar);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.TopBanner.languageChanged(() => {
                this_.LinksTile.languageChanged(() => {
                    this_.BottomBar.languageChanged(() => {
                        callback();
                    });
                });
            });
        });
    }

}   // Page

