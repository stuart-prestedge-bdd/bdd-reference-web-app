import { PopupWindow } from '../generic/popup-window';
import { Debug } from '../helpers/debug';
import { ObjectMap } from '../helpers/object-map';
import { DateOnly } from '../data-models/identity-model';

/**
 * Class representing the change date of birth popup window.
 */
export class ChangeDateOfBirthPopupWindow extends PopupWindow {

    /**
     * The name of the date of birth input parameter.
     */
    static readonly DateOfBirthInputName = 'dateOfBirth';

    /**
     * The date of birth.
     */
    public DateOfBirth: DateOnly;

    /**
     * Constructor.
     */
    constructor(id: string, dateOfBirth: DateOnly) {
        Debug.Tested();
        Debug.AssertString(id);

        super(id);
        this.DateOfBirth = dateOfBirth;
    }

    /**
     * Perform operations on close.
     */
    protected doClose(inputs: ObjectMap<any>) {
        Debug.Tested();
        Debug.AssertValid(inputs);

        super.doClose(inputs);
        inputs.set(ChangeDateOfBirthPopupWindow.DateOfBirthInputName, this.DateOfBirth);
    }
    
}   // ChangeDateOfBirthPopupWindow
