import { Debug } from '../helpers/debug';
import { Component } from '../generic/component';
import { ImageComponent } from '../generic/image-component';
import { TextField } from '../generic/text-field';
import { TextContentService } from '../services/text-content-service';
import { MainMenu } from './main-menu';
import { UserMenu } from './user-menu';

/**
 * Class representing the top banner.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47284403/2.2.4.2.+Top+Banner
 */
export class TopBanner extends Component {

    /**
     * The main menu.
     */
    public readonly MainMenu = new MainMenu();

    /**
     * The user menu.
     */
    public readonly UserMenu = new UserMenu();

    /**
     * The logo.
     */
    public readonly Logo = new ImageComponent();

    /**
     * The title.
     */
    public readonly Title = new TextField();

    /**
     * Constructor.
     */
    constructor() {
        super();
        Debug.Tested();        
    }

    /**
     * Setup the component.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;        
        super.doSetup(() => {
            this_.MainMenu.setup(() => {
                this_.UserMenu.setup(() => {
                    //??? Who sets up user menu icon?
                    this_.Logo.setup(() => {
                        TextContentService.getTextValue(TextContentService.TEXT_NAME_BDD_TITLE, (value) => {
                            Debug.AssertString(value);
                            this_.Title.setValue(value);
                            callback();
                        });
                    });
                });
            });
        });
    }

}   // TopBanner
