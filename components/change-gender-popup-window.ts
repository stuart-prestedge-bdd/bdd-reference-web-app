import { Debug } from '../helpers/debug';
import { PopupWindow } from '../generic/popup-window';
import { ObjectMap } from '../helpers/object-map';
import { Gender } from '../data-models/identity-model';

/**
 * Class representing the change gender popup window.
 */
export class ChangeGenderPopupWindow extends PopupWindow {

    /**
     * The name of the gender input parameter.
     */
    static readonly GenderInputName = 'gender';

    /**
     * The gender.
     */
    public Gender: Gender;

    /**
     * Constructor.
     */
    constructor(id: string, gender: Gender) {
        Debug.Tested();
        Debug.AssertString(id);

        super(id);
        this.Gender = gender;
    }

    /**
     * Perform operations on close.
     */
    protected doClose(inputs: ObjectMap<any>) {
        Debug.Tested();
        Debug.AssertValid(inputs);

        super.doClose(inputs);
        inputs.set(ChangeGenderPopupWindow.GenderInputName, this.Gender);
    }
    
}   // ChangeGenderPopupWindow
