import { Debug } from '../helpers/debug';
import { Component } from '../generic/component';
import { MainMenu } from './main-menu';
import { UserMenu } from './user-menu';

/**
 * Class representing the bottom bar.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47284428/2.2.4.12.+Bottom+Bar
 */
export class BottomBar extends Component {

    /**
     * Constructor.
     */
    constructor() {
        Debug.Tested();

        super();
    }

    /**
     * Setup the component.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }

}   // BottomBar
