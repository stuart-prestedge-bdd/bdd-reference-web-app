import { Debug } from '../helpers/debug';
import { MenuBase, MenuItem } from '../generic/menu-base';
import { Action } from '../generic/action';
import { SettingsContentService } from '../services/settings-content-service';
import { Image, TARGET_SRC_TYPE_URL } from '../generic/common-object-model';
import { AssetHelper } from '../helpers/asset-helper';
import { ErrorHelper } from '../helpers/error-helper';
import { WebApp } from './web-app';

/**
 * Class representing the language menu.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47022553/2.2.4.5.+Language+Menu
 */
export class LanguageMenu extends MenuBase {

    /**
     * Setup the menu.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.MenuItems);

        super.doSetup(() => {
            this.Icon.setImage(<Image>{ srcType: TARGET_SRC_TYPE_URL, url: AssetHelper.ASSET_LANGUAGE_MENU_ICON }, () => {
                SettingsContentService.getLanguages((languages) => {
                    Debug.AssertValid(languages);
                    Debug.Assert(languages.length > 0);
                    languages.forEach(language => {
                        Debug.AssertValid(language);
                        Debug.AssertString(language.icon);
                        Debug.AssertString(language.name);
                        let menuItem = new MenuItem(language.icon, new Action(null, (context, callback) => {
                            WebApp.getInstance().setCurrentLanguage(language, () => {
                                callback();
                            });
                        }));
                        menuItem.Label = language.name;
                        this.MenuItems.push(menuItem);
                    });
                    callback();
                });
            });
        });
    }

}   // LanguageMenu

