import { Debug } from '../helpers/debug';
import { MenuBase, MenuItem } from '../generic/menu-base';
import { Image, TARGET_SRC_TYPE_URL } from '../generic/common-object-model';
import { TextContentService } from '../services/text-content-service';
import { GameContentService } from '../services/game-content-service';
import { ErrorHelper } from '../helpers/error-helper';
import { Action } from '../generic/action';
import { AssetHelper } from '../helpers/asset-helper';
import { WebApp } from './web-app';
import { Constants } from '../constants';

/**
 * Class representing the main menu.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47055199/2.2.4.3.+Menu
 */
export class MainMenu extends MenuBase {

    /**
     * The possible menu items.
     */
    private BuyTicketsMenuItem = new MenuItem(AssetHelper.ASSET_MENU_BUY_TICKETS_ICON, new Action(Constants.PAGE_URL_BUY_TICKETS));
    private MyTicketsMenuItem = new MenuItem(AssetHelper.ASSET_MENU_MY_TICKETS, new Action(Constants.PAGE_URL_MY_TICKETS));
    private CurrentGameMenuItem = new MenuItem(AssetHelper.ASSET_MENU_CURRENT_GAME, new Action(Constants.PAGE_URL_GAME));
    private PrizesMenuItem = new MenuItem(AssetHelper.ASSET_MENU_PRIZES, new Action('/prizes'));
    private CausesMenuItem = new MenuItem(AssetHelper.ASSET_MENU_CAUSES, new Action(Constants.PAGE_URL_CAUSES));
    private CharitiesMenuItem = new MenuItem(AssetHelper.ASSET_MENU_CHARITIES, new Action(Constants.PAGE_URL_CHARITIES));
    private AmbassadorsMenuItem = new MenuItem(AssetHelper.ASSET_MENU_AMBASSADORS, new Action(Constants.PAGE_URL_AMBASSADORS));
    private ReferFriendMenuItem = new MenuItem(AssetHelper.ASSET_MENU_REFER_FRIEND, new Action(null, MainMenu.referFriend, this));

    /**
     * Setup the component.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;        
        super.doSetup(() => {
            this_.Icon.setImage(<Image>{ srcType: TARGET_SRC_TYPE_URL, url: AssetHelper.ASSET_MENU_ICON }, () => {
                GameContentService.getCurrentGame((game) => {
                    Debug.AssertValidOrNull(game);
                    WebApp.getInstance().getCurrentUser((currentUser, accessToken) => {
                        Debug.AssertValidOrNull(currentUser);
                        this_.MenuItems.push(this_.BuyTicketsMenuItem);
                        if (currentUser != null) {
                            this_.MenuItems.push(this_.MyTicketsMenuItem);
                        }
                        this_.MenuItems.push(this_.CurrentGameMenuItem);
                        if ((game != null) && (game.prizes.length > 0)) {
                            this_.MenuItems.push(this_.PrizesMenuItem);
                        }
                        this_.MenuItems.push(this_.CausesMenuItem);
                        this_.MenuItems.push(this_.CharitiesMenuItem);
                        this_.MenuItems.push(this_.AmbassadorsMenuItem);
                        this_.MenuItems.push(this_.ReferFriendMenuItem);
                        callback();
                    });
                });
            });
        });
    }

    /**
     * Load the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLoadStaticText(() => {
            let names = [
                TextContentService.TEXT_NAME_MENU_BUY_TICKETS,
                TextContentService.TEXT_NAME_MENU_MY_TICKETS,
                TextContentService.TEXT_NAME_MENU_CURRENT_GAME,
                TextContentService.TEXT_NAME_MENU_PRIZES,
                TextContentService.TEXT_NAME_MENU_CAUSES,
                TextContentService.TEXT_NAME_MENU_CHARITIES,
                TextContentService.TEXT_NAME_MENU_AMBASSADORS,
                TextContentService.TEXT_NAME_MENU_REFER_FRIEND
            ];
            TextContentService.getTextValues(names, (values) => {
                Debug.AssertValid(values);
                this_.BuyTicketsMenuItem.Label = values.get(TextContentService.TEXT_NAME_MENU_BUY_TICKETS);
                this_.MyTicketsMenuItem.Label = values.get(TextContentService.TEXT_NAME_MENU_MY_TICKETS);
                this_.CurrentGameMenuItem.Label = values.get(TextContentService.TEXT_NAME_MENU_CURRENT_GAME);
                this_.PrizesMenuItem.Label = values.get(TextContentService.TEXT_NAME_MENU_PRIZES);
                this_.CausesMenuItem.Label = values.get(TextContentService.TEXT_NAME_MENU_CAUSES);
                this_.CharitiesMenuItem.Label = values.get(TextContentService.TEXT_NAME_MENU_CHARITIES);
                this_.AmbassadorsMenuItem.Label = values.get(TextContentService.TEXT_NAME_MENU_AMBASSADORS);
                this_.ReferFriendMenuItem.Label = values.get(TextContentService.TEXT_NAME_MENU_REFER_FRIEND);
                callback();
            });
        });
    }

    /**
     * Refer friend to BDD.
     */
    private static referFriend(context: object, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(context);
        Debug.Assert(context instanceof MainMenu);
        Debug.AssertValid(callback);

        let application = WebApp.getInstance();
        Debug.AssertValid(application);
        let names = [
            TextContentService.TEXT_NAME_REFER_FRIEND_SUBJECT,
            TextContentService.TEXT_NAME_REFER_FRIEND_BODY_HTML
        ];
        TextContentService.getTextValues(names, (values) => {
            Debug.AssertValid(values);
            let subject = values.get(TextContentService.TEXT_NAME_REFER_FRIEND_SUBJECT);
            let bodyHTML = values.get(TextContentService.TEXT_NAME_REFER_FRIEND_BODY_HTML);
            application.composeEmail(subject, bodyHTML, true, callback);
        });
    }

}   // MainMenu

