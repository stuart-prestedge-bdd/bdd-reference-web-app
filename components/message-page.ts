import { Debug } from '../helpers/debug';
import { Page } from './page';
import { PhilanthropyContentService } from '../services/philanthropy-content-service';
import { VideoTile } from '../generic/video-tile';
import { TextContentService } from '../services/text-content-service';
import { IconTitleTextList } from '../generic/icon-title-text-list';
import { Action } from '../generic/action';
import { ImageHelper } from '../helpers/image-helper';
import { Button } from '../generic/button';
import { Constants } from '../constants';

/**
 * Class representing a message page.
 * This is a base class used by a number of concrete classes to simply show a message to the use with a possible redirect.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/48824396/2.2.5.1.2.+Message+Page
 */
export abstract class MessagePage extends Page {

    /**
     * The page title.
     */
    protected Title: string;

    /**
     * The page description HTML.
     */
    protected DescriptionHTML: string;

    /**
     * The action button.
     */
    public readonly ActionButton = new Button();

    //??++Panel w/ title & text

    /**
     * Constructor.
     */
    constructor(url: string,
                requiresUser: boolean = true) {
        super(url, requiresUser);
        Debug.Tested();
        Debug.AssertString(url);
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(() => {
            this.ActionButton.setHidden(true);
            this.ActionButton.setup(() => {
                callback();
            });
        });
    }
    
    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.ActionButton);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.ActionButton.languageChanged(() => {
                callback();
            });
        });
    }

    /**
     * Set the button attributes. 
     */
    protected setupButton(value: string, action: Action) {
        Debug.Tested();
        Debug.AssertString(value);
        Debug.AssertValid(action);

        this.ActionButton.setHidden(false);
        this.ActionButton.setExecuteAction(action);
        this.ActionButton.setLabel(value);
    }
    
    /**
     * Gets the message page title.
     */
    public getTitle(): string {
        Debug.Tested();

        return this.Title;
    }

    /**
     * Gets the message page description HTML.
     */
    public getDescriptionHTML(): string {
        Debug.Tested();

        return this.DescriptionHTML;
    }

}   // MessagePage
