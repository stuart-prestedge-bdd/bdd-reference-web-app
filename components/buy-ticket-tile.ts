import { Debug } from '../helpers/debug';
import { Component } from '../generic/component';
import { Button } from '../generic/button';
import { WebApp } from './web-app';
import { Page } from './page';
import { Action } from '../generic/action';
import { TextContentService } from '../services/text-content-service';
import { Constants } from '../constants';

/**
 * Class representing the buy tile.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47055207/2.2.4.7.+Buy+Ticket+Tile
 */
export class BuyTicketTile extends Component {

    /**
     * The buy button.
     */
    public readonly BuyButton = new Button(TextContentService.TEXT_NAME_BUTTON_BUY);

    /**
     * Constructor.
     */
    constructor() {
        super();
        Debug.Tested();
    }

    /**
     * Setup the component.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.BuyButton);

        let this_ = this;
        super.doSetup(() => {
            this_.BuyButton.setExecuteAction(new Action(Constants.PAGE_URL_BUY_TICKETS));
            this_.BuyButton.setup(callback);
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.BuyButton);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.BuyButton.languageChanged(callback);
        });
    }

}   // BuyTicketTile
