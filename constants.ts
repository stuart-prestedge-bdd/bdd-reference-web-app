/**
 * Class containing constants.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47153297/2.2.2.+Constants
 */
export class Constants {

    static readonly MAX_SIMULTANEOUS_LINES = 5;
    
    static readonly ASSET_ROOT = 'https://billiondraw.com/assets';
    static readonly APP_ROOT = 'http://s3.eu-central-1.amazonaws.com/dev.app.billiondraw.com';
    static readonly DATA_ROOT = 'http://s3.eu-central-1.amazonaws.com/dev.cms.billiondraw.com';

    /**
     * Page URLs.
     */
    static readonly PAGE_URL_HOME = '/';
    static readonly PAGE_URL_CAUSES = '/causes';
    static readonly PAGE_URL_CAUSE = `/cause/{id}`;
    static readonly PAGE_URL_CHARITIES = '/charities';
    static readonly PAGE_URL_CHARITY = '/charity/{id}';
    static readonly PAGE_URL_AMBASSADORS = '/ambassadors';
    static readonly PAGE_URL_AMBASSADOR = '/ambassador/{id}';
    static readonly PAGE_URL_BUY_TICKETS = '/buy-tickets';
    static readonly PAGE_URL_MY_TICKETS = '/my-tickets';
    static readonly PAGE_URL_TICKET = '/ticket';
    static readonly PAGE_URL_LOGIN = '/login';
    static readonly PAGE_URL_FORGOT_PASSWORD = '/forgot-password';
    static readonly PAGE_URL_CREATE_ACCOUNT = '/create-account';
    //??++DELETE THIS LINE WHEN ALL ABOVE IT. WHY IS IT HERE? CHECKING EACH PAGE FOR WHAT?
    static readonly PAGE_URL_GAME = '/game/{id-hash}';
    static readonly PAGE_URL_LOGGED_OUT = '/logged-out';
    static readonly PAGE_URL_GIFT_TICKET = '/gift-ticket';
    static readonly PAGE_URL_USER_PROFILE = '/user-profile';
    static readonly PAGE_URL_ACCOUNT_CREATED = '/account-created';
    static readonly PAGE_URL_ACCOUNT_CLOSED = '/account-closed';
    static readonly PAGE_URL_EMAIL_VERIFIED = '/email-verified';
    static readonly PAGE_URL_PAYMENT_DETAILS = '/payment-details';
    static readonly PAGE_URL_PASSWORD_RESET_SENT = '/password-reset-sent';
    static readonly PAGE_URL_CONFIRM_DETAILS = '/confirm-details';

    /**
     * Page parameters.
     */
    static readonly PAGE_PARAMETER_ACCOUNT_CREATED_USER_EMAIL = 'user-email';
    static readonly PAGE_PARAMETER_PASSWORD_RESET_SENT_USER_EMAIL = 'user-email';
    static readonly PAGE_PARAMETER_LOGIN_BUY_TICKETS = 'buy-tickets';
    static readonly PAGE_PARAMETER_CAUSE_ID = 'id';
    static readonly PAGE_PARAMETER_CHARITY_ID = 'id';
    static readonly PAGE_PARAMETER_AMBASSADOR_ID = 'id';
    static readonly PAGE_PARAMETER_TICKET_ID = 'id';
    static readonly PAGE_PARAMETER_GIFT_TICKET_ID = 'id';
    static readonly PAGE_PARAMETER_GAME_ID_HASH = 'id-hash';
    static readonly PAGE_PARAMETER_EMAIL_ADDRESS = 'email-address';
    static readonly PAGE_PARAMETER_EMAIL_VERIFICATION_ID = 'verification-id';
    
}   // Constants
