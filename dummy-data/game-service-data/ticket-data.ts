import { Ticket } from "../../services/ticket-service";

/**
 * Dummy data.
 */
export var ticket_data: Ticket[] = [
    {
        // Ticket number 0 purchased by user_1 on 01/01/2018
        ID: 'ticket_1',
        GameID: 'game_1',
        ReserverOrOwnerID: 'user_1',
        Number: 0,
        PurchaseDate: new Date(2018, 1, 1),
        ReservedDate: null,
        OfferedToID: null,
        OfferedDate: null
    },
    {
        // Ticket number 1 purchased by user_1 on 02/01/2018
        ID: 'ticket_2',
        GameID: 'game_1',
        ReserverOrOwnerID: 'user_1',
        Number: 1,
        PurchaseDate: new Date(2018, 1, 2),
        ReservedDate: null,
        OfferedToID: null,
        OfferedDate: null
    },
    {
        // Ticket number 2 purchased by user_2 on 02/01/2018
        ID: 'ticket_3',
        GameID: 'game_1',
        ReserverOrOwnerID: 'user_2',
        Number: 2,
        PurchaseDate: new Date(2018, 1, 2),
        ReservedDate: null,
        OfferedToID: null,
        OfferedDate: null
    },
    {
        // Ticket number 3 reserved by user_1 on 03/01/2018
        ID: 'ticket_4',
        GameID: 'game_1',
        ReserverOrOwnerID: 'user_1',
        Number: 3,
        PurchaseDate: null,
        ReservedDate: new Date(2018, 1, 3),
        OfferedToID: null,
        OfferedDate: null
    },
    {
        // Ticket number 4 purchased by user_2 on 01/01/2018 and offered to user_1.
        ID: 'ticket_5',
        GameID: 'game_1',
        ReserverOrOwnerID: 'user_2',
        Number: 4,
        PurchaseDate: new Date(2018, 1, 1),
        ReservedDate: null,
        OfferedToID: 'user_1',
        OfferedDate: new Date(2018, 1, 2)
    }
];
