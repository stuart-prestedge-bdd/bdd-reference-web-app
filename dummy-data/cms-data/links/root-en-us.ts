export var links_en_us =
    {
        "version": "1.0",
        "language": "en-us",
        "links": [
            {
                "id": "faqs",
                "name": "FAQs (en-us)",
                "link": {
                    "srcType": "json",
                    "json": "faqs-page-en-gb.json"
                }
            },
            {
                "id": "tcs",
                "name": "T&Cs (en-us)",
                "link": {
                    "srcType": "json",
                    "json": "t-and-c-page-en-gb.json"
                }
            },
            {
                "id": "gaming-license",
                "name": "Gaming license (en-us)",
                "link": {
                    "srcType": "url",
                    "url": "https://www.bdd.com/gaming-license.html?lang=en-gb"
                }
            },
            {
                "id": "logout",
                "name": "Logout (en-us)",
                "link": {
                    "srcType": "page",
                    "url": "logout"
                }
            }
        ]
    }