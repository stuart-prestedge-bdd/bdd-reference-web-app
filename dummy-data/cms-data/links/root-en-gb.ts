export var links_en_gb =
    {
        "version": "1.0",
        "language": "en-gb",
        "links": [
            {
                "id": "faqs",
                "name": "FAQs",
                "link": {
                    "srcType": "json",
                    "json": "faqs-page-en-gb.json"
                }
            },
            {
                "id": "tcs",
                "name": "T&Cs",
                "link": {
                    "srcType": "json",
                    "json": "t-and-c-page-en-gb.json"
                }
            },
            {
                "id": "gaming-license",
                "name": "Gaming license",
                "link": {
                    "srcType": "url",
                    "url": "https://www.bdd.com/gaming-license.html?lang=en-gb"
                }
            },
            {
                "id": "logout",
                "name": "Logout",
                "link": {
                    "srcType": "page",
                    "url": "logout"
                }
            }
        ]
    }