export var game_info = 
{
    "version": "1.0",
    "games": [
        {
            "idHash": "game_1_hash",
            "name": "game_1_name",
            "openDate": new Date(Date.UTC(2018, 11, 1, 0, 0, 0)),//"20181101T000000Z",
            "closeDate": new Date(Date.UTC(2019, 6, 1, 0, 0, 0)),//"20190601T000000Z",
            "ticketPrice": 500,
            "frozen": false,
            "ticketServiceURL": "https://tickets.bdd.com",
            "draws": [
                {
                    "idHash": "yyy",
                    "drawDate": new Date(Date.UTC(2019, 2, 1, 0, 0, 0)),//"20190201T000000Z",
                    "drawnDate": null,
                    "amount": 0,
                    "prizeIdHash": null,
                    "winningNumber": 0
                }
            ],
            "prizes": [
                {
                    "idHash": "zzz",
                    "drawHashId": null,
                    "name": "prize_1_name",
                    "shortDescription": "prize_1_short_description",
                    "fullDescription": "prize_1_full_description",
                    "smallImageURL": "<ASSET_ROOT>/prize1-small.png",
                    "largeImageURL": "<ASSET_ROOT>/prize1-large.png",
                }
            ]
        }
    ]
}