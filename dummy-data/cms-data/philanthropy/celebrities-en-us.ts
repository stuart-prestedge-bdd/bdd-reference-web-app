export var ambassadors_en_us = 
{
    "version": "1.0",
    "ambassadors": [
        {
            "id": "111",
            "name": "David Beckham",
            "avatar": [
                {
                    "width": 100,
                    "height": 100,
                    "srcType": "url",
                    "url": "https://www.bdd.com/assets/david-beckham.png"
                }
            ],
            "videoURL": "https://s3/bdd/videos/unicef.mp4",
            "images": [
                {
                    "width": 500,
                    "height": 200,
                    "srcType": "url",
                    "url": "https://www.bdd.com/assets/david-beckham-image-1.png"
                },
                {
                    "width": 500,
                    "height": 200,
                    "srcType": "url",
                    "url": "https://www.bdd.com/assets/david-beckham-image-2.png"
                }
            ],
            "description": {
                        "type": "text",
                        "text": "David spends a lot of time ... (en-us)"
                    },
            "charities": [ "5678" ]
        }
    ]
}