export var charities_en_gb = 
{
    "version": "1.0",
    "charities": [
        {
            "id": "5678",
            "name": "Unicef",
            "logo": [
                {
                    "width": 100,
                    "height": 100,
                    "srcType": "url",
                    "url": "https://www.bdd.com/assets/unicef.png"
                }
            ],
            "summary": {
                        "type": "html",
                        "html": "This charity summary..."
                    },
            "description": {
                        "type": "text",
                        "html": "The charity..."
                    },
            "videoURL": "https://s3/bdd/videos/unicef.mp4",
            "causes": [ "1234" ]
        }
    ]
}