export var charities_en_us = 
{
    "version": "1.0",
    "charities": [
        {
            "id": "5678",
            "name": "Unicef (Enu)",
            "logo": [
                {
                    "width": 100,
                    "height": 100,
                    "srcType": "url",
                    "url": "https://www.bdd.com/assets/unicef.png"
                }
            ],
            "summary": {
                        "type": "html",
                        "html": "This charity summary... (Enu)"
                    },
            "description": {
                        "type": "text",
                        "html": "The charity... (Enu)"
                    },
            "videoURL": "https://s3/bdd/videos/unicef.mp4",
            "causes": [ "1234" ]
        }
    ]
}