export var causes_en_us = 
{
    "version": "1.0",
    "videoUrl": "https://causes.mp4",
    "causes": [
        {
            "id": "1234",
            "name": "Poverty (ue-us)",
            "icon": [
                {
                    "width": 100,
                    "height": 100,
                    "srcType": "url",
                    "url": "https://www.bdd.com/assets/cause-poverty-small.png"
                },
                {
                    "width": 500,
                    "height": 500,
                    "srcType": "url",
                    "url": "https://www.bdd.com/assets/cause-poverty-big.png"
                }
            ],
            "videoUrl": "https://cause.mp4",
            "description": {
                "type": "html",
                "html": "This is <i>very</i> important cause... (ue-us)"
            }
        },
        {
            "id": "5678",
            "name": "Cancer (ue-us)",
            "icon": [
                {
                    "width": 100,
                    "height": 100,
                    "srcType": "svg",
                    "svg": "SOME_BASE_64_ENCODED_SVG"
                }
            ],
            "description": {
                "type": "text",
                "html": "This cause ... (ue-us)"
            }
        }
    ]
}
