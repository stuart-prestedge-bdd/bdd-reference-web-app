export var languages =
    {
        "version": "1.0",
        "languages": [
            {
                "icon": "flags/uk.png",
                "name": "English (UK)",
                "code": "en-gb"
            },
            {
                "icon": "flags/us.png",
                "name": "English (US)",
                "code": "en-us"
            },
            {
                "icon": "flags/france.png",
                "name": "Fançais",
                "code": "fr"
            }
        ]
    }