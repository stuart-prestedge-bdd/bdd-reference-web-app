import { Debug } from '../helpers/debug';
import { FormPage } from '../components/form-page';
import { Constants } from '../constants';
import { InputField } from '../generic/input-field';
import { Action } from '../generic/action';
import { PasswordHelper } from '../helpers/password-helper';
import { ToastHelper } from '../helpers/toast-helper';
import { TextContentService } from '../services/text-content-service';
import { IdentityService } from '../services/identity-service';
import { ErrorHelper } from '../helpers/error-helper';
import { PageBase } from '../generic/page-base';
import { EmailHelper } from '../helpers/email-helper';

/**
 * Class representing the confirm details page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47087860/2.2.5.24.+Confirm+Details+Page
 */
export class ConfirmDetailsPage extends FormPage {

    /**
     * The given name input field.
     */
    public readonly GivenNameField = new InputField(TextContentService.TEXT_NAME_GIVEN_NAME_HINT);

    /**
     * The family name input field.
     */
    public readonly FamilyNameField = new InputField(TextContentService.TEXT_NAME_FAMILY_NAME_HINT);

    /**
     * The password input field.
     */
    public readonly PasswordField = new InputField(TextContentService.TEXT_NAME_PASSWORD_HINT);

    //???Country & DOB?

    /**
     * The email address of the new user.
     */
    private EmailAddress: string;

    /**
     * The verification code sent to the new user.
     */
    private VerificationCode: string;

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_CONFIRM_DETAILS, false);
        Debug.Tested();
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doSetup(() => {
            this_.GivenNameField.setup(() => {
                this_.FamilyNameField.setup(() => {
                    this_.PasswordField.setup(() => {
                        callback();
                    });
                });
            });
        });
    }

    /**
     * Loads the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLoadStaticText(() => {
            let names = [
                TextContentService.TEXT_NAME_TITLE_CONFIRM_DETAILS,
                TextContentService.TEXT_NAME_BUTTON_CONFIRM_DETAILS
            ];
            TextContentService.getTextValues(names, (values) => {
                Debug.AssertValid(values);
                this_.Title.setValue(values.get(TextContentService.TEXT_NAME_TITLE_CONFIRM_DETAILS));
                this_.Button.setLabel(values.get(TextContentService.TEXT_NAME_BUTTON_CONFIRM_DETAILS));
                callback();
            });
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.GivenNameField);
        Debug.AssertValid(this.FamilyNameField);
        Debug.AssertValid(this.PasswordField);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.GivenNameField.languageChanged(() => {
                this_.FamilyNameField.languageChanged(() => {
                    this_.PasswordField.languageChanged(() => {
                        callback();
                    });
                });
            });
        });
    }

    /**
     * Called when the page is opened.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);

        let this_ = this;
        super.doOpen((currentPage) => {
            this_.EmailAddress = this_.getArgument(Constants.PAGE_PARAMETER_EMAIL_ADDRESS);
            Debug.AssertValidOrNull(this_.EmailAddress);
            this_.VerificationCode = this_.getArgument(Constants.PAGE_PARAMETER_EMAIL_VERIFICATION_ID);
            Debug.AssertValidOrNull(this_.VerificationCode);
            if (!EmailHelper.isValidEmailAddress(this_.EmailAddress) || !this_.VerificationCode) {
                ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_INVALID_VERIFICATION_DETAILS);
                this_.Application.navigateTo(Constants.PAGE_URL_HOME, () => {//??++THINK ABOUT BEST PAGE TO GO TO HERE!
                    callback(currentPage);
                });
            } else {
                callback(currentPage);
            }
        });
    }

    /**
     * Called when the 'Create account' button is clicked.
     */
    protected doButtonClicked(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);
        Debug.AssertValid(this.GivenNameField);
        Debug.AssertValid(this.FamilyNameField);
        Debug.AssertValid(this.PasswordField);

        let this_ = this;
        let givenName = this_.GivenNameField.getValue();
        Debug.AssertString(givenName);
        let familyName = this_.FamilyNameField.getValue();
        Debug.AssertString(familyName);
        let password = this_.PasswordField.getValue();
        Debug.AssertString(password);
        let dateOfBirth = null;//??++
        let addressX = null;//??++
        let allow = false;
        IdentityService.verifyEmailWithDetails(this_.EmailAddress, this_.VerificationCode, givenName, familyName, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, addressX, addressX, allow, (error) => {
            Debug.AssertValidOrNull(error);
            if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_VERIFY_EMAIL_ADDRESS)) {
                this_.Application.navigateTo(Constants.PAGE_URL_HOME, () => {//??++THINK ABOUT BEST PAGE TO GO TO HERE!
                    callback();
                });
            } else {
                callback();
            }
        });
    }

    /**
     * Reset the input fields so they are no longer in error state.
     */
    protected doResetInputFieldStates() {
        Debug.Tested();
        Debug.AssertValid(this.GivenNameField);
        Debug.AssertValid(this.FamilyNameField);
        Debug.AssertValid(this.PasswordField);

        this.GivenNameField.resetState();
        this.FamilyNameField.resetState();
        this.PasswordField.resetState();
    }
    
    /**
     * Validate the inputs.
     */
    protected doValidateInputs(): boolean {
        Debug.Tested();
        Debug.AssertValid(this.GivenNameField);
        Debug.AssertValid(this.FamilyNameField);
        Debug.AssertValid(this.PasswordField);

        let retVal = false;
        let givenName = this.GivenNameField.getValue();
        Debug.AssertValidOrNull(givenName);
        if (!!givenName) {
            let familyName = this.FamilyNameField.getValue();
            Debug.AssertString(familyName);
            if (!!familyName) {
                let password = this.PasswordField.getValue();
                Debug.AssertValidOrNull(password);
                if (!!password && PasswordHelper.isValidPassword(password)) {
                    retVal = true;
                } else {
                    Debug.Untested();
                    ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_INVALID_PASSWORD);
                    this.PasswordField.setErrorState();
                }
            } else {
                Debug.Untested();
                ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_INVALID_FAMILY_NAME);
                this.GivenNameField.setErrorState();
            }
        } else {
            Debug.Untested();
            ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_INVALID_GIVEN_NAME);
            this.GivenNameField.setErrorState();
        }
        return retVal;
    }
    
}   // ConfirmDetailsPage
