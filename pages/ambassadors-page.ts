import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { PhilanthropyContentService } from '../services/philanthropy-content-service';
import { VideoTile } from '../generic/video-tile';
import { TextContentService } from '../services/text-content-service';
import { IconTitleTextList } from '../generic/icon-title-text-list';
import { Action } from '../generic/action';
import { ImageHelper } from '../helpers/image-helper';
import { ErrorHelper } from '../helpers/error-helper';
import { Constants } from '../constants';
import { TextField } from '../generic/text-field';
import { URLHelper } from '../helpers/url-helper';

/**
 * Class representing the ambassadors page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47218944/2.2.5.7.+Ambassadors+Page
 */
export class AmbassadorsPage extends Page {

    /**
     * The page title.
     */
    public readonly Title = new TextField();

    /**
     * The page description.
     */
    public readonly Description = new TextField();

    /**
     * The icon/title/text list component.
     */
    public readonly IconTitleTextList = new IconTitleTextList();//??++CHANGE TO AmbassadorTileList

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_AMBASSADORS, false);
        Debug.Tested();
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.Description);
        Debug.AssertValid(this.IconTitleTextList);

        let this_ = this;
        super.doSetup(() => {
            PhilanthropyContentService.getAmbassadors((ambassadors) => {
                Debug.AssertValid(ambassadors);
                ambassadors.forEach(ambassador => {
                    Debug.AssertValid(ambassador);
                    let image = ImageHelper.selectImage(ambassador.avatar);
                    Debug.AssertValid(image);
                    let url = URLHelper.replaceURLParameter(Constants.PAGE_URL_AMBASSADOR, Constants.PAGE_PARAMETER_AMBASSADOR_ID, ambassador.id);
                    this_.IconTitleTextList.addItem(image, ambassador.name, ambassador.description, new Action(url));
                });
                this_.IconTitleTextList.setup(() => {
                    callback();
                });
            });
        });
    }
    
    /**
     * Load the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLoadStaticText(() => {
            let names = [
                TextContentService.TEXT_NAME_TITLE_AMBASSADORS,
                TextContentService.TEXT_NAME_DESCRIPTION_AMBASSADORS
            ];
            TextContentService.getTextValues(names, (values) => {
                Debug.AssertValid(values);
                this_.Title.setValue(values.get(TextContentService.TEXT_NAME_TITLE_AMBASSADORS));
                this_.Description.setValue(values.get(TextContentService.TEXT_NAME_DESCRIPTION_AMBASSADORS));
                callback();
            });
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.Description);
        Debug.AssertValid(this.IconTitleTextList);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.Title.languageChanged(() => {
                this_.Description.languageChanged(() => {
                    this_.IconTitleTextList.languageChanged(() => {
                        callback();
                    });
                });
            });
        });
    }

}   // AmbassadorsPage

