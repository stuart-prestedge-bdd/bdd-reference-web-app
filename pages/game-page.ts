import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { PageBase } from '../generic/page-base';
import { Constants } from '../constants';
import { Game } from '../data-models/game-model';
import { GameContentService } from '../services/game-content-service';
import { ErrorHelper } from '../helpers/error-helper';
import { DateHelper } from '../helpers/date-helper';
import { TextField } from '../generic/text-field';
import { TextContentService } from '../services/text-content-service';

/**
 * Class representing the game page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47153441/2.2.5.22.+Game+Page
 */
export class GamePage extends Page {

    /**
     * Sub-components.
     */
    public readonly Title = new TextField();
    public readonly GameOpenField = new TextField();
    public readonly GameCloseField = new TextField();

    /**
     * The game (<game>).
     */
    private Game: Game;

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_GAME, false);
        Debug.Tested();

        this.addParameter(Constants.PAGE_PARAMETER_GAME_ID_HASH);    // <GAME_ID_HASH>
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.GameOpenField);
        Debug.AssertValid(this.GameCloseField);

        let this_ = this;
        super.doSetup(() => {
            this_.Title.setup(() => {
                this_.GameOpenField.setup(() => {
                    this_.GameCloseField.setup(() => {
                        callback();
                    });
                });
            });
        });
    }
    
    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.GameOpenField);
        Debug.AssertValid(this.GameCloseField);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.GameOpenField.languageChanged(() => {
                this_.GameCloseField.languageChanged(() => {
                    callback();
                });
            });
        });
    }

    /**
     * Called when the page is opened.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.GameOpenField);
        Debug.AssertValid(this.GameCloseField);

        let this_ = this;
        super.doOpen((currentPage) => {
            let idHash = this_.getArgument(Constants.PAGE_PARAMETER_GAME_ID_HASH);
            Debug.AssertValidOrNull(idHash);
            if (!!idHash) {
                GameContentService.getGameByIDHash(idHash, (error, game) => {
                    Debug.AssertErrorOrObject(error, game);
                    this_.Game = game;
                    if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_GAME)) {
                        this_.Title.setValue(game.name);
                        let openDate = DateHelper.timestampTextToDate(game.openDate);
                        let closeDate = DateHelper.timestampTextToDate(game.closeDate);
                        DateHelper.timestampToText(openDate, (timestampText) => {
                            Debug.AssertString(timestampText);
                            this_.GameOpenField.setValue(timestampText);
                            DateHelper.timestampToText(closeDate, (timestampText) => {
                                Debug.AssertString(timestampText);
                                this_.GameCloseField.setValue(timestampText);
                                callback(currentPage);
                            });
                        });
                    } else {
                        //??++SETUP FIELDS APPROPRIATES = "NO GAME SELECTED"
                        callback(currentPage);
                    }
                });
            } else {
                this_.Game = null;
                callback(currentPage);
            }
        });
    }

}   // GamePage

