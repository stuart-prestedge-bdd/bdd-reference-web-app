import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { PhilanthropyContentService } from '../services/philanthropy-content-service';
import { VideoTile } from '../generic/video-tile';
import { TextContentService } from '../services/text-content-service';
import { IconTitleTextList } from '../generic/icon-title-text-list';
import { Action } from '../generic/action';
import { ImageHelper } from '../helpers/image-helper';
import { Cause } from '../data-models/philanthropy-model';
import { PageBase } from '../generic/page-base';
import { ErrorHelper } from '../helpers/error-helper';
import { Constants } from '../constants';
import { ImageComponent } from '../generic/image-component';

/**
 * Class representing the cause page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47251758/2.2.5.4.+Cause+Page
 */
export class CausePage extends Page {

    /**
     * The cause to show.
     */
    private Cause: Cause;

    /**
     * The video tile.
     */
    public readonly VideoTile = new VideoTile();

    /**
     * The cause logo image.
     */
    public readonly CauseLogo = new ImageComponent();

    /**
     * The icon/title/text list component.
     */
    public readonly IconTitleTextList = new IconTitleTextList();

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_CAUSE, false);
        Debug.Tested();

        this.addParameter(Constants.PAGE_PARAMETER_CAUSE_ID);    // <CAUSE_ID>
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }
    
    /**
     * Called when the page is opened.
     * Loads the correct cause.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.VideoTile);
        Debug.AssertValid(this.CauseLogo);
        Debug.AssertValid(this.IconTitleTextList);

        let this_ = this;
        super.doOpen((currentPage) => {
            let id = this_.getArgument(Constants.PAGE_PARAMETER_CAUSE_ID);
            Debug.AssertValidOrNull(id);
            if (!!id) {
                PhilanthropyContentService.getCausesInfo((causesInfo) => {
                    Debug.AssertValid(causesInfo);
                    Debug.AssertValid(causesInfo.causes);
                    this_.Cause = causesInfo.causes.get(id);
                    Debug.AssertValidOrNull(this_.Cause);
                    if (this_.Cause != null) {
                        this_.VideoTile.setURL(this_.Cause.videoURL, () => {
                            let logoImage = ImageHelper.selectImage(this_.Cause.icon);
                            Debug.AssertValid(logoImage);
                            this_.CauseLogo.setImage(logoImage, () => {
                                PhilanthropyContentService.getCharities((charities) => {
                                    Debug.AssertValid(charities);
                                    charities.forEach(charity => {
                                        Debug.AssertValid(charity);
                                        Debug.AssertValid(charity.causes);
                                        if (charity.causes.indexOf(this_.Cause.id) !== -1) {
                                            let image = ImageHelper.selectImage(charity.logo);
                                            Debug.AssertValid(image);
                                            let action = new Action(`${Constants.PAGE_URL_CHARITY}?${Constants.PAGE_PARAMETER_CHARITY_ID}=${charity.id}`);
                                            this_.IconTitleTextList.addItem(image, charity.name, charity.description, action);
                                        }
                                    });
                                    this_.IconTitleTextList.setup(() => {
                                        callback(currentPage);
                                    });
                                });
                            });
                        });
                    } else {
                        Debug.Untested();
                        this_.reset(() => {
                            callback(currentPage);
                        });
                    }
                });
            } else {
                Debug.Untested();
                this_.reset(() => {
                    callback(currentPage);
                });
            }
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.VideoTile);
        Debug.AssertValid(this.CauseLogo);
        Debug.AssertValid(this.IconTitleTextList);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.VideoTile.languageChanged(() => {
                this_.CauseLogo.languageChanged(() => {
                    this_.IconTitleTextList.languageChanged(() => {
                        callback();
                    });
                });
            });
        });
    }

    /**
     * Reset the contents of this page.
     */
    private reset(callback: () => void) {
        Debug.Untested();
        Debug.AssertValid(callback);
    
        let this_ = this;
        this_.Cause = null;
        this_.VideoTile.reset(() => {
            this_.IconTitleTextList.reset(() => {
                callback();
            });
        });
    }

    /**
     * Get the cause.
     */
    public getCause(): Cause {
        Debug.Untested();
        Debug.AssertValidOrNull(this.Cause);

        return this.Cause;
    }

}   // CausePage

