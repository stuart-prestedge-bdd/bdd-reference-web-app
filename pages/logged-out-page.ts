import { Debug } from '../helpers/debug';
import { MessagePage } from '../components/message-page';
import { TextContentService } from '../services/text-content-service';
import { ErrorHelper } from '../helpers/error-helper';
import { Constants } from '../constants';

/**
 * Class representing the logged out page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47251806/2.2.5.19.+Logged+Out+Page
 */
export class LoggedOutPage extends MessagePage {

    /**
     * Constructor.
     */
    constructor() {
        Debug.Tested();

        super(Constants.PAGE_URL_LOGGED_OUT, false);
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);

        let this_ = this;
        super.doSetup(() => {
            this_.Application.autoNavigateTo(Constants.PAGE_URL_HOME, 5);
            callback();
        });
    }
    
    /**
     * Load the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLoadStaticText(() => {
            let names = [
                TextContentService.TEXT_NAME_TITLE_LOGGED_OUT,
                TextContentService.TEXT_NAME_MESSAGE_LOGGED_OUT
            ];
            TextContentService.getTextValues(names, (values) => {
                Debug.AssertValid(values);
                this_.Title = values.get(TextContentService.TEXT_NAME_TITLE_LOGGED_OUT);
                this_.DescriptionHTML = values.get(TextContentService.TEXT_NAME_MESSAGE_LOGGED_OUT);
                callback();
            });
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLanguageChanged(() => {
            callback();
        });
    }

}   // LoggedOutPage

