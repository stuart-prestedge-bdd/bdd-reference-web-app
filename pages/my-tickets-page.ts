import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { BuyTicketTile } from '../components/buy-ticket-tile';
import { TicketService, TicketInfo } from '../services/ticket-service';
import { WebApp } from '../components/web-app';
import { ErrorHelper } from '../helpers/error-helper';
import { ListComponent, LIST_ACTION_OPEN } from '../generic/list-component';
import { PageBase } from '../generic/page-base';
import { Constants } from '../constants';
import { TextContentService } from '../services/text-content-service';

/**
 * Class representing the my tickets page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47153381/2.2.5.10.+My+Tickets+Page
 */
export class MyTicketsPage extends Page {

    /**
     * The tickets. Null if not loaded yet or failed to load.
     */
    private TicketInfos: TicketInfo[];

    /**
     * The 'buy ticket' tile.
     */
    public readonly BuyTicketTile = new BuyTicketTile();

    /**
     * The ticket list component.
     */
    public readonly TicketList = new ListComponent();

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_MY_TICKETS);
        Debug.Tested();
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doSetup(() => {
            this_.BuyTicketTile.setup(() => {
                this_.TicketList.setPerformActionCallback(MyTicketsPage.performTicketListAction, this_);
                callback();
            });
        });
    }
    
    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.BuyTicketTile);
        Debug.AssertValid(this.TicketList);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.BuyTicketTile.languageChanged(() => {
                this_.TicketList.languageChanged(() => {
                    callback();
                });
            });
        });
    }

    /**
     * Called when the page is opened.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);

        let this_ = this;
        super.doOpen((currentPage) => {
            this_.Application.getCurrentUser((currentUser, accessToken) => {
                Debug.AssertValidOrNull(currentUser);
                if (currentUser != null) {
                    Debug.AssertString(currentUser.ID);
                    Debug.AssertString(accessToken);
                    TicketService.getUserTickets(currentUser.ID, accessToken, (error, ticketInfos) => {
                        Debug.AssertErrorOrObject(error, ticketInfos);
                        ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_TICKETS);
                        this_.TicketInfos = ticketInfos;
                        if (error == null) {
                            this_.TicketList.setObjects(ticketInfos, () => {
                                callback(currentPage);
                            });
                        } else {
                            Debug.Untested();
                            callback(currentPage);
                        }
                    });
                }
            });
        });
    }

    /**
     * Called when an action is performed on a ticket in the list.
     */
    private static performTicketListAction(context: any, action: string, obj: any, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(context);
        Debug.Assert(context instanceof MyTicketsPage);
        Debug.AssertString(action);
        Debug.Assert(action === LIST_ACTION_OPEN);
        Debug.AssertValid(obj);
        //???Debug.Assert(obj instanceof TicketInfo);
        Debug.AssertValid(callback);
        
        let myTicketsPage = <MyTicketsPage>context;
        Debug.AssertValid(myTicketsPage.Application);
        let ticketID = (<TicketInfo>obj).ID;
        Debug.AssertString(ticketID);
        let url = `${Constants.PAGE_URL_TICKET}?${Constants.PAGE_PARAMETER_TICKET_ID}=${ticketID}`;
        myTicketsPage.Application.navigateTo(url, callback);
    }
    
}   // MyTicketsPage

