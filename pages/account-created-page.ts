import { Debug } from '../helpers/debug';
import { MessagePage } from '../components/message-page';
import { TextContentService } from '../services/text-content-service';
import { Page } from '../components/page';
import { PageBase } from '../generic/page-base';
import { Constants } from '../constants';
import { ErrorHelper } from '../helpers/error-helper';

/**
 * Class representing the account created page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47251802/2.2.5.17.+Account+Created+Page
 */
export class AccountCreatedPage extends MessagePage {

    /**
     * Constructor.
     */
    constructor() {
        Debug.Tested();

        super(Constants.PAGE_URL_ACCOUNT_CREATED, false);
        this.addParameter(Constants.PAGE_PARAMETER_ACCOUNT_CREATED_USER_EMAIL);
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }
    
    /**
     * Load the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        
        let this_ = this;
        super.doLoadStaticText(() => {
            TextContentService.getTextValue(TextContentService.TEXT_NAME_TITLE_ACCOUNT_CREATED, (value) => {
                Debug.AssertString(value);
                this_.Title = value;
                callback();
            });
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doLanguageChanged(callback);
    }

    /**
     * Called when the page is opened.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doOpen((currentPage) => {
            TextContentService.getTextValue(TextContentService.TEXT_NAME_MESSAGE_ACCOUNT_CREATED, (value) => {
                Debug.AssertString(value);
                let userEmail = this_.getArgument(Constants.PAGE_PARAMETER_ACCOUNT_CREATED_USER_EMAIL);//??++PASS THIS IN!
                this_.DescriptionHTML = value.replace('${email}', userEmail);
                callback(currentPage);
            });
        });
    }

}   // AccountCreatedPage

