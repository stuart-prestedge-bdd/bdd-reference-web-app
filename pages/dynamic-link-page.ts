import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { Link } from '../data-models/links-model';
import { LinksContentService } from '../services/links-content-service';
import { TARGET_SRC_TYPE_JSON, TEXT_TYPE_HTML } from '../generic/common-object-model';
import { TextField } from '../generic/text-field';
import { ErrorHelper } from '../helpers/error-helper';
import { TextContentService } from '../services/text-content-service';

/**
 * Class representing a dynamic page.
 */
export class DynamicLinkPage extends Page {

    /**
     * The text fields.
     */
    public readonly TitleField = new TextField();
    public readonly TextField = new TextField();

    /**
     * The link being shown
     */
    private Link: Link;

    /**
     * Constructor.
     */
    constructor(link: Link) {
        super(`/links/${link.id}`, false);
        Debug.Tested();
        Debug.AssertValid(link);
        Debug.AssertString(link.id);
        Debug.Assert(link.link.srcType === TARGET_SRC_TYPE_JSON);

        this.Link = link;
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Link);
        Debug.AssertValid(this.Link.link);
        Debug.AssertString(this.Link.link.json);

        let this_ = this;
        super.doSetup(() => {
            LinksContentService.getLink(this_.Link.link.json, (link) => {
                Debug.AssertValidOrNull(link);
                if (link != null) {
                    Debug.AssertValid(link.title);
                    Debug.AssertValid(link.text);
                    Debug.Assert(link.text.type === TEXT_TYPE_HTML);
                    Debug.AssertValid(link.text.html);
                    this_.TitleField.setValue(link.title);
                    this_.TextField.setValue(link.text.html);
                }
                callback();
            });
        });
    }
    
}   // DynamicLinkPage

