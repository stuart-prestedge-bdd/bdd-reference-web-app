import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { PageBase } from '../generic/page-base';
import { Constants } from '../constants';

/**
 * Class representing the payment details page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47219000/2.2.5.21.+Payment+Details+Page
 */
export class PaymentDetailsPage extends Page {

    /**
     * Constructor.
     */
    constructor() {
        Debug.Tested();

        super(Constants.PAGE_URL_PAYMENT_DETAILS);
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Uncoded();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }
    
    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLanguageChanged(() => {
            callback();
        });
    }

    /**
     * Called when the page is opened.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Uncoded();
        Debug.AssertValid(callback);

        super.doOpen(callback);
    }

}   // PaymentDetailsPage

