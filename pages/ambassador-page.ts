import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { PhilanthropyContentService } from '../services/philanthropy-content-service';
import { VideoTile } from '../generic/video-tile';
import { TextContentService } from '../services/text-content-service';
import { IconTitleTextList } from '../generic/icon-title-text-list';
import { Action } from '../generic/action';
import { ImageHelper } from '../helpers/image-helper';
import { Ambassador } from '../data-models/philanthropy-model';
import { ToastHelper } from '../helpers/toast-helper';
import { PageBase } from '../generic/page-base';
import { ErrorHelper } from '../helpers/error-helper';
import { Constants } from '../constants';
import { ImageComponent } from '../generic/image-component';

/**
 * Class representing the ambassador page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47218962/2.2.5.8.+Ambassador+Page
 */
export class AmbassadorPage extends Page {

    /**
     * The ambassador to show.
     */
    private Ambassador: Ambassador;

    /**
     * The video tile.
     */
    public readonly VideoTile = new VideoTile();

    /**
     * The image tile.
     */
    public readonly ImageTile = new ImageComponent();

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_AMBASSADOR, false);
        Debug.Tested();

        this.addParameter(Constants.PAGE_PARAMETER_AMBASSADOR_ID);    // <AMBASSADOR_ID>
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.VideoTile);
        Debug.AssertValid(this.ImageTile);

        let this_ = this;
        super.doSetup(() => {
            this_.VideoTile.setup(() => {
                this_.ImageTile.setup(() => {
                    callback();
                });
            });
        });
    }
    
    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.VideoTile);
        Debug.AssertValid(this.ImageTile);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.VideoTile.languageChanged(() => {
                this_.ImageTile.languageChanged(() => {
                    callback();
                });
            });
        });
    }

    /**
     * Called when the page is opened.
     * Loads the correct ambassador.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(this.VideoTile);
        Debug.AssertValid(this.ImageTile);

        let this_ = this;
        super.doOpen((currentPage) => {
            this_.Ambassador = null;
            let id = this_.getArgument(Constants.PAGE_PARAMETER_AMBASSADOR_ID);
            Debug.AssertValidOrNull(id);
            if (!!id) {
                PhilanthropyContentService.getAmbassadors((ambassadors) => {
                    Debug.AssertValid(ambassadors);
                    this_.Ambassador = ambassadors[id];
                    Debug.AssertValidOrNull(this_.Ambassador);
                    if (this_.Ambassador != null) {
                        this_.VideoTile.setURL(this_.Ambassador.videoURL, () => {
                            let image = ImageHelper.selectImage(this_.Ambassador.images);
                            Debug.AssertValid(image);
                            this_.ImageTile.setImage(image, () => {
                                callback(currentPage);
                            });
                        });
                    } else {
                        Debug.Untested();
                        callback(currentPage);
                    }
                });
            } else {
                Debug.Untested();
                callback(currentPage);
            }
        });
    }

    /**
     * Get the ambassador.
     */
    public getAmbassador(): Ambassador {
        Debug.Untested();
        Debug.AssertValidOrNull(this.Ambassador);

        return this.Ambassador;
    }

}   // AmbassadorPage
