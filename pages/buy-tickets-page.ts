import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { Button } from '../generic/button';
import { TextContentService } from '../services/text-content-service';
import { Component } from '../generic/component';
import { Constants } from '../constants';
import { ToastHelper } from '../helpers/toast-helper';
import { Action } from '../generic/action';
import { PageBase } from '../generic/page-base';
import { ErrorHelper } from '../helpers/error-helper';
import { TextField } from '../generic/text-field';
import { InputField } from '../generic/input-field';
import { Tickbox } from '../generic/tickbox';
import { WebApp } from '../components/web-app';
import { ChooseNumberPopupWindow } from '../components/choose-numbers-popup-window';
import { TicketNumberComponent } from '../components/ticket-number-component';
import { TicketService } from '../services/ticket-service';
import { IApplication } from '../interfaces/application';
//??--import { IdentityService } from '../services/identity-service';
import { ChangeDateOfBirthPopupWindow } from '../components/change-dob-popup-window';
import { UserDetails, DateOnly } from '../data-models/identity-model';
import { DateHelper } from '../helpers/date-helper';
import { PaymentService } from '../services/payment-service';
import { LocalStorageService } from '../services/local-storage-service';
import { PopupWindow } from '../generic/popup-window';

class NumberLine extends Component {

    /**
     * The application.
     */
    private Application: IApplication;

    /**
     * The save numbers callback function.
     */
    private SaveCallback: (context: any, callback: () => void) => void;

    /**
     * The remove link callback function.
     */
    private RemoveCallback: (context: any, numberLine: NumberLine, callback: () => void) => void;

    /**
     * The callback context.
     */
    private CallbackContext: any;

    /**
     * The ticket number.
     */
    public readonly Number = new TicketNumberComponent();

    /**
     * The 'Choose my own' link.
     */
    public readonly ChooseMyOwnLink = new TextField(TextContentService.TEXT_NAME_LINK_CHOOSE_MY_OWN);

    /**
     * The 'Choose for me' link.
     */
    public readonly ChooseForMeLink = new TextField(TextContentService.TEXT_NAME_LINK_CHOOSE_FOR_ME);

    /**
     * The Remove button.
     */
    public readonly RemoveButton = new Button(TextContentService.TEXT_NAME_BUTTON_REMOVE);

    /**
     * Constructor.
     */
    constructor(application: IApplication,
                callbackContext: any,
                saveCallback: (context: any, callback: () => void) => void,
                removeCallback?: (context: any, numberLine: NumberLine, callback: () => void) => void) {
        super();
        Debug.Tested();
        Debug.AssertValid(application);
        Debug.AssertValid(callbackContext);
        Debug.AssertValid(saveCallback);
        Debug.AssertValidOrNull(removeCallback);

        this.Application = application;
        this.CallbackContext = callbackContext;
        this.SaveCallback = saveCallback;
        this.RemoveCallback = removeCallback;
    }

    /**
     * Setup the component.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doSetup(() => {
            this_.Number.setup(() => {
                this_.ChooseMyOwnLink.setExecuteAction(new Action(null, NumberLine.chooseMyOwnClicked, this_));
                this_.ChooseForMeLink.setExecuteAction(new Action(null, NumberLine.chooseForMeClicked, this_));
                this_.RemoveButton.setHidden(this_.RemoveCallback == null);
                this_.RemoveButton.setExecuteAction(new Action(null, NumberLine.removeLineClicked, this_));
                callback();
            });
        });
    }
    
    /**
     * Loads the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.ChooseMyOwnLink);
        Debug.AssertValid(this.ChooseForMeLink);
        Debug.AssertValid(this.RemoveButton);

        let this_ = this;
        super.doLoadStaticText(() => {
            callback();
        });
    }

    /**
     * Called when the 'Choose my own' link is clicked.
     */
    private static chooseMyOwnClicked(context: object, callback: () => void) {
        Debug.Untested();
        Debug.AssertValid(context);
        Debug.Assert(context instanceof NumberLine);
        Debug.AssertValid(callback);
        
        let numberLine = <NumberLine>context;
        Debug.AssertValid(numberLine.Application);
        Debug.AssertValid(numberLine.CallbackContext);
        Debug.AssertValid(numberLine.SaveCallback);
        numberLine.Application.showPopup(new ChooseNumberPopupWindow('choose-number'), (inputs) => {
            Debug.Untested();
            Debug.AssertValid(inputs);
            if (inputs[PopupWindow.ButtonInputName] === PopupWindow.ButtonInputValueOK) {
                let number = inputs[ChooseNumberPopupWindow.NumberInputName];
                numberLine.Number.setValue(number);
                numberLine.SaveCallback(numberLine.CallbackContext, () => {
                    callback();
                });
            } else {
                callback();
            }
        });
    }

    /**
     * Called when the 'Choose for me' link is clicked.
     */
    private static chooseForMeClicked(context: object, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(context);
        Debug.Assert(context instanceof NumberLine);
        Debug.AssertValid(callback);
        
        let numberLine = <NumberLine>context;
        Debug.AssertValid(numberLine.Application);
        Debug.AssertValid(numberLine.CallbackContext);
        Debug.AssertValid(numberLine.SaveCallback);
        Debug.AssertValid(numberLine.Number);
        numberLine.Application.getCurrentUser((currentUser, accessToken) => {
            Debug.AssertValidOrNull(currentUser);
            if (currentUser != null) {
                Debug.AssertString(currentUser.ID);
                Debug.AssertString(accessToken);
                TicketService.chooseTicket(currentUser.ID, (error, ticket) => {
                    if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_CHOOSE_TICKET)) {
                        Debug.AssertValid(ticket);
                        numberLine.Number.setValue(ticket.Number);
                        numberLine.SaveCallback(numberLine.CallbackContext, callback);
                    } else {
                        callback();
                    }
                });
            } else {
                callback();
            }
        });
    }

    /**
     * Called when the 'Remove' link is clicked.
     */
    private static removeLineClicked(context: object, callback: () => void) {
        Debug.Untested();
        Debug.AssertValid(context);
        Debug.Assert(context instanceof NumberLine);
        Debug.AssertValid(callback);
        
        let numberLine = <NumberLine>context;
        Debug.AssertValid(numberLine.CallbackContext);
        Debug.AssertValid(numberLine.RemoveCallback);
        numberLine.RemoveCallback(numberLine.CallbackContext, numberLine, callback);
    }

}   // NumberLine

/**
 * Class representing the buy tickets page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47251762/2.2.5.9.+Buy+Tickets+Page
 */
export class BuyTicketsPage extends Page {

    /**
     * The static text.
     */
    public readonly Title = new TextField(TextContentService.TEXT_NAME_TITLE_BUY_TICKET);
    public readonly Description = new TextField(TextContentService.TEXT_NAME_DESCRIPTION_BUY_TICKETS);
    
    /**
     * The number lines.
     */
    public readonly NumberLines: NumberLine[] = [];

    /**
     * The 'Add' button.
     */
    public readonly AddButton = new Button(TextContentService.TEXT_NAME_BUTTON_ADD);
    
    /**
     * The 'Gift' tickbox & fields.
     */
    public readonly GiftTickbox = new Tickbox();
    public readonly GiftEmail = new InputField(TextContentService.TEXT_NAME_GIFT_EMAIL_HINT);
    public readonly GiftMessage = new InputField(TextContentService.TEXT_NAME_GIFT_MESSAGE_HINT);
    
    /**
     * The 'Buy' button.
     */
    public readonly BuyButton = new Button(TextContentService.TEXT_NAME_BUTTON_BUY);
    
    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_BUY_TICKETS);
        Debug.Tested();
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.Description);
        Debug.AssertValid(this.AddButton);
        Debug.AssertValid(this.GiftTickbox);
        Debug.AssertValid(this.GiftEmail);
        Debug.AssertValid(this.GiftMessage);
        Debug.AssertValid(this.BuyButton);

        let this_ = this;
        super.doSetup(() => {
            this_.GiftTickbox.setExecuteAction(new Action(null, BuyTicketsPage.giftClicked, this_));
            this_.BuyButton.setExecuteAction(new Action(null, BuyTicketsPage.buyClicked, this_));
            this_.Title.setup(() => {
                this_.Description.setup(() => {
                    this_.AddButton.setup(() => {
                        this_.GiftTickbox.setup(() => {
                            this_.GiftEmail.setup(() => {
                                this_.GiftMessage.setup(() => {
                                    this_.BuyButton.setup(() => {
                                        callback();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }
    
    /**
     * Virtual method to load the static text.
     * Override to load static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLoadStaticText(() => {
            callback();
        });        
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.Description);
        Debug.AssertValid(this.NumberLines);
        Debug.AssertValid(this.AddButton);
        Debug.AssertValid(this.GiftTickbox);
        Debug.AssertValid(this.GiftEmail);
        Debug.AssertValid(this.GiftMessage);
        Debug.AssertValid(this.BuyButton);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.Title.languageChanged(() => {
                this_.Description.languageChanged(() => {
                    this_.AddButton.languageChanged(() => {
                        this_.GiftTickbox.languageChanged(() => {
                            this_.GiftEmail.languageChanged(() => {
                                this_.GiftMessage.languageChanged(() => {
                                    this_.BuyButton.languageChanged(() => {
                                        this_.numberLineLanguageChanged(() => {
                                            callback();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }

    /**
     * Changes the languages of the pages.
     */
    private numberLineLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.NumberLines);

        let numberLines = [ ...this.NumberLines ];
        this.doNumberLineLanguageChanged(numberLines, () => {
            callback();
        });
    }
    
    /**
     * Changes the languages of the number lines.
     */
    private doNumberLineLanguageChanged(numberLines: NumberLine[], callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(numberLines);
        Debug.AssertValid(callback);

        if (numberLines.length > 0) {
            let numberLine = numberLines.splice(0, 1)[0];
            Debug.AssertValid(numberLine);
            numberLine.languageChanged(() => {
                this.doNumberLineLanguageChanged(numberLines, callback);
            })
        } else {
            callback();
        }
    }
    
    /**
     * Called when the page is opened.
     * Loads the numbers.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doOpen(() => {
            this_.loadNumbers((numbers) => {
                Debug.AssertValidOrNull(numbers);
                this_.NumberLines.splice(0);
                if (numbers != null) {
                    numbers.forEach((number, index) => {
                        Debug.AssertValid(number);
                        let removeCallback = (index > 0) ? BuyTicketsPage.removeLine : null;
                        let numberLine = new NumberLine(this_.Application, this_, BuyTicketsPage.saveNumbers, removeCallback);
                        numberLine.Number.setValue(number);
                        this_.NumberLines.push(numberLine);
                    });
                    this_.setupNumberLines(() => {
                        callback(this_);
                    });
                } else {
                    let numberLine = new NumberLine(this_.Application, this_, BuyTicketsPage.saveNumbers);
                    this_.NumberLines.push(numberLine);
                    numberLine.setup(() => {                
                        callback(this_);
                    });
                }
            });
        });
    }

    /**
     * Setup all the number lines.
     */
    private setupNumberLines(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(this.NumberLines);

        let numberLines = [ ...this.NumberLines ];
        this.doSetupNumberLines(numberLines, callback);
    }
    
    /**
     * Setup all the number lines.
     */
    private doSetupNumberLines(numberLines: NumberLine[], callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(numberLines);
        Debug.AssertValid(callback);

        let this_ = this;
        if (numberLines.length > 0) {
            let numberLine = numberLines.splice(0, 1)[0];
            Debug.AssertValid(numberLine);
            numberLine.setup(() => {                
                this_.doSetupNumberLines(numberLines, callback);
            });
        } else {
            callback();
        }
    }
    
    /**
     * Called when the 'Add line' link is clicked.
     */
    private addLineClicked(callback: () => void) {
        Debug.Untested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);
        Debug.AssertValid(this.NumberLines);

        let this_ = this;
        if (this_.NumberLines.length < Constants.MAX_SIMULTANEOUS_LINES) {
            let numberLine = new NumberLine(this_.Application, this_, BuyTicketsPage.saveNumbers, BuyTicketsPage.removeLine);
            numberLine.setup(() => {
                Debug.Untested();
                this_.NumberLines.push(numberLine);
                callback();
            });
        } else {
            ToastHelper.showInfo(TextContentService.TEXT_NAME_INFO_LIMIT_OF_LINES_REACHED);
            callback();
        }
    }

    /**
     * Called when the remove button is clicked for a line.
     */
    private static removeLine(context: any, numberLine: NumberLine, callback: () => void) {
        Debug.Untested();
        Debug.AssertValid(context);
        Debug.Assert(context instanceof BuyTicketsPage);
        Debug.AssertValid(numberLine);
        Debug.AssertValid(callback);
        
        let buyTicketsPage = <BuyTicketsPage>context;
        Debug.AssertValid(buyTicketsPage.NumberLines);
        let index = buyTicketsPage.NumberLines.findIndex(numberLine_ => (numberLine_ === numberLine));
        Debug.Assert(index !== -1);
        buyTicketsPage.NumberLines.splice(index, 1);
        callback();
    }

    /**
     * Called when the 'Gift' tick box is clicked.
     */
    private static giftClicked(context: object, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(context);
        Debug.Assert(context instanceof BuyTicketsPage);
        Debug.AssertValid(callback);

        let buyTicketsPage = <BuyTicketsPage>context;
        let giftTicket = buyTicketsPage.GiftTickbox.getIsTicked();
        buyTicketsPage.GiftEmail.setHidden(!giftTicket);
        buyTicketsPage.GiftMessage.setHidden(!giftTicket);
        callback();
    }

    /**
     * Called when the 'Buy' button is clicked.
     */
    private static buyClicked(context: object, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(context);
        Debug.Assert(context instanceof BuyTicketsPage);
        Debug.AssertValid(callback);

        let buyTicketsPage = <BuyTicketsPage>context;
        Debug.AssertValid(buyTicketsPage.Application);
        buyTicketsPage.ensureDateOfBirth((proceed) => {
            if (proceed) {
                buyTicketsPage.ensurePaymentDetailsSetup((proceed) => {
                    if (proceed) {
                        let numbers = buyTicketsPage.getNumbers();
                        Debug.AssertValid(numbers);
                        if (numbers.length > 0) {
                            buyTicketsPage.Application.getCurrentUser((currentUser, accessToken) => {
                                Debug.AssertValidOrNull(currentUser);
                                if (currentUser != null) {
                                    Debug.AssertString(currentUser.ID);
                                    Debug.AssertString(accessToken);
                                    let giftTicket = buyTicketsPage.GiftTickbox.getIsTicked();
                                    let giftEmailAddress = giftTicket ? buyTicketsPage.GiftEmail.getValue() : null;
                                    let giftMessage = giftTicket ? buyTicketsPage.GiftMessage.getValue() : null;
                                    TicketService.buyTickets(currentUser.ID, numbers, giftEmailAddress, giftMessage, (errors, tickets) => {
                                        Debug.AssertValid(errors);
                                        Debug.AssertValid(tickets);
                                        errors.forEach(error => {
                                            Debug.AssertValid(error);
                                            ErrorHelper.checkError(error);
                                        });
                                        tickets.forEach(ticket => {
                                            Debug.AssertValid(ticket);
                                            ToastHelper.showInfo(TextContentService.TEXT_NAME_INFO_TICKET_PURCHASED);
                                        });
                                        //??++REMOVE PURCHASED NUMBERS
                                        //??++ENSURE AT LEAST ONE EMPTY LINE
                                        callback();
                                    });
                                } else {
                                    callback();
                                }
                            });
                        } else {
                            ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_FAILED_TO_BUY_TICKETS);
                            callback();
                        }
                    } else {
                        Debug.Untested();
                        callback();
                    }
                });
            } else {
                Debug.Untested();
                callback();
            }
        });
    }

    /**
     * Get the numbers to buy.
     */
    private getNumbers(): number[] {
        Debug.Tested();
        Debug.AssertValid(this.NumberLines);

        let numbers: number[] = [];
        this.NumberLines.forEach(numberLine => {
            Debug.AssertValid(numberLine);
            Debug.AssertValid(numberLine.Number);
            let number = numberLine.Number.getValue();
            Debug.AssertValidOrNull(number);
            if (number != null) {
                numbers.push(number);
            }
        });
        return numbers;
    }

    /**
     * Ensure that the user's dat of birth is known and the user is 18 years old or more.
     * If not known, the user is asked for it. If they cancel then return false.
     * If known, check they are 18 and if they are not, show an error and return false.
     */
    private ensureDateOfBirth(callback: (proceed: boolean) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);

        let this_ = this;
        this_.Application.getCurrentUser((currentUser, accessToken) => {
            Debug.AssertValidOrNull(currentUser);
            if (currentUser != null) {
                Debug.AssertString(accessToken);
                Debug.AssertString(currentUser.ID);
                Debug.AssertValid(currentUser.DateOfBirth);
                //??-if (currentUser.DateOfBirth != null) {
                let oldEnough = BuyTicketsPage.userIsOldEnoughToBuyTickets(currentUser);
                callback(oldEnough);
                //??-- } else {
                //     this_.Application.changeUserDateOfBirth(currentUser, (success) => {
                //         if (success) {
                //             let oldEnough = BuyTicketsPage.userIsOldEnoughToBuyTickets(currentUser);
                //             callback(oldEnough);
                //         } else {
                //             callback(false);
                //         }
                //     });
                // }
            } else {
                callback(false);
            }
        });
    }
    
    /**
     * Is the user old enough to buy tickets?
     */
    private static userIsOldEnoughToBuyTickets(userDetails: UserDetails): boolean {
        Debug.Tested();
        Debug.AssertValid(userDetails);
        Debug.AssertValid(userDetails.DateOfBirth);

        let oldEnoughDate = new DateOnly(userDetails.DateOfBirth.Year + 18, userDetails.DateOfBirth.Month, userDetails.DateOfBirth.Day);
        let oldEnoughTimestamp = DateHelper.dateToTimestamp(oldEnoughDate);
        Debug.AssertValid(oldEnoughTimestamp);
        return (oldEnoughTimestamp <= new Date());
    }

    /**
     * Check that the payment details are setup or open the payment details page.
     */
    private ensurePaymentDetailsSetup(callback: (proceed: boolean) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);

        let this_ = this;
        this_.Application.getCurrentUser((currentUser, accessToken) => {
            Debug.AssertValidOrNull(currentUser);
            if (currentUser != null) {
                Debug.AssertString(currentUser.ID);
                Debug.AssertString(accessToken);
                PaymentService.getUserPaymentDetails(currentUser.ID, (error, details) => {
                    Debug.AssertErrorOrObject(error, details);
                    if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_CHECK_PAYMENT_DETAILS)) {
                        if (details.setup) {
                            if (!details.requireConfirmation) {
                                callback(true);
                            } else {
                                Debug.Untested();
                                //??+NEED TO CONFIRM PAYMENT?
                                //??++FOR NOW, DONT LET PURCHASE HAPPEN
                                callback(false);
                            }
                        } else {
                            Debug.Untested();
                            //??++ENSURE NUMBERS ARE SAVED
                            ToastHelper.showInfo(TextContentService.TEXT_NAME_INFO_MUST_SETUP_PAYMENT_DETAILS);
                            this_.Application.navigateTo(Constants.PAGE_URL_PAYMENT_DETAILS, (currentPage) => {
                                callback(false);
                            })
                        }
                    } else {
                        callback(false);
                    }
                });
            } else {
                callback(false);
            }
        });
    }
        
    /**
     * Save the setup numbers to local storage.
     */
    private static saveNumbers(context: any, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(context);
        Debug.Assert(context instanceof BuyTicketsPage);
        Debug.AssertValid(callback);

        let buyTicketsPage = <BuyTicketsPage>context;
        let numbers = buyTicketsPage.getNumbers();
        Debug.AssertValid(numbers);
        LocalStorageService.writeValue('numbers', numbers, (error) => {
            Debug.AssertValidOrNull(error);
            ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_SAVE_NUMBERS);
            callback();
        });
    }

    /**
     * Load the setup numbers from local storage.
     */
    private loadNumbers(callback: (numbers: number[]) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        LocalStorageService.readValue('numbers', (error, numbers) => {
            Debug.AssertValidOrNull(error);
            Debug.AssertValidOrNull(numbers);
            ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_NUMBERS);
            callback(numbers);
        });
    }

}   // BuyTicketsPage

