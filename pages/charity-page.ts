import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { PhilanthropyContentService } from '../services/philanthropy-content-service';
import { VideoTile } from '../generic/video-tile';
import { TextContentService } from '../services/text-content-service';
import { IconTitleTextList } from '../generic/icon-title-text-list';
import { Action } from '../generic/action';
import { ImageHelper } from '../helpers/image-helper';
import { Charity } from '../data-models/philanthropy-model';
import { PageBase } from '../generic/page-base';
import { Constants } from '../constants';
import { ErrorHelper } from '../helpers/error-helper';

/**
 * Class representing the charity page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47022400/2.2.5.6.+Charity+Page
 */
export class CharityPage extends Page {

    /**
     * The charity to show.
     */
    private Charity: Charity;

    /**
     * The video tile.
     */
    public readonly VideoTile = new VideoTile();

    /**
     * The icon/title/text list component.
     */
    public readonly IconTitleTextList = new IconTitleTextList();

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_CHARITY, false);
        Debug.Tested();

        this.addParameter(Constants.PAGE_PARAMETER_CHARITY_ID);    // <CHARITY_ID>
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }
    
    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.VideoTile);
        Debug.AssertValid(this.IconTitleTextList);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.VideoTile.languageChanged(() => {
                this_.IconTitleTextList.languageChanged(() => {
                    callback();
                });
            });
        });
    }

    /**
     * Called when the page is opened.
     * Loads the correct charity.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doOpen((currentPage) => {
            let id = this_.getArgument(Constants.PAGE_PARAMETER_CHARITY_ID);
            Debug.AssertValidOrNull(id);
            if (!!id) {
                PhilanthropyContentService.getCharities((charities) => {
                    Debug.AssertValid(charities);
                    this_.Charity = charities[id];
                    Debug.AssertValidOrNull(this_.Charity);
                    if (this_.Charity != null) {
                        this_.VideoTile.setURL(this_.Charity.videoURL, () => {
                            PhilanthropyContentService.getAmbassadors((ambassadors) => {
                                Debug.AssertValid(ambassadors);
                                ambassadors.forEach(ambassador => {
                                    Debug.AssertValid(ambassador);
                                    Debug.AssertValid(ambassador.charities);
                                    if (ambassador.charities.indexOf(this_.Charity.id) !== -1) {
                                        let image = ImageHelper.selectImage(ambassador.avatar);
                                        Debug.AssertValid(image);
                                        this_.IconTitleTextList.addItem(image, ambassador.name, ambassador.description, new Action(`${Constants.PAGE_URL_AMBASSADOR}?${Constants.PAGE_PARAMETER_AMBASSADOR_ID}=${ambassador.id}`));
                                    }
                                });
                                this_.IconTitleTextList.setup(() => {
                                    callback(currentPage);
                                });
                            });
                        });
                    } else {
                        Debug.Untested();
                        this_.reset(() => {
                            callback(currentPage);
                        });
                    }
                });
            } else {
                Debug.Untested();
                this_.reset(() => {
                    callback(currentPage);
                });
            }
        });
    }

    /**
     * Reset the contents of this page.
     */
    private reset(callback: () => void) {
        Debug.Untested();
        Debug.AssertValid(callback);
    
        let this_ = this;
        this_.Charity = null;
        this_.VideoTile.reset(() => {
            this_.IconTitleTextList.reset(() => {
                callback();
            });
        });
    }

    /**
     * Get the charity.
     */
    public getCharity(): Charity {
        Debug.Untested();
        Debug.AssertValidOrNull(this.Charity);

        return this.Charity;
    }

}   // CharityPage

