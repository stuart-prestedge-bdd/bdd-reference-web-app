import { Debug } from '../helpers/debug';
import { FormPage } from '../components/form-page';
import { TextContentService } from '../services/text-content-service';
//??--import { IdentityService } from '../services/identity-service';
import { InputField } from '../generic/input-field';
import { ToastHelper } from '../helpers/toast-helper';
import { EmailHelper } from '../helpers/email-helper';
import { PasswordHelper } from '../helpers/password-helper';
import { ErrorHelper } from '../helpers/error-helper';
import { WebApp } from '../components/web-app';
import { Button } from '../generic/button';
import { Action } from '../generic/action';
import { Constants } from '../constants';
import { PageBase } from '../generic/page-base';
import { Tickbox } from '../generic/tickbox';
import { TextField } from '../generic/text-field';

/**
 * Class representing the login page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47022424/2.2.5.12.+Login+Page
 */
export class LoginPage extends FormPage {

    /**
     * The email input field.
     */
    public readonly EmailField = new InputField(TextContentService.TEXT_NAME_EMAIL_HINT);

    /**
     * The password input field.
     */
    public readonly PasswordField = new InputField(TextContentService.TEXT_NAME_PASSWORD_HINT);

    /**
     * The 'remember me' tickbox field.
     */
    public readonly RememberMeTickbox = new Tickbox(TextContentService.TEXT_NAME_REMEMBER_ME);

    /**
     * The create account field.
     */
    public readonly CreateAccountLink = new TextField(TextContentService.TEXT_NAME_NO_ACCOUNT_CREATE_ACCOUNT);

    /**
     * Navigate to the buy-tickets page after login?
     */
    private BuyTicketsAfterLogin: boolean = false;

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_LOGIN, false);
        Debug.Tested();

        this.addParameter(Constants.PAGE_PARAMETER_LOGIN_BUY_TICKETS);
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doSetup(() => {
            this_.EmailField.setup(() => {
                this_.PasswordField.setup(() => {
                    this_.RememberMeTickbox.setup(() => {
                        this_.CreateAccountLink.setExecuteAction(new Action(Constants.PAGE_URL_CREATE_ACCOUNT));
                        this_.CreateAccountLink.setup(() => {
                            callback();
                        });
                    });
                });
            });
        });
    }
    
    /**
     * Loads the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLoadStaticText(() => {
            let names = [
                TextContentService.TEXT_NAME_TITLE_LOGIN,
                TextContentService.TEXT_NAME_BUTTON_LOGIN
            ];
            TextContentService.getTextValues(names, (values) => {
                Debug.AssertValid(values);
                this_.Title.setValue(values.get(TextContentService.TEXT_NAME_TITLE_LOGIN));
                this_.Button.setLabel(values.get(TextContentService.TEXT_NAME_BUTTON_LOGIN));
                callback();
            });
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLanguageChanged(() => {
            callback();
        });
    }

    /**
     * Called when the page is opened.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doOpen((currentPage) => {
            let buyTickets = this_.getArgument(Constants.PAGE_PARAMETER_LOGIN_BUY_TICKETS);
            Debug.AssertValidOrNull(buyTickets);
            this_.BuyTicketsAfterLogin = !!buyTickets;
            callback(this);
        });
    }
    
    /**
     * Called when the login button is clicked.
     * Callback is called when all processing complete.
     * //??++HANDLE 'REMEMBER ME' (cookie or JWT token?).
     */
    protected doButtonClicked(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);
        Debug.AssertValid(this.EmailField);
        Debug.AssertValid(this.PasswordField);

        let this_ = this;
        let emailAddress = this_.EmailField.getValue();
        Debug.AssertString(emailAddress);
        let password = this_.PasswordField.getValue();
        Debug.AssertString(password);
        this_.Application.login(emailAddress, password, (error) => {
            //??++__loginPage.RememberMeTickbox.getValue();
            Debug.AssertValidOrNull(error);
            if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOGIN)) {
                let url = this_.BuyTicketsAfterLogin ? Constants.PAGE_URL_BUY_TICKETS : Constants.PAGE_URL_HOME;
                this_.Application.navigateTo(url, () => {
                    callback();
                });
            } else {
                callback();
            }
        });
    }

    /**
     * Reset the input fields so they are no longer in error state.
     */
    protected doResetInputFieldStates() {
        Debug.Tested();
        Debug.AssertValid(this.EmailField);
        Debug.AssertValid(this.PasswordField);

        this.EmailField.resetState();
        this.PasswordField.resetState();
    }
    
    /**
     * Validate the inputs.
     */
    protected doValidateInputs(): boolean {
        Debug.Tested();
        Debug.AssertValid(this.EmailField);
        Debug.AssertValid(this.PasswordField);

        let retVal = false;
        let emailAddress = this.EmailField.getValue();
        Debug.AssertValidOrNull(emailAddress);
        if (!!emailAddress && EmailHelper.isValidEmailAddress(emailAddress)) {
            let password = this.PasswordField.getValue();
            Debug.AssertValidOrNull(password);
            if (!!password) {
                retVal = true;
            } else {
                Debug.Untested();
                ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_INVALID_PASSWORD);
                this.PasswordField.setErrorState();
            }
        } else {
            Debug.Untested();
            ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_INVALID_EMAIL);
            this.EmailField.setErrorState();
        }
        return retVal;
    }
    
}   // LoginPage
