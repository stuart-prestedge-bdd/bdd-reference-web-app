import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { WebApp } from '../components/web-app';
import { IdentityService } from '../services/identity-service';
import { TextContentService } from '../services/text-content-service';
import { ChangeNamePopupWindow } from '../components/change-name-popup-window';
import { PopupWindow } from '../generic/popup-window';
import { ChangeGenderPopupWindow } from '../components/change-gender-popup-window';
import { Gender, DateOnly, Address, UserDetails } from '../data-models/identity-model';
import { ChangeDateOfBirthPopupWindow } from '../components/change-dob-popup-window';
import { ChangeAddressPopupWindow } from '../components/change-address-popup-window';
import { ErrorHelper } from '../helpers/error-helper';
import { TextField } from '../generic/text-field';
import { GenderHelper } from '../helpers/gender-helper';
import { DateHelper } from '../helpers/date-helper';
import { AddressHelper } from '../helpers/address-helper';
import { PageBase } from '../generic/page-base';
import { Constants } from '../constants';
import { NameHelper } from '../helpers/name-helper';
import { Button } from '../generic/button';

/**
 * Class representing the user profile page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47022478/2.2.5.20.+User+Profile+Page
 */
export class UserProfilePage extends Page {

    /**
     * The text fields.
     */
    public readonly Title = new TextField(TextContentService.TEXT_NAME_TITLE_USER_PROFILE);
    public readonly SubTitlePersonal = new TextField(TextContentService.TEXT_NAME_SUBTITLE_PERSONAL);
    public readonly YourNameLabel = new TextField(TextContentService.TEXT_NAME_YOUR_NAME_LABEL);
    public readonly UserNameField = new TextField();
    public readonly UserGenderField = new TextField();
    public readonly UserDateOfBirthField = new TextField();
    public readonly UserAddressField = new TextField();

    /**
     * The change password button.
     */
    public readonly ChangePasswordButton = new Button(TextContentService.TEXT_NAME_CHANGE_PASSWORD_BUTTON);

    /**
     * The user details.
     */
    private UserDetails: UserDetails;

    //??++OTHER CONTROLS, links, buttons etc.

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_USER_PROFILE);
        Debug.Tested();
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.SubTitlePersonal);
        Debug.AssertValid(this.YourNameLabel);
        Debug.AssertValid(this.ChangePasswordButton);

        let this_ = this;
        super.doSetup(() => {
            this_.Title.setup(() => {
                this_.SubTitlePersonal.setup(() => {
                    this_.YourNameLabel.setup(() => {
                        this_.ChangePasswordButton.setup(() => {
                            callback();
                        });
                    });
                });
            });
        });
    }
    
    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.SubTitlePersonal);
        Debug.AssertValid(this.YourNameLabel);
        Debug.AssertValid(this.ChangePasswordButton);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.Title.languageChanged(() => {
                this_.SubTitlePersonal.languageChanged(() => {
                    this_.YourNameLabel.languageChanged(() => {
                        this_.ChangePasswordButton.languageChanged(callback);
                    });
                });
            });
        });
    }

    /**
     * Loads the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doLoadStaticText(callback);
    }

    /**
     * Called when the page is opened.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.UserNameField);
        Debug.AssertValid(this.Application);
        
        let this_ = this;
        super.doOpen((currentPage) => {
            this_.Application.getCurrentUser((currentUser, accessToken) => {
                Debug.AssertValidOrNull(currentUser);
                if (currentUser != null) {
                    Debug.AssertString(currentUser.ID);
                    Debug.AssertString(accessToken);
                    this_.UserDetails = currentUser;
                    this_.UserNameField.setValue(NameHelper.getUserFormalDisplayName(currentUser));
                    GenderHelper.genderToText(currentUser.Gender, (genderText) => {
                        this_.UserGenderField.setValue(genderText);
                    });
                    DateHelper.dateToText(currentUser.DateOfBirth, (dateText) => {
                        this_.UserDateOfBirthField.setValue(dateText);
                    });
                    AddressHelper.getUserAddressText(currentUser, (addressText) => {
                        this_.UserAddressField.setValue(addressText);
                    });
                }
                callback(currentPage);
            });
        });
    }

    /**
     * Called when the change name link is clicked.
     */
    public changeNameClicked(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);

        let this_ = this;
        this_.Application.showPopup(new ChangeNamePopupWindow('change-name'), (inputs) => {
            Debug.AssertValid(inputs);
            if (inputs[PopupWindow.ButtonInputName] === PopupWindow.ButtonInputValueOK) {
                let newGivenName = inputs[ChangeNamePopupWindow.GivenNameInputName];
                let newFamilyName = inputs[ChangeNamePopupWindow.FamilyNameInputName];
                let newFullName = inputs[ChangeNamePopupWindow.FullNameInputName];
                let newPreferredName = inputs[ChangeNamePopupWindow.PreferredNameInputName];
                //??++CHECK NAME HERE OR IN POPUP?
                Debug.AssertString(newGivenName);
                Debug.AssertString(newFamilyName);
                this_.Application.getCurrentUser((currentUser, accessToken) => {
                    Debug.AssertValid(currentUser);
                    Debug.AssertString(accessToken);
                    IdentityService.setUserName(accessToken, newGivenName, newFamilyName, newFullName, newPreferredName, (error) => {
                        if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_SET_USER_NAME)) {
                            this_.UserNameField.setValue(NameHelper.getFormalDisplayName(newGivenName, newFamilyName, newPreferredName, newFullName));
                        }
                        callback();
                    });
                });
            } else {
                Debug.Untested();
                callback();
            }
        });
    }

    /**
     * Called when the set/change gender link is clicked.
     */
    public changeGenderClicked(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);
        Debug.AssertValid(this.UserDetails);

        let this_ = this;
        this_.Application.showPopup(new ChangeGenderPopupWindow('change-gender', this_.UserDetails.Gender), (inputs) => {
            Debug.AssertValid(inputs);
            if (inputs[PopupWindow.ButtonInputName] === PopupWindow.ButtonInputValueOK) {
                let newGender = inputs[ChangeGenderPopupWindow.GenderInputName];
                this_.Application.getCurrentUser((currentUser, accessToken) => {
                    Debug.AssertValid(currentUser);
                    Debug.AssertString(accessToken);
                    IdentityService.setUserGender(accessToken, newGender, (error) => {
                        if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_SET_GENDER)) {
                            this_.UserDetails.Gender = newGender;
                            GenderHelper.genderToText(newGender, (genderText) => {
                                this_.UserGenderField.setValue(genderText);
                            });
                        }
                        callback();
                    });
                });
            } else {
                Debug.Untested();
                callback();
            }
        });
    }

    /**
     * Called when the set/change date of birth link is clicked.
     */
    //??-- public changeDateOfBirthClicked(callback: () => void) {
    //     Debug.Tested();
    //     Debug.AssertValid(callback);
    //     Debug.AssertValid(this.Application);
    //     Debug.AssertValid(this.UserDetails);

    //     let this_ = this;
    //     this_.Application.changeUserDateOfBirth(this_.UserDetails, (success) => {
    //         if (success) {
    //             DateHelper.dateToText(this_.UserDetails.DateOfBirth, (dateText) => {
    //                 this_.UserDateOfBirthField.setValue(dateText);
    //                 callback();
    //             });
    //         } else {
    //             callback();
    //         }
    //     });
    // }

    /**
     * Called when the set/change address link is clicked.
     */
    public changeAddressClicked(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);
        Debug.AssertValid(this.UserDetails);

        let this_ = this;
        let address = AddressHelper.addressFromUser(this_.UserDetails);
        Debug.AssertValid(address);
        this_.Application.showPopup(new ChangeAddressPopupWindow('change-address', address), (inputs) => {
            Debug.AssertValid(inputs);
            if (inputs[PopupWindow.ButtonInputName] === PopupWindow.ButtonInputValueOK) {
                let newAddress = inputs[ChangeAddressPopupWindow.AddressInputName];
                //??++CHECK ADDRESS HERE OR IN POPUP?
                this_.Application.getCurrentUser((currentUser, accessToken) => {
                    Debug.AssertValid(currentUser);
                    Debug.AssertString(accessToken);
                    IdentityService.setUserAddress(accessToken, newAddress, (error) => {
                        if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_SET_ADDRESS)) {
                            AddressHelper.setUserAddress(this_.UserDetails, newAddress);
                            AddressHelper.addressToText(newAddress, (addressText) => {
                                this_.UserAddressField.setValue(addressText);
                            });
                                    }
                        callback();
                    });
                });
            } else {
                callback();
            }
        });
    }

    //??++Change country?
    //??++Change email address flow initiation.
    //??++Change password flow initiation.
    //??++Change phone number

}   // UserProfilePage
