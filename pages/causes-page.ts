import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { PhilanthropyContentService } from '../services/philanthropy-content-service';
import { VideoTile } from '../generic/video-tile';
import { TextContentService } from '../services/text-content-service';
import { IconTitleTextList } from '../generic/icon-title-text-list';
import { Action } from '../generic/action';
import { ImageHelper } from '../helpers/image-helper';
import { ErrorHelper } from '../helpers/error-helper';
import { Constants } from '../constants';
import { TextField } from '../generic/text-field';
import { URLHelper } from '../helpers/url-helper';

/**
 * Class representing the causes page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47022396/2.2.5.3.+Causes+Page
 */
export class CausesPage extends Page {

    /**
     * The video tile.
     */
    public readonly VideoTile = new VideoTile();

    /**
     * The page title.
     */
    public readonly Title = new TextField();

    /**
     * The page description.
     */
    public readonly Description = new TextField();

    /**
     * The icon/title/text list component.
     */
    public readonly IconTitleTextList = new IconTitleTextList();

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_CAUSES, false);
        Debug.Tested();
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.VideoTile);
        Debug.AssertValid(this.IconTitleTextList);

        let this_ = this;
        super.doSetup(() => {
            PhilanthropyContentService.getCausesInfo((causesInfo) => {
                Debug.AssertValidOrNull(causesInfo);
                if (causesInfo != null) {
                    Debug.AssertValid(causesInfo.causesVideoURL);
                    Debug.AssertValid(causesInfo.causes);
                    this_.VideoTile.setURL(causesInfo.causesVideoURL, () => {
                        causesInfo.causes.forEach(cause => {
                            Debug.AssertValid(cause);
                            let icon = ImageHelper.selectImage(cause.icon);
                            Debug.AssertValid(icon);
                            let url = URLHelper.replaceURLParameter(Constants.PAGE_URL_CAUSE, Constants.PAGE_PARAMETER_CAUSE_ID, cause.id);
                            this_.IconTitleTextList.addItem(icon, cause.name, cause.description, new Action(url));
                        });
                        this_.IconTitleTextList.setup(() => {
                            callback();
                        });
                    });
                } else {
                    callback();
                }
            });
        });
    }
    
    /**
     * Load the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.Description);

        let this_ = this;
        super.doLoadStaticText(() => {
            let names = [
                TextContentService.TEXT_NAME_TITLE_CAUSES,
                TextContentService.TEXT_NAME_DESCRIPTION_CAUSES
            ];
            TextContentService.getTextValues(names, (values) => {
                Debug.AssertValid(values);
                this_.Title.setValue(values.get(TextContentService.TEXT_NAME_TITLE_CAUSES));
                this_.Description.setValue(values.get(TextContentService.TEXT_NAME_DESCRIPTION_CAUSES));
                callback();
            });
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.VideoTile);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.Description);
        Debug.AssertValid(this.IconTitleTextList);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.VideoTile.languageChanged(() => {
                this_.Title.languageChanged(() => {
                    this_.Description.languageChanged(() => {
                        this_.IconTitleTextList.languageChanged(() => {
                            callback();
                        });
                    });
                });
            });
        });
    }

}   // CausesPage
