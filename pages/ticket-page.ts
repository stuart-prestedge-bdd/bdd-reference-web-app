import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { TicketService, TicketInfo } from '../services/ticket-service';
import { ToastHelper } from '../helpers/toast-helper';
import { TextContentService } from '../services/text-content-service';
import { WebApp } from '../components/web-app';
import { TextField } from '../generic/text-field';
import { Button } from '../generic/button';
import { Action } from '../generic/action';
import { ErrorHelper } from '../helpers/error-helper';
import { DateHelper } from '../helpers/date-helper';
import { PageBase } from '../generic/page-base';
import { Constants } from '../constants';
import { TicketNumberComponent } from '../components/ticket-number-component';
import { BuyTicketTile } from '../components/buy-ticket-tile';

/**
 * Class representing the ticket page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47251780/2.2.5.11.+Ticket+Page
 */
export class TicketPage extends Page {

    /**
     * The static text.
     */
    public readonly Title = new TextField(TextContentService.TEXT_NAME_TITLE_TICKET);
    public readonly LabelPurchased = new TextField(TextContentService.TEXT_NAME_LABEL_PURCHASED);
    public readonly PurchasedField = new TextField();
    public readonly LabelWon = new TextField(TextContentService.TEXT_NAME_LABEL_WON);
    public readonly LabelNextDraw = new TextField(TextContentService.TEXT_NAME_LABEL_NEXT_DRAW);
    public readonly LabelGiftedFrom = new TextField(TextContentService.TEXT_NAME_LABEL_GIFTED_FROM);
    public readonly LabelGiftedDate = new TextField(TextContentService.TEXT_NAME_LABEL_GIFTED_DATE);
    public readonly LabelRemainingDraws = new TextField(TextContentService.TEXT_NAME_LABEL_REMAINING_DRAWS);
    public readonly LabelRedeemedDate = new TextField(TextContentService.TEXT_NAME_LABEL_REDEEMED_DATE);
    public readonly ButtonGift = new Button(TextContentService.TEXT_NAME_BUTTON_GIFT);
    public readonly ButtonRedeem = new Button(TextContentService.TEXT_NAME_BUTTON_REDEEM);

    /**
     * The ticket being shown.
     */
    private TicketInfo: TicketInfo;
    
    /**
     * The 'buy ticket' tile.
     */
    public readonly BuyTicketTile = new BuyTicketTile();

    /**
     * The ticket number.
     */
    public readonly Number = new TicketNumberComponent(null, true);

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_TICKET);
        Debug.Tested();

        this.addParameter(Constants.PAGE_PARAMETER_TICKET_ID);    // <TICKET_ID>
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.LabelPurchased);
        Debug.AssertValid(this.PurchasedField);
        Debug.AssertValid(this.LabelWon);
        Debug.AssertValid(this.LabelNextDraw);
        Debug.AssertValid(this.LabelGiftedFrom);
        Debug.AssertValid(this.LabelGiftedDate);
        Debug.AssertValid(this.LabelRemainingDraws);
        Debug.AssertValid(this.LabelRedeemedDate);
        Debug.AssertValid(this.ButtonGift);
        Debug.AssertValid(this.ButtonRedeem);
        Debug.AssertValid(this.Number);
        Debug.AssertValid(this.BuyTicketTile);

        let this_ = this;
        super.doSetup(() => {
            this_.ButtonGift.setExecuteAction(new Action(null, TicketPage.giftButtonClicked, this_));
            this_.ButtonRedeem.setExecuteAction(new Action(null, TicketPage.redeemButtonClicked, this_));
            this_.Title.setup(() => {
                this_.LabelPurchased.setup(() => {
                    this_.PurchasedField.setup(() => {
                        this_.LabelWon.setup(() => {
                            this_.LabelNextDraw.setup(() => {
                                this_.LabelGiftedFrom.setup(() => {
                                    this_.LabelGiftedDate.setup(() => {
                                        this_.LabelRemainingDraws.setup(() => {
                                            this_.LabelRedeemedDate.setup(() => {
                                                this_.ButtonGift.setup(() => {
                                                    this_.ButtonRedeem.setup(() => {
                                                        this_.Number.setup(() => {
                                                            this_.BuyTicketTile.setup(callback);
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }
    
    /**
     * Virtual method to load the static text.
     * Override to load static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLoadStaticText(callback);
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.LabelPurchased);
        Debug.AssertValid(this.PurchasedField);
        Debug.AssertValid(this.LabelWon);
        Debug.AssertValid(this.LabelNextDraw);
        Debug.AssertValid(this.LabelGiftedFrom);
        Debug.AssertValid(this.LabelGiftedDate);
        Debug.AssertValid(this.LabelRemainingDraws);
        Debug.AssertValid(this.LabelRedeemedDate);
        Debug.AssertValid(this.ButtonGift);
        Debug.AssertValid(this.ButtonRedeem);
        Debug.AssertValid(this.Number);
        Debug.AssertValid(this.BuyTicketTile);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.Title.languageChanged(() => {
                this_.LabelPurchased.languageChanged(() => {
                    this_.PurchasedField.languageChanged(() => {
                        this_.LabelWon.languageChanged(() => {
                            this_.LabelNextDraw.languageChanged(() => {
                                this_.LabelGiftedFrom.languageChanged(() => {
                                    this_.LabelGiftedDate.languageChanged(() => {
                                        this_.LabelRemainingDraws.languageChanged(() => {
                                            this_.LabelRedeemedDate.languageChanged(() => {
                                                this_.ButtonGift.languageChanged(() => {
                                                    this_.ButtonRedeem.languageChanged(() => {
                                                        this_.Number.languageChanged(() => {
                                                            this_.BuyTicketTile.languageChanged(callback);
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    }

    /**
     * Called when the page is opened.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doOpen((currentPage) => {
            let id = this_.getArgument(Constants.PAGE_PARAMETER_TICKET_ID);
            Debug.AssertValidOrNull(id);
            if (!!id) {
                this_.Application.getCurrentUser((currentUser, accessToken) => {
                    Debug.AssertValidOrNull(currentUser);
                    if (currentUser != null) {
                        Debug.AssertString(accessToken);
                        TicketService.getTicket(currentUser.ID, id, accessToken, (error, ticketInfo) => {
                            Debug.AssertErrorOrObject(error, ticketInfo);
                            this_.TicketInfo = ticketInfo;
                            if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_TICKET)) {
                                this_.Number.setValue(ticketInfo.Number);
                                DateHelper.timestampToText(ticketInfo.PurchaseDate, (timestampText) => {
                                    this_.PurchasedField.setValue(timestampText);
                                    this_.canGiftTicket(ticketInfo, (result) => {
                                        this_.ButtonGift.setHidden(!result);
                                        this_.canRedeemTicket(ticketInfo, (result) => {
                                            this_.ButtonRedeem.setHidden(!result);
                                            callback(currentPage);
                                        });
                                    });
                                });
                            } else {
                                callback(currentPage);
                            }
                        });
                    } else {
                        Debug.Untested();
                        this_.TicketInfo = null;
                        callback(currentPage);
                    }
                });
            } else {
                this_.TicketInfo = null;
                callback(currentPage);
            }
        });
    }

    /**
     * Can the specified ticket be gifted?
     */
    private canGiftTicket(ticketInfo: TicketInfo, callback: (retVal: boolean) => void) {
        Debug.Tested();
        Debug.AssertValid(ticketInfo);
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);

        this.Application.getCurrentUser((currentUser) => {
            Debug.AssertValidOrNull(currentUser);
            let retVal = false;
            if (currentUser != null) {
                Debug.AssertString(currentUser.ID);
                if (ticketInfo.PurchaseDate != null) {
                    if (ticketInfo.OwnerID === currentUser.ID) {
                        if (ticketInfo.OfferedToEmail == null) {
                            retVal = true;
                        }
                    }
                }
            }
            callback(retVal);
        });
    }

    /**
     * Can the specified ticket be redeemed?
     */
    private canRedeemTicket(ticketInfo: TicketInfo, callback: (retVal: boolean) => void) {
        Debug.Tested();
        Debug.AssertValid(ticketInfo);
        Debug.AssertValid(this.Application);

        this.Application.getCurrentUser((currentUser) => {
            Debug.AssertValidOrNull(currentUser);
            let retVal = false;
            if (currentUser != null) {
                Debug.AssertString(currentUser.ID);
                if (ticketInfo.PurchaseDate != null) {
                    if (ticketInfo.OwnerID !== currentUser.ID) {
                        if (ticketInfo.OfferedToEmail === currentUser.EmailAddress) {
                            retVal = true;
                        }
                    }
                }
            }
            callback(retVal);
        });
    }

    /**
     * Called when the 'Gift' button is clicked.
     */
    private static giftButtonClicked(context: object, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(context);
        Debug.Assert(context instanceof TicketPage);
        Debug.AssertValid(callback);
        
        let ticketPage = <TicketPage>context;
        Debug.AssertValid(ticketPage.TicketInfo);
        Debug.AssertString(ticketPage.TicketInfo.ID);
        Debug.AssertValid(ticketPage.Application);
        //??++Debug.Assert(ticketPage.canGiftTicket(ticketPage.Ticket));
        ticketPage.Application.navigateTo(`${Constants.PAGE_URL_GIFT_TICKET}?${Constants.PAGE_PARAMETER_GIFT_TICKET_ID}=${ticketPage.TicketInfo.ID}`, callback);
    }

    /**
     * Called when the 'Redeem' button is clicked.
     */
    private static redeemButtonClicked(context: object, callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(context);
        Debug.Assert(context instanceof TicketPage);
        Debug.AssertValid(callback);

        let ticketPage = <TicketPage>context;
        Debug.AssertValid(ticketPage.Application);
        Debug.AssertValid(ticketPage.TicketInfo);
        Debug.AssertString(ticketPage.TicketInfo.ID);
        //??++Debug.Assert(ticketPage.canRedeemTicket(ticketPage.Ticket));
        ticketPage.Application.getCurrentUser((currentUser, accessToken) => {
            Debug.AssertValidOrNull(currentUser);
            if (currentUser != null) {
                Debug.AssertString(currentUser.ID);
                Debug.AssertString(accessToken);
                TicketService.redeemTicket(ticketPage.TicketInfo.ID, currentUser.ID, (error) => {
                    Debug.AssertValidOrNull(error);
                    if (error == null) {
                        ToastHelper.showInfo(TextContentService.TEXT_NAME_INFO_TICKET_REDEEMED);
                    } else {
                        ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_TICKET_FAILED_TO_REDEEM);
                    }
                    callback();
                });
            } else {
                callback();
            }
        });
    }

}   // TicketPage

