import { Debug } from '../helpers/debug';
import { MessagePage } from '../components/message-page';
import { TextContentService } from '../services/text-content-service';
import { Action } from '../generic/action';
import { ErrorHelper } from '../helpers/error-helper';
import { Constants } from '../constants';
import { TextField } from '../generic/text-field';
import { PageBase } from '../generic/page-base';
//??--import { IdentityService } from '../services/identity-service';
import { ToastHelper } from '../helpers/toast-helper';
import { EmailHelper } from '../helpers/email-helper';

/**
 * Class representing the email verified page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47218996/2.2.5.18.+Email+Verified+Page
 */
export class EmailVerifiedPage extends MessagePage {

    /**
     * Constructor.
     */
    constructor() {
        Debug.Tested();

        super(Constants.PAGE_URL_EMAIL_VERIFIED, false);
        this.addParameter(Constants.PAGE_PARAMETER_EMAIL_ADDRESS);
        this.addParameter(Constants.PAGE_PARAMETER_EMAIL_VERIFICATION_ID);
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doSetup(() => {
            TextContentService.getTextValue(TextContentService.TEXT_NAME_BUTTON_LOGIN, (value) => {
                Debug.AssertString(value);
                this_.setupButton(value, new Action(Constants.PAGE_URL_LOGIN));
                callback();
            });
        });
    }
    
    /**
     * Load the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLoadStaticText(() => {
            let names = [
                TextContentService.TEXT_NAME_TITLE_EMAIL_VERIFIED,
                TextContentService.TEXT_NAME_MESSAGE_EMAIL_VERIFIED
            ];
            TextContentService.getTextValues(names, (values) => {
                Debug.AssertValid(values);
                this_.Title = values.get(TextContentService.TEXT_NAME_TITLE_EMAIL_VERIFIED);
                this_.DescriptionHTML = values.get(TextContentService.TEXT_NAME_MESSAGE_EMAIL_VERIFIED);
                callback();
            });
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLanguageChanged(() => {
            callback();
        });
    }

    /**
     * Called when the page is opened.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);

        let this_ = this;
        super.doOpen((currentPage) => {
            let emailAddress = this_.getArgument(Constants.PAGE_PARAMETER_EMAIL_ADDRESS);
            Debug.AssertValidOrNull(emailAddress);
            let verificationCode = this_.getArgument(Constants.PAGE_PARAMETER_EMAIL_VERIFICATION_ID);
            Debug.AssertValidOrNull(verificationCode);
            if (EmailHelper.isValidEmailAddress(emailAddress) && !!verificationCode) {
                this_.Application.verifyEmail(emailAddress, verificationCode, () => {
                    //??++THINK ABOUT BEST PAGE TO GO TO HERE!
                    callback(currentPage);
                });
            } else {
                Debug.Untested();
                ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_FAILED_TO_VERIFY_EMAIL_ADDRESS);
                this_.Application.navigateTo(Constants.PAGE_URL_HOME, () => {//??++THINK ABOUT BEST PAGE TO GO TO HERE!
                    callback(currentPage);
                });
            }
        });
    }

}   // EmailVerifiedPage

