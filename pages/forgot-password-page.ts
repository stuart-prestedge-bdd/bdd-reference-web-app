import { Debug } from '../helpers/debug';
import { FormPage } from '../components/form-page';
import { TextContentService } from '../services/text-content-service';
import { Constants } from '../constants';
import { InputField } from '../generic/input-field';
import { Button } from '../generic/button';
import { ErrorHelper } from '../helpers/error-helper';
import { Action } from '../generic/action';
import { WebApp } from '../components/web-app';
import { IdentityService } from '../services/identity-service';
import { EmailHelper } from '../helpers/email-helper';
import { ToastHelper } from '../helpers/toast-helper';

/**
 * Class representing the forgot password page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47022428/2.2.5.13.+Forgot+Password+Page
 */
export class ForgotPasswordPage extends FormPage {

    /**
     * The email input field.
     */
    public readonly EmailField = new InputField(TextContentService.TEXT_NAME_EMAIL_HINT);

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_FORGOT_PASSWORD, false);
        Debug.Tested();
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doSetup(() => {
            this_.EmailField.setup(() => {
                callback();
            });
        });
    }
    
    /**
     * Loads the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLoadStaticText(() => {
            let names = [
                TextContentService.TEXT_NAME_TITLE_RESET_PASSWORD,
                TextContentService.TEXT_NAME_BUTTON_RESET_PASSWORD
            ];
            TextContentService.getTextValues(names, (values) => {
                Debug.AssertValid(values);
                this_.Title.setValue(values.get(TextContentService.TEXT_NAME_TITLE_RESET_PASSWORD));
                this_.Button.setLabel(values.get(TextContentService.TEXT_NAME_BUTTON_RESET_PASSWORD));
                callback();
            });
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.EmailField);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.EmailField.languageChanged(() => {
                callback();
            });
        });
    }

    /**
     * Called when the 'Reset password' button is clicked.
     * Callback is called when all processing complete.
     */
    protected doButtonClicked(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);
        Debug.AssertValid(this.EmailField);
        
        let this_ = this;
        let emailAddress = this_.EmailField.getValue();
        Debug.AssertString(emailAddress);
        IdentityService.resetUserPassword(emailAddress, (error) => {
            Debug.AssertValidOrNull(error);
            if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_RESET_PASSWORD)) {
                //???ToastHelper.showInfo(TEXT_NAME_MESSAGE_PASSWORD_RESET_SENT);
                let url = `${Constants.PAGE_URL_PASSWORD_RESET_SENT}?${Constants.PAGE_PARAMETER_PASSWORD_RESET_SENT_USER_EMAIL}=${emailAddress}`;
                this_.Application.navigateTo(url, () => {
                    callback();
                });
            } else {
                callback();
            }
        });
    }

    /**
     * Reset the input fields so they are no longer in error state.
     */
    protected doResetInputFieldStates() {
        Debug.Tested();
        Debug.AssertValid(this.EmailField);

        this.EmailField.resetState();
    }
    
    /**
     * Validate the inputs.
     */
    protected doValidateInputs(): boolean {
        Debug.Tested();

        let retVal = false;
        let emailAddress = this.EmailField.getValue();
        Debug.AssertValidOrNull(emailAddress);
        if (!!emailAddress && EmailHelper.isValidEmailAddress(emailAddress)) {
            retVal = true;
        } else {
            ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_INVALID_EMAIL);
            this.EmailField.setErrorState();
        }
        return retVal;
    }
    
}   // ForgotPasswordPage
