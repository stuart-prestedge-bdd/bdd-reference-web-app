import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { VideoTile } from '../generic/video-tile';
import { BuyTicketTile } from '../components/buy-ticket-tile';
import { Constants } from '../constants';
import { IconBar } from '../generic/icon-bar';
import { PhilanthropyContentService } from '../services/philanthropy-content-service';
import { ErrorHelper } from '../helpers/error-helper';
import { ImageHelper } from '../helpers/image-helper';
import { Action } from '../generic/action';
import { TextContentService } from '../services/text-content-service';
import { ScrollingGraphicTile } from '../generic/scrolling-graphic-tile';

/**
 * Class representing the home page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47055080/2.2.5.2.+Home+Page
 */
export class HomePage extends Page {

    //??++ info text tile, prizes tile

    /**
     * The video tile.
     */
    public readonly VideoTile = new VideoTile();

    /**
     * The buy ticket tile.
     */
    public readonly BuyTicketTile = new BuyTicketTile();

    /**
     * The scrolling info graphic for the 'where the money goes'.
     */
    public readonly ScrollingGraphicTile = new ScrollingGraphicTile(TextContentService.TEXT_NAME_WHERE_YOUR_MONEY_GOES);

    /**
     * The charity icon bar.
     */
    public readonly CharityIconBar = new IconBar();

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_HOME, false);
        Debug.Tested();
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.VideoTile);
        Debug.AssertValid(this.BuyTicketTile);
        Debug.AssertValid(this.CharityIconBar);

        let this_ = this;
        super.doSetup(() => {
            this_.VideoTile.setURL('/promo.mp4', () => {
                this_.BuyTicketTile.setup(() => {
                    PhilanthropyContentService.getCharities((charities) => {//??++CONSIDER MOVING TO doOpen()
                        Debug.AssertValid(charities);
                        charities.forEach(charity => {
                            Debug.AssertValid(charity);
                            Debug.AssertValid(charity.logo);
                            let iconImage = ImageHelper.selectImage(charity.logo);
                            Debug.AssertValid(iconImage);
                            let executeAction = new Action(`${Constants.PAGE_URL_CHARITY}?${Constants.PAGE_PARAMETER_CHARITY_ID}=${charity.id}`);
                            this_.CharityIconBar.addImage(iconImage, executeAction);
                        });
                        this_.CharityIconBar.setup(() => {
                            callback();
                        });
                    });
                });
            });
        });
    }
    
    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.VideoTile);
        Debug.AssertValid(this.BuyTicketTile);
        Debug.AssertValid(this.CharityIconBar);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.VideoTile.languageChanged(() => {
                this_.BuyTicketTile.languageChanged(() => {
                    this_.CharityIconBar.languageChanged(() => {
                        callback();
                    });
                });
            });
        });
    }

}   // HomePage
