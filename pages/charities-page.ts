import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { PhilanthropyContentService } from '../services/philanthropy-content-service';
import { VideoTile } from '../generic/video-tile';
import { TextContentService } from '../services/text-content-service';
import { IconTitleTextList } from '../generic/icon-title-text-list';
import { Action } from '../generic/action';
import { ImageHelper } from '../helpers/image-helper';
import { ErrorHelper } from '../helpers/error-helper';
import { Constants } from '../constants';
import { TextField } from '../generic/text-field';
import { URLHelper } from '../helpers/url-helper';

/**
 * Class representing the charities page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47284354/2.2.5.5.+Charities+Page
 */
export class CharitiesPage extends Page {

    /**
     * The page title.
     */
    public readonly Title = new TextField();

    /**
     * The page description.
     */
    public readonly Description = new TextField();

    /**
     * The icon/title/text list component.
     */
    public readonly IconTitleTextList = new IconTitleTextList();

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_CHARITIES, false);
        Debug.Tested();
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.IconTitleTextList);

        let this_ = this;
        super.doSetup(() => {
            PhilanthropyContentService.getCharities((charities) => {
                Debug.AssertValid(charities);
                charities.forEach(charity => {
                    Debug.AssertValid(charity);
                    let image = ImageHelper.selectImage(charity.logo);
                    Debug.AssertValid(image);
                    let url = URLHelper.replaceURLParameter(Constants.PAGE_URL_CHARITY, Constants.PAGE_PARAMETER_CHARITY_ID, charity.id);
                    this_.IconTitleTextList.addItem(image, charity.name, charity.description, new Action(url));
                });
                this_.IconTitleTextList.setup(() => {
                    callback();
                });
            });
        });
    }
    
    /**
     * Load the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.Description);

        let this_ = this;
        super.doLoadStaticText(() => {
            let names = [
                TextContentService.TEXT_NAME_TITLE_CHARITIES,
                TextContentService.TEXT_NAME_DESCRIPTION_CHARITIES
            ];
            TextContentService.getTextValues(names, (values) => {
                Debug.AssertValid(values);
                this_.Title.setValue(values.get(TextContentService.TEXT_NAME_TITLE_CHARITIES));
                this_.Description.setValue(values.get(TextContentService.TEXT_NAME_DESCRIPTION_CHARITIES));
                callback();
            });
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Title);
        Debug.AssertValid(this.Description);
        Debug.AssertValid(this.IconTitleTextList);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.Title.languageChanged(() => {
                this_.Description.languageChanged(() => {
                    this_.IconTitleTextList.languageChanged(() => {
                        callback();
                    });
                });
            });
        });
    }

}   // CharitiesPage
