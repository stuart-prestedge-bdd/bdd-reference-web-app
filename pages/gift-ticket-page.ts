import { Debug } from '../helpers/debug';
import { Page } from '../components/page';
import { TicketService, TicketInfo } from '../services/ticket-service';
import { ToastHelper } from '../helpers/toast-helper';
import { ErrorHelper } from '../helpers/error-helper';
import { PageBase } from '../generic/page-base';
import { TextField } from '../generic/text-field';
import { TextContentService } from '../services/text-content-service';
import { Constants } from '../constants';
import { DateHelper } from '../helpers/date-helper';
import { TicketNumberComponent } from '../components/ticket-number-component';
import { EmailHelper } from '../helpers/email-helper';
import { InputField } from '../generic/input-field';
import { Button } from '../generic/button';
import { Action } from '../generic/action';
import { FormPage } from '../components/form-page';

/**
 * Class representing the gift ticket page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47219006/2.2.5.23.+Gift+Ticket+Page
 */
export class GiftTicketPage extends FormPage {

    /**
     * The static text.
     */
    public readonly LabelPurchased = new TextField(TextContentService.TEXT_NAME_LABEL_PURCHASED);
    public readonly PurchasedField = new TextField();

    /**
     * The ticket number.
     */
    public readonly Number = new TicketNumberComponent(null, true);

    /**
     * The gift input fields.
     */
    public readonly GiftEmail = new InputField(TextContentService.TEXT_NAME_GIFT_EMAIL_HINT);
    public readonly GiftMessage = new InputField(TextContentService.TEXT_NAME_GIFT_MESSAGE_HINT);
    
    /**
     * The ticket being gifted.
     */
    private TicketInfo: TicketInfo;
    
    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_GIFT_TICKET);
        Debug.Tested();

        this.addParameter(Constants.PAGE_PARAMETER_GIFT_TICKET_ID);    // <TICKET_ID>
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.LabelPurchased);
        Debug.AssertValid(this.PurchasedField);
        Debug.AssertValid(this.Number);
        Debug.AssertValid(this.GiftEmail);
        Debug.AssertValid(this.GiftMessage);

        let this_ = this;
        super.doSetup(() => {
            this_.LabelPurchased.setup(() => {
                this_.PurchasedField.setup(() => {
                    this_.Number.setup(() => {
                        this_.GiftEmail.setup(() => {
                            this_.GiftMessage.setup(() => {
                                callback();            
                            });
                        });
                    });
                });
            });
        });
    }
    
    /**
     * Virtual method to load the static text.
     * Override to load static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLoadStaticText(() => {
            let names = [
                TextContentService.TEXT_NAME_TITLE_GIFT_TICKET,
                TextContentService.TEXT_NAME_BUTTON_OFFER_GIFT
            ];
            TextContentService.getTextValues(names, (values) => {
                Debug.AssertValid(values);
                this_.Title.setValue(values.get(TextContentService.TEXT_NAME_TITLE_GIFT_TICKET));
                this_.Button.setLabel(values.get(TextContentService.TEXT_NAME_BUTTON_OFFER_GIFT));
                callback();
            });
        });        
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.LabelPurchased);
        Debug.AssertValid(this.PurchasedField);
        Debug.AssertValid(this.Number);
        Debug.AssertValid(this.GiftEmail);
        Debug.AssertValid(this.GiftMessage);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.LabelPurchased.languageChanged(() => {
                this_.PurchasedField.languageChanged(() => {
                    this_.Number.languageChanged(() => {
                        this_.GiftEmail.languageChanged(() => {
                            this_.GiftMessage.languageChanged(() => {
                                callback();
                            });
                        });
                    });
                });
            });
        });
    }

    /**
     * Called when the page is opened.
     */
    protected doOpen(callback: (currentPage: PageBase) => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);

        let this_ = this;
        super.doOpen((currentPage) => {
            let id = this_.getArgument(Constants.PAGE_PARAMETER_GIFT_TICKET_ID);
            Debug.AssertValidOrNull(id);
            if (!!id) {
                this_.Application.getCurrentUser((currentUser, accessToken) => {
                    Debug.AssertValidOrNull(currentUser);
                    if (currentUser != null) {
                        Debug.AssertString(accessToken);
                        TicketService.getTicket(currentUser.ID, id, accessToken, (error, ticketInfo) => {
                            Debug.AssertErrorOrObject(error, ticketInfo);
                            this_.TicketInfo = ticketInfo;
                            if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_TICKET)) {
                                this_.Number.setValue(ticketInfo.Number);
                                DateHelper.timestampToText(ticketInfo.PurchaseDate, (timestampText) => {
                                    Debug.AssertString(timestampText);
                                    this_.PurchasedField.setValue(timestampText);
                                    callback(currentPage);
                                });
                            } else {
                                Debug.Untested();
                                callback(currentPage);
                            }
                        });
                    } else {
                        Debug.Untested();
                        this_.TicketInfo = null;
                        callback(currentPage);
                    }
                });
            } else {
                Debug.Untested();
                this_.TicketInfo = null;
                callback(currentPage);
            }
        });
    }

    /**
     * Called when the 'Offer' button is clicked.
     */
    protected doButtonClicked(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);
        Debug.AssertValid(this.TicketInfo);
        Debug.AssertString(this.TicketInfo.ID);

        let this_ = this;
        this_.Application.getCurrentUser((currentUser, accessToken) => {
            Debug.AssertValidOrNull(currentUser);
            if (currentUser != null) {
                Debug.AssertString(currentUser.ID);
                Debug.AssertString(accessToken);
                let emailAddress = this_.GiftEmail.getValue();
                Debug.AssertValidOrNull(emailAddress);
                let message = this_.GiftMessage.getValue();
                Debug.AssertValidOrNull(message);
                TicketService.offerTicket(currentUser.ID, this_.TicketInfo.ID, emailAddress, message, (error) => {
                    Debug.AssertValidOrNull(error);
                    if (ErrorHelper.checkError(error, TextContentService.TEXT_NAME_ERROR_FAILED_TO_OFFER_TICKET)) {
                        this_.Application.navigateTo(Constants.PAGE_URL_TICKET, (currentPage) => {
                            callback();
                        });
                    } else {
                        callback();
                    }
                });
            }
        });
    }

    /**
     * Reset the input fields so they are no longer in error state.
     */
    protected doResetInputFieldStates() {
        Debug.Tested();
        Debug.AssertValid(this.GiftEmail);
        Debug.AssertValid(this.GiftMessage);

        this.GiftEmail.resetState();
        this.GiftMessage.resetState();
    }
    
    /**
     * Validate the inputs.
     */
    protected doValidateInputs(): boolean {
        Debug.Tested();
        Debug.AssertValid(this.GiftEmail);
        Debug.AssertValid(this.GiftMessage);

        let retVal = false;
        let emailAddress = this.GiftEmail.getValue();
        Debug.AssertValidOrNull(emailAddress);
        if (!!emailAddress && EmailHelper.isValidEmailAddress(emailAddress)) {
            retVal = true;
        } else {
            ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_INVALID_EMAIL);
            this.GiftEmail.setErrorState();
        }
        return retVal;
    }
    
}   // GiftTicketPage
