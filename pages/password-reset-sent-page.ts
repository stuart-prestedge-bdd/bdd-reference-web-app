import { Debug } from '../helpers/debug';
import { MessagePage } from '../components/message-page';
import { TextContentService } from '../services/text-content-service';
import { ErrorHelper } from '../helpers/error-helper';
import { Constants } from '../constants';

/**
 * Class representing the password reset sent page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47055121/2.2.5.15.+Password+Reset+Sent+Page
 */
export class PasswordResetSentPage extends MessagePage {

    /**
     * Constructor.
     */
    constructor() {
        Debug.Tested();

        super(Constants.PAGE_URL_PASSWORD_RESET_SENT, false);
        this.addParameter(Constants.PAGE_PARAMETER_PASSWORD_RESET_SENT_USER_EMAIL);
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        super.doSetup(callback);
    }
    
    /**
     * Load the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLoadStaticText(() => {
            let names = [
                TextContentService.TEXT_NAME_TITLE_PASSWORD_RESET_SENT,
                TextContentService.TEXT_NAME_MESSAGE_PASSWORD_RESET_SENT
            ];
            TextContentService.getTextValues(names, (values) => {
                Debug.AssertValid(values);
                let userEmail = this_.getArgument(Constants.PAGE_PARAMETER_ACCOUNT_CREATED_USER_EMAIL);                        
                Debug.AssertString(userEmail);//??++WHAT IF NOT VALID (OR NOT EXIST?)?
                this_.Title = values.get(TextContentService.TEXT_NAME_TITLE_PASSWORD_RESET_SENT);
                this_.DescriptionHTML = values.get(TextContentService.TEXT_NAME_MESSAGE_PASSWORD_RESET_SENT).replace('${email}', userEmail);
                callback();
            });
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLanguageChanged(() => {
            callback();
        });
    }

}   // PasswordResetSentPage

