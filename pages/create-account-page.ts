import { Debug } from '../helpers/debug';
import { FormPage } from '../components/form-page';
import { WebApp } from '../components/web-app';
import { InputField } from '../generic/input-field';
//??--import { IdentityService } from '../services/identity-service';
import { ErrorHelper } from '../helpers/error-helper';
import { EmailHelper } from '../helpers/email-helper';
import { PasswordHelper } from '../helpers/password-helper';
import { ToastHelper } from '../helpers/toast-helper';
import { TextContentService } from '../services/text-content-service';
import { Constants } from '../constants';
import { Action } from '../generic/action';
import { DateOnly, Month } from '../data-models/identity-model';

/**
 * Class representing the create account page.
 * https://billiondollardraw.atlassian.net/wiki/spaces/TEC/pages/47153385/2.2.5.14.+Create+Account+Page
 */
export class CreateAccountPage extends FormPage {

    /**
     * The given name input field.
     */
    public readonly GivenNameField = new InputField(TextContentService.TEXT_NAME_GIVEN_NAME_HINT);

    /**
     * The family name input field.
     */
    public readonly FamilyNameField = new InputField(TextContentService.TEXT_NAME_FAMILY_NAME_HINT);

    /**
     * The email input field.
     */
    public readonly EmailField = new InputField(TextContentService.TEXT_NAME_EMAIL_HINT);

    /**
     * The password input field.
     */
    public readonly PasswordField = new InputField(TextContentService.TEXT_NAME_PASSWORD_HINT);

    //???country? dob? others?
    //??++TOS, PP

    /**
     * Constructor.
     */
    constructor() {
        super(Constants.PAGE_URL_CREATE_ACCOUNT, false);
        Debug.Tested();
    }

    /**
     * Setup the page.
     */
    protected doSetup(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doSetup(() => {
            this_.GivenNameField.setup(() => {
                this_.FamilyNameField.setup(() => {
                    this_.EmailField.setup(() => {
                        this_.PasswordField.setup(() => {
                            callback();
                        });
                    });
                });
            });
        });
    }
    /**
     * Loads the static text.
     */
    protected doLoadStaticText(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);

        let this_ = this;
        super.doLoadStaticText(() => {
            let names = [
                TextContentService.TEXT_NAME_TITLE_CREATE_ACCOUNT,
                TextContentService.TEXT_NAME_BUTTON_CREATE_ACCOUNT
            ];
            TextContentService.getTextValues(names, (values) => {
                Debug.AssertValid(values);
                this_.Title.setValue(values.get(TextContentService.TEXT_NAME_TITLE_CREATE_ACCOUNT));
                this_.Button.setLabel(values.get(TextContentService.TEXT_NAME_BUTTON_CREATE_ACCOUNT));
                callback();
            });
        });
    }

    /**
     * Called when the language changes.
     */
    protected doLanguageChanged(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.GivenNameField);
        Debug.AssertValid(this.FamilyNameField);
        Debug.AssertValid(this.EmailField);
        Debug.AssertValid(this.PasswordField);

        let this_ = this;
        super.doLanguageChanged(() => {
            this_.GivenNameField.languageChanged(() => {
                this_.FamilyNameField.languageChanged(() => {
                    this_.EmailField.languageChanged(() => {
                        this_.PasswordField.languageChanged(() => {
                            callback();
                        });
                    });
                });
            });
        });
    }

    /**
     * Called when the 'Create account' button is clicked.
     */
    protected doButtonClicked(callback: () => void) {
        Debug.Tested();
        Debug.AssertValid(callback);
        Debug.AssertValid(this.Application);
        Debug.AssertValid(this.GivenNameField);
        Debug.AssertValid(this.FamilyNameField);
        Debug.AssertValid(this.EmailField);
        Debug.AssertValid(this.PasswordField);

        let givenName = this.GivenNameField.getValue();
        Debug.AssertString(givenName);
        let familyName = this.FamilyNameField.getValue();
        Debug.AssertString(familyName);
        let emailAddress = this.EmailField.getValue();
        Debug.AssertString(emailAddress);
        let password = this.PasswordField.getValue();
        Debug.AssertString(password);
        let dateOfBirth = new DateOnly(1970, Month.jan, 2);//??++GET FROM DOB CONTROL
        //??++ADDRESS (W/ COUNTRY)
        this.Application.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, null, null, null, null, null, null, null, null, () => {
            callback();
        });
    }

    /**
     * Reset the input fields so they are no longer in error state.
     */
    protected doResetInputFieldStates() {
        Debug.Tested();
        Debug.AssertValid(this.GivenNameField);
        Debug.AssertValid(this.FamilyNameField);
        Debug.AssertValid(this.EmailField);
        Debug.AssertValid(this.PasswordField);

        this.GivenNameField.resetState();
        this.FamilyNameField.resetState();
        this.EmailField.resetState();
        this.PasswordField.resetState();
    }
    
    /**
     * Validate the inputs.
     */
    protected doValidateInputs(): boolean {
        Debug.Tested();
        Debug.AssertValid(this.GivenNameField);
        Debug.AssertValid(this.FamilyNameField);
        Debug.AssertValid(this.EmailField);
        Debug.AssertValid(this.PasswordField);

        let retVal = false;
        let givenName = this.GivenNameField.getValue();
        Debug.AssertValidOrNull(givenName);
        if (!!givenName) {
            let familyName = this.FamilyNameField.getValue();
            Debug.AssertValidOrNull(familyName);
            if (!!familyName) {
                let emailAddress = this.EmailField.getValue();
                Debug.AssertValidOrNull(emailAddress);
                if (!!emailAddress && EmailHelper.isValidEmailAddress(emailAddress)) {
                    let password = this.PasswordField.getValue();
                    Debug.AssertValidOrNull(password);
                    if (!!password && PasswordHelper.isValidPassword(password)) {
                        retVal = true;
                    } else {
                        Debug.Untested();
                        ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_INVALID_PASSWORD);
                        this.PasswordField.setErrorState();
                    }
                } else {
                    Debug.Untested();
                    ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_INVALID_EMAIL);
                    this.EmailField.setErrorState();
                }
            } else {
                Debug.Untested();
                ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_INVALID_FAMILY_NAME);
                this.FamilyNameField.setErrorState();
            }
        } else {
            Debug.Untested();
            ToastHelper.showError(TextContentService.TEXT_NAME_ERROR_INVALID_GIVEN_NAME);
            this.GivenNameField.setErrorState();
        }
        return retVal;
    }
    
}   // CreateAccountPage
