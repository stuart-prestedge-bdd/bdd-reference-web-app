import { Debug } from './helpers/debug';
import { ToastHelper } from './helpers/toast-helper';
import { IApplication } from './interfaces/application';
import { Application } from './components/application';
import { WebApp } from './components/web-app';
import { ChangeNamePopupWindow } from './components/change-name-popup-window';
import { ChangeGenderPopupWindow } from './components/change-gender-popup-window';
import { ChangeDateOfBirthPopupWindow } from './components/change-dob-popup-window';
import { ChangeAddressPopupWindow } from './components/change-address-popup-window';
import { LoginPage } from './pages/login-page';
import { CreateAccountPage } from './pages/create-account-page';
import { AccountCreatedPage } from './pages/account-created-page';
import { CausesPage } from './pages/causes-page';
import { HomePage } from './pages/home-page';
import { CausePage } from './pages/cause-page';
import { CharityPage } from './pages/charity-page';
import { CharitiesPage } from './pages/charities-page';
import { AmbassadorsPage } from './pages/ambassadors-page';
import { AmbassadorPage } from './pages/ambassador-page';
import { UserProfilePage } from './pages/user-profile-page';
import { TicketPage } from './pages/ticket-page';
import { GiftTicketPage } from './pages/gift-ticket-page';
import { MyTicketsPage } from './pages/my-tickets-page';
import { LIST_ACTION_OPEN } from './generic/list-component';
import { BuyTicketsPage } from './pages/buy-tickets-page';
import { IdentityService, IdentityServiceAPI } from './services/identity-service';
import { LocalStorageService } from './services/local-storage-service';
import { TicketService } from './services/ticket-service';
import { Gender, DateOnly, Address, Month } from './data-models/identity-model';
import { Constants } from './constants';
import { GamePage } from './pages/game-page';
import { GameContentService } from './services/game-content-service';
import { EmailVerifiedPage } from './pages/email-verified-page';
import { ForgotPasswordPage } from './pages/forgot-password-page';
import { PasswordResetSentPage } from './pages/password-reset-sent-page';
import { ConfirmDetailsPage } from './pages/confirm-details-page';
import { TextContentService } from './services/text-content-service';
import { DynamicLinkPage } from './pages/dynamic-link-page';
import { URLHelper } from './helpers/url-helper';
import { PageBase } from './generic/page-base';
import { PopupWindow } from './generic/popup-window';
import { TestIdentityService } from './tests/test-identity-service';

/**
 * TODO:
 *  Look for all Uncoded and code up
 *  Look for all ??++ and handle
 *  Implement change password flow (send link, link goes to /reset-password which calls identity service).
 *  Payment details page (Q: what is flow on this and how to integrate with web app?).
 *  Make CMS root dependant on APP root (by default at least).
 *  Unit test all services
 */

/**
 * Notes for user of this library.
 *  Must first call LocalStorageService.setCallbacks() with callbacks for reading & writing from/to local storage.
 *  Must next call ToastHelper.setShowToastCallback() with callback to show toast.
 *  Must then call Application.setup().
 *  Application object is available using WebApp.getInstance(). All state information is available in the returned IApplication.
 *  Must call application.setCallbacks() with callbacks to show popup, auto navigate and compose an email.
 *  Current page available using application.getCurrentPage().
 *  Current user available using application.getCurrentUser().
 *  To navigate to a page using a URL, call application.navigateTo().
 *  When button, tickbox or link is clicked (or enter pressed while it has focus), call <component>.execute().
 *  When menu item is clicked (or enter pressed while it has focus), call <menu_component>.executeItem().
 *  When a list item is clicked, call <list>.performAction().
 */

/**
 * Notes for developers of this library.
 *  Each control must extend Component.
 *  Each control must implement doSetup(), doLoadStaticText() and doLanguageChanged().
 *  Each page control must extend Page (directly or indirectly via classes such as FormPage or MessagePage).
 *  Each page control must implement doOpen().
 *  doSetup() implementations must:
 *      Setup and use a 'this_' variable throughout.
 *      Call super.doSetup().
 *      Call subComponent.setup() for each sub-component.
 *      Call the passed in callback().
 *  doLoadStaticText() implementations must:
 *      Setup and use a 'this_' variable throughout.
 *      Call super.doLoadStaticText().
 *      NOT call subComponent.doLoadStaticText() for each sub-component.
 *      Load any static text using TextContentService.getTextValue() or TextContentService.getTextValues() for sub-components that do not load their own text.
 *      Call the passed in callback().
 */

/**
 * Startup class.
 */
class Startup {

    /**
     * The toasts.
     */
    private static Toasts: { info: boolean, text: string }[] = [];

    /**
     * Entry point method.
     */
    public static main() {
        Debug.Tested();

        console.log('MAIN');
        LocalStorageService.setCallbacks(
            (key: string, callback: (error: Error, value: any) => void) => {
                callback(null, null);
            },
            (key: string, value: any, callback: (error: Error) => void) => {
                callback(null);
            }
        );
        ToastHelper.setShowToastCallback(
            (info, text) => {
                Debug.Tested();
                Debug.AssertString(text);
                Startup.addToast(info, text);
            }
        );
        Application.setup((application: IApplication) => {
            Debug.AssertValid(application);
            application.setCallbacks(Startup.popupCallback, Startup.autoNavigateCallback, Startup.composeEmail);
            Test(() => {
                console.log('TESTS COMPLETE');
            });
        });
        return 0;
    }

    /**
     * The popup callback. 
     */
    private static popupCallback(popupWindow: PopupWindow, closedCallback: (closeAction: string) => void) {
        Debug.Tested();
        Debug.AssertValid(popupWindow);
        Debug.AssertValid(closedCallback);
        if (popupWindow.getID() === 'change-name') {
            Debug.Assert(popupWindow instanceof ChangeNamePopupWindow);
            (<ChangeNamePopupWindow>popupWindow).GivenNameField.setValue('Fred');
            (<ChangeNamePopupWindow>popupWindow).FamilyNameField.setValue('Smith');
            (<ChangeNamePopupWindow>popupWindow).FullNameField.setValue('Fred Smith');
            (<ChangeNamePopupWindow>popupWindow).PreferredNameField.setValue('Freddy');
        } else if (popupWindow.getID() === 'change-gender') {
            Debug.Assert(popupWindow instanceof ChangeGenderPopupWindow);
            (<ChangeGenderPopupWindow>popupWindow).Gender = Gender.female;
        } else if (popupWindow.getID() === 'change-dob') {
            Debug.Assert(popupWindow instanceof ChangeDateOfBirthPopupWindow);
            (<ChangeDateOfBirthPopupWindow>popupWindow).DateOfBirth = new DateOnly(1970, 1, 1);
        } else if (popupWindow.getID() === 'change-address') {
            Debug.Assert(popupWindow instanceof ChangeAddressPopupWindow);
            (<ChangeAddressPopupWindow>popupWindow).Address = new Address();
        } else {
            Debug.Unreachable();
        }
        closedCallback('OK');
    }

    /**
     * Callback for auto navigation.
     */
    private static autoNavigateCallback(currentPage: PageBase) {
        Debug.Tested();
        Debug.AssertValid(currentPage);
    }

    /**
     * Callback for composing an email.
     */
    private static composeEmail(subject: string, body: string, bodyIsHTML: boolean, callback: () => void) {
        Debug.Tested();
        Debug.AssertString(subject);
        Debug.AssertStringOrNull(body);
        Debug.AssertValid(callback);
        console.log(`Compose email: ${subject}`);
        callback();
    }

    /**
     * Get the next toast.
     */
    private static addToast(info: boolean, text: string) {
        Debug.Tested();
        Debug.AssertValid(Startup.Toasts);

        Startup.Toasts.push({ info, text });
    }

    /**
     * Get the next toast.
     */
    private static getNextToast(): { info: boolean, text: string } {
        Debug.Tested();
        Debug.AssertValid(Startup.Toasts);

        let retVal = null;
        if (Startup.Toasts.length > 0) {
            retVal = Startup.Toasts.splice(0, 1)[0];
            Debug.AssertValid(retVal);
        }
        return retVal;
    }

    /**
     * Get the next toast.
     */
    public static checkToast(info: boolean, text: string) {
        Debug.Tested();
        Debug.AssertValid(Startup.Toasts);

        let toast = Startup.getNextToast();
        Debug.AssertValid(toast);
        if (toast != null) {
            Debug.Assert(toast.info === info);
            Debug.Assert(toast.text === text);
        }
    }

    /**
     * Get that there is no toast.
     */
    public static checkNoToast() {
        Debug.Tested();
        Debug.AssertValid(Startup.Toasts);

        let toast = Startup.getNextToast();
        Debug.AssertNull(toast);
    }

} // Startup

Startup.main();

// Test the web app logic.
function Test(callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(callback);

    let webApp = WebApp.getInstance();
    Debug.AssertValid(webApp);
    TestServices(webApp, () => {
        Debug.scheduleCallback(() => {
            TestPages(webApp, () => {
                Debug.scheduleCallback(() => {
                    TestBadURL(() => {
                        Debug.scheduleCallback(() => {
                            TestFailCloseAccount(() => {
                                Debug.scheduleCallback(() => {
                                    TestSetLanguage(webApp, () => {
                                        Debug.scheduleCallback(() => {
                                            TestMainMenu(webApp, () => {
                                                Debug.scheduleCallback(() => {
                                                    TestUserMenu(webApp, () => {
                                                        Debug.scheduleCallback(() => {
                                                            TestLinks(webApp, () => {
                                                                callback();
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

// Test the services.
function TestServices(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    TestGameContentService(webApp, () => {
        Debug.scheduleCallback(() => {
            TestIdentityService(webApp, () => {
                Debug.Untested();
                Debug.scheduleCallback(() => {
                    TestLinksContentService(webApp, () => {
                        Debug.scheduleCallback(() => {
                            TestLocalStorageService(webApp, () => {
                                Debug.scheduleCallback(() => {
                                    TestPaymentService(webApp, () => {
                                        Debug.scheduleCallback(() => {
                                            TestPhilanthropyContentService(webApp, () => {
                                                Debug.scheduleCallback(() => {
                                                    TestSettingsContentService(webApp, () => {
                                                        Debug.scheduleCallback(() => {
                                                            TestTextContentService(webApp, () => {
                                                                Debug.scheduleCallback(() => {
                                                                    TestTicketService(webApp, () => {
                                                                        callback();
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

// Test the game content service.
function TestGameContentService(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Get all games
    GameContentService.getGames((games) => {
        Debug.AssertValid(games);
        Debug.Assert(games.length > 0);
        let game = games[0];
        Debug.AssertValid(game);
        Debug.AssertString(game.idHash);

        // Get current game
        GameContentService.getCurrentGame((currentGame) => {
            Debug.AssertNull(currentGame);

            // Get game by hash ID.
            GameContentService.getGameByIDHash(game.idHash, (error, game) => {
                Debug.AssertValid(game);

                // Done
                callback();
            });
        });
    });
}

// Test the links content service.
function TestLinksContentService(webApp: IApplication, callback: () => void) {
    Debug.Uncoded();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    callback();
}

// Test the local storage service.
function TestLocalStorageService(webApp: IApplication, callback: () => void) {
    Debug.Uncoded();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    callback();
}

// Test the payment service.
function TestPaymentService(webApp: IApplication, callback: () => void) {
    Debug.Uncoded();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    callback();
}

// Test the philanthropy content service.
function TestPhilanthropyContentService(webApp: IApplication, callback: () => void) {
    Debug.Uncoded();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    callback();
}

// Test the settings content service.
function TestSettingsContentService(webApp: IApplication, callback: () => void) {
    Debug.Uncoded();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    callback();
}

// Test the text content service.
function TestTextContentService(webApp: IApplication, callback: () => void) {
    Debug.Uncoded();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    callback();
}

// Test the ticket service.
function TestTicketService(webApp: IApplication, callback: () => void) {
    Debug.Uncoded();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    callback();
}

// Test a bad URL.
function TestBadURL(callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(callback);

    let webApp = WebApp.getInstance();
    Debug.AssertValid(webApp);
    webApp.navigateTo('xxx', (currentPage) => {
        callback();
    });
}

// Test a fail to close and account.
function TestFailCloseAccount(callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(callback);

    let webApp = WebApp.getInstance();
    Debug.AssertValid(webApp);
    webApp.closeAccount(null, () => {
        Startup.checkToast(false, TextContentService.TEXT_NAME_ERROR_FAILED_TO_CLOSE_ACCOUNT);
        callback();
    });
}

// Test setting the language.
function TestSetLanguage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    webApp.navigateTo(Constants.PAGE_URL_HOME, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof HomePage);
        let homePage = <HomePage>currentPage;
        Debug.AssertValid(homePage.TopBanner);
        Debug.AssertValid(homePage.TopBanner.UserMenu);
        Debug.AssertValid(homePage.TopBanner.UserMenu.LanguageMenu);
        Debug.AssertValid(homePage.TopBanner.UserMenu.LanguageMenu.MenuItems);
        Debug.Assert(homePage.TopBanner.UserMenu.LanguageMenu.MenuItems.length > 1);
        let menuItem = homePage.TopBanner.UserMenu.LanguageMenu.MenuItems[1];
        Debug.AssertValid(menuItem);
        homePage.TopBanner.UserMenu.LanguageMenu.executeItem(menuItem, () => {
            callback();
        });
    });
}

// Test the pages.
function TestPages(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    Debug.scheduleCallback(() => {
        TestHomePage(webApp, () => {
            Debug.scheduleCallback(() => {
                TestLoginPage(webApp, () => {
                    Debug.scheduleCallback(() => {
                        TestCreateAccountPage(webApp, () => {
                            Debug.scheduleCallback(() => {
                                TestPhilanthropyPages(webApp, () => {
                                    Debug.scheduleCallback(() => {
                                        TestUserProfilePage(webApp, () => {
                                            Debug.scheduleCallback(() => {
                                                TestTicketPages(webApp, () => {
                                                    Debug.scheduleCallback(() => {
                                                        TestGamePage(webApp, () => {
                                                            Debug.scheduleCallback(() => {
                                                                TestForgotPasswordPage(webApp, () => {
                                                                    Debug.scheduleCallback(() => {
                                                                        TestConfirmDetailsPage(webApp, () => {
                                                                            callback();
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

// Test the homepage.
function TestHomePage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestHomePage`);
    webApp.navigateTo(Constants.PAGE_URL_HOME, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof HomePage);
        let homePage = <HomePage>currentPage;
        Debug.AssertValid(homePage.TopBanner);
        Debug.AssertValid(homePage.TopBanner.Logo);
        Debug.AssertValid(homePage.TopBanner.MainMenu);
        Debug.AssertValid(homePage.TopBanner.MainMenu.Icon);
        Debug.AssertValid(homePage.TopBanner.MainMenu.Icon.getImage());
        //??++Debug.AssertValid(homePage.TopBanner.Logo.getImage());
        Debug.AssertValid(homePage.VideoTile);
        Debug.AssertString(homePage.VideoTile.getURL());
        Debug.AssertValid(homePage.CharityIconBar);
        let images = homePage.CharityIconBar.getImages();
        Debug.AssertValid(images);
        Debug.Assert(images.length > 0);
        //??++Test the sub-components.
        Startup.checkNoToast();
        callback();
    });
}

// Test the login page
function TestLoginPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestLoginPage`);
    //  First, test successful login.
    TestSuccessfulLogin(webApp, () => {
        //  Next, test unsuccessful login.
        TestUnsuccessfulLogin(webApp, () => {
            callback();
        });
    });
}

// Test successful login.
function TestSuccessfulLogin(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestSuccessfulLogin`);
    webApp.navigateTo(Constants.PAGE_URL_LOGIN, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof LoginPage);
        let loginPage = <LoginPage>currentPage;
        loginPage.EmailField.setValue('test1@test.com');
        loginPage.PasswordField.setValue('Passw0rd');
        loginPage.Button.execute(() => {
            webApp.getCurrentUser((currentUser) => {
                Debug.AssertValid(currentUser);
                currentPage = webApp.getCurrentPage();
                Debug.AssertValid(currentPage);
                Debug.Assert(currentPage instanceof HomePage);
                webApp.logout((error) => {
                    Debug.AssertNull(error);
                    Startup.checkNoToast();
                    callback();
                });
            });
        });
    });
}

// Test unsuccessful login.
function TestUnsuccessfulLogin(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestUnsuccessfulLogin`);
    webApp.navigateTo(Constants.PAGE_URL_LOGIN, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof LoginPage);
        let loginPage = <LoginPage>currentPage;
        loginPage.EmailField.setValue('bad-user@test.com');
        loginPage.PasswordField.setValue('Passw0rd');
        loginPage.Button.execute(() => {
            Startup.checkToast(false, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOGIN);
            webApp.getCurrentUser((currentUser) => {
                Debug.AssertNull(currentUser);
                currentPage = webApp.getCurrentPage();
                Debug.AssertValid(currentPage);
                Debug.Assert(currentPage instanceof LoginPage);
                Startup.checkNoToast();
                //??++CHECK ALL TYPES OF LOGIN ERROR
                // Done
                callback();
            });
        });
    });
}

// Test create account.
function TestCreateAccountPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestCreateAccountPage`);

    let givenName = 'Stuart';
    let familyName = 'Prestedge';
    let emailAddress = 'stuart@prestedge.com';
    let password = 'Passw0rd';

    // First, open the create account page to create an account.
    webApp.navigateTo(Constants.PAGE_URL_CREATE_ACCOUNT, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof CreateAccountPage);
        let createAccountPage = <CreateAccountPage>currentPage;
        Debug.AssertValid(createAccountPage.GivenNameField);
        Debug.AssertString(createAccountPage.GivenNameField.getHint());
        Debug.AssertValid(createAccountPage.FamilyNameField);
        Debug.AssertString(createAccountPage.FamilyNameField.getHint());
        Debug.AssertValid(createAccountPage.EmailField);
        Debug.AssertString(createAccountPage.EmailField.getHint());
        Debug.AssertValid(createAccountPage.PasswordField);
        Debug.AssertString(createAccountPage.PasswordField.getHint());
        createAccountPage.GivenNameField.setValue(givenName);
        createAccountPage.FamilyNameField.setValue(familyName);
        createAccountPage.EmailField.setValue(emailAddress);
        createAccountPage.PasswordField.setValue(password);
        //??++OTHER FIELDS?
        createAccountPage.Button.execute(() => {
            currentPage = webApp.getCurrentPage();
            Debug.AssertValid(currentPage);
            Debug.Assert(currentPage instanceof AccountCreatedPage);

            // Next, navigate to the 'email verified' page.
            let url = `${Constants.PAGE_URL_EMAIL_VERIFIED}?${Constants.PAGE_PARAMETER_EMAIL_ADDRESS}=${emailAddress}&${Constants.PAGE_PARAMETER_EMAIL_VERIFICATION_ID}=xxx`;
            webApp.navigateTo(url, (currentPage) => {
                Debug.AssertValid(currentPage);
                Debug.Assert(currentPage instanceof EmailVerifiedPage);
                Startup.checkToast(true, TextContentService.TEXT_NAME_INFO_EMAIL_ADDRESS_VERIFIED);
                let emailVerifiedPage = <EmailVerifiedPage>currentPage;
                Debug.AssertString(emailVerifiedPage.getTitle());
                Debug.AssertString(emailVerifiedPage.getDescriptionHTML());
                Debug.AssertValid(emailVerifiedPage.ActionButton);
                emailVerifiedPage.ActionButton.execute(() => {
                    currentPage = webApp.getCurrentPage();
                    Debug.AssertValid(currentPage);
                    Debug.Assert(currentPage instanceof LoginPage);
                    let loginPage = <LoginPage>currentPage;
                    loginPage.EmailField.setValue(emailAddress);
                    loginPage.PasswordField.setValue(password);
                    loginPage.Button.execute(() => {
                        webApp.getCurrentUser((currentUser) => {
                            Debug.AssertValid(currentUser);
                            currentPage = webApp.getCurrentPage();
                            Debug.AssertValid(currentPage);
                            Debug.Assert(currentPage instanceof HomePage);

                            // Now test closing the account, first without logging in.
                            webApp.closeAccount(password, () => {
                                Startup.checkNoToast();

                                // Done
                                callback();
                            });
                        });
                    });
                });
            });
        });
    });
}

// Test the philanthropy pages.
function TestPhilanthropyPages(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    TestCausesPage(webApp, () => {
        Debug.scheduleCallback(() => {
            TestCharitiesPage(webApp, () => {
                Debug.scheduleCallback(() => {
                    TestAmbassadorsPage(webApp, () => {
                        callback();
                    });
                });
            });
        });
    });
}

// Test the causes page.
function TestCausesPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestCausesPage`);
    webApp.navigateTo(Constants.PAGE_URL_CAUSES, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof CausesPage);
        let causesPage = <CausesPage>currentPage;
        let iconTitleTextList = causesPage.IconTitleTextList;
        Debug.AssertValid(iconTitleTextList);
        let items = iconTitleTextList.Items;
        Debug.AssertValid(items);
        Debug.Assert(items.length > 0);
        let item = items[0];
        Debug.AssertValid(item);
        iconTitleTextList.itemClicked(item, () => {
            currentPage = webApp.getCurrentPage();
            Debug.AssertValid(currentPage);
            Debug.Assert(currentPage instanceof CausePage);
            Startup.checkNoToast();
            callback();
        });
    });
}

// Test the charities page.
function TestCharitiesPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestCharitiesPage`);
    webApp.navigateTo(Constants.PAGE_URL_CHARITIES, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof CharitiesPage);
        let charitiesPage = <CharitiesPage>currentPage;
        let iconTitleTextList = charitiesPage.IconTitleTextList;
        Debug.AssertValid(iconTitleTextList);
        let items = iconTitleTextList.Items;
        Debug.AssertValid(items);
        Debug.Assert(items.length > 0);
        let item = items[0];
        Debug.AssertValid(item);
        iconTitleTextList.itemClicked(item, () => {
            currentPage = webApp.getCurrentPage();
            Debug.AssertValid(currentPage);
            Debug.Assert(currentPage instanceof CharityPage);
            Startup.checkNoToast();
            callback();
        });
    });
}

// Test the ambassadors page.
function TestAmbassadorsPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestAmbassadorsPage`);
    webApp.navigateTo(Constants.PAGE_URL_AMBASSADORS, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof AmbassadorsPage);
        let ambassadorsPage = <AmbassadorsPage>currentPage;
        let iconTitleTextList = ambassadorsPage.IconTitleTextList;
        Debug.AssertValid(iconTitleTextList);
        let items = iconTitleTextList.Items;
        Debug.AssertValid(items);
        Debug.Assert(items.length > 0);
        let item = items[0];
        Debug.AssertValid(item);
        iconTitleTextList.itemClicked(item, () => {
            currentPage = webApp.getCurrentPage();
            Debug.AssertValid(currentPage);
            Debug.Assert(currentPage instanceof AmbassadorPage);
            Startup.checkNoToast();
            callback();
        });
    });
}

// Test the user profile page.
function TestUserProfilePage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestUserProfilePage`);
    // First, while not logged in.
    webApp.navigateTo(Constants.PAGE_URL_USER_PROFILE, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof HomePage);
        // Next, while logged in.
        webApp.login('test1@test.com', 'Passw0rd', (error) => {
            Debug.AssertNull(error);
            webApp.navigateTo(Constants.PAGE_URL_USER_PROFILE, (currentPage) => {
                Debug.AssertValid(currentPage);
                Debug.Assert(currentPage instanceof UserProfilePage);
                let userProfilePage = <UserProfilePage>currentPage;
                userProfilePage.changeNameClicked(() => {
                    userProfilePage.changeGenderClicked(() => {
                        //??--userProfilePage.changeDateOfBirthClicked(() => {
                        userProfilePage.changeAddressClicked(() => {
                            webApp.logout((error) => {
                                Debug.AssertNull(error);
                                Startup.checkNoToast();
                                callback();
                            })
                        });
                        //??--});
                    });
                });
            });
        });
    });
}

// Test the ticket pages.
function TestTicketPages(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    TestMyTicketsPage(webApp, () => {
        Debug.scheduleCallback(() => {
            TestTicketPage(webApp, () => {
                Debug.scheduleCallback(() => {
                    TestBuyTicketPage(webApp, () => {
                        callback();
                    });
                });
            });
        });
    });
}

// Test the my tickets page.
function TestMyTicketsPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestMyTicketsPage`);
    webApp.login('test1@test.com', 'Passw0rd', (error) => {
        Debug.AssertNull(error);
        // First test clicking the buy tile's 'Buy' button.
        webApp.navigateTo(Constants.PAGE_URL_MY_TICKETS, (currentPage) => {
            Debug.AssertValid(currentPage);
            Debug.Assert(currentPage instanceof MyTicketsPage);
            let myTicketsPage = <MyTicketsPage>currentPage;
            myTicketsPage.BuyTicketTile.BuyButton.execute(() => {
                currentPage = WebApp.getInstance().getCurrentPage();
                Debug.AssertValid(currentPage);
                Debug.Assert(currentPage instanceof BuyTicketsPage);

                // Now test clicking a ticket
                webApp.navigateTo(Constants.PAGE_URL_MY_TICKETS, (currentPage) => {
                    Debug.AssertValid(currentPage);
                    Debug.Assert(currentPage instanceof MyTicketsPage);
                    let myTicketsPage = <MyTicketsPage>currentPage;
                    myTicketsPage.TicketList.performAction(LIST_ACTION_OPEN, 0, () => {
                        let currentPage = webApp.getCurrentPage();
                        Debug.AssertValid(currentPage);
                        Debug.Assert(currentPage instanceof TicketPage);
                        webApp.logout((error) => {
                            Debug.AssertNull(error);
                            Startup.checkNoToast();
                            callback();
                        });
                    });
                    //??? ticketPage.ButtonGift.clicked(() => {
                    //     currentPage = webApp.getCurrentPage();
                    //     Debug.AssertValid(currentPage);
                    //     Debug.Assert(currentPage instanceof GiftTicketPage);
                    //     callback();
                    // });
                });
            });
            //??? ticketPage.ButtonGift.clicked(() => {
            //     currentPage = webApp.getCurrentPage();
            //     Debug.AssertValid(currentPage);
            //     Debug.Assert(currentPage instanceof GiftTicketPage);
            //     callback();
            // });
        });
    });
}

// Test the ticket page.
function TestTicketPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestTicketPage`);
    TestTicketPageUnauthenticated(webApp, () => {
        Debug.scheduleCallback(() => {
            TestInvalidTicketPage(webApp, () => {
                Debug.scheduleCallback(() => {
                    TestValidTicketPage(webApp, () => {
                        callback();
                    });
                });
            });
        });
    });
}

// Test the ticket page without logging in.
function TestTicketPageUnauthenticated(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestTicketPageUnauthenticated`);
    webApp.navigateTo(`${Constants.PAGE_URL_TICKET}?${Constants.PAGE_PARAMETER_TICKET_ID}=xxx`, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof HomePage);
        Startup.checkNoToast();
        callback();
    });
}

// Test the ticket page with invalid inputs.
function TestInvalidTicketPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestInvalidTicketPage`);
    webApp.login('test1@test.com', 'Passw0rd', (error) => {
        Debug.AssertNull(error);

        // First test with no input.
        webApp.navigateTo(`${Constants.PAGE_URL_TICKET}`, (currentPage) => {
            Debug.AssertValid(currentPage);
            Debug.Assert(currentPage instanceof TicketPage);

            // Next test with invalid input.
            webApp.navigateTo(`${Constants.PAGE_URL_TICKET}?${Constants.PAGE_PARAMETER_TICKET_ID}=yyy`, (currentPage) => {
                Debug.AssertValid(currentPage);
                Debug.Assert(currentPage instanceof TicketPage);
                Startup.checkToast(false, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_TICKET);
                webApp.logout((error) => {
                    Debug.AssertNull(error);
                    Startup.checkNoToast();
                    callback();
                });
            });
        });
    });
}

// Test the valid ticket page.
function TestValidTicketPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestValidTicketPage`);
    webApp.login('test1@test.com', 'Passw0rd', (error) => {
        Debug.AssertNull(error);

        webApp.getCurrentUser((currentUser, accessToken) => {
            Debug.AssertValid(currentUser);
            Debug.AssertString(currentUser.ID);
            TicketService.getUserTickets(currentUser.ID, accessToken, (error, ticketInfos) => {
                Debug.AssertNull(error);
                Debug.AssertValid(ticketInfos);
                Debug.Assert(ticketInfos.length > 0);
                let ticketInfo = ticketInfos[0];
                Debug.AssertValid(ticketInfo);
                Debug.AssertString(ticketInfo.ID);

                // First test the ticket information and the 'Gift ticket' button.
                webApp.navigateTo(`${Constants.PAGE_URL_TICKET}?${Constants.PAGE_PARAMETER_TICKET_ID}=${ticketInfo.ID}`, (currentPage) => {
                    Debug.AssertValid(currentPage);
                    Debug.Assert(currentPage instanceof TicketPage);
                    let ticketPage = <TicketPage>currentPage;
                    Debug.AssertValid(ticketPage.ButtonGift);
                    Debug.Assert(!ticketPage.ButtonGift.getHidden());
                    ticketPage.ButtonGift.execute(() => {
                        currentPage = webApp.getCurrentPage();
                        Debug.AssertValid(currentPage);
                        Debug.Assert(currentPage instanceof GiftTicketPage);

                        // Offer without entering email address
                        let giftTicketPage = <GiftTicketPage>currentPage;
                        Debug.AssertValid(giftTicketPage.Button);
                        giftTicketPage.Button.execute(() => {
                            Startup.checkToast(false, TextContentService.TEXT_NAME_ERROR_INVALID_EMAIL);
                            currentPage = webApp.getCurrentPage();
                            Debug.AssertValid(currentPage);
                            Debug.Assert(currentPage instanceof GiftTicketPage);
                            let giftTicketPage = <GiftTicketPage>currentPage;
                            Debug.AssertValid(giftTicketPage.GiftEmail);
                            Debug.Assert(giftTicketPage.GiftEmail.getErrorState());

                            // Offer ticket to an email address.
                            giftTicketPage.GiftEmail.setValue('dummy@test.com');
                            giftTicketPage.Button.execute(() => {
                                currentPage = webApp.getCurrentPage();
                                Debug.AssertValid(currentPage);
                                Debug.Assert(currentPage instanceof TicketPage);

                                // Next, test the 'Redeem ticket' button.
                                webApp.navigateTo(`${Constants.PAGE_URL_TICKET}?${Constants.PAGE_PARAMETER_TICKET_ID}=ticket_5`, (currentPage) => {
                                    Debug.AssertValid(currentPage);
                                    Debug.Assert(currentPage instanceof TicketPage);
                                    ticketPage = <TicketPage>currentPage;
                                    Debug.AssertValid(ticketPage.ButtonRedeem);
                                    Debug.Assert(!ticketPage.ButtonRedeem.getHidden());
                                    ticketPage.ButtonRedeem.execute(() => {
                                        Startup.checkToast(true, TextContentService.TEXT_NAME_INFO_TICKET_REDEEMED);
                                        currentPage = webApp.getCurrentPage();
                                        Debug.AssertValid(currentPage);
                                        Debug.Assert(currentPage instanceof TicketPage);
                                        webApp.logout((error) => {
                                            Debug.AssertNull(error);
                                            Startup.checkNoToast();
                                            callback();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

// Test the ticket page.
function TestBuyTicketPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestBuyTicketPage`);
    // First test unauthenticated goes to home page.
    webApp.navigateTo(Constants.PAGE_URL_BUY_TICKETS, (currentPage) => {
        Startup.checkNoToast();
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof HomePage);
        // First test buy without setting any numbers.
        webApp.login('test1@test.com', 'Passw0rd', (error) => {
            Debug.AssertNull(error);
            webApp.navigateTo(Constants.PAGE_URL_BUY_TICKETS, (currentPage) => {
                Debug.AssertValid(currentPage);
                Debug.Assert(currentPage instanceof BuyTicketsPage);
                let buyTicketPage = <BuyTicketsPage>currentPage;
                Debug.AssertValid(buyTicketPage.BuyButton);
                Debug.AssertValid(buyTicketPage.NumberLines);
                Debug.Assert(buyTicketPage.NumberLines.length === 1);
                Debug.AssertValid(buyTicketPage.NumberLines[0]);
                Debug.AssertValid(buyTicketPage.NumberLines[0].ChooseForMeLink);
                buyTicketPage.BuyButton.execute(() => {
                    Startup.checkToast(false, TextContentService.TEXT_NAME_ERROR_FAILED_TO_BUY_TICKETS);
                    // Next test buy setting a numbers.
                    buyTicketPage.NumberLines[0].ChooseForMeLink.execute(() => {
                        Debug.AssertValid(buyTicketPage.NumberLines[0].Number.getValue());
                        buyTicketPage.BuyButton.execute(() => {
                            Startup.checkToast(true, TextContentService.TEXT_NAME_INFO_TICKET_PURCHASED);
                            //??++CHECK POPUP? OR CONFIRMATION POPUP OR JUST 'YOU'VE BOUGHT';
                            //??++CHECK CORRECT PAGE (MY TICKETS?).

                            webApp.logout((error) => {
                                Debug.AssertNull(error);
                                Startup.checkNoToast();
                                callback();
                            });
                        });
                    });
                });
            });
        });
    });
}

// Test the game page.
function TestGamePage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestGamePage`);
    TestInvalidGamePage(webApp, () => {
        Debug.scheduleCallback(() => {
            TestValidGamePage(webApp, () => {
                callback();
            });
        });
    });
}

// Test the game page with invalid inputs.
function TestInvalidGamePage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestInvalidGamePage`);
    Startup.checkNoToast();
    webApp.login('test1@test.com', 'Passw0rd', (error) => {
        Debug.AssertNull(error);
        Startup.checkNoToast();
        // First test with no input.
        let url = URLHelper.replaceURLParameter(Constants.PAGE_URL_GAME, Constants.PAGE_PARAMETER_GAME_ID_HASH, '');
        webApp.navigateTo(url, (currentPage) => {
            Debug.AssertValid(currentPage);
            Debug.Assert(currentPage instanceof GamePage);
            Startup.checkNoToast();
            // Next, test with invalid input.
            url = URLHelper.replaceURLParameter(Constants.PAGE_URL_GAME, Constants.PAGE_PARAMETER_GAME_ID_HASH, 'yyy');
            webApp.navigateTo(url, (currentPage) => {
                Debug.AssertValid(currentPage);
                Debug.Assert(currentPage instanceof GamePage);
                Startup.checkToast(false, TextContentService.TEXT_NAME_ERROR_FAILED_TO_LOAD_GAME);
                webApp.logout((error) => {
                    Debug.AssertNull(error);
                    Startup.checkNoToast();
                    callback();
                });
            });
        });
    });
}

// Test the valid game page.
function TestValidGamePage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestValidGamePage`);
    webApp.login('test1@test.com', 'Passw0rd', (error) => {
        Debug.AssertNull(error);

        // First test the game information.
        let url = URLHelper.replaceURLParameter(Constants.PAGE_URL_GAME, Constants.PAGE_PARAMETER_GAME_ID_HASH, 'game_1_hash');
        webApp.navigateTo(url, (currentPage) => {
            Debug.AssertValid(currentPage);
            Debug.Assert(currentPage instanceof GamePage);
            webApp.logout((error) => {
                Debug.AssertNull(error);
                Startup.checkNoToast();
                callback();
            });
        });
    });
}

// Test forgot password.
function TestForgotPasswordPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestForgotPasswordPage`);

    let emailAddress = 'stuart@prestedge.com';

    // First, open the forgot password page (no need to login) and submit with no email.
    webApp.navigateTo(Constants.PAGE_URL_FORGOT_PASSWORD, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof ForgotPasswordPage);
        let forgotPasswordPage = <ForgotPasswordPage>currentPage;
        Debug.AssertValid(forgotPasswordPage.Button);
        forgotPasswordPage.Button.execute(() => {
            Startup.checkToast(false, TextContentService.TEXT_NAME_ERROR_INVALID_EMAIL);
            currentPage = webApp.getCurrentPage();
            Debug.AssertValid(currentPage);
            Debug.Assert(currentPage instanceof ForgotPasswordPage);
            forgotPasswordPage = <ForgotPasswordPage>currentPage;
            Debug.AssertValid(forgotPasswordPage.EmailField);
            Debug.Assert(forgotPasswordPage.EmailField.getErrorState());
            Debug.AssertValid(forgotPasswordPage.Button);

            // Next, test an invalid email address
            forgotPasswordPage.EmailField.setValue('invalid-email');
            forgotPasswordPage.Button.execute(() => {
                Startup.checkToast(false, TextContentService.TEXT_NAME_ERROR_INVALID_EMAIL);
                currentPage = webApp.getCurrentPage();
                Debug.AssertValid(currentPage);
                Debug.Assert(currentPage instanceof ForgotPasswordPage);
                forgotPasswordPage = <ForgotPasswordPage>currentPage;
                Debug.AssertValid(forgotPasswordPage.EmailField);
                Debug.Assert(forgotPasswordPage.EmailField.getErrorState());
                Debug.AssertValid(forgotPasswordPage.Button);

                // Now test with a real email address
                forgotPasswordPage.EmailField.setValue(emailAddress);
                forgotPasswordPage.Button.execute(() => {
                    currentPage = webApp.getCurrentPage();
                    Debug.AssertValid(currentPage);
                    Debug.Assert(currentPage instanceof PasswordResetSentPage);
                    let passwordResetSentPage = <PasswordResetSentPage>currentPage;
                    Debug.AssertString(passwordResetSentPage.getTitle());
                    Debug.AssertString(passwordResetSentPage.getDescriptionHTML());
                    Startup.checkNoToast();

                    // Done
                    callback();
                });
            });
        });
    });
}

// Test confirm details.
function TestConfirmDetailsPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    TestInvalidInputsConfirmDetailsPage(webApp, () => {
        Debug.scheduleCallback(() => {
            TestValidConfirmDetailsPage(webApp, () => {
                callback();
            });
        });
    });
}

// Test confirm details with invalid inputs.
function TestInvalidInputsConfirmDetailsPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestInvalidInputsConfirmDetailsPage`);

    // Confirm new user details with no arguments.
    let url = `${Constants.PAGE_URL_CONFIRM_DETAILS}`;
    webApp.navigateTo(url, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof HomePage);
        Startup.checkToast(false, TextContentService.TEXT_NAME_ERROR_INVALID_VERIFICATION_DETAILS);
        // Confirm new user details with no invalid arguments.
        url = `${Constants.PAGE_URL_CONFIRM_DETAILS}?${Constants.PAGE_PARAMETER_EMAIL_ADDRESS}=&${Constants.PAGE_PARAMETER_EMAIL_VERIFICATION_ID}=`;
        webApp.navigateTo(url, (currentPage) => {
            Debug.AssertValid(currentPage);
            Debug.Assert(currentPage instanceof HomePage);
            Startup.checkToast(false, TextContentService.TEXT_NAME_ERROR_INVALID_VERIFICATION_DETAILS);
            Startup.checkNoToast();
            callback();
        });
    });
}

// Test confirm details.
function TestValidConfirmDetailsPage(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    console.log(`TestValidConfirmDetailsPage`);

    let emailAddress = 'marta@prestedge.com';

    // Login
    webApp.login('test1@test.com', 'Passw0rd', (error) => {
        Debug.AssertNull(error);
        // Navigate to buy page
        webApp.navigateTo(Constants.PAGE_URL_BUY_TICKETS, (currentPage) => {
            Debug.AssertValid(currentPage);
            Debug.Assert(currentPage instanceof BuyTicketsPage);
            let buyTicketPage = <BuyTicketsPage>currentPage;
            Debug.AssertValid(buyTicketPage.NumberLines);
            Debug.AssertValid(buyTicketPage.GiftTickbox);
            Debug.AssertValid(buyTicketPage.GiftEmail);
            Debug.AssertValid(buyTicketPage.BuyButton);
            Debug.Assert(buyTicketPage.NumberLines.length === 1);
            Debug.AssertValid(buyTicketPage.NumberLines[0]);
            Debug.AssertValid(buyTicketPage.NumberLines[0].ChooseForMeLink);
            // Set a number
            buyTicketPage.NumberLines[0].ChooseForMeLink.execute(() => {
                Debug.AssertValid(buyTicketPage.NumberLines[0].Number.getValue());
                // Set the gift tickbox
                buyTicketPage.GiftTickbox.setIsTicked(true, () => {
                    // Set the gift email
                    buyTicketPage.GiftEmail.setValue(emailAddress);
                    // Purchase
                    buyTicketPage.BuyButton.execute(() => {
                        Startup.checkToast(true, TextContentService.TEXT_NAME_INFO_TICKET_PURCHASED);
                        // Logout                        
                        webApp.logout((error) => {
                            Debug.AssertNull(error);

                            // Confirm new user details
                            let url = `${Constants.PAGE_URL_CONFIRM_DETAILS}?${Constants.PAGE_PARAMETER_EMAIL_ADDRESS}=${emailAddress}&${Constants.PAGE_PARAMETER_EMAIL_VERIFICATION_ID}=xxx`;
                            webApp.navigateTo(url, (currentPage) => {
                                Debug.AssertValid(currentPage);
                                Debug.Assert(currentPage instanceof ConfirmDetailsPage);
                                let confirmDetailsPage = <ConfirmDetailsPage>currentPage;
                                Debug.AssertValid(confirmDetailsPage.Button);
                                Debug.AssertValid(confirmDetailsPage.GivenNameField);
                                Debug.AssertValid(confirmDetailsPage.FamilyNameField);
                                Debug.AssertValid(confirmDetailsPage.PasswordField);
                                confirmDetailsPage.GivenNameField.setValue('Marta');
                                confirmDetailsPage.FamilyNameField.setValue('Prestedge');
                                confirmDetailsPage.PasswordField.setValue('Marta1');
                                confirmDetailsPage.Button.execute(() => {
                                    currentPage = webApp.getCurrentPage();
                                    Debug.AssertValid(currentPage);
                                    Debug.Assert(currentPage instanceof HomePage);
                                    Startup.checkNoToast();
                                    callback();
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

// Test the main menu.
function TestMainMenu(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    TestShowHideMainMenu(webApp, () => {
        Debug.scheduleCallback(() => {
            TestReferFriend(webApp, () => {
                callback();
            });
        });
    });
}

// Test show/hide the main menu.
function TestShowHideMainMenu(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    webApp.navigateTo(Constants.PAGE_URL_HOME, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof HomePage);
        let homePage = <HomePage>currentPage;
        Debug.AssertValid(homePage.TopBanner);
        Debug.AssertValid(homePage.TopBanner.MainMenu);
        homePage.TopBanner.MainMenu.setIsMenuShown(true);
        Debug.Assert(homePage.TopBanner.MainMenu.getIsMenuShown());
        homePage.TopBanner.MainMenu.setIsMenuShown(false);
        Debug.Assert(!homePage.TopBanner.MainMenu.getIsMenuShown());
        callback();
    });
}

// Test show/hide the main menu.
function TestReferFriend(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    webApp.navigateTo(Constants.PAGE_URL_HOME, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof HomePage);
        let homePage = <HomePage>currentPage;
        Debug.AssertValid(homePage.TopBanner);
        Debug.AssertValid(homePage.TopBanner.MainMenu);
        homePage.TopBanner.MainMenu.setIsMenuShown(true);
        Debug.Assert(homePage.TopBanner.MainMenu.getIsMenuShown());
        Debug.AssertValid(homePage.TopBanner.MainMenu.MenuItems);
        Debug.Assert(homePage.TopBanner.MainMenu.MenuItems.length >= 6);
        let referFriendMenuItem = homePage.TopBanner.MainMenu.MenuItems[5];
        Debug.AssertValid(referFriendMenuItem);
        homePage.TopBanner.MainMenu.executeItem(referFriendMenuItem, () => {
            callback();
        });
    });
}

// Test the user menu.
function TestUserMenu(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    TestShowHideUserMenu(webApp, () => {
        callback();
    });
}

// Test show/hide the main menu.
function TestShowHideUserMenu(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    webApp.navigateTo(Constants.PAGE_URL_HOME, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof HomePage);
        let homePage = <HomePage>currentPage;
        Debug.AssertValid(homePage.TopBanner);
        Debug.AssertValid(homePage.TopBanner.UserMenu);
        homePage.TopBanner.UserMenu.setIsMenuShown(true);
        Debug.Assert(homePage.TopBanner.UserMenu.getIsMenuShown());
        homePage.TopBanner.UserMenu.setIsMenuShown(false);
        Debug.Assert(!homePage.TopBanner.UserMenu.getIsMenuShown());
        callback();
    });
}

// Test the links.
function TestLinks(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    webApp.navigateTo(Constants.PAGE_URL_HOME, (currentPage) => {
        Debug.AssertValid(currentPage);
        Debug.Assert(currentPage instanceof HomePage);
        let homePage = <HomePage>currentPage;
        Debug.AssertValid(homePage.LinksTile);
        Debug.AssertValid(homePage.LinksTile.LinkFields);
        Debug.Assert(homePage.LinksTile.LinkFields.length > 0);
        let linkField = homePage.LinksTile.LinkFields[0];
        Debug.AssertValid(linkField);
        linkField.execute(() => {
            currentPage = webApp.getCurrentPage();
            Debug.AssertValid(currentPage);
            Debug.Assert(currentPage instanceof DynamicLinkPage);
            let dynamicLinkPage = <DynamicLinkPage>currentPage;
            Debug.AssertValid(dynamicLinkPage.TitleField);
            Debug.AssertString(dynamicLinkPage.TitleField.getValue());
            Debug.AssertValid(dynamicLinkPage.TextField);
            Debug.AssertString(dynamicLinkPage.TextField.getValue());
            callback();
        });
    });
}

