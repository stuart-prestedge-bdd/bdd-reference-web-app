import { Debug } from '../helpers/debug';
import { IApplication } from '../interfaces/application';
import { UserDetails, DateOnly, Month, Gender, Address } from '../data-models/identity-model';
import { IdentityService, IdentityServiceAPI } from '../services/identity-service';

// Test the identity service.
export function TestIdentityService(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    TestCreateAccount(webApp, () => {
        Debug.scheduleCallback(() => {
            TestCloseAccount(webApp, () => {
                Debug.scheduleCallback(() => {
                    TestVerifyEmail(webApp, () => {
                        Debug.scheduleCallback(() => {
                            TestVerifyEmailWithDetails(webApp, () => {
                                Debug.scheduleCallback(() => {
                                    TestLogin(webApp, () => {
                                        Debug.scheduleCallback(() => {
                                            TestLogout(webApp, () => {
                                                Debug.scheduleCallback(() => {
                                                    TestExtendAccessToken(webApp, () => {
                                                        Debug.scheduleCallback(() => {
                                                            TestGetUserDetails(webApp, () => {
                                                                Debug.scheduleCallback(() => {
                                                                    TestSetUserName(webApp, () => {
                                                                        Debug.scheduleCallback(() => {
                                                                            TestSetUserEmail(webApp, () => {
                                                                                Debug.scheduleCallback(() => {
                                                                                    TestSetUserPassword(webApp, () => {
                                                                                        Debug.scheduleCallback(() => {
                                                                                            TestResetUserPassword(webApp, () => {
                                                                                                Debug.scheduleCallback(() => {
                                                                                                    TestSetUserGender(webApp, () => {
                                                                                                        Debug.scheduleCallback(() => {
                                                                                                            TestSetUserAddress(webApp, () => {
                                                                                                                Debug.scheduleCallback(() => {
                                                                                                                    TestSetUserPhoneNumber(webApp, () => {
                                                                                                                        Debug.scheduleCallback(() => {
                                                                                                                            TestSetUserAllowNonEssentialEmails(webApp, () => {
                                                                                                                                Debug.scheduleCallback(() => {
                                                                                                                                    TestSetUserLimits(webApp, () => {
                                                                                                                                        Debug.scheduleCallback(() => {
                                                                                                                                            //??--TestSetUserDateOfBirth(webApp, () => {
                                                                                                                                            callback();
                                                                                                                                            //??--});
                                                                                                                                        });
                                                                                                                                    });
                                                                                                                                });
                                                                                                                            });
                                                                                                                        });
                                                                                                                    });
                                                                                                                });
                                                                                                            });
                                                                                                        });
                                                                                                    });
                                                                                                });
                                                                                            });
                                                                                        });
                                                                                    });
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

// Test functionality by creating a new account, verifying, logging in, running the test then closing the account.
function TestWithTestAccount(webApp: IApplication,
                             emailAddress: string,
                             performTestsCallback: (accessToken: string, userDetails: UserDetails, callback: () => void) => void,
                             callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(performTestsCallback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                // Check user details
                IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                    Debug.AssertNull(error);
                    Debug.AssertValid(userDetails);

                    // Perform the tests
                    performTestsCallback(accessToken, userDetails, () => {

                        // Close the account
                        IdentityService.closeAccount(accessToken, password, (error) => {
                            Debug.AssertNull(error);

                            // Done
                            callback();
                        });
                    });
                });
            });
        });
    });
}

// Test the identity service create account method.
function TestCreateAccount(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Successful account creation
    let givenName = 'Fred';
    let familyName = 'Smith';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, 'fred@smith.com', 'Fred$Passw0rd', dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Failed account creation (user exists)
        IdentityService.createAccount('Fred', familyName, 'fred@smith.com', 'Fred$Passw0rd', dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
            Debug.AssertValid(error);
            Debug.Assert(error.message === IdentityService.ERROR_USER_ALREADY_EXISTS);

            // Done
            callback();
        });
    });
}

// Test the identity service close account method.
function TestCloseAccount(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Successful account creation
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'jim@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                // Close account
                IdentityService.closeAccount(accessToken, password, (error) => {
                    Debug.AssertNull(error);
                    //??++TEST BAD AUTH TOKEN & ALREADY CLOSED
                    // Done
                    callback();
                });
            });
        });
    });
}

// Test the identity service verify email method.
function TestVerifyEmail(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Successful verify email
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-verify@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            //??++ERROR CASE

            // Done
            callback();
        });
    });
}

// Test the identity service verify email with details method.
function TestVerifyEmailWithDetails(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Successful verify email
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-verify@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";

    // Create the new user internally.
    IdentityServiceAPI.createUserInternal(emailAddress);

    // Get the verification ID
    let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
    Debug.AssertString(verificationID);

    // Verify email
    IdentityService.verifyEmailWithDetails(emailAddress, verificationID, givenName, familyName, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        //??++ERROR CASE

        // Done
        callback();
    });
}

// Test the identity service login method.
function TestLogin(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-login@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                //??++TEST ERROR CASE

                // Done
                callback();
            });
        });
    });
}

// Test the identity service logout method.
function TestLogout(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-logout@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                // Successful logout
                IdentityService.logout(accessToken, (error) => {
                    Debug.AssertNull(error);

                    //??++TEST ERROR CASE

                    // Done
                    callback();
                });
            });
        });
    });
}

// Test the identity service zzz method.
function TestExtendAccessToken(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-extend-access-toke@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                // Successful extend access token
                IdentityService.extendAccessToken(accessToken, (error) => {
                    Debug.AssertNull(error);

                    //??++TEST ERROR CASE

                    // Done
                    callback();
                });
            });
        });
    });
}

// Test the identity service get user service method.
function TestGetUserDetails(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-get-user-details@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                // Successful get user details
                IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                    Debug.AssertNull(error);
                    Debug.AssertValid(userDetails);
                    Debug.Assert(userDetails.GivenName === givenName);
                    Debug.Assert(userDetails.FamilyName === familyName);
                    Debug.Assert(userDetails.EmailAddress === emailAddress);
                    Debug.Assert(userDetails.EmailAddressVerified);

                    //??++TEST ERROR CASE

                    // Done
                    callback();
                });
            });
        });
    });
}

// Test the identity service set user name method.
function TestSetUserName(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-set-user-name@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                // Successful set user name
                let newGivenName = 'JANE';
                let newFamilyName = 'JONES';
                let newFullName = 'JANE JONES';
                let newPreferredName = 'JANIE';
                IdentityService.setUserName(accessToken, newGivenName, newFamilyName, newFullName, newPreferredName, (error) => {
                    Debug.AssertNull(error);

                    // Check user details
                    IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                        Debug.AssertNull(error);
                        Debug.AssertValid(userDetails);
                        Debug.Assert(userDetails.GivenName === newGivenName);
                        Debug.Assert(userDetails.FamilyName === newFamilyName);
                        Debug.Assert(userDetails.FullName === newFullName);
                        Debug.Assert(userDetails.PreferredName === newPreferredName);

                        //??++TEST ERROR CASE

                        // Done
                        callback();
                    });
                });
            });
        });
    });
}

// Test the identity service set user email method.
function TestSetUserEmail(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-set-user-email@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                // Successful set user email flow initiation
                let newEmailAddress = 'new-test-set-user-email@smith.com';
                IdentityService.setUserEmail(accessToken, newEmailAddress, (error) => {
                    Debug.AssertNull(error);

                    // Verify email
                    verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === newEmailAddress));
                    Debug.AssertString(verificationID);
                    IdentityService.verifyEmail(newEmailAddress, verificationID, (error) => {
                        Debug.AssertNull(error);

                        // Done
                        callback();
                    });
                });
            });
        });
    });
}

// Test the identity service set user password method.
function TestSetUserPassword(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-set-user-password@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                // Successful set user password
                let newPassword = 'NewPa$$w0rd';
                IdentityService.setUserPassword(accessToken, password, newPassword, (error) => {
                    Debug.AssertNull(error);

                    // Done
                    callback();
                });
            });
        });
    });
}

// Test the identity service reset user password method.
function TestResetUserPassword(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-reset-user-password@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Reset the user password
            IdentityService.resetUserPassword(emailAddress, (error) => {
                Debug.AssertNull(error);

                // Reset the user password
                let linkID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_RESET_PASSWORD, linkInfo => (linkInfo.emailAddress === emailAddress));
                Debug.AssertString(linkID);
                let newPassword = 'NewPa$$w0rd';
                IdentityService.setUserPasswordAfterReset(linkID, emailAddress, newPassword, (error) => {
                    Debug.AssertNull(error);

                    //??++TEST ERROR CASE

                    // Done
                    callback();
                });
            });
        });
    });
}

// Test the identity service set user gender method.
function TestSetUserGender(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-set-user-gender@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                // Check user details
                IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                    Debug.AssertNull(error);
                    Debug.AssertValid(userDetails);
                    Debug.Assert(userDetails.Gender == null);

                    // Successful set user gender
                    IdentityService.setUserGender(accessToken, Gender.female, (error) => {
                        Debug.AssertNull(error);

                        // Check user details
                        IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                            Debug.AssertNull(error);
                            Debug.AssertValid(userDetails);
                            Debug.Assert(userDetails.Gender === Gender.female);

                            //??++TEST ERROR CASE

                            // Done
                            callback();
                        });
                    });
                });
            });
        });
    });
}

// Test the identity service set user address method.
function TestSetUserAddress(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-set-user-address@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                // Check user details
                IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                    Debug.AssertNull(error);
                    Debug.AssertValid(userDetails);
                    Debug.Assert(userDetails.Address1 == addressX);

                    // Successful set user address
                    let newAddressX = 'yyy';
                    let newAddress = <Address>{
                        Line1: newAddressX,
                        Country: newAddressX,
                        PostalCode: newAddressX
                    };
                    IdentityService.setUserAddress(accessToken, newAddress, (error) => {
                        Debug.AssertNull(error);

                        // Check user details
                        IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                            Debug.AssertNull(error);
                            Debug.AssertValid(userDetails);
                            Debug.Assert(userDetails.Address1 == newAddressX);
                            Debug.Assert(userDetails.Country == newAddressX);
                            Debug.Assert(userDetails.PostalCode == newAddressX);

                            //??++TEST ERROR CASE

                            // Done
                            callback();
                        });
                    });
                });
            });
        });
    });
}

// Test the identity service set user phone number method.
function TestSetUserPhoneNumber(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-set-user-phone-number@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                // Check user details
                IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                    Debug.AssertNull(error);
                    Debug.AssertValid(userDetails);
                    Debug.Assert(userDetails.PhoneNumber == null);

                    // Successful set user phone number
                    let newPhoneNumber = '1234';
                    IdentityService.setUserPhoneNumber(accessToken, newPhoneNumber, (error) => {
                        Debug.AssertNull(error);

                        // Check user details
                        IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                            Debug.AssertNull(error);
                            Debug.AssertValid(userDetails);
                            Debug.Assert(userDetails.PhoneNumber === newPhoneNumber);

                            //??++TEST ERROR CASE

                            // Done
                            callback();
                        });
                    });
                });
            });
        });
    });
}

// Test the identity service set user 'allow non-essential emails' flag method.
function TestSetUserAllowNonEssentialEmails(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Create account
    let givenName = 'Jim';
    let familyName = 'Smith';
    let emailAddress = 'test-set-user-allow-non-essential-emails@smith.com';
    let password = 'Pa$$w0rd';
    let dateOfBirth = new DateOnly(1970, Month.jan, 2);
    let addressX = "xxx";
    let country = "gb";
    IdentityService.createAccount(givenName, familyName, emailAddress, password, dateOfBirth, addressX, addressX, addressX, addressX, addressX, addressX, country, addressX, false, (error) => {
        Debug.AssertNull(error);

        // Get the verification ID
        let verificationID = IdentityServiceAPI.GetLinkIDInternal(IdentityServiceAPI.LINK_TYPE_VERIFY_EMAIL, linkInfo => (linkInfo.emailAddress === emailAddress));
        Debug.AssertString(verificationID);

        // Verify email
        IdentityService.verifyEmail(emailAddress, verificationID, (error) => {
            Debug.AssertNull(error);

            // Login
            IdentityService.login(emailAddress, password, (error, accessToken) => {
                Debug.AssertNull(error);
                Debug.AssertString(accessToken);

                // Check user details
                IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                    Debug.AssertNull(error);
                    Debug.AssertValid(userDetails);
                    Debug.Assert(!userDetails.AllowNonEssentialEmails);

                    // Successful set user 'allow non-essential emails' flag.
                    IdentityService.setUserAllowNonEssentialEmails(accessToken, true, (error) => {
                        Debug.AssertNull(error);

                        // Check user details
                        IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                            Debug.AssertNull(error);
                            Debug.AssertValid(userDetails);
                            Debug.Assert(userDetails.AllowNonEssentialEmails);

                            //??++TEST ERROR CASE

                            // Done
                            callback();
                        });
                    });
                });
            });
        });
    });
}

// Test the identity service set user limits method.
function TestSetUserLimits(webApp: IApplication, callback: () => void) {
    Debug.Tested();
    Debug.AssertValid(webApp);
    Debug.AssertValid(callback);

    // Setup
    let emailAddress = 'test-set-user-limits@smith.com';
    TestWithTestAccount(webApp, emailAddress, (accessToken, userDetails, callback) => {
        Debug.AssertString(accessToken);
        Debug.AssertValid(userDetails);
        Debug.AssertValid(callback);
        Debug.AssertNull(userDetails.MaxDailySpendingAmount);
        Debug.AssertNull(userDetails.MaxTimeLoggedIn);
        Debug.AssertNull(userDetails.ExcludeUntil);

        // Successful set user limits.
        let excludeUntil = new Date();
        IdentityService.setUserLimits(accessToken, 10, 60, excludeUntil, (error) => {
            Debug.AssertNull(error);

            // Check user details
            IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                Debug.AssertNull(error);
                Debug.AssertValid(userDetails);
                Debug.Assert(userDetails.MaxDailySpendingAmount === 10);
                Debug.Assert(userDetails.MaxTimeLoggedIn === 60);
                Debug.Assert(userDetails.ExcludeUntil === excludeUntil);

                // Increase the limits
                let newExcludeUntil = new Date();
                newExcludeUntil.setTime(excludeUntil.getTime() - 1000);
                IdentityService.setUserLimits(accessToken, 20, 100, newExcludeUntil, (error) => {
                    Debug.AssertNull(error);

                    // Check user details
                    IdentityService.getUserDetails(accessToken, (error, userDetails) => {
                        Debug.AssertNull(error);
                        Debug.AssertValid(userDetails);
                        Debug.Assert(userDetails.MaxDailySpendingAmount === 10);
                        Debug.Assert(userDetails.NewMaxDailySpendingAmount === 20);
                        Debug.AssertValid(userDetails.NewMaxDailySpendingAmountTime);
                        Debug.Assert(userDetails.MaxTimeLoggedIn === 60);
                        Debug.Assert(userDetails.NewMaxTimeLoggedIn === 100);
                        Debug.AssertValid(userDetails.NewMaxTimeLoggedInTime);
                        Debug.Assert(userDetails.ExcludeUntil === excludeUntil);
                        Debug.Assert(userDetails.NewExcludeUntil === newExcludeUntil);
                        Debug.AssertValid(userDetails.NewExcludeUntilTime);

                        //??++TEST ERROR CASE

                        // Done
                        callback();
                    });
                });
            });
        });
    }, callback);
}

