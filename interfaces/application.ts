import { UserDetails, DateOnly } from '../data-models/identity-model';
import { Language } from '../data-models/language';
import { PageBase } from '../generic/page-base';
import { PopupWindow } from '../generic/popup-window';
import { ObjectMap } from '../helpers/object-map';

/**
 * Class representing the application.
 */
export interface IApplication {

    /**
     * Get the language to use.
     * Uses the current user, the local storage and the browser settings.
     */
    getLanguageCode(callback: (languageCode: string) => void);

    /**
     * Get the language to use.
     * Uses the current user, the local storage and the browser settings.
     */
    setCurrentLanguage(language: Language, callback: () => void);

    /**
     * Navigate to a page by URL.
     */
    navigateTo(url: string, callback?: (currentPage: PageBase) => void);

    /**
     * Navigate to a page by URL.
     * This method is used when the app logic itself causes an auto navigation without user input.
     */
    autoNavigateTo(url: string, seconds: number);

    /**
     * Get the current page.
     */
    getCurrentPage(): PageBase;

    /**
     * Create a new account.
     */
    createAccount(givenName: string, familyName: string, emailAddress: string, password: string, dateOfBirth: DateOnly,
                  address1: string,
                  address2: string,
                  address3: string,
                  address4: string,
                  city: string,
                  region: string,
                  country: string,
                  postalCode: string,
                  callback: () => void);

    /**
     * Verify an email address.
     */
    verifyEmail(emailAddress: string, verificationID: string, callback: () => void);
    
    /**
     * Close the current account.
     */
    closeAccount(password: string, callback: () => void);

    /**
     * Login.
     * Sets the current user on successful login.
     */
    login(emailAddress: string, password: string, callback: (error: Error) => void);

    /**
     * Logout.
     */
    logout(callback: (error: Error) => void);

    /**
     * Get the current user.
     */
    getCurrentUser(callback: (user: UserDetails, accessToken: string) => void);

    /**
     * Set the actual callbacks.
     */
    setCallbacks(popupCallback: (popupWindow: PopupWindow, closedCallback: (closeAction: string) => void) => void,
                 autoNavigateCallback: (currentPage: PageBase) => void,
                 composeEmail: (subject: string, body: string, bodyIsHTML: boolean, callback: () => void) => void);
    
    /**
     * Show a popup window.
     * When closed any inputs entered into the popup are passed to the callback, including the name of the button pressed.
     */
    showPopup(popup: PopupWindow, callback: (inputs_: ObjectMap<any>) => void);

    /**
     * Change a user's date of birth.
     */
    //??--changeUserDateOfBirth(userDetails: UserDetails, callback: (success: boolean) => void);

    /**
     * Compose an email with given subject and body.
     */
    composeEmail(subject: string, body: string, bodyIsHTML, callback: () => void);
    
}   // Application
